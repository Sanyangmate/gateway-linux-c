#ifndef _WIFI_H
#define _WIFI_H
#include <string.h>
#include "usart.h"
#define WiFi_RxCounter    USART2_RxCounter    
#define WiFi_RX_BUF       USART2_RxBuff 

#define ID   "shixunshi1"                     
#define PASSWORD   "hqyj2016" 

#define ServerIP   "192.168.31.14"                     
#define ServerPort   9931

char WIFI_Config0(int time);
char WIFI_Config(int time,char*cmd,char*response);
char WIFI_Router(int time);
char WIFI_ConnectTCP(int time);
void WIFI_Connect(void);
int WIFI_SendData(const char *data);
uint8_t DHT11_Read_Data(uint8_t *humiH,uint8_t *humiL,uint8_t *tempH,uint8_t *tempL);
uint8_t FS_DHT11_Init(void);

extern int connect_flag;  //是否连接成功的标志
extern int tt_flag;  //是否有网络发过来的透传数据标志

#endif
