/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2023 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "wifi.h"
#include "lcd.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "cJSON.h"
// uint8_t byte;
// uint8_t buf[1024];
// uint16_t len;
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
uint8_t LED1 = 1;
int tmax = 30;
int tmin = 10;
int humi = 80;
float wendu;
float shidu;
float dianya;
float maxT = 30;
float maxH = 30;
uint8_t temp[12];
uint8_t humit[12];
uint8_t buf1[12];
uint8_t buf2[12];
uint8_t buf4[12];
uint16_t buf3[3];
char t[128];
char h[128];
char tm[128];
char hm[128];
char voltage[128];
char* buf5;
char byte;
uint8_t bu[12];
int len;
static uint32_t fac_us = 0; //us延时倍乘数

void delay_init(uint8_t SYSCLK)
{
    fac_us = SYSCLK;
}

void delay_us(uint32_t nus)//100  6800
{
    uint32_t ticks;
    uint32_t told, tnow, tcnt = 0;
    uint32_t reload = SysTick->LOAD; //LOAD的值
    ticks = nus * fac_us;            //需要的节拍数
    told = SysTick->VAL;             // 24  刚进入时的计数器值
    while(1)
    {
        tnow = SysTick->VAL;//22  20  0
        if(tnow != told)
        {
            if(tnow < told)
                tcnt += told - tnow; //这里注意一下SYSTICK是一个递减的计数器就可以了.
            else
                tcnt += reload - tnow + told;
            told = tnow;
            if(tcnt >= ticks)
                break; //时间超过/等于要延迟的时间,则退出.
        }
    };
}
void delay_ms(uint16_t nms)
{
    uint32_t i;
    for(i = 0; i < nms; i++)
        delay_us(1000);
}
int fputc(int ch, FILE* f) //发送一位数据
{
    while(!(USART1->ISR & (1 << 7))) {}
    USART1->TDR = ch;
    return ch;
}
int fgetc(FILE* F)
{
    while(!(USART1->ISR & (1 << 5))) {}
    return USART1->RDR;
}

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef* htim)
{
    if(htim->Instance == TIM1)
    {
        HAL_UART_Transmit(&huart1, buf4, sizeof(buf4), 10);

    }

}
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
uint8_t humiH;
uint8_t humiL;
uint8_t tempH;
uint8_t tempL;
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
    /* USER CODE BEGIN 1 */
    delay_init(68);
    /* USER CODE END 1 */

    /* MCU Configuration--------------------------------------------------------*/

    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();

    /* USER CODE BEGIN Init */
    /*初始化dht11*/
    FS_DHT11_Init();
    delay_init(68);
    /* USER CODE END Init */

    /* Configure the system clock */
    SystemClock_Config();

    /* USER CODE BEGIN SysInit */

    /* USER CODE END SysInit */

    /* Initialize all configured peripherals */
    MX_GPIO_Init();
    MX_USART1_UART_Init();
    MX_USART2_UART_Init();
    MX_ADC1_Init();
    MX_TIM1_Init();
    /* USER CODE BEGIN 2 */
    //开启串口2的空闲中断
    HAL_UART_Receive_IT(&huart2, USART2_RxBuff, 1024);
    __HAL_UART_ENABLE_IT(&huart2, UART_IT_IDLE);

    WIFI_Connect();
    Lcd_Init();
    Lcd_Clear(GRAY2);
    Gui_DrawFont_GBK16(5, 5, BLACK, WHITE, (uint8_t*)"ID:"); //显示ID
    Gui_DrawFont_GBK16(5, 20, BLACK, WHITE, (uint8_t*)"Elect:"); //显示电压
    Gui_DrawFont_GBK16(5, 35, BLACK, WHITE, (uint8_t*)"temp:"); //显示温度
    Gui_DrawFont_GBK16(5, 50, BLACK, WHITE, (uint8_t*)"humi:"); //显示湿度
    Gui_DrawFont_GBK16(5, 65, BLACK, WHITE, (uint8_t*)"TMAX:"); //显示高温阈值
    Gui_DrawFont_GBK16(5, 80, BLACK, WHITE, (uint8_t*)"HMAX:"); //显示低温阈值
    Gui_DrawFont_GBK16(5, 95, BLACK, WHITE, (uint8_t*)"humiY:"); //显示湿度阈值
    Gui_DrawFont_GBK16(5, 110, BLACK, WHITE, (uint8_t*)"FJ:"); //显示风机
    Gui_DrawFont_GBK16(45, 110, BLACK, WHITE, (uint8_t*)"ZR:"); //显示制热
    Gui_DrawFont_GBK16(85, 110, BLACK, WHITE, (uint8_t*)"ZL:"); //显示制冷
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, LED1);
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, 0);
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, 0);
    /* USER CODE END 2 */

    /* Infinite loop */
    /* USER CODE BEGIN WHILE */
    int time = 0;
    while(1)
    {
        /* USER CODE END WHILE */

        /* USER CODE BEGIN 3 */
        //这个例子我们以10ms为一个时间片，每10ms进行一次串口2数据检测，这些数据就相当于网络透传过来需要我们处理的
        //每2s往串口写一次数据，这些数据相当于要从网络发出去的
        HAL_Delay(10);


        //获取温湿度
        DHT11_Read_Data(&humiH, &humiL, &tempH, &tempL);
        wendu = tempH + tempL * 0.1; //温度
        shidu = humiH + humiL * 0.1; //湿度
        sprintf(t, "%d.%d", tempH, tempL);
        Gui_DrawFont_GBK16(45, 36, BLACK, WHITE, (uint8_t*)t);
        sprintf(h, "%d.%d", humiH, humiL);
        Gui_DrawFont_GBK16(45, 50, BLACK, WHITE, (uint8_t*)h);
        //获取电压
        HAL_ADC_Start(&hadc1);

        while(!(ADC1->ISR & (1 << 2))) {} //判断通道是否转换完成，第一次判断证明这里是第一个通道转换完了，即PA1（按键的转换完了）
        buf3[0] = HAL_ADC_GetValue(&hadc1); //把转换完的数据获取出来（按键的）

        while(!(ADC1->ISR & (1 << 3))) {} //序列转换完成，因为我们只有两个通道所以序列转换完数据寄存器里存的是第二个通道的数据（最后一个通道），光敏电阻的
        buf3[1] = HAL_ADC_GetValue(&hadc1); //读出值（电池电压的）

        HAL_ADC_Stop(&hadc1);//停止ADC转换
        dianya = buf3[1] * (3.256 / 4095); //电池电压
        sprintf(voltage, "%0.2fV", dianya);
        Gui_DrawFont_GBK16(52, 20, BLACK, RED, (uint8_t*)voltage);

        int8_t full = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_0);


        cJSON* root = cJSON_CreateObject();
        if(NULL == root)
        {
            printf("create err\n");
            return -1;
        }

        //将数据序列化发送给wifi
        //创建温湿度树干
        cJSON* item = cJSON_CreateNumber(wendu);

        cJSON_AddItemToObject(root, "_stmData_dhtTemp", item);
        item = cJSON_CreateNumber(shidu);
        cJSON_AddItemToObject(root, "_stmData_dhtHumi", item);

        //创建电池电压树
        item = cJSON_CreateNumber(dianya);
        cJSON_AddItemToObject(root, "_stmData_batValue", item);
        item = cJSON_CreateBool(!full);
        cJSON_AddItemToObject(root, "_stmData_ledState", item);

        item = cJSON_CreateString("STM32");
        cJSON_AddItemToObject(root, "who", item);



        char* p = cJSON_PrintUnformatted(root);
        if(time++ == 20)
        {
            WIFI_SendData(p);
            time = 0;
        }

        if(tt_flag == 1)
        {
            u1_printf("recv msg:%s\n", USART2_RxBuff);
            cJSON* root1 = cJSON_Parse((const char*)USART2_RxBuff);
            cJSON* item1 = cJSON_GetObjectItem(root1, "cmd");
            if(strstr(item1->valuestring, "ctlLed"))
            {
                cJSON* item4 = cJSON_GetObjectItem(root1, "ledSet");
                LED1 = item4->valueint;
                HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, LED1 == 0 ? 1 : 0);
            }
            else if(strstr(item1->valuestring, "setThreshold"))
            {
                cJSON* item2 = cJSON_GetObjectItem(root1, "key");
                cJSON* item3 = cJSON_GetObjectItem(root1, "maxval");
                if(item2->valueint == 105)
                {
                    maxT = item3->valuedouble;
                    sprintf(tm, "%.2f", item3->valuedouble);
                    Gui_DrawFont_GBK16(40, 65, BLACK, WHITE, (uint8_t*)tm);
                }
                else if(item2->valueint == 106)
                {
                    maxH = item3->valuedouble;
                    sprintf(hm, "%.2f", item3->valuedouble);
                    Gui_DrawFont_GBK16(40, 80, BLACK, WHITE, (uint8_t*)hm);
                }
                memset(USART2_RxBuff, 0, 1024);
                tt_flag = 0;

            }
            cJSON_Delete(root1);
        }
        HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, wendu > maxT ? 0 : 1);
        HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, shidu > maxH ? 0 : 1);
        free(p);
        cJSON_Delete(root);

    }

    /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
    RCC_OscInitTypeDef RCC_OscInitStruct = {0};
    RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
    RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

    /** Configure the main internal regulator output voltage
    */
    HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1);
    /** Initializes the RCC Oscillators according to the specified parameters
    * in the RCC_OscInitTypeDef structure.
    */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
    RCC_OscInitStruct.HSEState = RCC_HSE_ON;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
    RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV1;
    RCC_OscInitStruct.PLL.PLLN = 16;
    RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
    RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
    if(HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
        Error_Handler();
    }
    /** Initializes the CPU, AHB and APB buses clocks
    */
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
                                  | RCC_CLOCKTYPE_PCLK1;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

    if(HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
    {
        Error_Handler();
    }
    /** Initializes the peripherals clocks
    */
    PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1 | RCC_PERIPHCLK_ADC;
    PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
    PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_SYSCLK;
    if(HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
    {
        Error_Handler();
    }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
    /* USER CODE BEGIN Error_Handler_Debug */
    /* User can add his own implementation to report the HAL error return state */
    __disable_irq();
    while(1)
    {
    }
    /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* USER CODE BEGIN 6 */
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
    /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
