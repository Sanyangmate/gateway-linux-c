#include "wifi.h"
#include "main.h"
#include "adc.h"
//#include "usart.h"
#include "lcd.h"
//#include <stdlib.h>
//#include <string.h>
//#include <stdio.h>
#include "cJSON.h"

#define DHT11_GPIO_PORT 		GPIOB
#define DHT11_GPIO_PIN 			GPIO_PIN_8
#define	DHT11_DQ_IN  		    HAL_GPIO_ReadPin(DHT11_GPIO_PORT, DHT11_GPIO_PIN)

extern float temp;
extern float humidity;

int connect_flag = 0; // 是否连接成功的标志
int tt_flag = 0;	  // 是否有网络发过来的透传数据标志

char WIFI_Config0(int time) // 找到ready 返回 没有找到返回1
{
	memset(WiFi_RX_BUF, 0, 1024);
	WiFi_RxCounter = 0;
	while (time--)
	{
		HAL_Delay(100);
		if (strstr((char *)WiFi_RX_BUF, "ready"))
		{
			break;
		}
		// u1_printf ("WIFI_Config0:=%s",WiFi_RX_BUF);
	}
	if (time > 0)
		return 0;
	else
		return 1;
}

char WIFI_Config(int time, char *cmd, char *response) // 等待时间 发送内容 判断返回的内容
{
	memset(WiFi_RX_BUF, 0, 1024);
	WiFi_RxCounter = 0;
	u2_printf("%s\r\n", cmd);
	while (time--)
	{
		HAL_Delay(100);
		if (strstr((char *)WiFi_RX_BUF, response))
		{
			break;
		}
		// u1_printf("%d ", time);
	}
	if (time > 0)
		return 0;
	else
		return 1;
}

char WIFI_Router(int time) // 配置WIFI名和密码
{
	memset(WiFi_RX_BUF, 0, 1024);
	WiFi_RxCounter = 0;
	//	u2_printf("AT+CIPMUX=0");
	//	HAL_Delay(100);
	u2_printf("AT+CWJAP_DEF=\"%s\",\"%s\"\r\n", ID, PASSWORD);
	while (time--)
	{
		HAL_Delay(1000);
		if (strstr((char *)WiFi_RX_BUF, "OK"))
		{
			break;
		}
		u1_printf("连接路由倒计时%d ", time);
	}
	if (time > 0)
		return 0;
	else
		return 1;
}

char WIFI_ConnectTCP(int time) // 配置TCP
{
	memset(WiFi_RX_BUF, 0, 1024);
	WiFi_RxCounter = 0;
	u2_printf("AT+CIPSTART=\"TCP\",\"%s\",%d\r\n", ServerIP, ServerPort);
	while (time--)
	{
		HAL_Delay(100);
		if (strstr((char *)WiFi_RX_BUF, "OK"))
		{
			break;
		}
		// u1_printf("%d ", time);
	}
	if (time > 0)
		return 0;
	else
		return 1;
}

enum WIFI_STATE
{
	WIFI_READY = 0,
	WIFI_CONFIG1,
	WIFI_ROUTER,
	WIFI_CONFIG2, // 进入透传模式
	WIFI_CONNECT, // 连接服务器
	WIFI_CONFIG3,
	WIFI_COMPLETE // 连接完成
};

#define WAIT_TIME 1000

void WIFI_Connect()
{
	int state = WIFI_READY; // 默认等待复位

	while (1)
	{
		if (state == WIFI_COMPLETE)
		{
			u1_printf("连接服务器完成并开启透传!\r\n");
			// WIFI_SendData("hello world");
			break; // 这里直接跳出while(1)循环即可
		}
		switch (state)
		{
		case WIFI_READY:
			/*0、按键复位*/
			u1_printf("0、准备按键复位!\r\n");
			if (WIFI_Config0(100))
			{
				u1_printf("按键复位失败!\r\n");
				HAL_Delay(WAIT_TIME);
			}
			else
			{
				u1_printf("按键复位成功!\r\n");
				state = WIFI_CONFIG1;
			}
			break;

		case WIFI_CONFIG1:
			/*1、配置WIFI模式*/
			u1_printf("1、准备配置WIFI模式!\r\n");
			if (WIFI_Config(50, "AT+CWMODE=1\r\n", "OK"))
			{
				u1_printf("配置WIFI模式失败!\r\n");
				HAL_Delay(WAIT_TIME);
				break;
			}
			else
				u1_printf("配置WIFI模式成功!\r\n");
			u1_printf("\r\n");
			/*2、重启(命令方式)*/
			u1_printf("2、准备复位!\r\n");
			if (WIFI_Config(50, "AT+RST\r\n", "ready"))
			{
				u1_printf("复位失败!\r\n");
				HAL_Delay(WAIT_TIME);
				break;
			}
			else
				u1_printf("复位成功!\r\n");
			u1_printf("\r\n");
			/*3、取消自动连接*/
			u1_printf("3、准备取消自动连接\r\n");
			if (WIFI_Config(50, "AT+CWAUTOCONN=0\r\n", "OK"))
			{
				u1_printf("取消自动连接失败!\r\n");
				HAL_Delay(WAIT_TIME);
				break;
			}
			else
				u1_printf("取消自动连接成功!\r\n");

			WiFi_RxCounter = 0;
			state = WIFI_ROUTER;
			break;
		case WIFI_ROUTER:
			/*4、连接路由器*/
			u1_printf("4、准备连接路由器\r\n");
			if (WIFI_Router(50))
			{
				u1_printf("连接路由器失败!\r\n");
				HAL_Delay(WAIT_TIME);
			}
			else
			{
				u1_printf("连接路由器成功!\r\n");
				state = WIFI_CONFIG2;
			}
			break;
		case WIFI_CONFIG2:
			/*5、配置单路连接模式*/
			u1_printf("5、准备配置单路连接模式!\r\n");
			if (WIFI_Config(50, "AT+CIPMUX=0\r\n", "OK"))
			{
				u1_printf("配置单路连接模式失败!\r\n");
				HAL_Delay(WAIT_TIME);
				break;
			}
			else
			{
				u1_printf("配置单路连接模式成功!\r\n");
			}
			u1_printf("\r\n");
			/*6、开启透传模式*/
			u1_printf("6、准备开启透传模式\r\n");
			if (WIFI_Config(50, "AT+CIPMODE=1\r\n", "OK"))
			{
				u1_printf("开启透传模式失败!\r\n");
				HAL_Delay(WAIT_TIME);
				break;
			}
			else
			{
				u1_printf("开启透传模式成功!\r\n");
			}
			state = WIFI_CONNECT;
			break;
		case WIFI_CONNECT:
			/*7、建立TCP连接*/
			u1_printf("7、准备建立TCP连接\r\n");
			if (WIFI_ConnectTCP(50))
			{
				u1_printf("建立TCP连接失败!\r\n");
				HAL_Delay(WAIT_TIME);
			}
			else
			{
				u1_printf("建立TCP连接成功!\r\n");
				state = WIFI_CONFIG3;
			}
			break;
		case WIFI_CONFIG3:
			/*8、进入透传模式*/
			u1_printf("8、准备进入透传模式\r\n");
			if (WIFI_Config(50, "AT+CIPSEND\r\n", "\r\nOK\r\n\r\n>"))
			{
				u1_printf("进入透传模式失败!\r\n");
				HAL_Delay(WAIT_TIME);
			}
			else
			{
				u1_printf("进入透传模式成功!\r\n");
				state = WIFI_COMPLETE;
				connect_flag = 1;
				break;
			}
		default:
			break;
		}
	}
}

int WIFI_SendData(const char *data)
{
	u2_printf("%s", data);
	u1_printf("数据发送完成\r\n");
	return 0;
}


//


static void DHT11_IO_IN(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};

    GPIO_InitStruct.Pin = DHT11_GPIO_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    HAL_GPIO_Init(DHT11_GPIO_PORT, &GPIO_InitStruct);
}

static void DHT11_IO_OUT(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};

    GPIO_InitStruct.Pin = DHT11_GPIO_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(DHT11_GPIO_PORT, &GPIO_InitStruct);
}

//复位DHT11  \\起始
void DHT11_Rst(void)
{
    DHT11_IO_OUT(); 	
    HAL_GPIO_WritePin(DHT11_GPIO_PORT, DHT11_GPIO_PIN, GPIO_PIN_RESET); 	//拉低DQ
    HAL_Delay(20); 
    HAL_GPIO_WritePin(DHT11_GPIO_PORT, DHT11_GPIO_PIN, GPIO_PIN_SET);		//DQ=1
    delay_us(30);     	//主机拉高20~40us
}

//等待DHT11的回应
//返回1:未检测到DHT11的存在
//返回0:存在
uint8_t DHT11_Check(void)
{
    uint8_t retry=0;
    DHT11_IO_IN();//SET INPUT
    while (DHT11_DQ_IN&&retry<100)//DHT11会拉低40~80us
    {
        retry++;
        delay_us(1);
    };
    if(retry>=100)return 1;
    else retry=0;
    while (!DHT11_DQ_IN&&retry<100)//DHT11拉低后会再次拉高40~80us
    {
        retry++;
        delay_us(1);
    };
    if(retry>=100)return 1;
    return 0;
}

//从DHT11读取一个位
//返回值：1/0
uint8_t DHT11_Read_Bit(void)
{
    uint8_t retry=0;
    while(DHT11_DQ_IN&&retry<100)//等待变为低电平
    {
        retry++;
        delay_us(1);
    }//延时100
    retry=0;
    while(!DHT11_DQ_IN&&retry<100)//等待变高电平
    {
        retry++;
        delay_us(1);
    }
    delay_us(40);//等待40us
    if(DHT11_DQ_IN)return 1;
    else return 0;
}

//从DHT11读取一个字节
//返回值：读到的数据
uint8_t DHT11_Read_Byte(void)
{
    uint8_t i,dat;
    dat=0;
    for (i=0; i<8; i++)
    {
        dat<<=1;
        dat|=DHT11_Read_Bit();
    }
    return dat;
}

//从DHT11读取一次数据
//temp:温度值(范围:0~50°)
//humi:湿度值(范围:20%~90%)
//返回值：HAL_OK,正常;1,读取失败
uint8_t DHT11_Read_Data(uint8_t *humiH,uint8_t *humiL,uint8_t *tempH,uint8_t *tempL)
{
    uint8_t buf[5];
    uint8_t i;
    DHT11_Rst();
    if(DHT11_Check()==0)
    {
        for(i=0; i<5; i++) //读取40位数据
        {
            buf[i]=DHT11_Read_Byte();
        }
        if((buf[0]+buf[1]+buf[2]+buf[3])==buf[4])
        {
            *humiH=buf[0];
            *humiL=buf[1];
            *tempH=buf[2];
            *tempL=buf[3];

        }
    } else
        return HAL_ERROR;

    return HAL_OK;
}


//初始化DHT11的IO口 DQ 同时检测DHT11的存在
//返回1:不存在
//返回0:存在
uint8_t FS_DHT11_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};

    GPIO_InitStruct.Pin = DHT11_GPIO_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(DHT11_GPIO_PORT, &GPIO_InitStruct);

    HAL_GPIO_WritePin(DHT11_GPIO_PORT, DHT11_GPIO_PIN, GPIO_PIN_SET);	// 输出高
    DHT11_Rst();  //复位DHT11
    return DHT11_Check();//等待DHT11的回应
}






