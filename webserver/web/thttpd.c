#include "thttpd.h"
#include "custom_handle.h"
#include <sys/types.h>
#include <sys/wait.h>
#include <ctype.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sqlite3.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include "custom_handle.h"
#include <time.h>

static int *total;		  //指向共享内存中数据节点总个数
static data_t *node_arr;  //指向共享内存中节点缓存数组头
static data_m *node_arrr; //指向共享内存中节点缓存数组头

int init_server(int _port) //创建监听套接字
{
	int sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock < 0)
	{
		perror("socket failed");
		exit(2);
	}

	//设置地址重用
	int opt = 1;
	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

	struct sockaddr_in local;
	local.sin_family = AF_INET;
	local.sin_port = htons(_port);
	local.sin_addr.s_addr = INADDR_ANY;

	if (bind(sock, (struct sockaddr *)&local, sizeof(local)) < 0)
	{
		perror("bind failed");
		exit(3);
	}

	if (listen(sock, 5) < 0)
	{
		perror("listen failed");
		exit(4);
	}

	return sock;
}

static int get_line(int sock, char *buf) //按行读取请求报头
{
	char ch = '\0';
	int i = 0;
	ssize_t ret = 0;
	while (i < SIZE && ch != '\n')
	{
		ret = recv(sock, &ch, 1, 0);
		if (ret > 0 && ch == '\r')
		{
			ssize_t s = recv(sock, &ch, 1, MSG_PEEK);
			if (s > 0 && ch == '\n')
			{
				recv(sock, &ch, 1, 0);
			}
			else
			{
				ch = '\n';
			}
		}
		buf[i++] = ch;
	}
	buf[i] = '\0';
	return i;
}

static void clear_header(int sock) //清空消息报头
{
	char buf[SIZE];
	int ret = 0;
	do
	{
		ret = get_line(sock, buf);
	} while (ret != 1 && (strcmp(buf, "\n") != 0));
}

static void show_404(int sock) //404错误处理
{
	clear_header(sock);
	char *msg = "HTTP/1.0 404	Not Found\r\n";
	send(sock, msg, strlen(msg), 0);	   //发送状态行
	send(sock, "\r\n", strlen("\r\n"), 0); //发送空行

	struct stat st;
	stat("wwwroot/404.html", &st);
	int fd = open("wwwroot/404.html", O_RDONLY);
	sendfile(sock, fd, NULL, st.st_size);
	close(fd);
}

void echo_error(int sock, int err_code) //错误处理
{
	switch (err_code)
	{
	case 403:
		break;
	case 404:
		show_404(sock);
		break;
	case 405:
		break;
	case 500:
		break;
	defaut:
		break;
	}
}

static int echo_www(int sock, const char *path, size_t s) //处理非CGI的请求
{
	int fd = open(path, O_RDONLY);
	if (fd < 0)
	{
		echo_error(sock, 403);
		return 7;
	}

	char *msg = "HTTP/1.0 200 OK\r\n";
	send(sock, msg, strlen(msg), 0);	   //发送状态行
	send(sock, "\r\n", strlen("\r\n"), 0); //发送空行

	//sendfile方法可以直接把文件发送到网络对端
	if (sendfile(sock, fd, NULL, s) < 0)
	{
		echo_error(sock, 500);
		return 8;
	}
	close(fd);
	return 0;
}

static int handle_request(int sock, const char *method,
						  const char *path, const char *query_string)
{
	char line[SIZE];
	int ret = 0;
	int content_len = -1;
	if (strcasecmp(method, "GET") == 0)
	{
		//清空消息报头
		clear_header(sock);
	}
	else
	{
		//获取post方法的参数大小
		do
		{
			ret = get_line(sock, line);
			if (strncasecmp(line, "content-length", 14) == 0) //post的消息体记录正文长度的字段
			{
				content_len = atoi(line + 16); //求出正文的长度
			}
		} while (ret != 1 && (strcmp(line, "\n") != 0));
	}

	printf("method = %s\n", method);
	printf("query_string = %s\n", query_string);
	printf("content_len = %d\n", content_len);

	char req_buf[4096] = {0};

	//如果是POST方法，那么肯定携带请求数据，那么需要把数据解析出来
	if (strcasecmp(method, "POST") == 0)
	{
		int len = recv(sock, req_buf, content_len, 0);
		printf("len = %d\n", len);
		printf("req_buf = %s\n", req_buf);
	}

	//先发送状态码
	char *msg = "HTTP/1.1 200 OK\r\n\r\n";
	send(sock, msg, strlen(msg), 0);

	//请求交给自定义代码来处理，这是业务逻辑
	parse_and_process(sock, query_string, req_buf);

	return 0;
}

int handler_msg(int sock) //浏览器请求处理函数
{
	char del_buf[SIZE] = {};

	//通常recv()函数的最后一个参数为0，代表从缓冲区取走数据
	//而当为MSG_PEEK时代表只是查看数据，而不取走数据。
	recv(sock, del_buf, SIZE, MSG_PEEK);

#if 1 //初学者强烈建议打开这个开关，看看tcp实际请求的协议格式
	puts("---------------------------------------");
	printf("recv:%s\n", del_buf);
	puts("---------------------------------------");
#endif

	//接下来method方法判断之前的代码，可以不用重点关注
	//知道是处理字符串，把需要的信息过滤出来即可
	char buf[SIZE];
	int count = get_line(sock, buf);
	int ret = 0;
	char method[32];
	char url[SIZE];
	char *query_string = NULL;
	int i = 0;
	int j = 0;
	int need_handle = 0;

	//获取请求方法和请求路径
	while (j < count)
	{
		if (isspace(buf[j]))
		{
			break;
		}
		method[i] = buf[j];
		i++;
		j++;
	}
	method[i] = '\0';
	while (isspace(buf[j]) && j < SIZE) //过滤空格
	{
		j++;
	}

	//这里开始就开始判断发过来的请求是GET还是POST了
	if (strcasecmp(method, "POST") && strcasecmp(method, "GET"))
	{
		printf("method failed\n"); //如果都不是，那么提示一下
		echo_error(sock, 405);
		ret = 5;
		goto end;
	}

	if (strcasecmp(method, "POST") == 0)
	{
		need_handle = 1;
	}

	i = 0;
	while (j < count)
	{
		if (isspace(buf[j]))
		{
			break;
		}
		if (buf[j] == '?')
		{
			//将资源路径（和附带数据，如果有的话）保存再url中，并且query_string指向附带数据
			query_string = &url[i];
			query_string++;
			url[i] = '\0';
		}
		else
		{
			url[i] = buf[j];
		}
		j++;
		i++;
	}
	url[i] = '\0';

	printf("query_string = %s\n", query_string);

	//浏览器通过http://192.168.8.208:8080/?test=1234这种形式请求
	//是携带参数的意思，那么就需要额外处理了
	if (strcasecmp(method, "GET") == 0 && query_string != NULL)
	{
		need_handle = 1;
	}

	//我们把请求资源的路径固定为wwwroot/下的资源，这个自己可以改
	char path[SIZE];
	sprintf(path, "wwwroot%s", url);

	printf("path = %s\n", path);

	//如果请求地址没有携带任何资源，那么默认返回index.html
	if (path[strlen(path) - 1] == '/') //判断浏览器请求的是不是目录
	{
		strcat(path, "index.html"); //如果请求的是目录，则就把该目录下的首页返回回去
	}

	//如果请求的资源不存在，就要返回传说中的404页面了
	struct stat st;
	if (stat(path, &st) < 0) //获取客户端请求的资源的相关属性
	{
		printf("can't find file\n");
		echo_error(sock, 404);
		ret = 6;
		goto end;
	}

	//到这里基本就能确定是否需要自己的程序来处理后续请求了
	printf("need progress handle:%d\n", need_handle);

	//如果是POST请求或者带参数的GET请求，就需要我们自己来处理了
	//这些是业务逻辑，所以需要我们自己写代码来决定怎么处理
	if (need_handle)
	{
		ret = handle_request(sock, method, path, query_string);
	}
	else
	{
		clear_header(sock);
		//如果是GET方法，而且没有参数，则直接返回资源
		ret = echo_www(sock, path, st.st_size);
	}

end:
	close(sock);
	return ret;
}

// 共享内存缓冲区的大小
#define BUFFER_SIZE 1024

// 共享内存的键值
#define SHM_KEY 1234

struct shm_param datebase;

void create_database()
{

	sqlite3 *db;
	int rc;

	// 打开或创建数据库
	rc = sqlite3_open("mydb.db", &db);
	if (rc)
	{
		fprintf(stderr, "Can't open datebase: %s\n", sqlite3_errmsg(db));
		exit(1);
	}

	// 创建表格
	char *sql = "create table datebase(time char,key int,type int,val float,name char);";
	if (sqlite3_exec(db, sql, NULL, 0, NULL) != SQLITE_OK)
	{
		fprintf(stderr, "SQL error: %s\n", sqlite3_errmsg(db));
	}

	// 关闭数据库连接
	sqlite3_close(db);
}

void insert_data_to_database(const char *data, int semid)
{
#if 1 // yuanlai
	sqlite3 *db;
	int rc;
	char *errmsg = NULL; // 指向 文字常量区的 错误信息

	// 打开数据库
	rc = sqlite3_open("mydb.db", &db);
	if (rc)
	{
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
		exit(1);
	}

	// 获取当前时间
	time_t current_time;
	struct tm *time_info;
	char date_string[20];
	char time_string[20];
	time(&current_time);
	time_info = localtime(&current_time);
	strftime(date_string, sizeof(date_string), "%Y-%m-%d", time_info);
	strftime(time_string, sizeof(time_string), "%H:%M:%S", time_info);

	// // 构建插入语句
	//前4个字节存储实际的数目
	char *node_p = (void *)data; //基地址
	total = (int *)data;		 //个数的指针
	//后面空间存储数据点
	node_arr = (data_t *)(node_p + sizeof(int)); //对应结构提类型的 结构提数组 收地址

	char sql_cmd[256] = {0}; //sql cmd

	//初始化
	union semun sem;
	sem.val = 0;
	semctl(semid, 0, SETVAL, sem);
printf("6666\n");
	//读
	struct sembuf s;
	s.sem_num = 0;
	s.sem_op = -1;
	s.sem_flg = 0;
	semop(semid, &s, 1);

printf("6666\n");
	for (int i = 0; i < *(total); i++)
	{

		data_t np = *(node_arr + i);

		if (np.type == 3)
		{
			printf("---stm32----\n%s\t%d\t%d\t%f\t%s\n", time_string, np.key, np.type, np.data.f_val, np.name);
			sprintf(sql_cmd, "insert into datebase values(\"%s\",%d,%d,%.1f,\"%s\");", time_string, np.key, np.type, np.data.f_val, np.name);
		}
		else
		{
			printf("---stm32----\n%s\t%d\t%d\t%.1f\t%s\n", time_string, np.key, np.type, (float)np.data.i_val, np.name);
			sprintf(sql_cmd, "insert into datebase values(\"%s\",%d,%d,%.1f,\"%s\");", time_string, np.key, np.type, (float)np.data.i_val, np.name);
		}

		// 执行命令语句
		if (sqlite3_exec(db, sql_cmd, NULL, NULL, &errmsg) != SQLITE_OK)
		{
			// 根据错误，会不断改变errmsg的指向
			printf("insert into is err: %s\n", errmsg);
			return;
		}
	}

	// 关闭数据库连接
	sqlite3_close(db);

#else
	int flag = 1;

	float temp;
	float humi;
	float bat;
	int led_state;

#define NUM_DATA_STM32
	data_t *pData_Shm_stm32 = (data_t *)(data + sizeof(int)); //对应结构提类型的 结构提数组 收地址

	for (int i = 0; i < NUM_DATA_STM32; i++)
	{
		switch ((((data_t *)pData_Shm_stm32) + i)->key)
		{
		//温度
		case 101:
		{
			temp = (((data_t *)pData_Shm_stm32) + i)->data.f_val;

			if (flag == 1)
				printf("key:%d val:%f\n", (((data_t *)pData_Shm_stm32) + i)->key,
					   (((data_t *)pData_Shm_stm32) + i)->data.f_val);

			break;
		}
		//湿度
		case 102:
		{
			humi = (((data_t *)pData_Shm_stm32) + i)->data.f_val;

			if (flag == 1)
				printf("key:%d val:%f\n", (((data_t *)pData_Shm_stm32) + i)->key,
					   (((data_t *)pData_Shm_stm32) + i)->data.f_val);

			break;
		}
		//电池电压
		case 103:
		{
			bat = (((data_t *)pData_Shm_stm32) + i)->data.f_val;

			if (flag == 1)
				printf("key:%d val:%f\n", (((data_t *)pData_Shm_stm32) + i)->key,
					   (((data_t *)pData_Shm_stm32) + i)->data.f_val);

			break;
		}
		//led开关
		case 104:
		{
			led_state = (((data_t *)pData_Shm_stm32) + i)->data.i_val;

			if (flag == 1)
				printf("key:%d val:%d\n", (((data_t *)pData_Shm_stm32) + i)->key,
					   (((data_t *)pData_Shm_stm32) + i)->data.i_val);

			break;
		}
		}
	}

#endif
}

void insert_data_to_databasee(const char *data, int semid)
{
	sqlite3 *db;
	int rc;
	char *errmsg = NULL; // 指向 文字常量区的 错误信息

	// 打开数据库
	rc = sqlite3_open("mydb.db", &db);
	if (rc)
	{
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
		exit(1);
	}

	// 获取当前时间
	time_t current_time;
	struct tm *time_info;
	char date_string[20];
	char time_string[20];
	time(&current_time);
	time_info = localtime(&current_time);
	strftime(date_string, sizeof(date_string), "%Y-%m-%d", time_info);
	strftime(time_string, sizeof(time_string), "%H:%M:%S", time_info);

	// 前4个字节存储实际的数据数目
	char *node_p = (void *)data; //首地址
	total = (int *)data;
	//后面空间存储数据点
	node_arrr = (data_m *)(node_p + sizeof(int));

	char sql_cmd[256] = {0}; //sql cmd

	//读
	struct sembuf s;
	s.sem_num = 1;
	s.sem_op = -1;
	s.sem_flg = 0;
	semop(semid, &s, 1);

	for (int i = 0; i < *(total); i++)
	{
		data_m np = *(node_arrr + i);

		if (np.type == 3)
		{
			printf("---modbus----\n%s\t%d\t%d\t%f\t%s\n", time_string, np.key, np.type, np.data.f_val, np.name);
			sprintf(sql_cmd, "insert into datebase values(\"%s\",%d,%d,%.1f,\"%s\");", time_string, np.key, np.type, np.data.f_val, np.name);
		}
		else
		{
			printf("---modubs----\n%s\t%d\t%d\t%.1f\t%s\n", time_string, np.key, np.type, (float)np.data.i_val, np.name);
			sprintf(sql_cmd, "insert into datebase values(\"%s\",%d,%d,%.1f,\"%s\");", time_string, np.key, np.type, (float)np.data.i_val, np.name);
		}

		// 执行命令语句
		if (sqlite3_exec(db, sql_cmd, NULL, NULL, &errmsg) != SQLITE_OK)
		{
			// 根据错误，会不断改变errmsg的指向
			printf("insert into is err: %s\n", errmsg);
			return;
		}
	}

	// 关闭数据库连接
	sqlite3_close(db);
}

#define MAX_NODE_STM 4	 //最大stm32设备数
#define MAX_NODE_SLAVE 3 //最大modbus设备数
#define STD_NODE_LEN sizeof(data_t)
#define MOD_NODE_LEN sizeof(data_m)
#define MAX_NODE_SIZE (sizeof(int) + (MAX_NODE_STM * STD_NODE_LEN))
#define MAX_MOD_SIZE (sizeof(int) + (MAX_NODE_SLAVE * MOD_NODE_LEN))
#define MSG_PATH "/tmp/ipc/sem"

struct shm_param para_stm32;
struct shm_param para_modbus;

void read_from_shared_memory_and_insert()
{
	int ret = 0;
	ret = shm_init(&para_stm32, Sheme_ID_stm32, MAX_NODE_SIZE);
	printf("1key:%d\n", ret);
	if (ret < 0)
	{
		return;
	}
	void *node_p = shm_getaddr(&para_stm32);
	if (node_p == NULL)
	{
		return;
	}
	int rett = 0;
	rett = shm_init(&para_modbus, Sheme_ID_slave, MAX_MOD_SIZE);
	printf("key:%d\n", rett);
	if (rett < 0)
	{
		return;
	}
	void *node_pp = shm_getaddr(&para_modbus);
	if (node_pp == NULL)
	{
		return;
	}

	key_t key;
	int msgid;
	char sys_cmd[256];
	char path[256];

	sprintf(path, "%s%s", MSG_PATH, "/mysem");

	//文件不存在创建
	if (access(path, F_OK) < 0)
	{
		sprintf(sys_cmd, "%s %s", "touch", path);
		ret = system(sys_cmd);
		UNUSED(ret);
	}

	//创建key
	key = ftok(path, 'j');
	if (key < 0)
	{
		perror("fail to ftok");
		return;
	}

	int semid = semget(key, 1, IPC_CREAT | IPC_EXCL | 0666);
	if (semid <= 0)
	{
		if (errno == 17)
			semid = semget(key, 1, 0666);
		else
		{
			perror("semget err");
			return;
		}
	}
	printf("semid:%d\n", semid);

	// 从共享内存读取资源并插入数据库
	while (1)
	{
		// 插入数据库
		insert_data_to_database(node_p, semid);
		insert_data_to_databasee(node_pp, semid);

		// 延时
		sleep(INSERT_TIMER_SQL);
	}

	// 解除共享内存关联
	shmdt(node_p);
	shmdt(node_pp);
}
