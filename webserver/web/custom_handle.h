/***********************************************************************************
Copy right:	    Coffee Tech.
Author:         jiaoyue
Date:           2022-03-24
Description:    http请求自定义处理部分
***********************************************************************************/

#ifndef CUSTOM_HANDLE_H
#define CUSTOM_HANDLE_H

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "cJSON.h"
#include "shmem.h"
#include "msg_queue_peer.h"

#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>



struct msgbuf
{
    long mtype;
    char mdata[256];
};

typedef union {
    int b_val;
    int i_val;
    float f_val;
} val_t;

typedef struct
{
    int key;       //唯一key标识
    char name[32]; //唯一命名标识
    int type;      //数据格式
    val_t data;    //共用体数据
} data_t;

typedef struct
{
    int key;       //唯一key标识
    char name[32]; //唯一命名标识
    int addr;
    int type;   //数据格式
    val_t data; //共用体数据
} data_m;

typedef struct
{
    long mtype; //固定的,不能小于0,用时可以都设置为1

    char who[32]; //谁下发的,web后台,还是Qt,还是阿里云进程

    char deviceName[32]; //被控执行器的名字,比如网关通用点表里的stm32Device
    int key;             //被控执行器的key标识,与deviceName同个作用

    int cmdType;            //命令格式:1是下发控制,2是要求立刻刷新数据,并写入到共享内存
    val_t ctlDeviceState_1; //写入状态,用int型的bool状态,1开0关
    val_t ctlDeviceState_2; //依次类推,可根据想控制的执行器数量添加
} queueMsg_t;

struct shm
{
    char buf[128];
    int flag;
};

// 共享内存标识
#define Sheme_ID_stm32 "sheme_Id_stm32" // 1.存放stm32数据用的
#define Sheme_ID_slave "sheme_Id_slave" // 2.存放modbus数据用的

#define QueueMsg_Recv "QueueMsg_Recv" //发给消息队列的标识

#define Str_Who_Web "Str_Who_Web"

int parse_and_process(int sock, const char *query_string, const char *input);

#endif  // CUSTOM_HANDLE_H
