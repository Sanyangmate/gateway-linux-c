/***********************************************************************************
Copy right:	    hqyj Tech.
Author:         jiaoyue
Date:           2023.07.01
Description:    http请求处理
***********************************************************************************/

#include <sys/types.h>
#include <sys/socket.h>
#include "custom_handle.h"
#include <stdio.h>
//22

//
#define KB 1024
#define HTML_SIZE (64 * KB)

//普通的文本回复需要增加html头部
#define HTML_HEAD "Content-Type: text/html\r\n" \
                  "Connection: close\r\n"
//


static int *total;        //指向共享内存中数据节点总个数
static data_t *node_arr;  //指向共享内存中节点缓存数组头
static data_m *node_arrr; //指向共享内存中节点缓存数组头

#define MAX_NODE_STM 4   //最大支持学生数目
#define MAX_NODE_SLAVE 3 //最大支持学生数目
#define STD_NODE_LEN sizeof(data_t)
#define MOD_NODE_LEN sizeof(data_m)
#define MAX_NODE_SIZE (sizeof(int) + (MAX_NODE_STM * STD_NODE_LEN))
#define MAX_MOD_SIZE (sizeof(int) + (MAX_NODE_SLAVE * MOD_NODE_LEN))

struct shm_param para_stm32;
struct shm_param para_modbus;



//

static int handle_login(int sock, const char *input)
{
    char reply_buf[HTML_SIZE] = {0};
    char *uname = strstr(input, "username=");
    uname += strlen("username=");
    char *p = strstr(input, "password");
    *(p - 1) = '\0';
    printf("username = %s\n", uname);

    char *passwd = p + strlen("password=");
    printf("passwd = %s\n", passwd);

    if (strcmp(uname, "2308104") == 0 && strcmp(passwd, "1") == 0)
    {
        strcpy(reply_buf, "<script>window.location.href = '/temp_index.html';</script>");
        // fputs(reply_buf, stdout);
        // sprintf(reply_buf, "<script>localStorage.setItem('usr_user_name', '%s');</script>", uname);
        // strcat(reply_buf, "<script>window.location.href = '/homepage.html';</script>");
        send(sock, reply_buf, strlen(reply_buf), 0);
    }
    else
    {
        printf("web login failed\n");

        //"用户名或密码错误"提示，chrome浏览器直接输送utf-8字符流乱码，没有找到太好解决方案，先过渡
        char out[128] = {0xd3, 0xc3, 0xbb, 0xa7, 0xc3, 0xfb, 0xbb, 0xf2, 0xc3, 0xdc, 0xc2, 0xeb, 0xb4, 0xed, 0xce, 0xf3};
        sprintf(reply_buf, "<script charset='gb2312'>alert('%s');</script>", out);
        strcat(reply_buf, "<script>window.location.href = '/login.html';</script>");
        send(sock, reply_buf, strlen(reply_buf), 0);
    }

    return 0;
}

static int handle_add(int sock, const char *input)
{
    int number1, number2;

    //input必须是"data1=1data2=6"类似的格式，注意前端过来的字符串会有双引号
    sscanf(input, "\"data1=%ddata2=%d\"", &number1, &number2);
    printf("num1 = %d\n", number1);

    char reply_buf[HTML_SIZE] = {0};
    printf("num = %d\n", number1 + number2);
    sprintf(reply_buf, "%d", number1 + number2);
    printf("resp = %s\n", reply_buf);
    send(sock, reply_buf, strlen(reply_buf), 0);

    return 0;
}

/**
 * @brief 处理自定义请求，在这里添加进程通信
 * @param input
 * @return
 */
int parse_and_process(int sock, const char *query_string, const char *input)
{
    //query_string不一定能用的到

    //先处理登录操作
    if (strstr(input, "username=") && strstr(input, "password="))
    {
        return handle_login(sock, input);
    }
    else if (strstr(input, "open") || strstr(input, "close"))
    {
        queueMsg_t send_buf;
        // //这个mtype可以不用，但是必须赋一个不小于0的数
        send_buf.mtype = 1;
        strcpy(send_buf.who, Str_Who_Web);
        // cJSON *root = cJSON_CreateObject();
        if (strstr(input, "open_led"))
        {
            send_buf.key = 104;
            send_buf.cmdType = 1;
            strcpy(send_buf.deviceName, "stm32Device");
            send_buf.ctlDeviceState_1.i_val = 1;
            if (msg_queue_send(QueueMsg_Recv, &send_buf, sizeof(send_buf), 0) < 0)
            {
                printf("msg_queue_send error\n");
                return -1;
            }
        }
        else if (strstr(input, "close_led"))
        {
            send_buf.key = 104;
            send_buf.cmdType = 1;
            strcpy(send_buf.deviceName, "stm32Device");
            send_buf.ctlDeviceState_1.i_val = 0;
            if (msg_queue_send(QueueMsg_Recv, &send_buf, sizeof(send_buf), 0) < 0)
            {
                printf("msg_queue_send error\n");
                return -1;
            }
        }
        else if (strstr(input, "open_fan"))
        {
            send_buf.key = 203;
            send_buf.cmdType = 1;
            strcpy(send_buf.deviceName, "modbusSlave");
            send_buf.ctlDeviceState_1.i_val = 1;
            if (msg_queue_send(QueueMsg_Recv, &send_buf, sizeof(send_buf), 0) < 0)
            {
                printf("msg_queue_send error\n");
                return -1;
            }
        }
        else if (strstr(input, "close_fan"))
        {
            send_buf.key = 203;
            send_buf.cmdType = 1;
            strcpy(send_buf.deviceName, "modbusSlave");
            send_buf.ctlDeviceState_1.i_val = 0;
            if (msg_queue_send(QueueMsg_Recv, &send_buf, sizeof(send_buf), 0) < 0)
            {
                printf("msg_queue_send error\n");
                return -1;
            }
        }
    }
    else if (strstr(input, "finish")) //剩下的都是json请求，这个和协议有关了
    {
        // 构建要回复的JSON数据
        int ret = 0;
        ret = shm_init(&para_stm32, Sheme_ID_stm32, MAX_NODE_SIZE);
        printf("key:%d\n", ret);
        if (ret < 0)
        {
            return -1;
        }
        void *node_p = shm_getaddr(&para_stm32);
        if (node_p == NULL)
        {
            return -1;
        }

        //前4个字节存储实际的学生数目
        total = (int *)node_p;
        //后面空间存储数据点
        node_arr = (data_t *)(node_p + sizeof(int));

        cJSON *root = cJSON_CreateObject();
        // 创建 "data" 数组
        cJSON *dataArray = cJSON_CreateArray();
        if (NULL == root)
        {
            printf("create err\n");
            return -1;
        }
        char *json_response = NULL;
        for (int i = 0; i < *(total); i++)
        {
            data_t *mp = node_arr;
            data_t np = *(mp + i);
            cJSON *dataObj = cJSON_CreateObject();
            cJSON_AddNumberToObject(dataObj, "key", np.key);
            cJSON *item = cJSON_CreateNumber(1);
            cJSON_AddItemToObject(root, "1", item);
            printf("%d", np.key);
            if (np.type == 1)
            {
                cJSON_AddNumberToObject(dataObj, "val", np.data.b_val);
            }
            else if (np.type == 2)
            {
                cJSON_AddNumberToObject(dataObj, "val", np.data.i_val);
            }
            else if (np.type == 3)
            {
                cJSON_AddNumberToObject(dataObj, "val", np.data.f_val);
            }
            // json_response = cJSON_PrintUnformatted(root);
            cJSON_AddItemToArray(dataArray, dataObj);
        }
        cJSON_AddItemToObject(root, "data", dataArray);
        json_response = cJSON_PrintUnformatted(root);
        printf("555555555555555\n%s\n", json_response);
        cJSON_Delete(root);

        // 发送HTTP响应给客户端
        send(sock, json_response, strlen(json_response), 0);
        free(json_response);
    }
    else
    {
        //----------------------------------
        // 构建要回复的JSON数据
        int rett = 0;
        rett = shm_init(&para_modbus, Sheme_ID_slave, MAX_MOD_SIZE);
        printf("key:%d\n", rett);
        if (rett < 0)
        {
            return -1;
        }
        void *node_pp = shm_getaddr(&para_modbus);
        if (node_pp == NULL)
        {
            return -1;
        }

        //前4个字节存储实际的学生数目
        total = (int *)node_pp;
        //后面空间存储数据点
        node_arrr = (data_m *)(node_pp + sizeof(int));

        cJSON *roott = cJSON_CreateObject();
        // 创建 "data" 数组
        cJSON *dataArrayy = cJSON_CreateArray();
        if (NULL == roott)
        {
            printf("create err\n");
            return -1;
        }
        char *json_responsee = NULL;
        for (int i = 0; i < *(total); i++)
        {
            data_m *mpp = node_arrr;
            data_m npp = *(mpp + i);
            cJSON *dataObj = cJSON_CreateObject();
            cJSON_AddNumberToObject(dataObj, "key", npp.key);
            cJSON *item = cJSON_CreateNumber(1);
            cJSON_AddItemToObject(roott, "1", item);
            printf("%d", npp.key);

            if (npp.type == 1)
            {
                cJSON_AddNumberToObject(dataObj, "val", npp.data.b_val);
            }
            else if (npp.type == 2)
            {
                cJSON_AddNumberToObject(dataObj, "val", npp.data.i_val);
            }
            else if (npp.type == 3)
            {
                cJSON_AddNumberToObject(dataObj, "val", npp.data.f_val);
            }
            json_responsee = cJSON_PrintUnformatted(roott);
            cJSON_AddItemToArray(dataArrayy, dataObj);
        }
        cJSON_AddItemToObject(roott, "data", dataArrayy);
        json_responsee = cJSON_PrintUnformatted(roott);
        printf("<---------------->\n%s", json_responsee);
        cJSON_Delete(roott);

        // 发送HTTP响应给客户端
        send(sock, json_responsee, strlen(json_responsee), 0);

        free(json_responsee);
    }

    return 0;
}
