#ifndef _TCPSERVER_H_
#define _TCPSERVER_H_

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <poll.h>
#include <signal.h>
#include <pthread.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include "cJSON.h"
#include "list.h"
#include "helpFunc.h"
#include "mqttClinet.h"

#define TcpServerPort 9931

#define RECV_BUF_LENGTH 2048 //tcp rx buf

#define WHO_QT "Qt"
#define WHO_STM32 "STM32"

#define KEEP_ALIVE_TOPIC "yes"
#define REPORT_KEEP_ALIVE "{\"keepAlive\" : \"ok\", \"who\" : \"gataway\"}"

#define PATH_CONFIG_JSON_FILE "./res/node.json"

struct parseDataTcp
{
    char who[32];
};

enum
{
    whoQt = 10,
    whoSTM32
};

//tcp 服务器
void tcpServer();

// 线程 处理qtTcp 或者 esp的tcp 收发
// 子线程的子线程
void *handler_tcpTransmit(void *arg);

//解析TCP JSON,判断是谁
int parsingJsonTcpData(struct parseDataTcp *dataTcp, char *recvbuf);

// 解析tcp 收到的json
int deal_QtTcp_Data(char *recvbuf, int acceptfd);

//收到的点表,写入本地文件
int writeToFile(const char *filename, const char *text, int size);

//处理esp32的json数据
int deal_EspTcp_data(char *recvbuf);

#endif
