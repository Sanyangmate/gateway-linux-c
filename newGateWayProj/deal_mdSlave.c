#include "deal_mdSlave.h"
#include "sem.h"

//从机对象
modbus_t *ctx; //modbus实例

//ip port 从机的ID号 //Slave是服务器
//这里是对指针变量的值修改，所以二级指针
int init_connectToDestSlave(modbus_t **ctx, const char slaveIp[16],
                            const int slavePort, const int slaveId)
{
    // 创建实例
    // 参数是目标modbusSlave设备的ip和port
    *ctx = modbus_new_tcp(slaveIp, slavePort);
    if (NULL == *ctx)
    {
        DEBUG_Err("ModbusSlave Tcp connect err");
        return -1;
    }

    // 设置从机
    if (modbus_set_slave(*ctx, slaveId) < 0)
    {
        DEBUG_Err("ModbusSlave Tcp set slave err");
        return -2;
    }

    // 连接从机
    if (modbus_connect(*ctx) < 0)
    {
        DEBUG_Err("ModbusSlave Tcp modbus_connect  err");
        return -3;
    }

    //正常连接
    return 0;
}

void get_SlaveData(modbus_t *ctx)
{
    //是否输出调试
    int flag = 0;

    /* 获取数据 */

    //光照 co2 (2个)
    uint16_t dest[2] = {0}; //2字节的数组,能存2个
    modbus_read_input_registers(ctx, 0, 2, dest);
    if (flag == 1)
        printf("    > modbus read: s%d %d <\n", dest[0], dest[1]);

    //读取线圈 风扇开关 (1个)
    uint8_t coil[1] = {0}; //1个字节
    modbus_read_bits(ctx, 0, 1, coil);

    if (flag == 1)
        printf("    > read coli state:%d\n", coil[0]);

    /* 赋值到共享内存 */

    //共享内存首地址
    //共享内存 数据段 起始地址
    extern void *pData_Shm_slave;

    for (int i = 0; i < NUM_DATA_SLAVE; i++)
    {
        switch ((((mbSlaveNodeVal_t *)(pData_Shm_slave)) + i)->key)
        {
        //lux
        case 201:
        {
            (((mbSlaveNodeVal_t *)(pData_Shm_slave)) + i)->data.i_val = dest[0];

            if (flag == 1)
                printf("    > lux:%d<\n", (((mbSlaveNodeVal_t *)(pData_Shm_slave)) + i)->data.i_val);

            break;
        }
        //co2
        case 202:
        {
            (((mbSlaveNodeVal_t *)(pData_Shm_slave)) + i)->data.i_val = dest[1];

            if (flag == 1)
                printf("    > co2:%d<\n", (((mbSlaveNodeVal_t *)(pData_Shm_slave)) + i)->data.i_val);

            break;
        }
        //fan
        case 203:
        {
            (((mbSlaveNodeVal_t *)(pData_Shm_slave)) + i)->data.i_val = coil[0];

            if (flag == 1)
                printf("    > fan:%d<\n", (((mbSlaveNodeVal_t *)(pData_Shm_slave)) + i)->data.i_val);

            break;
        }
        }
    }

    extern int semid;
    int res = semctl(semid, NUM_SLAVE, GETVAL);
    if (res == 0)
        V_operation(semid, NUM_SLAVE);
    else
    {
        //非 0 或者 1 就设置为 1
        if (res > 1 || res < 0)
        {
            union semun sem;
            sem.val = 1;
            semctl(semid, 0, SETVAL, sem); //slave
        }
    }
}

//不考虑,从机不在线情况,必须是在线才行
int set_SlaveState(modbus_t *ctx, /* 起始地址 */ int coil_addr, /* status */ int status)
{
    return modbus_write_bit(ctx, /*偏移地址 */ coil_addr, status);
}

void func_Modbus(int n)
{
    /* modbus salve 连接 */
    char slaveIp[16]; //从机IP
    strcpy(slaveIp, MDSLAVE_ADDR_IP);

    int slavePort; //从机port
    slavePort = MDSLAVE_ADDR_PORT;

    int slaveId; //从机ID
    slaveId = MDSLAVE_ADDR_ID;

    int ret;
    if ((ret = init_connectToDestSlave(&ctx, slaveIp, slavePort, slaveId)) < 0)
        DEBUG_Err("init_connectToDestSlave err");
    else
        DEBUG_Success("init_connectToDestSlave ok");

    for (;;)
    {
        //如果链接上了
        if (ret >= 0)
        {
            //获取数据并写入共享内存
            get_SlaveData(ctx);

            //间隔时间
            sleep(n);
        }
    }
}