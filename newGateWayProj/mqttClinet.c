#include "mqttClinet.h"

/** 说明:
 *  来自Mqtt的消息,目前,仅仅是来自Qt和fx模拟得到 */

static char mqttServerAddrIp[64] = {0};    //mqtt服务器的ip
static char mqttClinetID[32] = {0};        //本客户端ID
static char mqttTopic_Subscribe[32] = {0}; //订阅的主题
char mqttTopic_Publish[32] = {0};          //上报的主题
int QOS = 1;                               //质量
long TIMEOUT = 5000L;                      //超时ms

int rc;                            //存放一些返回值的结果
MQTTClient client;                 //客户端句柄（描述符）
extern _upLoadPolicy uploadpolicy; //上报策略结构体

//自定义mqtt连接状态标志 0没 1连上了
static char flagMqttConnect = 0;

volatile int recvCount = 0;                                 //用这个,记录第几次收到消息
MQTTClient_message pubmsg = MQTTClient_message_initializer; //发布用的结构体
MQTTClient_deliveryToken token;                             //发布次数的计数
// static volatile MQTTClient_deliveryToken deliveredtoken; //第几次收到的计数(不用他)

int dealMqttTask()
{
    //mqtt服务器的ip
    char ip[16];
    getLocalIp(ip); //获取本机ip
    sprintf(mqttServerAddrIp, "tcp://%s:%d", ip, MQTT_DEFAULT_PORT);

    //本客户端ID
    sprintf(mqttClinetID, "%s", MyMqttClinetId);

    //订阅的主题
    strcpy(mqttTopic_Subscribe, MySubscribeTopic);

    //上报的主题
    strcpy(mqttTopic_Publish, MyPublishTopic);

    //连接参数
    MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;

    //创建客户端，并且指定客户端连接的mqtt服务器地址和客户端ID
    MQTTClient_create(&client, mqttServerAddrIp, mqttClinetID,
                      MQTTCLIENT_PERSISTENCE_NONE, NULL);

    //初始化连接参数(还没连接呢)
    conn_opts.keepAliveInterval = 20;
    conn_opts.cleansession = 1;

    //设置回调接口，只需要关注msgarrvd：消息到达后，会自动调用这个接口
    //参数5,收到消息后的计数函数
    //参数4,收到消息后的处理函数
    //参数3,失去链接后的处理函数
    //参数2,context暂时无用
    //参数1,客户端句柄
    MQTTClient_setCallbacks(client, NULL, connectLost, msgarrvd, delivered);

    //连接到broker
    if ((rc = MQTTClient_connect(client, &conn_opts)) != MQTTCLIENT_SUCCESS)
    {
        DEBUG_Err("Failed to connect");
        printf("\treturn code %d\n", rc);

        pthread_exit(NULL);
    }
    flagMqttConnect = 1;
    DEBUG_Success("broker connect ok");

    //订阅某个主题，指定订阅主题的名字，可以指定qos服务质量
    MQTTClient_subscribe(client, mqttTopic_Subscribe, QOS);

    //订阅的提示,有那些订阅
    printf("\t>[success subscribe topic:%s]\n", mqttTopic_Subscribe);
    printf("\t>[myHost is:%s]\n", mqttClinetID);
    printf("\t>[using QoS%d]\n", QOS);

    //对上报策略进行处理
    switch (uploadpolicy.type)
    {
    // 周期上报
    case 2:
    {
        DEBUG_Success("upload_period");
        upload_period();
        break;
    }
    //数据变化则上报
    case 1:
    {
        DEBUG_Success("changeUpload");
        changeUpload();
        break;
    }
    //不上报,等着Qt来问
    case 0:
    {
        //把任务发送到消息队列去处理,这里不操作
        DEBUG_Success("DEBUG_Success");
        activityUpload();
        break;
    }
    }

    //写成个死循环,保持这个子线程
    //除了周期上报自带for,剩下的俩都用
    for (;;)
        ;
}

// 和broker断开链接
void connectLost(void *context, char *cause)
{
    //一般是被动掉线,比如服务器泵了,或者被顶下来了
    flagMqttConnect = 0;
    uninit_time(); //取消定时器
    DEBUG_Str("Connection lost");
    printf("\tcause: %s\n", cause);
}

//每次 对外 发送完,都会有个计数,基本没用带着,不用管
void delivered(void *context, MQTTClient_deliveryToken dt)
{
    // printf("Send Confirm <%d>\n", dt);
    // deliveredtoken = dt;
}

/** 接收消息的回调函数
 * @param context   消息正文(暂无大用,别管了)
 * @param topicName 收到来自哪个主题的消息
 * @param topicLen  主题的长度
 * @param message   消息体:payload(消息体，字符串)  payloadlen：消息的长度
 * @return int 返回值  */
int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message)
{
    ++recvCount;

    // printf("\t>>[New Message arrived]\n");
    // printf("\t>>topic: %s\n", topicName);
    // printf("\t>>recv <%d>msg = %s\n", deliveredtoken, (char *)message->payload);
    // printf("\t>>Mqtt len:%d\n", strlen((char *)message->payload));
    // puts("");

    //得到负载的正文消息
    printf(">>>RECV New <%d> MQTT: \n%s\n", recvCount, (char *)message->payload);

    //解析存放Qt下发的命令
    //这里不通用,消息队列才是通用的
    cmd_Qt cmdStruct;
    if (parseQtCmdMqttMsg(&cmdStruct, (char *)message->payload) < 0)
    {
        DEBUG_Err("parseQtCmdMqttMsg");
        goto err2;
    }

    //处理Qt下发的命令,都存在结构体里
    //先判断是否真的是Qt
    if (strstr(cmdStruct.who, Str_Qt) == NULL)
    {
        //不是Qt,可能mqtt发错了
        goto err2;
    }

    //把命令结构体,整合后,发送给消息队列
    queueMsg_t queueMsg;
    if (combineQueueMsgStr_Qt(&queueMsg, &cmdStruct) < 0)
    {
        DEBUG_Warn("Qt Send Err Cmd Format");
        goto err2;
    }

    //正常发送命令给消息队列
    if (msg_queue_send(QueueMsg_Recv, &queueMsg, sizeof(queueMsg), /*立即发送*/ 0) < 0)
    {
        DEBUG_Err("msg_queue_send error");
        goto err2;
    }
    else
    {
        DEBUG_Success("QueueMsg send ok from Qt Cmd");
    }

err2:

    //释放资源
    MQTTClient_freeMessage(&message);
    MQTTClient_free(topicName);
    return 1; //切记返回值
}

int parseQtCmdMqttMsg(cmd_Qt *cmdStruct, char *payload)
{
    //mqtt接受到消息,要不要打印
    int debug_Flag = 0;

    memset(cmdStruct, 0, sizeof(cmd_Qt));

    if (payload == NULL)
    {
        DEBUG_Err("payload Empty!!err");
        return -1;
    }

    cJSON *json = cJSON_Parse(payload); //根
    if (json == NULL || json->type != cJSON_Object)
    {
        DEBUG_Err("Parse err Json Mqtt");

        cJSON_Delete(json);

        return -1;
    }

    //判断是谁
    //只能是Qt。不是就错
    cJSON *who = cJSON_GetObjectItem(json, "who");
    if (who != NULL && who->type == cJSON_String)
    {
        if (debug_Flag > 0)
            printf("\tQt Mqtt who: %s\n", who->valuestring);

        strncpy(cmdStruct->who, who->valuestring, strlen(who->valuestring));
    }
    else
    {
        DEBUG_Warn("Mqtt Recv From Not Mu Qt");

        cJSON_Delete(json);

        return -1;
    }

    //解析下发的控制指令
    cJSON *cmd = cJSON_GetObjectItem(json, "cmd");
    if (cmd != NULL && cmd->type == cJSON_String)
    {
        if (debug_Flag > 0)
            printf("\tQt Mqtt cmd: %s\n", cmd->valuestring);

        strncpy(cmdStruct->cmd, cmd->valuestring, strlen(cmd->valuestring));
    }

    /* 具体的命令 */

    //命令列表
    cJSON *cmdList = cJSON_GetObjectItem(json, "cmdList");
    if (cmdList != NULL && cmdList->type == cJSON_Object)
    {
        cJSON *key = cJSON_GetObjectItem(cmdList, "key");
        if (key != NULL && key->type == cJSON_Number)
        {
            if (debug_Flag > 0)
                printf("\tQt Mqtt key: %d\n", key->valueint);

            cmdStruct->key = key->valueint;
        }

        //下发控制状态
        cJSON *state = cJSON_GetObjectItem(cmdList, "status");
        if (state != NULL && state->type == cJSON_True)
        {
            if (debug_Flag > 0)
                printf("\tQt Mqtt state: %d\n", state->type);

            cmdStruct->state = state->type;
        }

        //阈值数据
        cJSON *status = cJSON_GetObjectItem(cmdList, "status");
        if (status != NULL && status->type == cJSON_Number)
        {
            if (debug_Flag > 0)
                printf("\tQt Mqtt status: %f\n", status->valuedouble);

            cmdStruct->status = status->valuedouble;
        }
    }

    //解析下发的修改上报策的 msg
    cJSON *reportCfg = cJSON_GetObjectItem(json, "reportCfg");
    if (reportCfg != NULL)
    {
        int period = cJSON_GetObjectItem(reportCfg, "period")->valueint;
        int type = cJSON_GetObjectItem(reportCfg, "type")->valueint;
        printf("Recv reportCfg: period=%d, type=%d\n", period, type);

        //key   代表策略
        //state 代表周期
        cmdStruct->key = type;
        cmdStruct->state = period;
    }

    // printf("++++++++++++++++++cmdStruct INFO:\n\
    //           cmd:%s\n\
    //           who:%s\n\
    //           key:%d\n\
    //           state:%d\n",
    //        cmdStruct->cmd, cmdStruct->who, cmdStruct->key, cmdStruct->state);

    cJSON_Delete(json);

    return 0;
}

int combineQueueMsgStr_Qt(queueMsg_t *queueMsg, cmd_Qt *cmdStruct)
{
    memset(queueMsg, 0, sizeof(queueMsg_t));

    queueMsg->mtype = 1;            //long
    queueMsg->key = cmdStruct->key; //唯一key标识
    // strcpy(queueMsg->who, cmdStruct->who); //who
    strcpy(queueMsg->who, Str_Who_Qt); //who 用于消息队列

    //设备名字
    if (cmdStruct->key <= 104 || cmdStruct->key >= 101) //Str_DeviceName
        strcpy(queueMsg->deviceName, Str_DeviceName_Stm32);
    else if (cmdStruct->key <= 203 || cmdStruct->key >= 201) //Str_DeviceName_Slave
        strcpy(queueMsg->deviceName, Str_DeviceName_Slave);

    //如果是控制
    if (strstr(cmdStruct->cmd, Str_ctl) != NULL)
    {
        queueMsg->cmdType = 1;                               //下发控制
        queueMsg->ctlDeviceState_1.i_val = cmdStruct->state; //开关状态qmsg
    }
    //如果下发 更新数据的命令
    //具体,重新索要数据和上发,在读取消息队列时候执行,这里只下发
    else if (strstr(cmdStruct->cmd, Str_updata) != NULL)
    {
        queueMsg->cmdType = 2; //快速更新数据
    }
    //如果是 修改阈值
    else if (strstr(cmdStruct->cmd, Str_threshold) != NULL)
    {
        queueMsg->cmdType = 3;                                //修改阈值
        queueMsg->ctlDeviceState_1.f_val = cmdStruct->status; //数值
    }
    //如果是 修改策略，发给消息队列
    else if (strstr(cmdStruct->cmd, Str_Policy) != NULL)
    {
        queueMsg->cmdType = 4;                               //修改上报策略
        queueMsg->ctlDeviceState_1.i_val = cmdStruct->key;   //策略格式
        queueMsg->ctlDeviceState_2.i_val = cmdStruct->state; //周期（只周期上报用的上）
        DEBUG_Success("send modify upload policy Msg Queue");
    }
    else
    {
        DEBUG_Warn("unkonw Mqtt msg Format");

        //代表不让发给消息队列错误的消息
        return -1;
    }

    return 0;
}

/* 三种上报策略----------------------------------------------------------------- */
// 定时周期上报-------------------------------------------------------------------
static struct itimerval oldtv;
void upload_period()
{
    signal(SIGALRM, signal_handler); //注册当接收到SIGALRM时会发生是么函数；
    set_timer();                     //启动定时器，
}

void set_timer()
{
    struct itimerval itv;
    itv.it_interval.tv_sec = uploadpolicy.period; //  设置为n秒
    itv.it_interval.tv_usec = 0;
    itv.it_value.tv_sec = 1; //在1秒后启动
    itv.it_value.tv_usec = 0;
    setitimer(ITIMER_REAL, &itv, &oldtv); //此函数为linux的api,不是c的标准库函数
}

void uninit_time()
{
    struct itimerval value;
    value.it_value.tv_sec = 0;
    value.it_value.tv_usec = 0;
    value.it_interval = value.it_value;
    setitimer(ITIMER_REAL, &value, NULL);
}

void signal_handler(int m)
{
    /* 周期上报 具体在这里 */

    /* 先判断mqtt连接状态 */
    if (flagMqttConnect == 0)
    {
        DEBUG_Err("Mqtt connect Err");
        return;
    }

    /* 生成上报的JSON字符串 */
    char *jsonStr = createJsonStrUploadPeriod();

    //给消息结构体赋值
    pubmsg.payload = jsonStr;                 //指向消息字符串的首地址
    pubmsg.payloadlen = (int)strlen(jsonStr); //消息长度
    pubmsg.qos = QOS;                         //质量
    pubmsg.retained = 0;                      //消息是否被保留

    //去发消息
    MQTTClient_publishMessage(client, mqttTopic_Publish, &pubmsg, &token);
    free(jsonStr);

    //超时检测 arg1:客户端对象 arg2:计数 arg3:超时
    //暂时用5s超时
    if (rc = MQTTClient_waitForCompletion(client, token, TIMEOUT) != 0)
        DEBUG_Err("Mqtt Delivery To Qt Err!");
    else
    {
        // printf("\t>[Send Mqtt To Qt Ok: <%d>]\n", token);
    }

    // printf("    >To Send, Waiting for up to %d seconds\n", (int)(TIMEOUT / 1000));              //时间
    // printf("    >Publication of \n%s\n", jsonStr);                                                  //内容
    // printf("    >on topic %s for client with ClientID: %s\n", mqttTopic_Publish, mqttClinetID); //主题 ID
}

char *createJsonStrUploadPeriod()
{
    //调试标志位
    int flag = 0;

    /* 共享内存部分 */

    //共享内存API 识别标签
    extern struct shm_param para_stm32;
    extern struct shm_param para_slave;

    //共享内存基地址
    extern void *pBase_Shm_Data_stm32;
    extern void *pBase_Shm_Data_slave;

    //共享内存 数据起始 地址
    extern void *pData_Shm_stm32;
    extern void *pData_Shm_slave;

    //定义很多变量来接收
    float temp;
    float humi;
    float bat;
    int led_state;

    int lux;
    int co2;
    int fan_state;

    for (int i = 0; i < NUM_DATA_STM32; i++)
    {
        switch ((((stm32NodeVal_t *)pData_Shm_stm32) + i)->key)
        {
        //温度
        case 101:
        {
            temp = (((stm32NodeVal_t *)pData_Shm_stm32) + i)->data.f_val;

            if (flag == 1)
                printf("key:%d val:%f Name:%s\n", (((stm32NodeVal_t *)pData_Shm_stm32) + i)->key,
                       (((stm32NodeVal_t *)pData_Shm_stm32) + i)->data.f_val,
                       (((stm32NodeVal_t *)pData_Shm_stm32) + i)->name);

            break;
        }
        //湿度
        case 102:
        {
            humi = (((stm32NodeVal_t *)pData_Shm_stm32) + i)->data.f_val;

            if (flag == 1)
                printf("key:%d val:%f Name:%s\n", (((stm32NodeVal_t *)pData_Shm_stm32) + i)->key,
                       (((stm32NodeVal_t *)pData_Shm_stm32) + i)->data.f_val,
                       (((stm32NodeVal_t *)pData_Shm_stm32) + i)->name);

            break;
        }
        //电池电压
        case 103:
        {
            bat = (((stm32NodeVal_t *)pData_Shm_stm32) + i)->data.f_val;

            if (flag == 1)
                printf("key:%d val:%f Name:%s\n", (((stm32NodeVal_t *)pData_Shm_stm32) + i)->key,
                       (((stm32NodeVal_t *)pData_Shm_stm32) + i)->data.f_val,
                       (((stm32NodeVal_t *)pData_Shm_stm32) + i)->name);

            break;
        }
        //led开关
        case 104:
        {
            led_state = (((stm32NodeVal_t *)pData_Shm_stm32) + i)->data.i_val;

            if (flag == 1)
                printf("key:%d val:%f Name:%s\n", (((stm32NodeVal_t *)pData_Shm_stm32) + i)->key,
                       (((stm32NodeVal_t *)pData_Shm_stm32) + i)->data.f_val,
                       (((stm32NodeVal_t *)pData_Shm_stm32) + i)->name);

            break;
        }
        }
    }

    for (int i = 0; i < NUM_DATA_SLAVE; i++)
    {
        //光照强度
        if ((((mbSlaveNodeVal_t *)pData_Shm_slave) + i)->key == 201)
        {
            lux = (((mbSlaveNodeVal_t *)pData_Shm_slave) + i)->data.i_val;

            if (flag == 1)
                printf("key:%d val:%d Name:%s\n", (((mbSlaveNodeVal_t *)pData_Shm_slave) + i)->key,
                       (((mbSlaveNodeVal_t *)pData_Shm_slave) + i)->data.i_val,
                       (((mbSlaveNodeVal_t *)pData_Shm_slave) + i)->name);
        }
        //co2浓度
        else if ((((mbSlaveNodeVal_t *)pData_Shm_slave) + i)->key == 202)
        {
            co2 = (((mbSlaveNodeVal_t *)pData_Shm_slave) + i)->data.i_val;

            if (flag == 1)
                printf("key:%d val:%d Name:%s\n", (((mbSlaveNodeVal_t *)pData_Shm_slave) + i)->key,
                       (((mbSlaveNodeVal_t *)pData_Shm_slave) + i)->data.i_val,
                       (((mbSlaveNodeVal_t *)pData_Shm_slave) + i)->name);
        }
        //风扇开关
        else if ((((mbSlaveNodeVal_t *)pData_Shm_slave) + i)->key == 203)
        {
            fan_state = (((mbSlaveNodeVal_t *)pData_Shm_slave) + i)->data.b_val;

            if (flag == 1)
                printf("key:%d val:%d Name:%s\n", (((mbSlaveNodeVal_t *)pData_Shm_slave) + i)->key,
                       (((mbSlaveNodeVal_t *)pData_Shm_slave) + i)->data.i_val,
                       (((mbSlaveNodeVal_t *)pData_Shm_slave) + i)->name);
        }
    }

    // 创建顶层JSON对象
    cJSON *root = cJSON_CreateObject();

    // 添加 "who" 字段
    cJSON_AddStringToObject(root, "who", "gateway");

    // 创建 "data" 数组
    cJSON *dataArray = cJSON_CreateArray();

    // 创建并添加每个数据对象到 "data" 数组中
    /* 1：bool类型 2：int型  3：float型 */

    //温度
    cJSON *obj_temp = cJSON_CreateObject();
    cJSON_AddNumberToObject(obj_temp, "key", 101);
    cJSON_AddNumberToObject(obj_temp, "type", 3);
    cJSON_AddNumberToObject(obj_temp, "status", temp);
    cJSON_AddItemToArray(dataArray, obj_temp);

    //湿度
    cJSON *obj_humi = cJSON_CreateObject();
    cJSON_AddNumberToObject(obj_humi, "key", 102);
    cJSON_AddNumberToObject(obj_humi, "type", 3);
    cJSON_AddNumberToObject(obj_humi, "status", humi);
    cJSON_AddItemToArray(dataArray, obj_humi);

    //电池电压 (后来改成 百分数 浮点数)
    cJSON *obj_bat = cJSON_CreateObject();
    cJSON_AddNumberToObject(obj_bat, "key", 103);
    cJSON_AddNumberToObject(obj_bat, "type", 3);
    cJSON_AddNumberToObject(obj_bat, "status", bat);
    cJSON_AddItemToArray(dataArray, obj_bat);

    //led开关 bool
    cJSON *obj_led_bool = cJSON_CreateObject();
    cJSON_AddNumberToObject(obj_led_bool, "key", 104);
    cJSON_AddNumberToObject(obj_led_bool, "type", 1);
    cJSON_AddItemToObject(obj_led_bool, "status", cJSON_CreateBool(led_state));
    cJSON_AddItemToArray(dataArray, obj_led_bool);

    //光照强度
    cJSON *obj_lux = cJSON_CreateObject();
    cJSON_AddNumberToObject(obj_lux, "key", 201);
    cJSON_AddNumberToObject(obj_lux, "type", 2);
    cJSON_AddNumberToObject(obj_lux, "status", lux);
    cJSON_AddItemToArray(dataArray, obj_lux);

    //co2
    cJSON *obj_co2 = cJSON_CreateObject();
    cJSON_AddNumberToObject(obj_co2, "key", 202);
    cJSON_AddNumberToObject(obj_co2, "type", 2);
    cJSON_AddNumberToObject(obj_co2, "status", co2);
    cJSON_AddItemToArray(dataArray, obj_co2);

    //风扇
    cJSON *obj_fan_bool = cJSON_CreateObject();
    cJSON_AddNumberToObject(obj_fan_bool, "key", 203);
    cJSON_AddNumberToObject(obj_fan_bool, "type", 1);
    cJSON_AddItemToObject(obj_fan_bool, "status", cJSON_CreateBool(fan_state));
    cJSON_AddItemToArray(dataArray, obj_fan_bool);

    // 将 "data" 数组添加到顶层JSON对象中
    cJSON_AddItemToObject(root, "data", dataArray);

    // 将JSON对象转换为字符串
    char *jsonStr = cJSON_Print(root);

    // 打印生成的JSON字符串
    // printf("%s\n", jsonStr);

    // 释放内存
    cJSON_Delete(root);

    if (flag == 1)
        printf("\nVar From ShareMemory Val::\n\
        1.temp:%f\n\
        2.humi:%f\n\
        3.bat:%f\n\
        4.led:%d\n\
        5.lux:%d\n\
        6.co2:%d\n\
        7.fan:%d\n",
               temp, humi, bat, led_state, lux, co2, fan_state);

    return jsonStr;
}

//主动所要数据更新----------------------------------------------------------
void activityUpload()
{
    //把任务发送到消息队列去处理,这里不操作
}

//变化上报-----------------------------------------------------------------
void changeUpload()
{
    //先从共享内存,拿到第一次运行该任务的变量数据
    //没有很及时，是因为收到数据不及时，并不是这个算法问题

    //定义很多变量来接收 旧数据
    float temp_old;
    float humi_old;
    float bat_old;
    int led_state_old;

    int lux_old;
    int co2_old;
    int fan_state_old;
    uploadDataVal(&temp_old, &humi_old, &bat_old, &led_state_old,
                  &lux_old, &co2_old, &fan_state_old);

    //定义很多变量来接收 新数据
    float temp_new;
    float humi_new;
    float bat_new;
    int led_state_new;

    int lux_new;
    int co2_new;
    int fan_state_new;

    for (;;)
    {
        //获取新数据
        uploadDataVal(&temp_new, &humi_new, &bat_new, &led_state_new,
                      &lux_new, &co2_new, &fan_state_new);

        //新数据和旧数据对比
        //并覆盖旧数据
        char *pstr = createChangeUpload_JsonStr(&temp_old, &humi_old, &bat_old, &led_state_old,
                                                &lux_old, &co2_old, &fan_state_old,
                                                &temp_new, &humi_new, &bat_new, &led_state_new,
                                                &lux_new, &co2_new, &fan_state_new);
        //空就是没更新的数据
        if (pstr == NULL)
        {
            continue;
        }
        //否则有更新的数据,需要发送给Qt
        else
        {
            //给消息结构体赋值
            pubmsg.payload = pstr;                 //指向消息字符串的首地址
            pubmsg.payloadlen = (int)strlen(pstr); //消息长度
            pubmsg.qos = QOS;                      //质量
            pubmsg.retained = 0;                   //消息是否被保留

            //去发消息
            MQTTClient_publishMessage(client, mqttTopic_Publish, &pubmsg, &token);

            //超时检测 arg1:客户端对象 arg2:计数 arg3:超时
            //暂时用5s超时
            if (rc = MQTTClient_waitForCompletion(client, token, TIMEOUT) != 0)
                DEBUG_Err("Mqtt Delivery To Qt Err!");
            else
                printf("\t>[Send Mqtt To Qt Ok: <%d>]\n", token);

            //发完释放
            free(pstr);
        }
    }
}

void uploadDataVal(float *temp, float *humi, float *bat, int *led_state,
                   int *lux, int *co2, int *fan_state)
{
    /* 共享内存部分 */

    //共享内存API 识别标签
    extern struct shm_param para_stm32;
    extern struct shm_param para_slave;

    //共享内存基地址
    extern void *pBase_Shm_Data_stm32;
    extern void *pBase_Shm_Data_slave;

    //共享内存 数据起始 地址
    extern void *pData_Shm_stm32;
    extern void *pData_Shm_slave;

    //先进行第一次赋值

    for (int i = 0; i < NUM_DATA_STM32; i++)
    {
        //温度
        if ((((stm32NodeVal_t *)pData_Shm_stm32) + i)->key == 101)
            *temp = (((stm32NodeVal_t *)pData_Shm_stm32) + i)->data.f_val;
        //湿度
        else if ((((stm32NodeVal_t *)pData_Shm_stm32) + i)->key == 102)
            *humi = (((stm32NodeVal_t *)pData_Shm_stm32) + i)->data.f_val;
        //电池电压
        else if ((((stm32NodeVal_t *)pData_Shm_stm32) + i)->key == 103)
            *bat = (((stm32NodeVal_t *)pData_Shm_stm32) + i)->data.f_val;
        //led
        else if ((((stm32NodeVal_t *)pData_Shm_stm32) + i)->key == 104)
            *led_state = (((stm32NodeVal_t *)pData_Shm_stm32) + i)->data.b_val;
    }

    for (int i = 0; i < NUM_DATA_SLAVE; i++)
    {
        //光照强度
        if ((((mbSlaveNodeVal_t *)pData_Shm_slave) + i)->key == 201)
            *lux = (((mbSlaveNodeVal_t *)pData_Shm_slave) + i)->data.i_val;
        //co2浓度
        else if ((((mbSlaveNodeVal_t *)pData_Shm_slave) + i)->key == 202)
            *co2 = (((mbSlaveNodeVal_t *)pData_Shm_slave) + i)->data.i_val;
        //风扇开关
        else if ((((mbSlaveNodeVal_t *)pData_Shm_slave) + i)->key == 203)
            *fan_state = (((mbSlaveNodeVal_t *)pData_Shm_slave) + i)->data.b_val;
    }
}

char *createChangeUpload_JsonStr(
    //旧数据
    float *temp_old, float *humi_old, float *bat_old, int *led_state_old,
    int *lux_old, int *co2_old, int *fan_state_old,

    //新数据
    float *temp_new, float *humi_new, float *bat_new, int *led_state_new,
    int *lux_new, int *co2_new, int *fan_state_new)
{

    //是否发送的标志位,为真就发送
    int sendFlag = 0;

    // 创建顶层JSON对象
    cJSON *root = cJSON_CreateObject();

    // 添加 "who" 字段
    cJSON_AddStringToObject(root, "who", "gateway");

    // 创建 "data" 数组
    // 创建并添加每个数据对象到 "data" 数组中
    /* 1：bool类型 2：int型  3：float型 */
    cJSON *dataArray = cJSON_CreateArray();

    /* -------数据对比-------------------------------------------------- */

    //温度 101
    if ((*temp_old) != (*temp_new))
    {
        sendFlag = 1;

        (*temp_old) = (*temp_new);

        cJSON *obj = cJSON_CreateObject();
        cJSON_AddNumberToObject(obj, "key", 101);
        cJSON_AddNumberToObject(obj, "type", 3);
        cJSON_AddNumberToObject(obj, "status", (*temp_new));
        cJSON_AddItemToArray(dataArray, obj);
    }

    //湿度 102
    if ((*humi_old) != (*humi_new))
    {
        sendFlag = 1;

        (*humi_old) = (*humi_new);

        cJSON *obj = cJSON_CreateObject();
        cJSON_AddNumberToObject(obj, "key", 102);
        cJSON_AddNumberToObject(obj, "type", 3);
        cJSON_AddNumberToObject(obj, "status", (*humi_new));
        cJSON_AddItemToArray(dataArray, obj);
    }

    //电池电压 103
    if ((*bat_old) != (*bat_new))
    {
        sendFlag = 1;

        (*bat_old) = (*bat_new);

        cJSON *obj = cJSON_CreateObject();
        cJSON_AddNumberToObject(obj, "key", 103);
        cJSON_AddNumberToObject(obj, "type", 3);
        cJSON_AddNumberToObject(obj, "status", (*bat_new));
        cJSON_AddItemToArray(dataArray, obj);
    }

    //led 104
    if ((*led_state_old) != (*led_state_new))
    {
        sendFlag = 1;

        (*led_state_old) = (*led_state_new);

        cJSON *obj = cJSON_CreateObject();
        cJSON_AddNumberToObject(obj, "key", 104);
        cJSON_AddNumberToObject(obj, "type", 1);
        cJSON_AddItemToObject(obj, "status", cJSON_CreateBool((*led_state_new)));
        cJSON_AddItemToArray(dataArray, obj);
    }

    //光照强度 201
    if ((*lux_old) != (*lux_new))
    {
        sendFlag = 1;

        (*lux_old) = (*lux_new);

        cJSON *obj = cJSON_CreateObject();
        cJSON_AddNumberToObject(obj, "key", 201);
        cJSON_AddNumberToObject(obj, "type", 2);
        cJSON_AddNumberToObject(obj, "status", (*lux_new));
        cJSON_AddItemToArray(dataArray, obj);
    }

    //co2 202
    if ((*co2_old) != (*co2_new))
    {
        sendFlag = 1;

        (*co2_old) = (*co2_new);

        cJSON *obj = cJSON_CreateObject();
        cJSON_AddNumberToObject(obj, "key", 202);
        cJSON_AddNumberToObject(obj, "type", 2);
        cJSON_AddNumberToObject(obj, "status", (*co2_new));
        cJSON_AddItemToArray(dataArray, obj);
    }

    //风扇 203
    if ((*fan_state_old) != (*fan_state_new))
    {
        sendFlag = 1;

        (*fan_state_old) = (*fan_state_new);

        cJSON *obj = cJSON_CreateObject();
        cJSON_AddNumberToObject(obj, "key", 203);
        cJSON_AddNumberToObject(obj, "type", 1);
        cJSON_AddItemToObject(obj, "status", cJSON_CreateBool((*fan_state_new)));
        cJSON_AddItemToArray(dataArray, obj);
    }

    //数组添加到根
    cJSON_AddItemToObject(root, "data", dataArray);

    char *pstr = cJSON_Print(root);
    cJSON_Delete(root);

    //没有更新的数据
    if (sendFlag == 0)
        pstr = NULL;

    return pstr;
}