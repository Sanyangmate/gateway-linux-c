#ifndef __MAIN_H__
#define __MAIN_H__

//标准库
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>

//三方库
#include "MQTTClient.h"
#include "cJSON.h"

//自定义
#include "msg_queue_peer.h"
#include "shmem.h"
#include "list.h"

#include "dataType.h"

//自己的功能
#include "taskFunc.h"   //线程任务
#include "helpFunc.h"   //辅助功能
#include "mqttClinet.h" //mqtt
#include "udpSearch.h"  //udp
#include "tcpServer.h"  //tcp
#include "sem.h"

//配置宏

#define UDP_RECV_PORT 9211   //udp端口
#define UDP_RECV_BUF_LEN 256 //udp接收数组
#define UDP_CMP_STR "OGC"    //udp私有数据

//modbus 目前用 手动制定
#define MDSLAVE_ADDR_IP "192.168.91.153" //从机ip
#define MDSLAVE_ADDR_PORT 502            //从机port
#define MDSLAVE_ADDR_ID 1                //从机ID

//配置结构体

#endif