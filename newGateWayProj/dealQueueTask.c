#include "dealQueueTask.h"
#include "helpFunc.h"
#include "dataType.h"
#include "tcpServer.h"
#include "modbus.h"
#include "deal_mdSlave.h"
#include "MQTTClient.h"
#include "taskFunc.h"

void deal_QueueTask(void)
{
    queueMsg_t qmsg;

    for (;;)
    {
        if (msg_queue_recv(QueueMsg_Recv, &qmsg,
                           sizeof(qmsg), 0, 0) > 0)
        {
            DEBUG_Success("recv QueueMsg");

            //来自web下发的控制命令
            if (strstr(qmsg.who, Str_Who_Web) != NULL)
            {
                deal_queueCmd_from_Web(qmsg);
            }
            //来自Qt下发的控制命令
            else if (strstr(qmsg.who, Str_Who_Qt) != NULL)
            {
                deal_queueCmd_from_Qt(qmsg);
            }
            //来自AliSDK下发的控制命令
            else if (strstr(qmsg.who, Str_Who_AliSDK) != NULL)
            {
                deal_queueCmd_from_SDK(qmsg);
            }
            //来自未知者下发的控制命令
            else
            {
                DEBUG_Warn("unknow QueueMsg");
                printf("    <?>who:%s\n", qmsg.who);
            }
        }
        else
        {
            DEBUG_Err("recv error");
        }
    }
}

//处理Qt系列的
int deal_queueCmd_from_Qt(queueMsg_t qmsg)
{
    //cmdType == 1 控制
    //cmdType == 2 索要数据
    //cmdType == 3 修改阈值
    //cmdType == 4 修改上报策略

    //已经处理过who,不用再看

    switch (qmsg.cmdType)
    {
    case 1:
    {
        deal_Qt_ctl(qmsg);
        break;
    }
    case 2:
    {
        deal_Qt_upData(qmsg);
        break;
    }
    case 3:
    {
        deal_Qt_threshold(qmsg);
        break;
    }
    case 4:
    {
        extern _upLoadPolicy uploadpolicy; //上报策略结构体
        if (deal_Qt_Policy(qmsg, &uploadpolicy, PATH_CONFIG_JSON_FILE) <= 0)
            DEBUG_Err("modify upload Policy err");
        break;
    }
    }
}

//处理Web系列的
void deal_queueCmd_from_Web(queueMsg_t qmsg)
{
    //只有控制
    //104 led
    //203 fan
    deal_Web_SetCtl(qmsg);
}

//处理AliSDK系列的
void deal_queueCmd_from_SDK(queueMsg_t qmsg)
{
    deal_AliSDK_Set(qmsg);
}

// -------------------------------------------------------------------------

/* Qt部分 */

//处理Qt要求 开关 灯或者风扇
//能控制的设备,stm32,slave 都能控制
int deal_Qt_ctl(queueMsg_t qmsg)
{
    //如果是modbus slave 的控制，优先处理
    if (qmsg.key == 203)
    {
        extern modbus_t *ctx; //modbus实例

        //模拟一个设备，地址认为网关提前知道了
        //暂时没必要设计通用性
        set_SlaveState(ctx, 0, qmsg.ctlDeviceState_1.b_val);

        return 0;
    }

    //否则就，下发控制给stm32
    sendCtlToEspStm(qmsg);

    return 0;
}

//处理Qt下发的 主动索要数据
int deal_Qt_upData(queueMsg_t qmsg)
{
    /* 立刻更新所有数据 */

    // 更新所有数据
    // stm32的 (可能不是最新的,但会把更新命令发给stm,下一次一般就是最新的了)
    flushData();

    /* 生成json字符串 */
    char *psend = createJsonStrUploadPeriod();

    /* 准备发送 */
    sendMqtt_Qt(psend);

    //发完释放
    free(psend);
    psend = NULL;
    return 0;
}

//处理Qt下发的 修改阈值 给stm32
int deal_Qt_threshold(queueMsg_t qmsg)
{
    //先判断esp是否存在,不然不能发
    extern int flag_saveSignalEspTcp;
    if (flag_saveSignalEspTcp != 1)
    {
        DEBUG_Warn("esp Not Online,can't Not Send Ctl cmd");
        return -1;
    }

    //生成json
    cJSON *root = cJSON_CreateObject();
    cJSON_AddStringToObject(root, "cmd", "setThreshold");
    cJSON_AddNumberToObject(root, "key", qmsg.key);
    cJSON_AddNumberToObject(root, "maxval", qmsg.ctlDeviceState_1.f_val);
    cJSON_AddNumberToObject(root, "type", 3); //浮点数
    cJSON_AddStringToObject(root, "who", "gataway");

    char *jsonString = cJSON_PrintUnformatted(root);
    printf("%s\n", jsonString);
    cJSON_Delete(root);

    //准备 TCP发送
    extern int socketEspOnly; //esp 的 socketfd
    send(socketEspOnly, jsonString, strlen(jsonString), 0);

    //发完释放
    free(jsonString);
    return 0;
}

/* Web部分 */

//处理Web要求 开关 灯或者风扇
int deal_Web_SetCtl(queueMsg_t qmsg)
{
    switch (qmsg.key)
    {
    case 104: //led
    {
        sendCtlToEspStm(qmsg);

        break;
    }

    case 203: //fan
    {
        extern modbus_t *ctx; //modbus实例

        //模拟一个设备，地址认为网关提前知道了
        //暂时没必要设计通用性
        set_SlaveState(ctx, 0, qmsg.ctlDeviceState_1.b_val);

        break;
    }
    }
}

//给stm发送 led的开关状态
void sendCtlToEspStm(queueMsg_t qmsg)
{
    //先判断esp是否存在,不然不能发
    extern int flag_saveSignalEspTcp;
    if (flag_saveSignalEspTcp == 0)
    {
        DEBUG_Warn("esp Not Online,can't Not Send Ctl cmd");
        return;
    }

    //存在,则准备正常发

    //先生成json
    cJSON *root = cJSON_CreateObject();
    cJSON_AddStringToObject(root, "who", "gataway");
    cJSON_AddStringToObject(root, "cmd", "ctlLed");
    cJSON_AddBoolToObject(root, "ledSet", qmsg.ctlDeviceState_1.b_val); //开关状态
    char *jsonString = cJSON_PrintUnformatted(root);
    cJSON_Delete(root);

    //准备 TCP发送
    extern int socketEspOnly; //esp 的 socketfd
    send(socketEspOnly, jsonString, strlen(jsonString), 0);

    //发完释放
    free(jsonString);
}

// 让modbus和stm32快速重新获取数据,
// 写入共享内存(获取数据函数,自带实现),
// 给Qt和Web用
void flushData()
{
    //modbus实例
    extern modbus_t *ctx;

    //获取数据并写入共享内存
    get_SlaveData(ctx);

    /* -------------- */

    //先判断esp是否存在,不然不能发.
    extern int flag_saveSignalEspTcp;
    if (flag_saveSignalEspTcp == 0)
    {
        DEBUG_Warn("esp Not Online,can't Not Send Ctl cmd");
        return;
    }

    //准备 TCP发送
    extern int socketEspOnly; //esp 的 socketfd
    send(socketEspOnly, CMD_FLUSH_STM, strlen(CMD_FLUSH_STM), 0);
}

//处理 AliSDK的下发命令
int deal_AliSDK_Set(queueMsg_t qmsg)
{
    deal_Web_SetCtl(qmsg);
}

//mqtt Qt 修改上报策略
int deal_Qt_Policy(queueMsg_t qmsg, _upLoadPolicy *flag, char *FilePath)
{
    //已有策略
    //已有周期
    //进行修改

    /* 创建堆区存放，复制粘帖的，没有优化 */

    //先判断文件是否存在
    if (access(FilePath, F_OK) == 0)
        DEBUG_Success("FilePath Exist!");
    else
    {
        DEBUG_Err("FilePath DO not Exist!");
        return -1;
    }

    //读json文件,这里不需要写
    FILE *file = fopen(FilePath, "r+");
    if (NULL == file)
    {
        DEBUG_Err("fopen JSON LOCAL file ERR!");
        return -2;
    }

    //计算json文件的字符长度,
    fseek(file, 0, SEEK_END); //到EOF位置,比如两个长度就指到下标是2的位置
    long lenght = ftell(file);
    fseek(file, 0, SEEK_SET);

    //动态开辟合适长度的空间
    char *buf = (char *)malloc(lenght + 1); //存放'\0'
    if (NULL == buf)
    {
        DEBUG_Err("malloc err buf");
        return 0;
    }
    __bzero(buf, lenght + 1);

    //读文件的内容到buf,下标是lenght的位不被影响
    size_t len_read = fread(buf, 1, lenght, file);
    if (len_read < 0)
    {
        DEBUG_Err("read file To buf err");
        return -3;
    }
    // puts(buf); //测试打印

    //关闭文件指针,用不到了
    fclose(file);

    /* ------------------JSON部分------------------- */

    //json的反序列化,把字符串转换成json树
    cJSON *root = cJSON_Parse(buf);
    if (NULL == root)
    {
        printf("Parse err\n");
        free(buf);
        return -4;
    }
    // 解析成功了也 释放申请的读取文件空间
    free(buf);

    // 创建新的 reportCfg JSON 对象
    cJSON *reportCfg = cJSON_CreateObject();
    cJSON_AddNumberToObject(reportCfg, "type", qmsg.ctlDeviceState_1.i_val);   // 修改 type 的值为
    cJSON_AddNumberToObject(reportCfg, "period", qmsg.ctlDeviceState_2.i_val); // 修改 period 的值为

    // 替换总点表中的 reportCfg
    cJSON_ReplaceItemInObject(root, "reportCfg", reportCfg);

    //新点表文件
    char *newJsonStr = cJSON_Print(root);

    // 释放解析buf过程中分配的内存.
    cJSON_Delete(root);

    /* 配置都 写入到文件里 */

    //重新写入到文件
    int ret = writeToFile(FilePath, newJsonStr, strlen(newJsonStr));
    if (ret <= 0)
    {
        // 肯定大于0,因为写入了，不然就错
        DEBUG_Err("update upload Policy err");
        return -5;
    }

    //修改标志位，修改过的,状态标记
    //不管是第一次,还是后续修改,都会更改这个标志位
    extern int cfg_change;
    cfg_change = 1;

    DEBUG_Success("update upLoad Policy ok!");

    printf("\t>Policy:%d period:%d\n",
           qmsg.ctlDeviceState_1.i_val, qmsg.ctlDeviceState_2.i_val);

    // 可能调试用，看需求
    flag->type = qmsg.ctlDeviceState_1.i_val;
    flag->period = qmsg.ctlDeviceState_2.i_val;

    //注意 这里是1
    return ret;
}