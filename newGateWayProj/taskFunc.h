#ifndef __taskFunc
#define __taskFunc

#include "main.h"
#include "mqttClinet.h"
#include "modbus.h"
#include "deal_mdSlave.h"

/** 这里存放线程主要任务,
 *  主要是数据采集,如果下发和别的任务,在具体文件实现
 *  进行基础的初始化操作
 */

/** 初始化 信号灯集
 *  用于 解决 读写冲突的问题
 */
int func_initSems(int *arg);


/** 开启udp 单线程模型
 *  用于Qt udp 搜索
 */
void *task_UdpObject(void *arg);

/** 开启tcp 多线程并发服务器模型
 *  用于Qt 下发点表
 *  用于esp上传数据
 */
void *task_TcpServer(void *arg);

/** 解析收到的或者本地存在的点表数据
 *  用来解除阻塞,允许下面的任务运行
 */
int func_parseJsonCfg(int *arg);

/** 从本地解析,读取 上报策略
 *  不会影响程序,只是mqtt上报会读取该变量
 */
int func_upLoadPolicy(_upLoadPolicy *flag);

/** 初始化共享内存
 *  对其清0,等待更新
 */
int func_initShareMemory();

/** 创建线程,连接mqtt服务器
 *  监听Qt下发的命令
 *  并给Qt上报数据,根据从本地读取的指定策略
 */
void *task_mqtt_CreateListen_UpLoad(void *arg);

/** 开启线程,周期定时,从modbusSlave获取数据(无下发)
 */
void *task_getSlaveData_Period(void *arg);

/** 开启线程,监听处理消息队列
 */
void *task_ListenQueueMsg(void *arg);

/** 主线程,for用来检测点表配置是否被修改
 *  如果修改了,就要结束进程,进行手动重启
 */
void func_listenCfgChange();

#endif