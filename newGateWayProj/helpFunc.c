#include "helpFunc.h"
#include "tcpServer.h"
#include "main.h"
#include "mqttClinet.h"
#include "dataType.h"

void getLocalIp(char ip[16])
{
    /* 如果是被网管代理的校园网,那么无法获取 */

    FILE *fp;
    char line[256];

    // 执行ifconfig命令并读取输出
    fp = popen("/sbin/ifconfig", "r");
    if (fp == NULL)
    {
        perror("Error");
        exit(EXIT_FAILURE);
    }

    while (fgets(line, sizeof(line), fp) != NULL)
    {
        if (strstr(line, "inet addr:192.168.") != NULL)
        {
            char *p = strstr(line, "inet addr:") + 10;
            char *q = strstr(p, " ");
            *q = '\0';
            strcpy(ip, p);
            break;
        }
    }

    pclose(fp);
}

void printBaseInfo()
{
    //获取本机ip
    char ip[16] = {0};
    getLocalIp(ip);
    printf(">> local Ip:\t%s\n", ip);
    printf(">> TcpServer port:\t%d\n", TcpServerPort);
    printf(">> UdpNode port:\t%d\n", UDP_RECV_PORT);
    printf(">> MqttServer port:\t%d\n", MQTT_DEFAULT_PORT);

    putchar(10);
}

int modifyCfgVar(int *flag)
{
    //打开文件,到堆区数组
    char *buf = readFileToBuff_Heap(PATH_CONFIG_JSON_FILE);
    if (NULL == buf)
    {
        DEBUG_Err("PATH_CONFIG_JSON_FILE Read err");
        return -1;
    }

    /*------- cjson部分 --------*/

    //json的反序列化,把字符串转换成json树
    cJSON *root = cJSON_Parse(buf);
    if (NULL == root)
    {
        DEBUG_Err("Parse err");
        return -1;
    }

    //解析ver节点,从树干上获取ver这个树杈
    cJSON *item = cJSON_GetObjectItem(root, "is_first_initialized");
    if (NULL == item) //判断是否获取成功
    {
        DEBUG_Err("1:object item get err");
        DEBUG_Err("2:maybe the file is err format!");
        return -1;
    }

    //更新状态位(是否被第一次初始化过)
    *flag = item->valueint;

    // 释放解析buf过程中分配的内存.只有这里和buf有关系
    cJSON_Delete(root);

    // 释放申请的读取文件空间
    free(buf);

    return 0;
}

int modifyUpLoadPolicy(_upLoadPolicy *flag)
{
    //打开文件,到堆区数组
    char *buf = readFileToBuff_Heap(PATH_CONFIG_JSON_FILE);
    if (NULL == buf)
    {
        DEBUG_Err("PATH_CONFIG_JSON_FILE Read err");
        return -1;
    }

    /*------- cjson部分 --------*/

    //json的反序列化,把字符串转换成json树
    cJSON *root = cJSON_Parse(buf);
    if (NULL == root)
    {
        printf("Parse err\n");
        return -1;
    }

    //解析ver节点,从树干上获取ver这个树杈
    cJSON *item = cJSON_GetObjectItem(root, "reportCfg");
    if (NULL == item) //判断是否获取成功
    {
        DEBUG_Err("1:object item get err");
        DEBUG_Err("2:maybe the file is err format!");
        return -1;
    }
    int len = cJSON_GetArraySize(item); //数组长度
    if (len != 2)
    {
        DEBUG_Err("1:Array object item get err");
        DEBUG_Err("2:maybe Lenght err!");
        return -1;
    }
    for (size_t i = 0; i < len; i++) //更新状态位(上报策略)
    {
        //先拿到数组的每一个节点
        cJSON *tmp = cJSON_GetArrayItem(item, i);

        //判断是哪个?
        if (strncmp(tmp->string, "period", strlen("period")) == 0)
            flag->period = tmp->valueint;
            
        else if (strncmp(tmp->string, "type", strlen("type")) == 0)
            flag->type = tmp->valueint;

        //打印每次结果
        // printf("[now:%d, %s = %d]\n", i, tmp->string, tmp->valueint);
    }
    printf("[policy:%d, period:%d]\n", flag->type, flag->period);
    DEBUG_Success("get upLoad Policy ok");

    // 释放解析buf过程中分配的内存.只有这里和buf有关系
    cJSON_Delete(root);

    // 释放申请的读取文件空间
    free(buf);
}

char *readFileToBuff_Heap(char *FilePath)
{
    //先判断文件是否存在
    if (access(FilePath, F_OK) == 0)
    {
        DEBUG_Success("FilePath Exist!");
    }
    else
    {
        DEBUG_Err("FilePath DO not Exist!");
        return NULL;
    }

    //读json文件,这里不需要写
    FILE *file = fopen(FilePath, "r");
    if (NULL == file)
    {
        DEBUG_Err("fopen JSON LOCAL file ERR!");
        return NULL;
    }

    //计算json文件的字符长度,
    fseek(file, 0, SEEK_END); //到EOF位置,比如两个长度就指到下标是2的位置
    long lenght = ftell(file);
    fseek(file, 0, SEEK_SET);

    //动态开辟合适长度的空间
    char *buf = (char *)malloc(lenght + 1); //存放'\0'
    if (NULL == buf)
    {
        DEBUG_Err("malloc err buf");
        return NULL;
    }
    buf[lenght] = '\0'; //字符串结束标志

    //读文件的内容到buf,下标是lenght的位不被影响
    size_t len_read = fread(buf, 1, lenght, file);
    if (len_read < 0)
    {
        DEBUG_Err("read file To buf err");
        return NULL;
    }
    // puts(buf); //测试打印

    //关闭文件指针,用不到了
    fclose(file);

    return buf;
}

int initShareMemoy()
{
    //共享内存API 识别标签
    extern struct shm_param para_stm32;
    extern struct shm_param para_slave;

    //共享内存基地址
    extern void *pBase_Shm_Data_stm32;
    extern void *pBase_Shm_Data_slave;

    //共享内存数据起始地址
    extern void *pData_Shm_stm32;
    extern void *pData_Shm_slave;

    int ret, size;

    /* 先打开或创建 */

    //stm32存放数据的共享内存
    size = sizeof(int) + (NUM_DATA_STM32) * (sizeof(stm32NodeVal_t));
    ret = shm_init(&para_stm32, Sheme_ID_stm32, size);
    if (ret < 0)
    {
        DEBUG_Err("Stm32 Data shareMemory malloc Err!");
        return -1;
    }
    pBase_Shm_Data_stm32 = shm_getaddr(&para_stm32);
    __bzero(pBase_Shm_Data_stm32, size);

    //slave存放数据的共享内存
    size = sizeof(int) + (NUM_DATA_SLAVE) * (sizeof(mbSlaveNodeVal_t));
    ret = shm_init(&para_slave, Sheme_ID_slave, size);
    if (ret < 0)
    {
        DEBUG_Err("Slave Data shareMemory malloc Err!");
        return -1;
    }
    pBase_Shm_Data_slave = shm_getaddr(&para_slave);
    __bzero(pBase_Shm_Data_slave, size);

    //写入数据,个数
    *((int *)pBase_Shm_Data_stm32) = NUM_DATA_STM32;
    *((int *)pBase_Shm_Data_slave) = NUM_DATA_SLAVE;

    //数据节点开始的地址
    pData_Shm_stm32 = pBase_Shm_Data_stm32 + sizeof(int); //地址偏移
    pData_Shm_slave = pBase_Shm_Data_slave + sizeof(int); //地址偏移

    /* 再通过点表初始化 */

    char *buf = readFileToBuff_Heap(PATH_CONFIG_JSON_FILE);
    if (NULL == buf)
    {
        DEBUG_Err("init sharememory, Json read to buf err.\nNoData Write ShareMemory");
        return -1;
    }

    //Json解析
    cJSON *root = cJSON_Parse(buf);
    if (NULL == root)
    {
        DEBUG_Err("Failed open json to init shareMemory");
        goto err1;
    }

    // 并放入共享内存

    //stm32部分
    cJSON *stm32Device = cJSON_GetObjectItem(root, "stm32Device");
    cJSON *data = cJSON_GetObjectItem(stm32Device, "data"); //数组的数据元素列表

    for (int i = 0; i < cJSON_GetArraySize(data); i++)
    {
        cJSON *item = cJSON_GetArrayItem(data, i); //从列表里获取第一份

        cJSON *key = cJSON_GetObjectItem(item, "key");
        cJSON *type = cJSON_GetObjectItem(item, "type");
        cJSON *valName = cJSON_GetObjectItem(item, "valName");

        ((stm32NodeVal_t *)pData_Shm_stm32 + i)->key = key->valueint;
        ((stm32NodeVal_t *)pData_Shm_stm32 + i)->type = type->valueint;

        strncpy(
            (((stm32NodeVal_t *)(pData_Shm_stm32)) + i)->name,
            valName->valuestring,
            strlen(valName->valuestring));

        printf("---Data %d: key=%d, type=%d, valName=%s\n",
               (i + 1), (key->valueint), (type->valueint), (valName->valuestring));
    }

    //slave部分
    cJSON *modbusSlave = cJSON_GetObjectItem(root, "modbusSlave");
    data = cJSON_GetObjectItem(modbusSlave, "data"); //数组的数据元素列表

    for (int i = 0; i < cJSON_GetArraySize(data); i++)
    {
        cJSON *item = cJSON_GetArrayItem(data, i); //从列表里获取第一份

        cJSON *key = cJSON_GetObjectItem(item, "key");
        cJSON *type = cJSON_GetObjectItem(item, "type");
        cJSON *addr = cJSON_GetObjectItem(item, "addr");
        cJSON *valName = cJSON_GetObjectItem(item, "valName");

        ((mbSlaveNodeVal_t *)pData_Shm_slave + i)->key = key->valueint;
        ((mbSlaveNodeVal_t *)pData_Shm_slave + i)->type = type->valueint;
        ((mbSlaveNodeVal_t *)pData_Shm_slave + i)->addr = addr->valueint;

        strncpy(
            (((mbSlaveNodeVal_t *)(pData_Shm_slave)) + i)->name,
            valName->valuestring,
            strlen(valName->valuestring));

        printf("---Data %d: key=%d, type=%d, addr=%d, valName=%s\n",
               (i + 1), (key->valueint), (type->valueint), addr->valueint, (valName->valuestring));
    }

err1:
    cJSON_Delete(root);
    free(buf);
    return 0;
}

int sendMqtt_Qt(char *msg)
{
    extern MQTTClient_message pubmsg;      //发布用的结构体
    extern MQTTClient_deliveryToken token; //发布次数的计数
    extern int rc;                         //存放一些返回值的结果
    extern MQTTClient client;              //客户端句柄（描述符）
    extern int QOS;                        //质量
    extern char mqttTopic_Publish[32];     //上报的主题
    extern long TIMEOUT;                   //超时ms

    //给消息结构体赋值
    pubmsg.payload = msg;                 //指向消息字符串的首地址
    pubmsg.payloadlen = (int)strlen(msg); //消息长度
    pubmsg.qos = QOS;                     //质量
    pubmsg.retained = 0;                  //消息是否被保留

    //去发消息
    MQTTClient_publishMessage(client, mqttTopic_Publish, &pubmsg, &token);

    //超时检测 arg1:客户端对象 arg2:计数 arg3:超时
    //暂时用5s超时
    if (rc = MQTTClient_waitForCompletion(client, token, TIMEOUT) != 0)
    {
        DEBUG_Err("Mqtt Delivery To Qt Err!");
        return -1;
    }
    else
    {
        printf("\t>[Send Mqtt To Qt Ok: <%d>]\n", token);
        return 0;
    }
}
