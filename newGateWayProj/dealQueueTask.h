#ifndef _DEALQUEUETASK_H_
#define _DEALQUEUETASK_H_

#include "dataType.h"
#include "helpFunc.h"

#define CMD_FLUSH_STM "{\"cmd\": \"flushData\",\"who\": \"gataway\"}"

void deal_QueueTask(void);
int deal_queueCmd_from_Qt(queueMsg_t qmsg);
void deal_queueCmd_from_Web(queueMsg_t qmsg);
void deal_queueCmd_from_SDK(queueMsg_t qmsg);
int deal_Qt_ctl(queueMsg_t qmsg);
int deal_Qt_upData(queueMsg_t qmsg);
int deal_Qt_threshold(queueMsg_t qmsg);
void sendCtlToEspStm(queueMsg_t qmsg);
int deal_Web_SetCtl(queueMsg_t qmsg);
int deal_AliSDK_Set(queueMsg_t qmsg);
int deal_Qt_Policy(queueMsg_t qmsg, _upLoadPolicy *flag, char *FilePath);


//刷新单片机的slave的数据，尽快更新
void flushData();

#endif