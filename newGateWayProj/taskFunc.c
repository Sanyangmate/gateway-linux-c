#include "taskFunc.h"
#include "mqttClinet.h"
#include "tcpServer.h"
#include "udpSearch.h"
#include "deal_mdSlave.h"
#include "dealQueueTask.h"
#include "sem.h"
#include "main.h"

//共享内存API 识别标签
struct shm_param para_stm32;
struct shm_param para_slave;

//共享内存 基地址
void *pBase_Shm_Data_stm32;
void *pBase_Shm_Data_slave;

//共享内存 数据段 起始地址
void *pData_Shm_stm32;
void *pData_Shm_slave;

int func_initSems(int *arg)
{
    return initSem(*arg);
}

void *task_UdpObject(void *arg)
{
    udpSearch();
}

void *task_TcpServer(void *arg)
{
    tcpServer();
}

int func_parseJsonCfg(int *arg)
{
    return modifyCfgVar(arg);
}

int func_upLoadPolicy(_upLoadPolicy *flag)
{
    return modifyUpLoadPolicy(flag);
}

int func_initShareMemory()
{
    return initShareMemoy();
}

void *task_mqtt_CreateListen_UpLoad(void *arg)
{
    dealMqttTask();
}

void *task_getSlaveData_Period(void *arg)
{
    func_Modbus(Modbus_Period_Time);
}

void *task_ListenQueueMsg(void *arg)
{
    deal_QueueTask();
}

void func_listenCfgChange()
{
    extern int cfg_change;

    if (cfg_change == 1)
    {
        DEBUG_Success("Recv New Config, Exit Self, Need Reboot");
        close(1); //关闭标准输出
        sleep(1); //延时
        exit(0);  //决定是否要退出进程
    }
}