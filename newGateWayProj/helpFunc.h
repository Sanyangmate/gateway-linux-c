#ifndef _HELPFUNC_H_
#define _HELPFUNC_H_

#include "tcpServer.h"
#include "dataType.h"
#include "mqttClinet.h"
#include "msg_queue_peer.h"
#include "shmem.h"

/* 不懂,为什么放在这里才能编译,猜测是预处理的文件先后导入问题 */
//上报策略结构体
typedef struct
{
    int type;   //方式
    int period; //周期
} _upLoadPolicy;

/**
 * 调试宏,都有 字符串和位置文件函数s
 * 都需要输入参数
 */
//基本调试
#define DEBUG_Str(format, ...) printf("<Debug>:Func:%s Line:%d File:%s\n\t[" format "]\n", \
                                      __func__, __LINE__, __FILE__)
//Warn警告(带浅颜色)
#define DEBUG_Warn(format, ...) printf("\033[4;36m<Warn>:Func:%s Line:%d File:%s\n\t[" format "]\033[0m\n", \
                                       __func__, __LINE__, __FILE__)
//err报错(红颜色)
#define DEBUG_Err(format, ...) printf("\033[4;31m<Err>:Func:%s Line:%d File:%s\n\t[" format "]\033[0m\n", \
                                      __func__, __LINE__, __FILE__)
//成功提示
#define DEBUG_Success(format, ...) printf("\t<Ok>:[" format "]\n")

// 共享内存,存入几个数据
// 有几个存在的属性
#define NUM_DATA_STM32 4
#define NUM_DATA_SLAVE 3



// 获取本地ip
void getLocalIp(char ip[16]);

// 打印基础网络信息s
void printBaseInfo();

//从文件读取到堆区数组
char *readFileToBuff_Heap(char *FilePath);

//从点表解析,修改初始化状态标志位置
int modifyCfgVar(int *flag);

// 从本地解析,读取 上报策略
int modifyUpLoadPolicy(_upLoadPolicy *flag);

// 创建或者打开共享内存,并用点表初始化
int initShareMemoy();

// 发给Qt Mqtt消息（缺点，指定了这单个的客户端）
int sendMqtt_Qt(char *msgh);

#endif