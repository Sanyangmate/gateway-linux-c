#include "main.h"

//是否为 从来没被Qt下发过配置点表
//1代表以前 被下发过 配置，0代表以前 没有被下发过 配置
int flag_first_config = 0;

//被Qt初d始化过后,现在在线时,有没有被Qt在线修改配置
//1代表现在 被又修改了,0代表现在 还没有被下发控制命令
int cfg_change = 0;

//信号灯集
int semid;

//上报策略,
//不管怎样,都要在JSON解析后,进行修改该策略
//仅重启后运行一次
_upLoadPolicy uploadpolicy; //上报策略结构体

pthread_t tid_udpBroad;
pthread_t tid_tcpServer;
pthread_t tid_mqttListenUpload;
pthread_t tid_getModbus;
pthread_t tid_dealQueueMsg;

int main(int argc, char const *argv[])
{
    //打印提示信息
    printBaseInfo();

    //初始化信号灯集
    if (0 == func_initSems(&semid))
        DEBUG_Warn("semId =0, Need del then Try Again");

    //创建线程，开启Udp通信，
    if (pthread_create(&tid_udpBroad, NULL, task_UdpObject, NULL) == 0)
        DEBUG_Success("task_UdpObject Create!");
    else
    {
        DEBUG_Err("task_UdpObject Create Err!");
        pthread_detach(tid_udpBroad);
        return -1;
    }

    //创建线程，开启多线程Tcp并发模型
    if (pthread_create(&tid_tcpServer, NULL, task_TcpServer, NULL) == 0)
        DEBUG_Success("task_TcpServer Create!");
    else
    {
        DEBUG_Err("task_TcpServer Create Err!");
        pthread_detach(tid_tcpServer);
        return -1;
    }

    //从点表中解析,是否有被网关第一次初始化过,该状态可以被Qt在运行时改变
    //函数返回值是,解析本地json是否成功
    if (func_parseJsonCfg(&flag_first_config) < 0)
    {
        //如果失败就会下面阻塞,一直等待状态标志位被修改
        //flag_first_config修改只来自 本地解析 或者 Qt tcp下发点表
        //都是在重启后更新
        DEBUG_Err("Parse json Cfg failed");
    }
    else
    {
        //解析完成后继续处理
        flag_first_config == 0 ? DEBUG_Warn("need be Sended flag_first_config")
                               : DEBUG_Success("json had been configed");
    }

    //判断是否有被网关第一次初始化过,没则阻塞等待
    while (flag_first_config == 0)
        ;

    //解析本地json文件的上报策略
    //只会从本地解析,因为每次都会重启后再读取,不允许动态更改
    func_upLoadPolicy(&uploadpolicy);

    //共享内存
    //并写入基础json点表信息
    (func_initShareMemory() < 0) ? DEBUG_Err("share Memory Init Err!")
                                 : DEBUG_Success("share Memeory Init ok");

    //Mqtt客户端
    //创建线程，监听,并根据策略,上报数据给Qt
    if (pthread_create(&tid_mqttListenUpload, NULL, task_mqtt_CreateListen_UpLoad, NULL) == 0)
        DEBUG_Success("task_mqtt_CreateListen_UpLoad Create!");
    else
    {
        DEBUG_Err("task_mqtt_CreateListen_UpLoad Create Err!");
        pthread_detach(tid_mqttListenUpload);
        return -1;
    }

    //定时获取modbus 数据
    //创建线程，与slave 通信
    if (pthread_create(&tid_getModbus, NULL, task_getSlaveData_Period, NULL) == 0)
        DEBUG_Success("task_getSlaveData_Period Create!");
    else
    {
        DEBUG_Err("task_getSlaveData_Period Create Err!");
        pthread_detach(tid_getModbus);
        return -1;
    }

    //消息队列
    //放在最后因为,这是服务如上的
    if (pthread_create(&tid_dealQueueMsg, NULL, task_ListenQueueMsg, NULL) == 0)
        DEBUG_Success("task_ListenQueueMsg Create!");
    else
    {
        DEBUG_Err("task_ListenQueueMsg Create Err!");
        pthread_detach(tid_dealQueueMsg);
        return -1;
    }

    for (;;)
    {
        //中途被修改了,决定是否(热)重启
        func_listenCfgChange();
    }
}
