#include <stdio.h>
#include <stdlib.h>
#include "cJSON.h"

int main()
{
    char *json_string = "{ \"id\": \"1318932750\", \"method\": \"thing.service.property.set\", \"params\": { \"FanSwitch\": 1, \"LightSwitch\": 0 }, \"version\": \"1.0.0\" }";

    cJSON *root = cJSON_Parse(json_string);
    if (root == NULL)
    {
        printf("Error before: [%s]\n", cJSON_GetErrorPtr());
        return 1;
    }

    cJSON *params = cJSON_GetObjectItem(root, "params");
    cJSON *fan_switch = cJSON_GetObjectItem(params, "FanSwitch");
    cJSON *light_switch = cJSON_GetObjectItem(params, "LightSwitch");

    printf("FanSwitch: %d\n", fan_switch->valueint);
    printf("LightSwitch: %d\n", light_switch->valueint);

    cJSON_Delete(root);
    return 0;
}
