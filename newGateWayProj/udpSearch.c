#include "udpSearch.h"
#include "main.h"

void udpSearch()
{
    //收到的第几条
    int count_UdpMsg = 1;

    //创建一个socket文件描述符
    int broadfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (broadfd < 0)
    {
        DEBUG_Err("sock err");
        pthread_exit(NULL);
    }

    //接收者的,绑定udp套接字(ip+port)
    struct sockaddr_in udpRecvAddr;
    udpRecvAddr.sin_family = AF_INET;            //ipv4
    udpRecvAddr.sin_port = htons(UDP_RECV_PORT); //端口号
    udpRecvAddr.sin_addr.s_addr = INADDR_ANY;    //允许接收局域网内任意ip的设备
    int addrlen = sizeof(udpRecvAddr);           //网络信息结构体的大小

    //复用ip 和 port
    int opt = 1;
    int ret_1 = setsockopt(broadfd, SOL_SOCKET, SO_REUSEADDR, (void *)&opt, sizeof(opt));
    if (ret_1 == -1)
        printf("<err> Udp setsockopt err!!");

    // 绑定，面向无连接的等待
    if (bind(broadfd, (struct sockaddr *)&udpRecvAddr, sizeof(udpRecvAddr)) < 0)
    {
        DEBUG_Err("<err>udp server err bind");
        pthread_exit(NULL);
    }
    DEBUG_Success("udp Node bind ok");
    DEBUG_Success("udp Node Init All ok!");

    for (;;)
    {
        DEBUG_Success("udp broad waiting Recv...");

        //来自发送者的,存储网络信息
        char recvbuf[UDP_RECV_BUF_LEN] = {0};  //接收消息的数组
        struct sockaddr_in sendaddr;           //发送者的信息结构体
        int sendaddrlen = sizeof(udpRecvAddr); //发送者的网络信息结构体的大小

        //等待接收
        int recvlen = recvfrom(broadfd, recvbuf, sizeof(recvbuf),
                               0, (struct sockaddr *)&sendaddr, &sendaddrlen);

        printf("[>recv new udp msg]\n%s\n", recvbuf);

        //看看是不是自己的Qt客户端
        int ret = judge_isMyQt(recvbuf);
        if (ret != 0)
        {
            DEBUG_Warn("not my Qt clinet");
            continue;
        }
        else
        {
            //确认了这个Qt客户端
            DEBUG_Success("is my OGC");

            //生成要发送给Qt的UdpMsg
            cJSON *root;
            char *p = createUdpSendJsonStr(&root);
            int lenMsg = strlen(p);
            printf("\t<Ok>:[udp Json msg len:%d]\n", lenMsg);

            //发送出去
            sendto(broadfd, p, lenMsg, 0,
                   (struct sockaddr *)&sendaddr, sendaddrlen);
            DEBUG_Success("udp msg sended!");

            //释放内存
            cJSON_Delete(root);
            free(p);
        }

        printf("\t<Ok>:[udp Msg :%d]\n", count_UdpMsg++);
    }
}

static int judge_isMyQt(char *recvbuf)
{
    //json的反序列化,把字符串转换成json树
    cJSON *root = cJSON_Parse(recvbuf);
    if (NULL == root)
    {
        DEBUG_Warn("parse err");
        return -1;
    }

    //解析askMeg节点,从树干上获取askMeg这个树杈
    cJSON *item = cJSON_GetObjectItem(root, "askMeg");
    if (NULL == item) //判断是否获取成功
    {
        DEBUG_Warn("object item get err");
        return -1;
    }

    //解析并释放树
    int ret = strncmp(item->valuestring, UDP_CMP_STR, strlen(UDP_CMP_STR));
    cJSON_Delete(root);

    //判断结果
    if (ret == 0)
    {
        //是自己的Qt
        return 0;
    }
    else
    {
        DEBUG_Warn("unkwen err");
        return -1;
    }
}

static char *createUdpSendJsonStr(cJSON **root)
{
    //序列化,去构造字符串
    /*回复消息,告诉收到了,并回复相关信息,消息有::*/
    *root = cJSON_CreateObject();
    if (NULL == (*root))
    {
        DEBUG_Err("create err");
        return NULL;
    }

    //获取本机ip
    char ip[16];
    getLocalIp(ip);

    //两种结点
    cJSON *item;
    cJSON *array;

    //who是谁 gataway
    item = cJSON_CreateString("gataway");      //创建子树杈,存放数据
    cJSON_AddItemToObject(*root, "who", item); //给子树杈起名,再放到树干root上

    //给Qt的回应 respond
    item = cJSON_CreateString("yyds");             //创建子树杈,存放数据
    cJSON_AddItemToObject(*root, "respond", item); //给子树杈起名,再放到树干root上

    //tcpServer的ip port
    array = cJSON_CreateArray();
    cJSON_AddItemToArray(array, cJSON_CreateString(ip));
    cJSON_AddItemToArray(array, cJSON_CreateNumber(TcpServerPort));
    cJSON_AddItemToObject(*root, "tcpServer", array);

    //mqttServer的ip port
    array = cJSON_CreateArray();
    cJSON_AddItemToArray(array, cJSON_CreateString(ip));
    cJSON_AddItemToArray(array, cJSON_CreateNumber(MQTT_DEFAULT_PORT)); //默认就是
    cJSON_AddItemToObject(*root, "mqttServer", array);

    //UdpNode的ip port
    array = cJSON_CreateArray();
    cJSON_AddItemToArray(array, cJSON_CreateString(ip));
    cJSON_AddItemToArray(array, cJSON_CreateNumber(UDP_RECV_PORT));
    cJSON_AddItemToObject(*root, "udpNode", array);

    char *p = cJSON_Print(*root);
    if (p == NULL)
    {
        DEBUG_Err("cJson create parse err!!");
        return NULL;
    }
    else
    {
        DEBUG_Success("cJson Print Str success!!");
    }

    return p;
}