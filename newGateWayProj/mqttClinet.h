#ifndef _MQTTCLINET_H_
#define _MQTTCLINET_H_

#include "MQTTClient.h"
#include "helpFunc.h"
#include "dataType.h"
#include "msg_queue_peer.h"
#include <time.h>
#include <sys/time.h> // 包含setitimer()函数
#include <stdlib.h>
#include <signal.h> //包含signal()函数
#include "cJSON.h"

#define Str_Qt "Qt"
#define Str_ctl "ctl"
#define Str_updata "Str_updata"
#define Str_threshold "Str_threshold"
#define Str_Policy "Str_Policy"

#define MQTT_DEFAULT_PORT 1883 //mqtt服务器 默认端口

#define MyMqttClinetId "gateWayClientPub"            //客户端ID
#define MySubscribeTopic "clinet_downCmd_ToGateway"  //订阅的主题
#define MyPublishTopic "gateway_uploadData_ToClinet" //上报的主题

//存放Qt下发的命令
typedef struct
{
    char cmd[32];
    char who[32];
    int key;
    int state;
    float status;
} cmd_Qt;

//初始化并连接MQTT服务器
int dealMqttTask();

// 和broker断开链接
void connectLost(void *context, char *cause);

//每次发送完,都会有个计数,基本没用带着,不用管
void delivered(void *context, MQTTClient_deliveryToken dt);

// 和broker断开链接
void connectLost(void *context, char *cause);

//每次发送完,都会有个计数,基本没用带着,不用管
void delivered(void *context, MQTTClient_deliveryToken dt);

// 接收消息的回调函数
int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message);

//解析来自Qt的Mqtt的JSON命令
int parseQtCmdMqttMsg(cmd_Qt *cmdStruct, char *payload);

//生成Qt想发给消息队列的结构体
int combineQueueMsgStr_Qt(queueMsg_t *queueMsg, cmd_Qt *cmdStruct);

/* 周期上报 */
void upload_period();              //入口函数
void set_timer();                  //设置定时,linux默认每个进程只有1个
void uninit_time();                //取消定时,断开连接后调用
void signal_handler(int m);        //信号处理函数
char *createJsonStrUploadPeriod(); //生成周期上报的json

/* 主动上报 
*  其实就是调用 周期上报的函数*/
void activityUpload();

/* 变化上报 */
void changeUpload();

//更新数据
void uploadDataVal(float *temp, float *humi, float *bat, int *led_state,
                   int *lux, int *co2, int *fan_state);

char *createChangeUpload_JsonStr(
    //旧数据
    float *temp_old, float *humi_old, float *bat_old, int *led_state_old,
    int *lux_old, int *co2_old, int *fan_state_old,

    //新数据
    float *temp_new, float *humi_new, float *bat_new, int *led_state_new,
    int *lux_new, int *co2_new, int *fan_state_new);

#endif