#ifndef __GET_DATA_H
#define __GET_DATA_H

/* 1：bool类型 2：int型  3：float型 */

//结构体的通用数据成员
//任何时候都可能用到
typedef union {
    int b_val;   //bool类型存储空间
    int i_val;   //整形值存储空间
    float f_val; //浮点值存储空间
} val_t;

// ----共享内存部分Begin------------------------------------------------------------------------

//stm32每个数据项,
//在共享内存中,用很多这个,来存放stm32单片机的单个数据项
typedef struct
{
    int key;       //唯一key标识
    char name[32]; //唯一命名标识
    int type;      //数据格式
    val_t data;    //共用体数据
} stm32NodeVal_t;

//modbus从机的每个数据项,
//在共享内存中,用很多这个,来存放modbus从机的单个数据项
typedef struct
{
    int key;       //唯一key标识
    char name[32]; //唯一命名标识
    int addr;      //寄存器地址
    int type;      //数据格式
    val_t data;    //共用体数据
} mbSlaveNodeVal_t;

// ----共享内存部分End------------------------------------------------------------------------

// ----消息队列部分Begin------------------------------------------------------------------------

#define Str_Who_Web "Str_Who_Web"
#define Str_Who_Qt "Str_Who_Qt"
#define Str_Who_AliSDK "Str_Who_AliSDK"

#define Str_DeviceName_Stm32 "Str_DeviceName_Stm32"
#define Str_DeviceName_Slave "Str_DeviceName_Slave"

//发给消息队列的标识
#define QueueMsg_Recv "QueueMsg_Recv"

// 共享内存标识
#define Sheme_ID_stm32 "sheme_Id_stm32" // 1.存放stm32数据用的
#define Sheme_ID_slave "sheme_Id_slave" // 2.存放modbus数据用的

//用消息队列,下发控制命令的的结构体格式
typedef struct
{
    long mtype; //固定的,不能小于0,用时可以都设置为1

    char who[32]; //谁下发的,web后台,还是Qt,还是阿里云进程

    char deviceName[32]; //被控执行器的名字,比如网关通用点表里的stm32Device
    int key;             //被控执行器的key标识,与deviceName同个作用

    int cmdType;            //命令格式:1是下发控制,2是要求立刻刷新数据,并写入到共享内存,3 upload policy
    val_t ctlDeviceState_1; //写入状态,用int型的bool状态,1开0关

    val_t ctlDeviceState_2; //依次类推,可根据想控制的执行器数量添加
} queueMsg_t;

// ----消息队列部分End------------------------------------------------------------------------

#endif