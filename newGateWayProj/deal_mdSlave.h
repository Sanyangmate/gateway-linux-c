#ifndef __DEAL_MDSLAVE_H_
#define __DEAL_MDSLAVE_H_

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include "modbus.h"
#include "dataType.h"
#include "helpFunc.h"
#include "main.h"

#define Modbus_Period_Time 1

int init_connectToDestSlave(modbus_t **ctx, const char slaveIp[16],
                            const int slavePort, const int slaveId);

void get_SlaveData(modbus_t *ctx);

int set_SlaveState(modbus_t *ctx, /* 起始地址 */ int coil_addr, /* status */ int status);

void func_Modbus(int n);

#endif