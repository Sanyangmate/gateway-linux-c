#include "tcpServer.h"
#include "sem.h"

#if 1 //esp 部分

//保证存储唯一esp的tcp,暂时不用多个的链表
//esp 0代表还无,1代表已有
int flag_saveSignalEspTcp = 0;

// 专门存储唯一一个的 esp tcp 客户端连接的结构体,就不用链表了
int socketEspOnly; //esp 的 socketfd
struct sockaddr_in espClinetAddr;
socklen_t espClinetAddrlen = sizeof(espClinetAddr);

#endif

extern int flag_first_config;
extern int cfg_change;

void tcpServer()
{
    //建立用于客户端连接的fd
    int sockServerTcpfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockServerTcpfd < 0)
    {
        DEBUG_Err("sockServerTcpfd err");
        pthread_exit(NULL);
    }
    DEBUG_Success("sockServerTcpfd ok!");
    printf("tcp fd:%d\n", sockServerTcpfd);

    // 填充服务器的结构体
    struct sockaddr_in serverAddr;
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(TcpServerPort);
    serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    socklen_t serverAddrlen = sizeof(serverAddr);

    //复用ip 和 port
    int opt = 1;
    int setSockOpt = setsockopt(sockServerTcpfd, SOL_SOCKET, SO_REUSEADDR, (void *)&opt, sizeof(opt));
    if (setSockOpt == -1)
        DEBUG_Err("Tcp setSockOpt err!!");
    else
        DEBUG_Success("Tcp setSockOpt ok");

    // 绑定
    // 如果别的进程也绑定了,那么会绑定失败
    if (bind(sockServerTcpfd, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) < 0)
    {
        DEBUG_Err("sockServerTcpfd bind err");
        pthread_exit(NULL);
    }
    else
    {
        DEBUG_Success("sockServerTcpfd bind ok!");
    }

    // 监听
    if (listen(sockServerTcpfd, 6) < 0)
    {
        DEBUG_Err("sockServerTcpfd listen err");
        pthread_exit(NULL);
    }
    DEBUG_Success("sockServerTcpfd Init ALL ok!");

    // 定义新的用来客户端连接的结构体
    struct sockaddr_in newClinetAddr;
    socklen_t newClinetAddrlen = sizeof(newClinetAddr);

    /* 多线程并发服务器 */
    for (;;)
    {
        DEBUG_Success("wait recving from TCP...");

        //阻塞等待新连接
        int newAcceptfd = accept(sockServerTcpfd, (struct sockaddr *)&newClinetAddr,
                                 &newClinetAddrlen);
        if (newAcceptfd < 0)
        {
            DEBUG_Err("newAcceptfd accept err");
            pthread_exit(NULL);
        }

        //存储唯一esp tcp 的连接
        if (flag_saveSignalEspTcp == 0)
        {
            // 想再次重新保存esp,需要esp断开链接,后会更改
            // 会变成1,就不能在这修改
            // 为0进入,存储Esp的tcp信息
            // 每次都赋值,进入处理线程后,再处理

            socketEspOnly = newAcceptfd;   //fd
            espClinetAddr = newClinetAddr; //结构体
        }

        // 子线程 线程 接收 终端消息 发送给客户端
        pthread_t tid;
        pthread_create(&tid, NULL, handler_tcpTransmit, &newAcceptfd);
        pthread_detach(tid);
    }
}

void *handler_tcpTransmit(void *arg)
{
    DEBUG_Success("new tcp connect!");

    int acceptfd = *((int *)arg);  //sock sfd
    char recvbuf[RECV_BUF_LENGTH]; //recvbuf 循环清0

    for (;;)
    {
        __bzero(recvbuf, sizeof(recvbuf));

        //阻塞,等待接收到数据后
        int recvbyte = recv(acceptfd, recvbuf, sizeof(recvbuf), 0);
        DEBUG_Success(">recv new Tcp Msg");

        //接收错误情况
        if (recvbyte < 0)
        {
            DEBUG_Warn("recv err server,Maybe everything");
        }
        //客户端退出
        else if (recvbyte == 0)
        {
            DEBUG_Warn("client is exit");

            //断开后又允许新的esp唯一保存
            //在正常接收判断后修改为1,代表是唯一essp
            flag_saveSignalEspTcp = 0;

            // 游离态的线程结束自身
            pthread_exit(NULL);
        }
        //接收正常情况
        else
        {
            /* tcp 来源,只有两种 */
            /* 收到的数据,一定是JSON格式 */

            //<0 数据解析失败,数据非JSON格式
            struct parseDataTcp dataTcp; //存放解析的数据
            memset(&dataTcp, 0, sizeof(dataTcp));

            //只是判断是谁,这个结构体的作用
            int ret = parsingJsonTcpData(&dataTcp, recvbuf);
            if (ret < 0)
            {
                if (ret != (-2))
                    DEBUG_Warn("Tcp Data JSON parse ErrFormat");
                continue;
            }
            else
            {
                switch (ret)
                {
                case whoQt:
                {
                    //处理Qt的Tcp数据
                    //要么更新点表,要么保活
                    deal_QtTcp_Data(recvbuf, acceptfd);
                    break;
                }
                case whoSTM32:
                {
                    //唯一esp的标志
                    flag_saveSignalEspTcp = 1;

                    //处理Stm32 Esp32的Tcp数据
                    deal_EspTcp_data(recvbuf);
                    break;
                }
                }
            }
        }
    }
}

int parsingJsonTcpData(struct parseDataTcp *dataTcp, char *recvbuf)
{
    // json解析,json的反序列化,把字符串转换成json树
    cJSON *root = cJSON_Parse(recvbuf); //内容来自这里
    if (NULL == root)
    {
        DEBUG_Warn("tcp recv err format json");
        return -1;
    }

    //判断是谁
    cJSON *item = cJSON_GetObjectItem(root, "who");
    if (NULL == item)
    {
        DEBUG_Warn("Not find the <who> item");
        return -1;
    }
    strcpy(dataTcp->who, item->valuestring);

    //对Qt进行的操作
    if (strstr(dataTcp->who, WHO_QT) != NULL)
    {
        DEBUG_Success("Qt Tcp Msg");
        return whoQt;
    }
    // 找到了Stm32
    else if (strstr(dataTcp->who, WHO_STM32) != NULL)
    {
        DEBUG_Success("Stm Esp Tcp Msg");
        return whoSTM32;
    }
    // 不知道who是谁
    else
    {
        DEBUG_Warn("Json Parse OK, buf Who No <Qt or STM32>");
        return -2;
    }

    cJSON_Delete(root);
}

int deal_QtTcp_Data(char *recvbuf, int acceptfd)
{
    cJSON *item;

    // json解析,json的反序列化,把字符串转换成json树
    cJSON *root = cJSON_Parse(recvbuf); //内容来自这里
    if (NULL == root)
    {
        DEBUG_Warn("tcp recv err format json");
        return -1;
    }

    /* 
        tcpServer 收到Qt,两种情况
        要么保活
        要么修改点表
     */

#if 1 //保活

    //判断 保活 的键名 是否存在,
    //因为这个更频繁
    item = cJSON_GetObjectItem(root, "keepAlive");
    if (item != NULL)
    {
        //再次对保活值进行判断
        if (strstr(item->valuestring, KEEP_ALIVE_TOPIC) != NULL)
        {
            //确定是 保活任务,回复
            DEBUG_Success("Qt KEEP ALIVE MSG");

            send(acceptfd, REPORT_KEEP_ALIVE, strlen(REPORT_KEEP_ALIVE), 0);

            cJSON_Delete(root);
            return 0;
        }
    }

#endif

#if 1 //更新点表

    //上述不存在 保活键名,
    //这里只会是更新或者第一次下发点表
    item = cJSON_GetObjectItem(root, "is_first_initialized");
    if (item == NULL)
    {
        DEBUG_Err("down JSON cfg err!");
        cJSON_Delete(root);
        return -1;
    }
    //进行内容处理
    else
    {
        //如果是 第一次修改
        //修改布尔,解除阻塞,代表已经被第一次初始化过了
        if (item->valueint == 1)
            flag_first_config = 1;

        //写入本地
        int returnVal;
        if (writeToFile(PATH_CONFIG_JSON_FILE, recvbuf, strlen(recvbuf)) < 0)
        {
            //写入失败失败
            DEBUG_Err("write to json err");
            returnVal = -1;
        }
        else
        {
            //写入成功
            DEBUG_Success("Write Json To Host ok");
            returnVal = 0;
        }

        cJSON_Delete(root);

        //修改过的,状态标记
        //不管是第一次,还是后续修改,都会更改这个标志位
        extern int cfg_change;
        cfg_change = 1;

        return returnVal;
    }

#endif
}

int writeToFile(const char *filename, const char *text, int size)
{
    int dest = open(filename, O_WRONLY | O_TRUNC | O_CREAT, 0666);
    if (dest < 0)
    {
        DEBUG_Err("dest open err");
        return -1;
    }

    ssize_t ret = write(dest, text, size);

    close(dest);
    return ret;
}

int deal_EspTcp_data(char *recvbuf)
{
    //调试标志位
    int flag = 0;

    /* 解析 */
    char _who[32] = {0};
    float _dhtTemp;
    float _dhtHumi;
    float _batValue;
    int _ledState;

    cJSON *root = cJSON_Parse(recvbuf);
    if (root == NULL)
    {
        DEBUG_Err("Parse Esp Data err");
        printf("Error before: [%s]\n", cJSON_GetErrorPtr());
        return -1;
    }

    cJSON *batValue = cJSON_GetObjectItem(root, "_stmData_batValue");
    cJSON *dhtHumi = cJSON_GetObjectItem(root, "_stmData_dhtHumi");
    cJSON *dhtTemp = cJSON_GetObjectItem(root, "_stmData_dhtTemp");
    cJSON *ledState = cJSON_GetObjectItem(root, "_stmData_ledState");
    cJSON *who = cJSON_GetObjectItem(root, "who");

    //who
    if (who != NULL && who->type == cJSON_String)
        strcpy(_who, who->valuestring);
    else
        DEBUG_Err("who != NULL && who->type == cJSON_String");

    //电池电压
    if (batValue != NULL && batValue->type == cJSON_Number)
        _batValue = (batValue->valuedouble);
    else
        DEBUG_Err("batValue != NULL && batValue->type == cJSON_Number");

    //湿度
    if (dhtHumi != NULL && dhtHumi->type == cJSON_Number)
        _dhtHumi = dhtHumi->valuedouble;
    else
        DEBUG_Err("dhtHumi != NULL && dhtHumi->type == cJSON_Number");

    //温度
    if (dhtTemp != NULL && dhtTemp->type == cJSON_Number)
        _dhtTemp = dhtTemp->valuedouble;
    else
        DEBUG_Err("dhtTemp != NULL && dhtTemp->type == cJSON_Number");

    //灯开关
    if (ledState != NULL)
        _ledState = ((ledState->type) == cJSON_True);
    else
        DEBUG_Err("ledState != NULL");

    cJSON_Delete(root);

    /* 处理数据 */

    //数据放入共享内存
    extern void *pData_Shm_stm32;
    stm32NodeVal_t *p = (stm32NodeVal_t *)pData_Shm_stm32;

    for (int i = 0; i < NUM_DATA_STM32; i++)
    {
        switch ((p + i)->key)
        {
        //temp
        case 101:
        {
            (p + i)->data.f_val = _dhtTemp;

            if (flag == 1)
                printf(">>ShareMemory: key:%d _temp:%f\n", (p + i)->key, (p + i)->data.f_val);

            break;
        }
        //humi
        case 102:
        {
            (p + i)->data.f_val = _dhtHumi;

            if (flag == 1)
                printf(">>ShareMemory: key:%d _humi:%f\n", (p + i)->key, (p + i)->data.f_val);

            break;
        }
        //bat
        case 103:
        {
            (p + i)->data.f_val = _batValue;

            if (flag == 1)
                printf(">>ShareMemory: key:%d _batValue:%f\n", (p + i)->key, (p + i)->data.f_val);

            break;
        }
        //led
        case 104:
        {
            (p + i)->data.i_val = _ledState;

            if (flag == 1)
                printf(">>ShareMemory: key:%d _ledState:%d\n", (p + i)->key, (p + i)->data.i_val);

            break;
        }
        }
        // printf("---------------%d\n", i);
    }
    DEBUG_Success("esp stm32 data write ok!");

    extern int semid;
    int res = semctl(semid, NUM_STM32, GETVAL);
    if (res == 0)
        V_operation(semid, NUM_STM32);
    else
    {
        //非 0 或者 1 就设置为 1
        if (res > 1 || res < 0)
        {
            union semun sem;
            sem.val = 1;
            semctl(semid, 0, SETVAL, sem); //stm32
        }
    }

    printf("Var Val::\nwho:%s\n1.temp:%f\n2.humi:%f\n3.bat:%f\n4.led:%d\n",
           _who, _dhtTemp, _dhtHumi, _batValue, _ledState);
}
