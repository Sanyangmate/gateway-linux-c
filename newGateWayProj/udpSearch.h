#ifndef _UDPSEARCH_H_
#define _UDPSEARCH_H_

#include <netinet/in.h>
#include <netinet/ip.h> /* superset of previous */
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netdb.h>
#include "mqttClinet.h"
#include "cJSON.h"

//udp主任务
void udpSearch();

//判断是不是自己的Qt,进行Json格式检测
static int judge_isMyQt(char *recvbuf);

//生成udp发给Qt的udp
static char *createUdpSendJsonStr(cJSON **root);

#endif