#ifndef _SEM_H_
#define _SEM_H_

#include <stdio.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include "main.h"
#include "dataType.h"
#include "helpFunc.h"
#include <sys/types.h>
#include <sys/sem.h>

#define NUM_STM32 0
#define NUM_SLAVE 1

union semun {
    int val;
};

int initSem();
int P_operation(int semid, int num);
int V_operation(int semid, /* 哪个灯 */ int num);

#endif