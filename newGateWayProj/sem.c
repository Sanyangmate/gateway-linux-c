#include "sem.h"

//临时目录的路径
#define TMP_PATH "/tmp/ipc/sem/"

//单个字符标识
#define MAGIC_ID 'j'

//想自己弄得文件名字
#define MY_FILE_NAME "mysem"

union semun sem;

//创建或打开共享灯集
int initSem()
{
    //生成路径和文件名字
    char myName[256] = {0};
    sprintf(myName, "%s%s", TMP_PATH, MY_FILE_NAME);
    printf("myName:> %s\n", myName);

    char sys_cmd[128] = {0};
    sprintf(sys_cmd, "%s %s", "touch", myName);
    int ret = system(sys_cmd);
    UNUSED(ret);

    // 1.创建key
    key_t key = ftok(myName, MAGIC_ID); //.目录也是个文件
    if (key < 0)
    {
        DEBUG_Err("ftok err");
        return -1;
    }
    printf("key:%d\n", key);

    // 2.创建 或 打开信号灯集
    int semid = semget(key, 2, IPC_CREAT | IPC_EXCL | 0666);
    if (semid <= 0) //错误情况
    {
        if (errno == EEXIST)
        {
            semid = semget(key, /* 个数 */ 2, 0666);

            goto t1;
        }
        else
        {
            // 如果semidl为0,则不可使用，需要删除再创建
            DEBUG_Err("semget err");
            return -1;
        }
    }

t1:
    //正常情况,初始化
    //最开始都不有资源
    sem.val = 0;
    semctl(semid, 0, SETVAL, sem); //stm32

    sem.val = 0;
    semctl(semid, 1, SETVAL, sem); //modbusslsave

    printf("semid: %d\n", semid);
    printf("now 0: %d\n", semctl(semid, 0, GETVAL));
    printf("now 1: %d\n", semctl(semid, 1, GETVAL));

    return semid;
}

int P_operation(int semid, int num)
{
    // buf.sem_num = 1;
    // buf.sem_op = 1; //释放资源
    // buf.sem_flg = 0;

    // semop(semid, &buf, 1);
    // printf("%d\n", semctl(semid, 1, GETVAL));
}

int V_operation(int semid, /* 哪个灯 */ int num)
{
    struct sembuf buf;

    buf.sem_num = num;
    buf.sem_op = 1; //释放资源
    buf.sem_flg = 0;

    semop(semid, &buf, /* 个数 */ 1);

    int ret = semctl(semid, num, GETVAL);
    printf("P operation ok %d\n", ret);
    return ret;
}