#ifndef SETWIG_H
#define SETWIG_H

#include <QWidget>

namespace Ui {
class SetWig;
}

class SetWig : public QWidget
{
    Q_OBJECT

public:
    explicit SetWig(QWidget *parent = 0);
    ~SetWig();

private:
    Ui::SetWig *ui;
};

#endif // SETWIG_H
