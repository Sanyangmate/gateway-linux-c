#include "m0wig.h"
#include "ui_m0wig.h"

//sudo ./mjpg_streamer -i "./input_uvc.so -r 320x240" -o
//"./output_udp.so -p 8888" -o "./output_http.so -w ./www"

M0Wig::M0Wig(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::M0Wig)
{
    ui->setupUi(this);
    udpsocket = new QUdpSocket(this);
    timer = new QTimer(this);
    timer1 = new QTimer(this);
    connect(timer,SIGNAL(timeout()),
            this,SLOT(timeoutSlot()));
    connect(timer1,SIGNAL(timeout()),
            this,SLOT(time1outSlot()));
    //开启监控按钮
    connect(ui->monitor,SIGNAL(clicked()),
            this,SLOT(monitorSlot()));

    //截图按钮
    connect(ui->picture,SIGNAL(clicked()),
            this,SLOT(screenSlot()));

    //照片保存按钮
    connect(ui->savepicture,SIGNAL(clicked()),
            this,SLOT(savePictureSlot()));

    //读取视频流
    connect(udpsocket,SIGNAL(readyRead()),
            this,SLOT(readReadSlot()));
}
//开启监控定时器调用监控超时槽函数
void M0Wig::monitorSlot()
{
    timer->setInterval(100);
    timer->setSingleShot(false);
    timer->start();
}


//开启拍照定时器调用监控超时槽函数
void M0Wig::screenSlot()
{
    qDebug() << "拍一下照";
    timer1->setInterval(100);
    timer1->setSingleShot(false);
    timer1->start();
}

//启动监控超时槽函数
void M0Wig::timeoutSlot()
{
    // 这是由 timer 触发的，连接udp服务器后，超时频繁发送命令
    QByteArray array = "picture";
    udpsocket->writeDatagram(array,QHostAddress("192.168.31.26"),8888);
}

//拍照
void M0Wig::time1outSlot()
{
    // 这是由 timer1 触发的
    ui->labelpicture->setFixedSize(ui->labelpicture->width(),ui->labelpicture->height());
    ui->labelpicture->setPixmap(QPixmap::fromImage(image));
    qDebug() << "截图了";
    timer1->stop();
}

//保存照片
void M0Wig::savePictureSlot()
{
    QString filter = "所有文件(*.*);;Qt(*.cpp *.h *.ui *.pro)";

    QString path = QFileDialog::getSaveFileName(this,"保存",":/new/prefix1/screen",filter);
    writePath = path;
    if(writePath == "")
    {
        QMessageBox::warning(this,"提示","你没有选择要保存的路径");
        return;
    }

    QPixmap pixmap = *ui->labelpicture->pixmap();
    if (!pixmap.isNull()) { // 检查是否真的有图片显示在label上
        pixmap.save(writePath); // 保存图片到用户选择的路径
        QMessageBox::information(this,"提示","图片已成功保存");
    } else {
        QMessageBox::warning(this,"提示","没有要保存的图片");
    }
}



void M0Wig::readReadSlot()
{
    qDebug() << "收到了";
    QByteArray data;
    while (udpsocket->hasPendingDatagrams()) {
        data.resize(udpsocket->pendingDatagramSize());
        udpsocket->readDatagram(data.data(), data.size());
        // 调整 data 的大小以匹配实际读取的数据
        bool success = image.loadFromData(data);
        if(success){
            ui->labelmonitor->setFixedSize(ui->labelmonitor->width(),ui->labelmonitor->height());
            ui->labelmonitor->setPixmap(QPixmap::fromImage(image));
        }
    }
}



M0Wig::~M0Wig()
{
    if(timer->isActive()) // 如果正在运行，则先关闭
        timer->stop();
    if(timer1->isActive()) // 如果正在运行，则先关闭
        timer1->stop();
    delete timer; //销毁
    delete timer1;
    delete ui;
}
