#-------------------------------------------------
#
# Project created by QtCreator 2022-06-02T13:58:27
#
#-------------------------------------------------

QT       += core gui network multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = gateway_app
TEMPLATE = app


SOURCES += main.cpp\
        homepage.cpp \
    m0wig.cpp \
    modbuswig.cpp \
    mbappwig.cpp \
    setwig.cpp \
    globalval.cpp \
    mqtthandler.cpp \
    mudpwig.cpp

HEADERS  += homepage.h \
    m0wig.h \
    modbuswig.h \
    mbappwig.h \
    setwig.h \
    globalval.h \
    mqtthandler.h \
    mudpwig.h

FORMS    += homepage.ui \
    m0wig.ui \
    modbuswig.ui \
    mbappwig.ui \
    setwig.ui \
    mudpwig.ui

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/lib/ -lQt5Qmqttd
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/lib/ -lQt5Qmqttd

INCLUDEPATH += $$PWD/.
INCLUDEPATH += $$PWD/mqtt
DEPENDPATH += $$PWD/.

RESOURCES += \
    res.qrc
