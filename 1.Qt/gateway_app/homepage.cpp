#include "homepage.h"
#include "ui_homepage.h"
#include "globalval.h"

HomePage::HomePage(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::HomePage)
{
    ui->setupUi(this);

    MqttHandler *mqtt = new MqttHandler;
    GlobalVal::mqtt = mqtt;


    //实例化四个界面
    m0Wig = new M0Wig;
    modbusWig = new ModbusWig;
    mbappWig = new MbappWig;
    setWig = new SetWig;
    mudpWig = new MudpWig;

    //将四个界面依次添加到堆栈窗体
    ui->stackedWidget->addWidget(mudpWig);
    ui->stackedWidget->addWidget(mbappWig);
    ui->stackedWidget->addWidget(m0Wig);
    ui->stackedWidget->addWidget(modbusWig);


    ui->stackedWidget->addWidget(setWig);


    //连接列表界面和堆栈窗体的信号
    QObject::connect(ui->listWidget, SIGNAL(currentRowChanged(int)),
                     ui->stackedWidget, SLOT(setCurrentIndex(int)));
    connect(mbappWig, SIGNAL(goToM0Wig()), this, SLOT(onGoToM0Wig()));


    //mqtt相关
    //哪个界面需要接收Mqtt的消息，就订阅这个信号，然后在自己的界面文件中处理消息
    QObject::connect(mqtt, SIGNAL(dataUpSignal(QByteArray)),
                     mbappWig, SLOT(dataUpSlot(QByteArray)));
}
void HomePage::onGoToM0Wig()
{  // 定义槽函数
    ui->listWidget->setCurrentRow(2);
        //ui->stackedWidget->setCurrentWidget(m0Wig);  // 切换到m0Wig界面
    }

HomePage::~HomePage()
{
    delete ui;
}
