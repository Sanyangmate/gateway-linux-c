#include "mudpwig.h"
#include "ui_mudpwig.h"

MudpWig::MudpWig(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MudpWig)
{
    ui->setupUi(this);

    //创建定时器对象
    timer = new QTimer(this);
    timer->setInterval(5000);
    timer->setSingleShot(true);
    timer1 = new QTimer(this);
    timer1->setInterval(10000);
    timer->setSingleShot(true);

    socket1 = new QTcpSocket(this);
    socket = new QUdpSocket(this);
    ui->connectTcp->setEnabled(false);
    //udp搜索
    connect(ui->search,SIGNAL(clicked()),
            this,SLOT(searchSlot()));
    //udp处理接收到的消息
    connect(socket, SIGNAL(readyRead()),
            this, SLOT(readyReadSlot()));
    //选择连接的tcp
    connect(ui->listWidget,SIGNAL(currentTextChanged(QString)),
            this,SLOT(lineEditShow(QString)));
    //连接tcp
    connect(ui->connectTcp,SIGNAL(clicked()),
            this,SLOT(connectTcpSlot()));
    //发送点表
    connect(ui->sendJson,SIGNAL(clicked()),
            this,SLOT(sendJsonSlot()));


    //发送心跳包按钮槽函数
    connect(ui->sendHeart,SIGNAL(clicked()),
            this,SLOT(sendHeartSlot()));
    connect(socket1,SIGNAL(readyRead()),
            this,SLOT(receiveHeartSlot()));
    connect(timer,SIGNAL(timeout()),
            this,SLOT(timeoutSlot()));
    connect(timer1,SIGNAL(timeout()),
            this,SLOT(timeoutSlot1()));
}

void MudpWig::searchSlot()
{
    //发送udp连接标志位并接受服务器传来的IP和端口号
    QByteArray array = "{\"askMeg\": \"OGC\",\"who\": \"Qt\"}";
    socket->writeDatagram(array,QHostAddress("192.168.31.255"),9211);
}


void MudpWig::readyReadSlot()
{
    QByteArray data;
    //读取服务器传来的消息并保存
    while(socket->hasPendingDatagrams())
    {
        data.resize(socket->pendingDatagramSize());
        socket->readDatagram(data.data(),data.size());
        qDebug() << data;
    }
    QJsonDocument jsonDoc = QJsonDocument::fromJson(data);
    QJsonObject jsonObj = jsonDoc.object();

    QString who = jsonObj["who"].toString();
    QString respond = jsonObj["respond"].toString();
    if(respond == "yyds")
    {
        tcpServer = jsonObj["tcpServer"].toArray();
        mqttServer = jsonObj["mqttServer"].toArray();
        udpNode = jsonObj["udpNode"].toArray();
    }
    GlobalVal::mqtt->setHost(QHostAddress(mqttServer[0].toString()));
    qDebug() << mqttServer[0].toString();
    GlobalVal::mqtt->setPort(mqttServer[1].toInt());
    qDebug() << mqttServer[1].toInt();
    GlobalVal::mqtt->connectToHost();
    ui->listWidget->addItem(tcpServer[0].toString()+":" +QString::number(tcpServer[1].toInt()));
    socket->close();
}

void MudpWig::lineEditShow(QString message)
{
    ui->lineEdit->setText(message);
    ui->connectTcp->setEnabled(true);
}
//连接tcp
void MudpWig::connectTcpSlot()
{
    ui->connectTcp->setText("已连接");
    QString ip;
    int port;
    //拿到用户输入的IP地址和端口号
    QString tcp = ui->lineEdit->text();
    QStringList list = tcp.split(":");
    ip = list[0];
    port = list[1].toInt();
    qDebug() << ip;
    qDebug() << port;
    //连接到mqtt服务器
    socket1->connectToHost(ip,port);

}

//打开点表文件

void MudpWig::sendJsonSlot()
{
    QString readpath = ":/new/prefix1/node.json";
    QFile readFile(readpath);
    readFile.open(QIODevice::ReadOnly);
    QByteArray buffer;
    qint64 totalSize = readFile.size();
    qint64 hasRead = 0;
    int lastPer = 0;
    while(!readFile.atEnd())
    {
        buffer = readFile.read(1024);
        socket1->write(buffer);
    }
}

//发送心跳包函数
void MudpWig::sendHeartSlot()
{
    timer->start();
    timer1->start();
}

void MudpWig::receiveHeartSlot()
{
    timer1->stop();
    QByteArray block = socket1->readAll();
    QJsonDocument jsonDoc = QJsonDocument::fromJson(block);
    QJsonObject jsonObj = jsonDoc.object();
    QString keepAlive = jsonObj["keepAlive"].toString();
    if(keepAlive == "ok")
    {
        sendHeartSlot();
    }

}

void MudpWig::timeoutSlot()
{
    QByteArray block = "{\"keepAlive\":\"yes\",\"who\":\"Qt\"}";
    qDebug() << block;
    socket1->write(block);
    socket1->flush();
}
void MudpWig::timeoutSlot1()
{
    QMessageBox::critical(this,"错误","tcp链接断开了",QMessageBox::Ok);
    return;
}

MudpWig::~MudpWig()
{
    if(socket->isOpen()) //如果数据流处于打开状态
        socket->close();
    if(socket1->isOpen()) //如果数据流处于打开状态
        socket1->close();
    delete ui;
}
