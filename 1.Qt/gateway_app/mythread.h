#ifndef MYTHREAD_H
#define MYTHREAD_H

#include <QObject>
#include <QThread> // 线程类
#include <QTcpSocket>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

class MyThread : public QThread
{
public:
    MyThread(QObject *parent =0);
    void gettcpSocket(QTcpSocket *);

protected:
    void run();

private:
    QTcpSocket *tcpsocket;
private slots:
    void recvHeartSlot(int);
signals:
    void timestop();
    ~MyThread();
};

#endif // MYTHREAD_H
