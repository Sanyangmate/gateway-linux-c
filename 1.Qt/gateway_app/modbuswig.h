#ifndef MODBUSWIG_H
#define MODBUSWIG_H

#include <QWidget>
#include <QPushButton>
#include <QDebug>
#include "globalval.h"
namespace Ui {
class ModbusWig;
}

class ModbusWig : public QWidget
{
    Q_OBJECT

public:
    explicit ModbusWig(QWidget *parent = 0);
    ~ModbusWig();

private slots:
    void dataUpSlot(QByteArray);
    void ctrlUpSlot();
private:
    Ui::ModbusWig *ui;
    QPushButton *Modbussend;

};

#endif // MODBUSWIG_H
