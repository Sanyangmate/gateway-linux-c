#ifndef MUDPWIG_H
#define MUDPWIG_H

#include <QWidget>
#include <QUdpSocket>
#include <QPushButton>
#include <QCoreApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QDebug>
#include <QTcpSocket> // 连接类
#include <QTextStream>
#include <QFile>
#include "globalval.h"
#include <QTimer>//定时器
#include <QMessageBox>
#include <QThread> // 线程类
#include "mythread.h"

namespace Ui {
class MudpWig;
}

class MudpWig : public QWidget
{
    Q_OBJECT

public:
    explicit MudpWig(QWidget *parent = 0);
    ~MudpWig();

private:
    Ui::MudpWig *ui;
    QUdpSocket *socket;
    QPushButton *search;
    QTcpSocket *socket1; // 客户端对象
    QJsonArray tcpServer;
    QJsonArray mqttServer;
    QJsonArray udpNode;
    QTimer *timer; //定时器对象
    QTimer *timer1;
private slots:
    void searchSlot();
    void readyReadSlot();
    void lineEditShow(QString);
    void connectTcpSlot();
    void sendHeartSlot();
    void timeoutSlot();
    void receiveHeartSlot();
    void timeoutSlot1();
    void sendJsonSlot();
};

#endif // MUDPWIG_H
