#include "mythread.h"

MyThread::MyThread(QObject *parent):QThread(parent)
{

}

void MyThread::gettcpSocket(QTcpSocket *tcpsocket)
{
    this->tcpsocket = tcpsocket;
    connect(tcpsocket,SIGNAL(readyRead()),
            this,SLOT(recvHeartSlot(int)));
}

void MyThread::recvHeartSlot(int count)
{
    QByteArray block = tcpsocket->readAll();
    QJsonDocument jsonDoc = QJsonDocument::fromJson(block);
    QJsonObject jsonObj = jsonDoc.object();
    QString keepAlive = jsonObj["keepAlive"].toString();
    if(keepAlive == "ok")
    {
        count = 1;
    }
}

MyThread::~MyThread()
{

}

