#ifndef HOMEPAGE_H
#define HOMEPAGE_H

#include <QtWidgets>
#include "m0wig.h"
#include "modbuswig.h"
#include "mbappwig.h"
#include "setwig.h"
#include "mudpwig.h"

namespace Ui {
class HomePage;
}

class HomePage : public QWidget
{
    Q_OBJECT

public:
    explicit HomePage(QWidget *parent = 0);
    ~HomePage();

private:
    Ui::HomePage *ui;
    M0Wig *m0Wig;
    ModbusWig *modbusWig;
    MbappWig *mbappWig;
    SetWig *setWig;
    MudpWig *mudpWig;
private slots:
    void onGoToM0Wig();
};

#endif // HOMEPAGE_H
