#include "mbappwig.h"
#include "ui_mbappwig.h"

MbappWig::MbappWig(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MbappWig)
{
    ui->setupUi(this);
    nodeInit();
    group = new QButtonGroup(this);
    group->addButton(ui->Buttonled,1);
    group->addButton(ui->Buttonfun,2);
    group->addButton(ui->Buttoneye,3);
    group->addButton(ui->Buttontemp,4);
    group->addButton(ui->Buttonhumi,5);
    group->addButton(ui->Buttontime,6);
    group->addButton(ui->Buttonclock,7);
    group->addButton(ui->Buttonfalsh,8);
    connect(group,SIGNAL(buttonClicked(int)),
            this,SLOT(groupButtonSlot(int)));
    // 刷新时间
    timeoutSlot();
    //创建定时器对象
    timer = new QTimer(this);
    //设置定时器参数（时间、一次性）
    timer->setInterval(1000);
    timer->setSingleShot(false);
    //连接信号槽函数
    connect(timer,SIGNAL(timeout()),
            this,SLOT(timeoutSlot()));
    ui->falshdata->setEnabled(false);
    connect(ui->falshdata,SIGNAL(clicked()),
            this,SLOT(flashDataSlot()));
    //启动定时器
    timer->start();
    //刷新AA
    timeoutSlot();
}

//初始化设备key值并存到容器中
void MbappWig::nodeInit()
{
    ledObject.key=104;
    batObject.key=103;
    humiObject.key=102;
    tempObject.key=101;
    luxObject.key=201;
    coObject.key=202;
    fanObject.key=203;
    tempMaxObject.key=105;
    humiMaxObject.key=106;
    nodeKeyList << ledObject << batObject << humiObject << tempObject
                << luxObject << coObject << fanObject << tempMaxObject << humiObject;
}



//接受MQTT发送的消息并进行处理
void MbappWig::dataUpSlot(QByteArray bytes)
{
    QString json = bytes;
    QJsonParseError jsonerror;
    QJsonDocument doc = QJsonDocument::fromJson(json.toUtf8(),&jsonerror);
    QJsonObject jsonObject = doc.object();
    QString who = jsonObject["who"].toString();
    qDebug() << who;
    QJsonArray data = jsonObject["data"].toArray();
    int nSize = data.size();
    for(int i = 0 ; i <nSize; ++i)
    {
        QJsonObject value = data.at(i).toObject();
        int key0 = value["key"].toInt();
        int type0 = value["type"].toInt();
        if(type0 == 1)
        {
            status0 = value["status"].toBool();
            recvShow(key0,status0.toBool());
            qDebug() << key0 << type0 << status0;
        }
        else if(type0 == 2)
        {
            status0 = value["status"].toInt();
            recvShow(key0,status0);
        }
        else if(type0 == 3)
        {
            status0 = value["status"].toDouble();
            recvShow(key0,status0.toDouble());
        }
    }
}

void MbappWig::recvShow(int key, QVariant status)
{
    if(key == ledObject.key)
    {
        if(status == true)
        {
            QPixmap icon(":/new/prefix1/picture/ledopen.png");
            ui->Buttonled->setIcon(icon);
            ledObject.val._datab=true;
        }else
        {
            QPixmap icon(":/new/prefix1/picture/openclose.png");
            ui->Buttonled->setIcon(icon);
            ledObject.val._datab=false;
        }
    }
    else if(key == batObject.key)
    {
        QString bat;
        double value = status.toDouble(); // 将 QVariant 转换为 double
        bat.append(QString::number(value, 'g', 2)).append("v"); // 将 double 转换为 QString
        ui->labelbat->setText(bat);
    }
    else if(key == humiObject.key)
    {
        QString humi = "湿度";
        float value = status.toFloat(); // 将 QVariant 转换为 double
        humi.append(":").append(QString::number(value, 'g', 2));
        ui->labelhumi->setText(humi);
    }
    else if(key == tempObject.key)
    {
        QString temp = "温度";
        float value = status.toFloat(); // 将 QVariant 转换为 double
        temp.append(":").append(QString::number(value, 'g', 2)); // 将 double 转换为 QString
        ui->labeltemp->setText(temp);
    }
    else if(key == luxObject.key)
    {
        QString lux = "光强";
        lux.append(":").append(QString::number(status.toInt()));
        ui->labellux_2->setText(lux);
    }
    else if(key == coObject.key)
    {
        QString co = "二氧化碳";
        co.append(":").append(QString::number(status.toInt()));
        ui->labelco->setText(co);
    }
    else if(key == fanObject.key)
    {
        if(status == true)
        {
            QPixmap icon(":/new/prefix1/picture/fanopen.png");
            ui->Buttonfun->setIcon(icon);
            fanObject.val._datab=true;
        }else
        {
            QPixmap icon(":/new/prefix1/picture/fanclose.png");
            ui->Buttonfun->setIcon(icon);
            fanObject.val._datab=false;
        }
    }
    else if(key == tempMaxObject.key)
    {
        QString tpmax = "温度阈值";
        float value = status.toFloat(); // 将 QVariant 转换为 double
        tempMaxObject.val._dataf = value;
        tpmax.append(QString::number(value, 'g', 2)); // 将 double 转换为 QString
        ui->tempmax->setText(tpmax);
    }
    else if(key == humiMaxObject.key)
    {
        QString hmmax = "湿度阈值";
        float value = status.toFloat();
        humiMaxObject.val._dataf = value;
        hmmax.append(QString::number(value, 'g', 2)); // 将 double 转换为 QString
        ui->humimax->setText(hmmax);
    }
}

void MbappWig::lcdStatus()
{
    if(ledObject.val._datab == true)
    {
        QPixmap icon(":/new/prefix1/picture/openclose.png");
        ui->Buttonled->setIcon(icon);
        ledObject.val._datab=false;
    }else
    {
        QPixmap icon(":/new/prefix1/picture/ledopen.png");
        ui->Buttonled->setIcon(icon);
        ledObject.val._datab=true;
    }
    reportLed();
}

void MbappWig::fanStatus()
{
    if(fanObject.val._datab == true)
    {
        QPixmap icon(":/new/prefix1/picture/fanclose.png");
        ui->Buttonfun->setIcon(icon);
        fanObject.val._datab=false;
    }else
    {
        QPixmap icon(":/new/prefix1/picture/fanopen.png");
        ui->Buttonfun->setIcon(icon);
        fanObject.val._datab=true;
    }
    reportFan();
}

void MbappWig::monitorStatus()
{
    emit goToM0Wig();
}

////触发按钮组槽函数
void MbappWig::groupButtonSlot(int id)
{
    if(id == 1)
    {
        lcdStatus();
    }
    else if(id == 2)
    {
        fanStatus();
    }
    else if(id == 3)
    {
        monitorStatus();
    }
    else if(id == 4)
    {
        setTempMax();
    }
    else if(id == 5)
    {
        setHumiMax();
    }
    else if(id == 6 || id==7 || id==8)
    {
        setReportPart(id);
    }
}

//修改上报方式
void MbappWig::setReportPart(int id)
{

    //实时发送
    if(id == 6)
    {
        ui->falshdata->setEnabled(false);
        QJsonObject jsonObject;
        QJsonObject reportCfg;
        reportCfg["period"] = 5;
        reportCfg["type"] = 1;
        jsonObject["reportCfg"] = reportCfg;
        jsonObject["who"] = "Qt";
        jsonObject["cmd"] = "Str_Policy";
        // 将QJsonObject转换为QJsonDocument
        QJsonDocument doc(jsonObject);
        QByteArray byteArray = doc.toJson(QJsonDocument::Compact);
        QMQTT::Message pushTest;
        pushTest.setTopic("clinet_downCmd_ToGateway");
        pushTest.setPayload(byteArray);
        GlobalVal::mqtt->publish(pushTest);
    }
    //
    else if(id == 7)
    {
        ui->falshdata->setEnabled(false);
        QJsonObject jsonObject;
        QJsonObject reportCfg;
        reportCfg["period"] = 5;
        reportCfg["type"] = 2;
        jsonObject["reportCfg"] = reportCfg;
        jsonObject["who"] = "Qt";
        jsonObject["cmd"] = "Str_Policy";
        // 将QJsonObject转换为QJsonDocument
        QJsonDocument doc(jsonObject);
        QByteArray byteArray = doc.toJson(QJsonDocument::Compact);
        QMQTT::Message pushTest;
        pushTest.setTopic("clinet_downCmd_ToGateway");
        pushTest.setPayload(byteArray);
        GlobalVal::mqtt->publish(pushTest);
    }
    else if(id == 8)
    {
        ui->falshdata->setEnabled(true);
        QJsonObject jsonObject;
        QJsonObject reportCfg;
        reportCfg["period"] = 5;
        reportCfg["type"] = 3;
        jsonObject["reportCfg"] = reportCfg;
        jsonObject["who"] = "Qt";
        jsonObject["cmd"] = "Str_Policy";
        // 将QJsonObject转换为QJsonDocument
        QJsonDocument doc(jsonObject);
        QByteArray byteArray = doc.toJson(QJsonDocument::Compact);
        QMQTT::Message pushTest;
        pushTest.setTopic("clinet_downCmd_ToGateway");
        pushTest.setPayload(byteArray);
        GlobalVal::mqtt->publish(pushTest);
    }

}

//刷新数据
void MbappWig::flashDataSlot()
{
    QJsonObject jsonObject;

    // 设置"cmd"键的值
    jsonObject["cmd"] = "Str_updata";

    // 设置"who"键的值，这里不再需要嵌套对象
    jsonObject["who"] = "Qt";
    // 创建JSON文档并设置其内容为JSON对象
    QJsonDocument jsonDoc(jsonObject);
    QByteArray byteArray = jsonDoc.toJson(QJsonDocument::Compact);
    QMQTT::Message pushTest;
    pushTest.setTopic("clinet_downCmd_ToGateway");
    pushTest.setPayload(byteArray);
    GlobalVal::mqtt->publish(pushTest);
}

//led状态上报函数
void MbappWig::reportLed()
{
    QJsonObject jsonObject;
    jsonObject["cmd"] = "ctl";
    jsonObject["who"] = "Qt";

    // 创建一个 QJsonArray 并添加键值对
    QJsonObject cmdListObject;
    cmdListObject["key"] = 101;
    cmdListObject["key"] = 101;
    cmdListObject["status"] = ledObject.val._datab;
    jsonObject["cmdList"] = cmdListObject;

    // 将 QJsonObject 转换为 QJsonDocument
    QJsonDocument jsonDoc(jsonObject);

    QByteArray byteArray = jsonDoc.toJson(QJsonDocument::Compact);
    QMQTT::Message pushTest;
    pushTest.setTopic("clinet_downCmd_ToGateway");
    pushTest.setPayload(byteArray);
    GlobalVal::mqtt->publish(pushTest);
}

void MbappWig::reportFan()
{
    QJsonObject jsonObject;
    jsonObject["cmd"] = "ctl";
    jsonObject["who"] = "Qt";

    // 创建一个 QJsonArray 并添加键值对
    QJsonObject cmdListObject;
    cmdListObject["key"] = 203;
    cmdListObject["status"] = fanObject.val._datab;
    jsonObject["cmdList"] = cmdListObject;

    // 将 QJsonObject 转换为 QJsonDocument
    QJsonDocument jsonDoc(jsonObject);

    QByteArray byteArray = jsonDoc.toJson(QJsonDocument::Compact);
    QMQTT::Message pushTest;
    pushTest.setTopic("clinet_downCmd_ToGateway");
    pushTest.setPayload(byteArray);
    GlobalVal::mqtt->publish(pushTest);
}

//设置阈值
void MbappWig::setTempMax()
{
    QString text = ui->textEdittemp->toPlainText();
    float textmax = text.toFloat();
    tempMaxObject.val._dataf = textmax;
    QJsonObject jsonObject;
    jsonObject["cmd"] = "Str_threshold";
    jsonObject["who"] = "Qt";

    // 创建一个 QJsonArray 并添加键值对
    QJsonObject cmdListObject;
    cmdListObject["key"] = 105;
    cmdListObject["status"] = tempMaxObject.val._dataf;
    jsonObject["cmdList"] = cmdListObject;

    // 将 QJsonObject 转换为 QJsonDocument
    QJsonDocument jsonDoc(jsonObject);

    QByteArray byteArray = jsonDoc.toJson(QJsonDocument::Compact);
    QMQTT::Message pushTest;
    pushTest.setTopic("clinet_downCmd_ToGateway");
    pushTest.setPayload(byteArray);
    GlobalVal::mqtt->publish(pushTest);
    QString tpmax = "温度阈值";
    tpmax.append(text); // 将 double 转换为 QString
    ui->tempmax->setText(tpmax);
}

void MbappWig::setHumiMax()
{
    QString text = ui->textEdithumi->toPlainText();
    float humimax = text.toFloat();
    humiMaxObject.val._dataf = humimax;
    QJsonObject jsonObject;
    jsonObject["cmd"] = "Str_threshold";
    jsonObject["who"] = "Qt";

    // 创建一个 QJsonArray 并添加键值对
    QJsonObject cmdListObject;
    cmdListObject["key"] = 106;
    cmdListObject["status"] = humiMaxObject.val._dataf;
    jsonObject["cmdList"] = cmdListObject;

    // 将 QJsonObject 转换为 QJsonDocument
    QJsonDocument jsonDoc(jsonObject);

    QByteArray byteArray = jsonDoc.toJson(QJsonDocument::Compact);
    QMQTT::Message pushTest;
    pushTest.setTopic("clinet_downCmd_ToGateway");
    pushTest.setPayload(byteArray);
    GlobalVal::mqtt->publish(pushTest);
    QString humax = "温度阈值";
    humax.append(text); // 将 double 转换为 QString
    ui->humimax->setText(humax);
}
void MbappWig::timeoutSlot()
{
    //获得当前时间
    QString time = QDateTime::currentDateTime().toString("hh:mm:ss");
    //设置显示
    ui->lcdNumber->display(time);
}
MbappWig::~MbappWig()
{
    if(timer->isActive()) // 如果正在运行，则先关闭
        timer->stop();
    delete timer; //销毁
    delete ui;
}
