#ifndef KEYTYPEVALNAME_H
#define KEYTYPEVALNAME_H


#include <QString> // 注意头文件

class KeyTypeValName
{
public:
    KeyTypeValName(int ,int ,QString);
    ~KeyTypeValName();
private:
    int key;
    int type;
    QString valName;
};

#endif // KEYTYPEVALNAME_H
