#ifndef MBAPPWIG_H
#define MBAPPWIG_H

#include <QWidget>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>
#include <QJsonParseError>
#include <QJsonValue>
#include <QJsonDocument>
#include <QJsonObject>
#include <QList>
#include <QButtonGroup>
#include <QVariant>
#include <QMessageBox>
#include <QTimer> // 定时器
#include <QDateTime>
#include "globalval.h"
#include "keytypevalname.h"
#include "mybtn.h"


union val_t
{
    bool _datab;
    int _datai;
    float _dataf;
};


struct node_data
{
    int key;  //唯一键值
    int type;  //数据点类型
    val_t val;
};


namespace Ui {
class MbappWig;
}

class MbappWig : public QWidget
{
    Q_OBJECT

public:
    explicit MbappWig(QWidget *parent = 0);
    struct node_data ledObject;
    struct node_data tempObject;
    struct node_data humiObject;
    struct node_data batObject;
    struct node_data luxObject;
    struct node_data coObject;
    struct node_data fanObject;
    struct node_data tempMaxObject;
    struct node_data humiMaxObject;
    void nodeInit();
    void lcdStatus();
    void fanStatus();
    void monitorStatus();
    void reportLed();
    void reportFan();
    void setTempMax();
    void setHumiMax();
    void setReportPart(int);
    void recvShow(int,QVariant);
    ~MbappWig();

private:
    Ui::MbappWig *ui;
    QString jsonString;
    QButtonGroup *group;
    QVariant status0;
    QList<node_data> nodeKeyList;
    QTimer *timer; // 定时器对象
signals:
    void goToM0Wig();
private slots:
    void dataUpSlot(QByteArray);
    void groupButtonSlot(int);
    void timeoutSlot(); // 与timeout信号连接的槽函数
    void flashDataSlot();
};

#endif // MBAPPWIG_H
