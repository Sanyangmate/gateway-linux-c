#ifndef M0WIG_H
#define M0WIG_H

#include <QWidget>
#include <QTimer> // 定时器
#include <QDateTime>
#include <QUdpSocket>
#include <QPixmap>
#include <QImage>
#include <QMessageBox>
#include <QFileDialog>
#include <QDataStream>
#include <QImageReader>
#include <QtMultimedia/QMediaRecorder>
#include <QtMultimedia/QVideoFrame>


namespace Ui {
class M0Wig;
}

class M0Wig : public QWidget
{
    Q_OBJECT

public:
    explicit M0Wig(QWidget *parent = 0);
    ~M0Wig();

private:
    Ui::M0Wig *ui;
    QTimer *timer; // 定时器对象
    QTimer *timer1;
    QUdpSocket *udpsocket;
    QString writePath;
    QString readPath;
    QImage image;

private slots:
    void timeoutSlot(); // 与timeout信号连接的槽函数
    void readReadSlot();
    void monitorSlot();
    void time1outSlot();
    void screenSlot();
//    void recordSlot();
    void savePictureSlot();
//    void recordSaveSlot();
//private:
//    int countFilesInDirectory(const QString&);
};

#endif // M0WIG_H
