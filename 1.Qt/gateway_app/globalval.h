#ifndef GLOBALVAL_H
#define GLOBALVAL_H

#include <QObject>
#include "mqtthandler.h"
#include <QMap>
/**
 * @brief 此类只处理全局变量
 */
class GlobalVal
{
public:
    GlobalVal();
    static MqttHandler *mqtt;
    //QList<DeviceData> deviceDataList;

};

#endif // GLOBALVAL_H
