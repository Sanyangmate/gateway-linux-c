#ifndef MQTTHANDLER_H
#define MQTTHANDLER_H

#include <QObject>
#include "mqtt/qmqtt.h"

class MqttHandler : public QObject
{
    Q_OBJECT
public:
    explicit MqttHandler(QObject *parent = 0);
    void connectToHost();
    void setHost(const QHostAddress& host){client->setHost(host);}
    void setPort(const quint16 port){client->setPort(port);}

signals:
    void dataUpSignal(const QByteArray &);
    void ctrlUpSignal(const QByteArray &);

public slots:
    void doConnected();
    void doSubscribed(const QString& topic, const quint8 qos);
    void doDataReceived(QMQTT::Message message);
    void detectState();
    quint16 publish(const QMQTT::Message& message);

private:
    QMQTT::Client *client;
};

#endif // MQTTHANDLER_H
