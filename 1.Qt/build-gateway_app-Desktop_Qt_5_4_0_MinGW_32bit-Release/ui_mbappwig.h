/********************************************************************************
** Form generated from reading UI file 'mbappwig.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MBAPPWIG_H
#define UI_MBAPPWIG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MbappWig
{
public:
    QVBoxLayout *verticalLayout_12;
    QVBoxLayout *verticalLayout_4;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout;
    QLCDNumber *lcdNumber;
    QSpacerItem *horizontalSpacer;
    QLabel *labelbat;
    QLabel *label_2;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QLabel *label_3;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_4;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *Buttontime;
    QPushButton *Buttonclock;
    QPushButton *Buttonfalsh;
    QPushButton *falshdata;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_5;
    QPushButton *Buttonled;
    QLabel *label_5;
    QVBoxLayout *verticalLayout_6;
    QPushButton *Buttonfun;
    QLabel *label_6;
    QVBoxLayout *verticalLayout_7;
    QPushButton *Buttoneye;
    QLabel *label_7;
    QHBoxLayout *horizontalLayout_4;
    QVBoxLayout *verticalLayout_8;
    QPushButton *pushButton;
    QLabel *labeltemp;
    QLabel *tempmax;
    QTextEdit *textEdittemp;
    QPushButton *Buttontemp;
    QVBoxLayout *verticalLayout_9;
    QPushButton *pushButton_2;
    QLabel *labelhumi;
    QLabel *humimax;
    QTextEdit *textEdithumi;
    QPushButton *Buttonhumi;
    QVBoxLayout *verticalLayout_10;
    QPushButton *pushButton_3;
    QLabel *labelco;
    QVBoxLayout *verticalLayout_11;
    QPushButton *pushButton_4;
    QLabel *labellux_2;

    void setupUi(QWidget *MbappWig)
    {
        if (MbappWig->objectName().isEmpty())
            MbappWig->setObjectName(QStringLiteral("MbappWig"));
        MbappWig->resize(974, 575);
        QFont font;
        font.setFamily(QString::fromUtf8("\351\273\221\344\275\223"));
        MbappWig->setFont(font);
        verticalLayout_12 = new QVBoxLayout(MbappWig);
        verticalLayout_12->setObjectName(QStringLiteral("verticalLayout_12"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        lcdNumber = new QLCDNumber(MbappWig);
        lcdNumber->setObjectName(QStringLiteral("lcdNumber"));
        lcdNumber->setDigitCount(8);

        horizontalLayout->addWidget(lcdNumber);

        horizontalSpacer = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        labelbat = new QLabel(MbappWig);
        labelbat->setObjectName(QStringLiteral("labelbat"));
        labelbat->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        horizontalLayout->addWidget(labelbat);

        label_2 = new QLabel(MbappWig);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setMaximumSize(QSize(30, 30));
        label_2->setStyleSheet(QStringLiteral(""));
        label_2->setPixmap(QPixmap(QString::fromUtf8(":/new/prefix1/picture/bat.png")));
        label_2->setScaledContents(true);

        horizontalLayout->addWidget(label_2);

        horizontalLayout->setStretch(0, 8);
        horizontalLayout->setStretch(1, 2);
        horizontalLayout->setStretch(2, 1);
        horizontalLayout->setStretch(3, 1);

        verticalLayout_2->addLayout(horizontalLayout);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label = new QLabel(MbappWig);
        label->setObjectName(QStringLiteral("label"));
        QFont font1;
        font1.setFamily(QStringLiteral("Arial"));
        font1.setPointSize(22);
        font1.setBold(true);
        font1.setItalic(true);
        font1.setWeight(75);
        label->setFont(font1);
        label->setTextFormat(Qt::AutoText);
        label->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        verticalLayout->addWidget(label);

        label_3 = new QLabel(MbappWig);
        label_3->setObjectName(QStringLiteral("label_3"));
        QFont font2;
        font2.setFamily(QStringLiteral("Arial"));
        font2.setPointSize(16);
        font2.setBold(true);
        font2.setWeight(75);
        label_3->setFont(font2);

        verticalLayout->addWidget(label_3);


        verticalLayout_2->addLayout(verticalLayout);

        verticalLayout_2->setStretch(0, 1);
        verticalLayout_2->setStretch(1, 6);

        verticalLayout_4->addLayout(verticalLayout_2);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        label_4 = new QLabel(MbappWig);
        label_4->setObjectName(QStringLiteral("label_4"));

        verticalLayout_3->addWidget(label_4);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(10);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        Buttontime = new QPushButton(MbappWig);
        Buttontime->setObjectName(QStringLiteral("Buttontime"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(Buttontime->sizePolicy().hasHeightForWidth());
        Buttontime->setSizePolicy(sizePolicy);
        QFont font3;
        font3.setFamily(QString::fromUtf8("\347\255\211\347\272\277 Light"));
        font3.setPointSize(11);
        font3.setBold(false);
        font3.setWeight(50);
        Buttontime->setFont(font3);
        Buttontime->setStyleSheet(QStringLiteral(""));
        QIcon icon;
        icon.addFile(QStringLiteral(":/new/prefix1/picture/time.png"), QSize(), QIcon::Normal, QIcon::Off);
        Buttontime->setIcon(icon);
        Buttontime->setIconSize(QSize(30, 30));

        horizontalLayout_2->addWidget(Buttontime);

        Buttonclock = new QPushButton(MbappWig);
        Buttonclock->setObjectName(QStringLiteral("Buttonclock"));
        sizePolicy.setHeightForWidth(Buttonclock->sizePolicy().hasHeightForWidth());
        Buttonclock->setSizePolicy(sizePolicy);
        QFont font4;
        font4.setFamily(QString::fromUtf8("\347\255\211\347\272\277 Light"));
        font4.setPointSize(11);
        Buttonclock->setFont(font4);
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/new/prefix1/picture/time1.png"), QSize(), QIcon::Normal, QIcon::Off);
        Buttonclock->setIcon(icon1);

        horizontalLayout_2->addWidget(Buttonclock);

        Buttonfalsh = new QPushButton(MbappWig);
        Buttonfalsh->setObjectName(QStringLiteral("Buttonfalsh"));
        sizePolicy.setHeightForWidth(Buttonfalsh->sizePolicy().hasHeightForWidth());
        Buttonfalsh->setSizePolicy(sizePolicy);
        Buttonfalsh->setFont(font4);
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/new/prefix1/picture/falsh.png"), QSize(), QIcon::Normal, QIcon::Off);
        Buttonfalsh->setIcon(icon2);

        horizontalLayout_2->addWidget(Buttonfalsh);

        falshdata = new QPushButton(MbappWig);
        falshdata->setObjectName(QStringLiteral("falshdata"));

        horizontalLayout_2->addWidget(falshdata);


        verticalLayout_3->addLayout(horizontalLayout_2);


        verticalLayout_4->addLayout(verticalLayout_3);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        Buttonled = new QPushButton(MbappWig);
        Buttonled->setObjectName(QStringLiteral("Buttonled"));
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/new/prefix1/picture/openclose.png"), QSize(), QIcon::Normal, QIcon::Off);
        Buttonled->setIcon(icon3);
        Buttonled->setIconSize(QSize(50, 50));
        Buttonled->setFlat(true);

        verticalLayout_5->addWidget(Buttonled);

        label_5 = new QLabel(MbappWig);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setAlignment(Qt::AlignCenter);

        verticalLayout_5->addWidget(label_5);


        horizontalLayout_3->addLayout(verticalLayout_5);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        Buttonfun = new QPushButton(MbappWig);
        Buttonfun->setObjectName(QStringLiteral("Buttonfun"));
        Buttonfun->setLayoutDirection(Qt::LeftToRight);
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/new/prefix1/picture/fanclose.png"), QSize(), QIcon::Normal, QIcon::Off);
        Buttonfun->setIcon(icon4);
        Buttonfun->setIconSize(QSize(50, 50));
        Buttonfun->setFlat(true);

        verticalLayout_6->addWidget(Buttonfun);

        label_6 = new QLabel(MbappWig);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setAlignment(Qt::AlignCenter);

        verticalLayout_6->addWidget(label_6);


        horizontalLayout_3->addLayout(verticalLayout_6);

        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        Buttoneye = new QPushButton(MbappWig);
        Buttoneye->setObjectName(QStringLiteral("Buttoneye"));
        QIcon icon5;
        icon5.addFile(QStringLiteral(":/new/prefix1/picture/shipinclose.png"), QSize(), QIcon::Normal, QIcon::Off);
        Buttoneye->setIcon(icon5);
        Buttoneye->setIconSize(QSize(50, 50));
        Buttoneye->setFlat(true);

        verticalLayout_7->addWidget(Buttoneye);

        label_7 = new QLabel(MbappWig);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setAlignment(Qt::AlignCenter);

        verticalLayout_7->addWidget(label_7);


        horizontalLayout_3->addLayout(verticalLayout_7);


        verticalLayout_4->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setObjectName(QStringLiteral("verticalLayout_8"));
        pushButton = new QPushButton(MbappWig);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(pushButton->sizePolicy().hasHeightForWidth());
        pushButton->setSizePolicy(sizePolicy1);
        QIcon icon6;
        icon6.addFile(QStringLiteral(":/new/prefix1/picture/temp.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton->setIcon(icon6);
        pushButton->setIconSize(QSize(80, 80));
        pushButton->setFlat(true);

        verticalLayout_8->addWidget(pushButton);

        labeltemp = new QLabel(MbappWig);
        labeltemp->setObjectName(QStringLiteral("labeltemp"));
        labeltemp->setAlignment(Qt::AlignCenter);

        verticalLayout_8->addWidget(labeltemp);

        tempmax = new QLabel(MbappWig);
        tempmax->setObjectName(QStringLiteral("tempmax"));

        verticalLayout_8->addWidget(tempmax);

        textEdittemp = new QTextEdit(MbappWig);
        textEdittemp->setObjectName(QStringLiteral("textEdittemp"));
        QSizePolicy sizePolicy2(QSizePolicy::MinimumExpanding, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(textEdittemp->sizePolicy().hasHeightForWidth());
        textEdittemp->setSizePolicy(sizePolicy2);
        textEdittemp->setMaximumSize(QSize(16777215, 30));

        verticalLayout_8->addWidget(textEdittemp);

        Buttontemp = new QPushButton(MbappWig);
        Buttontemp->setObjectName(QStringLiteral("Buttontemp"));

        verticalLayout_8->addWidget(Buttontemp);


        horizontalLayout_4->addLayout(verticalLayout_8);

        verticalLayout_9 = new QVBoxLayout();
        verticalLayout_9->setObjectName(QStringLiteral("verticalLayout_9"));
        pushButton_2 = new QPushButton(MbappWig);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        QIcon icon7;
        icon7.addFile(QStringLiteral(":/new/prefix1/picture/humi.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_2->setIcon(icon7);
        pushButton_2->setIconSize(QSize(80, 80));
        pushButton_2->setFlat(true);

        verticalLayout_9->addWidget(pushButton_2);

        labelhumi = new QLabel(MbappWig);
        labelhumi->setObjectName(QStringLiteral("labelhumi"));
        labelhumi->setAlignment(Qt::AlignCenter);

        verticalLayout_9->addWidget(labelhumi);

        humimax = new QLabel(MbappWig);
        humimax->setObjectName(QStringLiteral("humimax"));

        verticalLayout_9->addWidget(humimax);

        textEdithumi = new QTextEdit(MbappWig);
        textEdithumi->setObjectName(QStringLiteral("textEdithumi"));
        QSizePolicy sizePolicy3(QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(textEdithumi->sizePolicy().hasHeightForWidth());
        textEdithumi->setSizePolicy(sizePolicy3);
        textEdithumi->setMaximumSize(QSize(16777215, 30));

        verticalLayout_9->addWidget(textEdithumi);

        Buttonhumi = new QPushButton(MbappWig);
        Buttonhumi->setObjectName(QStringLiteral("Buttonhumi"));

        verticalLayout_9->addWidget(Buttonhumi);


        horizontalLayout_4->addLayout(verticalLayout_9);

        verticalLayout_10 = new QVBoxLayout();
        verticalLayout_10->setObjectName(QStringLiteral("verticalLayout_10"));
        verticalLayout_10->setContentsMargins(20, -1, -1, -1);
        pushButton_3 = new QPushButton(MbappWig);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        QIcon icon8;
        icon8.addFile(QStringLiteral(":/new/prefix1/picture/co2.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_3->setIcon(icon8);
        pushButton_3->setIconSize(QSize(80, 80));
        pushButton_3->setFlat(true);

        verticalLayout_10->addWidget(pushButton_3);

        labelco = new QLabel(MbappWig);
        labelco->setObjectName(QStringLiteral("labelco"));
        labelco->setAlignment(Qt::AlignCenter);

        verticalLayout_10->addWidget(labelco);

        verticalLayout_10->setStretch(1, 1);

        horizontalLayout_4->addLayout(verticalLayout_10);

        verticalLayout_11 = new QVBoxLayout();
        verticalLayout_11->setObjectName(QStringLiteral("verticalLayout_11"));
        pushButton_4 = new QPushButton(MbappWig);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));
        QIcon icon9;
        icon9.addFile(QStringLiteral(":/new/prefix1/picture/sun.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_4->setIcon(icon9);
        pushButton_4->setIconSize(QSize(80, 80));
        pushButton_4->setFlat(true);

        verticalLayout_11->addWidget(pushButton_4);

        labellux_2 = new QLabel(MbappWig);
        labellux_2->setObjectName(QStringLiteral("labellux_2"));
        labellux_2->setAlignment(Qt::AlignCenter);

        verticalLayout_11->addWidget(labellux_2);


        horizontalLayout_4->addLayout(verticalLayout_11);


        verticalLayout_4->addLayout(horizontalLayout_4);


        verticalLayout_12->addLayout(verticalLayout_4);


        retranslateUi(MbappWig);

        QMetaObject::connectSlotsByName(MbappWig);
    } // setupUi

    void retranslateUi(QWidget *MbappWig)
    {
        MbappWig->setWindowTitle(QApplication::translate("MbappWig", "Form", 0));
        labelbat->setText(QString());
        label_2->setText(QString());
        label->setText(QApplication::translate("MbappWig", "\347\273\204\345\220\215", 0));
        label_3->setText(QApplication::translate("MbappWig", "\346\210\220\345\221\230\345\220\215", 0));
        label_4->setText(QApplication::translate("MbappWig", "\344\270\212\346\212\245\346\226\271\345\274\217", 0));
        Buttontime->setText(QApplication::translate("MbappWig", "\345\256\236\346\227\266", 0));
        Buttonclock->setText(QApplication::translate("MbappWig", "\345\256\232\346\227\266", 0));
        Buttonfalsh->setText(QApplication::translate("MbappWig", "\345\210\267\346\226\260", 0));
        falshdata->setText(QApplication::translate("MbappWig", "\345\210\267\346\226\260\346\225\260\346\215\256", 0));
        Buttonled->setText(QString());
        label_5->setText(QApplication::translate("MbappWig", "LED\347\201\257", 0));
        Buttonfun->setText(QString());
        label_6->setText(QApplication::translate("MbappWig", "\351\243\216\346\211\207", 0));
        Buttoneye->setText(QString());
        label_7->setText(QApplication::translate("MbappWig", "\347\233\221\346\216\247", 0));
        pushButton->setText(QString());
        labeltemp->setText(QApplication::translate("MbappWig", "\346\270\251\345\272\246", 0));
        tempmax->setText(QApplication::translate("MbappWig", "\346\270\251\345\272\246\351\230\210\345\200\274", 0));
        textEdittemp->setHtml(QApplication::translate("MbappWig", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'\351\273\221\344\275\223'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">30</p></body></html>", 0));
        Buttontemp->setText(QApplication::translate("MbappWig", "\346\270\251\345\272\246\351\230\210\345\200\274\344\277\256\346\224\271", 0));
        pushButton_2->setText(QString());
        labelhumi->setText(QApplication::translate("MbappWig", "\346\271\277\345\272\246", 0));
        humimax->setText(QApplication::translate("MbappWig", "\346\271\277\345\272\246\351\230\210\345\200\274", 0));
        textEdithumi->setHtml(QApplication::translate("MbappWig", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'\351\273\221\344\275\223'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">30</p></body></html>", 0));
        Buttonhumi->setText(QApplication::translate("MbappWig", "\346\271\277\345\272\246\351\230\210\345\200\274\344\277\256\346\224\271", 0));
        pushButton_3->setText(QString());
        labelco->setText(QApplication::translate("MbappWig", "\344\272\214\346\260\247\345\214\226\347\242\263", 0));
        pushButton_4->setText(QString());
        labellux_2->setText(QApplication::translate("MbappWig", "\344\272\256\345\272\246", 0));
    } // retranslateUi

};

namespace Ui {
    class MbappWig: public Ui_MbappWig {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MBAPPWIG_H
