/********************************************************************************
** Form generated from reading UI file 'mudpwig.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MUDPWIG_H
#define UI_MUDPWIG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MudpWig
{
public:
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QPushButton *search;
    QHBoxLayout *horizontalLayout_2;
    QListWidget *listWidget;
    QLabel *label2;
    QLineEdit *lineEdit;
    QPushButton *connectTcp;
    QPushButton *sendJson;
    QPushButton *sendHeart;

    void setupUi(QWidget *MudpWig)
    {
        if (MudpWig->objectName().isEmpty())
            MudpWig->setObjectName(QStringLiteral("MudpWig"));
        MudpWig->resize(444, 399);
        horizontalLayout_3 = new QHBoxLayout(MudpWig);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label = new QLabel(MudpWig);
        label->setObjectName(QStringLiteral("label"));
        label->setMaximumSize(QSize(25, 25));
        label->setPixmap(QPixmap(QString::fromUtf8(":/new/prefix1/picture/search.png")));
        label->setScaledContents(true);

        horizontalLayout->addWidget(label);

        search = new QPushButton(MudpWig);
        search->setObjectName(QStringLiteral("search"));

        horizontalLayout->addWidget(search);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        listWidget = new QListWidget(MudpWig);
        listWidget->setObjectName(QStringLiteral("listWidget"));

        horizontalLayout_2->addWidget(listWidget);

        label2 = new QLabel(MudpWig);
        label2->setObjectName(QStringLiteral("label2"));
        label2->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        label2->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        horizontalLayout_2->addWidget(label2);

        horizontalLayout_2->setStretch(0, 1);
        horizontalLayout_2->setStretch(1, 1);

        verticalLayout->addLayout(horizontalLayout_2);

        lineEdit = new QLineEdit(MudpWig);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        verticalLayout->addWidget(lineEdit);

        connectTcp = new QPushButton(MudpWig);
        connectTcp->setObjectName(QStringLiteral("connectTcp"));

        verticalLayout->addWidget(connectTcp);

        sendJson = new QPushButton(MudpWig);
        sendJson->setObjectName(QStringLiteral("sendJson"));

        verticalLayout->addWidget(sendJson);

        sendHeart = new QPushButton(MudpWig);
        sendHeart->setObjectName(QStringLiteral("sendHeart"));

        verticalLayout->addWidget(sendHeart);


        horizontalLayout_3->addLayout(verticalLayout);


        retranslateUi(MudpWig);

        QMetaObject::connectSlotsByName(MudpWig);
    } // setupUi

    void retranslateUi(QWidget *MudpWig)
    {
        MudpWig->setWindowTitle(QApplication::translate("MudpWig", "Form", 0));
        label->setText(QString());
        search->setText(QApplication::translate("MudpWig", "\346\220\234\347\264\242", 0));
        label2->setText(QString());
        lineEdit->setPlaceholderText(QApplication::translate("MudpWig", "\344\275\240\351\200\211\346\213\251\347\232\204TCP", 0));
        connectTcp->setText(QApplication::translate("MudpWig", "\350\277\236\346\216\245TCP", 0));
        sendJson->setText(QApplication::translate("MudpWig", "\345\217\221\351\200\201\347\202\271\350\241\250", 0));
        sendHeart->setText(QApplication::translate("MudpWig", "\345\217\221\351\200\201\345\277\203\350\267\263\345\214\205", 0));
    } // retranslateUi

};

namespace Ui {
    class MudpWig: public Ui_MudpWig {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MUDPWIG_H
