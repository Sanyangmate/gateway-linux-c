/********************************************************************************
** Form generated from reading UI file 'modbuswig.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MODBUSWIG_H
#define UI_MODBUSWIG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ModbusWig
{
public:

    void setupUi(QWidget *ModbusWig)
    {
        if (ModbusWig->objectName().isEmpty())
            ModbusWig->setObjectName(QStringLiteral("ModbusWig"));
        ModbusWig->resize(602, 464);

        retranslateUi(ModbusWig);

        QMetaObject::connectSlotsByName(ModbusWig);
    } // setupUi

    void retranslateUi(QWidget *ModbusWig)
    {
        ModbusWig->setWindowTitle(QApplication::translate("ModbusWig", "Form", 0));
    } // retranslateUi

};

namespace Ui {
    class ModbusWig: public Ui_ModbusWig {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MODBUSWIG_H
