/********************************************************************************
** Form generated from reading UI file 'm0wig.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_M0WIG_H
#define UI_M0WIG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_M0Wig
{
public:
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_4;
    QVBoxLayout *verticalLayout;
    QTextEdit *recording;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_3;
    QLabel *labelpicture;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *picture;
    QPushButton *savepicture;
    QVBoxLayout *verticalLayout_2;
    QLabel *labelmonitor;
    QPushButton *monitor;

    void setupUi(QWidget *M0Wig)
    {
        if (M0Wig->objectName().isEmpty())
            M0Wig->setObjectName(QStringLiteral("M0Wig"));
        M0Wig->resize(573, 403);
        verticalLayout_5 = new QVBoxLayout(M0Wig);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        recording = new QTextEdit(M0Wig);
        recording->setObjectName(QStringLiteral("recording"));

        verticalLayout->addWidget(recording);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));

        verticalLayout->addLayout(horizontalLayout);


        verticalLayout_4->addLayout(verticalLayout);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        labelpicture = new QLabel(M0Wig);
        labelpicture->setObjectName(QStringLiteral("labelpicture"));
        labelpicture->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        verticalLayout_3->addWidget(labelpicture);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        picture = new QPushButton(M0Wig);
        picture->setObjectName(QStringLiteral("picture"));

        horizontalLayout_2->addWidget(picture);

        savepicture = new QPushButton(M0Wig);
        savepicture->setObjectName(QStringLiteral("savepicture"));

        horizontalLayout_2->addWidget(savepicture);


        verticalLayout_3->addLayout(horizontalLayout_2);

        verticalLayout_3->setStretch(0, 2);
        verticalLayout_3->setStretch(1, 1);

        verticalLayout_4->addLayout(verticalLayout_3);


        horizontalLayout_3->addLayout(verticalLayout_4);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        labelmonitor = new QLabel(M0Wig);
        labelmonitor->setObjectName(QStringLiteral("labelmonitor"));
        labelmonitor->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        labelmonitor->setTextFormat(Qt::AutoText);
        labelmonitor->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        verticalLayout_2->addWidget(labelmonitor);

        monitor = new QPushButton(M0Wig);
        monitor->setObjectName(QStringLiteral("monitor"));

        verticalLayout_2->addWidget(monitor);


        horizontalLayout_3->addLayout(verticalLayout_2);

        horizontalLayout_3->setStretch(0, 1);
        horizontalLayout_3->setStretch(1, 1);

        verticalLayout_5->addLayout(horizontalLayout_3);


        retranslateUi(M0Wig);

        QMetaObject::connectSlotsByName(M0Wig);
    } // setupUi

    void retranslateUi(QWidget *M0Wig)
    {
        M0Wig->setWindowTitle(QApplication::translate("M0Wig", "Form", 0));
        recording->setHtml(QApplication::translate("M0Wig", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'SimSun'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Helvetica Neue,Helvetica,Arial,sans-serif'; font-size:14px; color:#333333; background-color:#ffffff;\">\346\210\221\344\270\200\347\233\264\347\234\213\347\235\200\344\275\240 </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Helvetica Neue,Helvetica,Arial,sans-serif'; font-size:14px; color:#333333; background-color:#ffffff;\">\345\275\223\344\275\240\345\234\250\345\257\202\351\235\231\347\232\204\346\267\261\345\244\234\347"
                        "\213\254\350\207\252\350\241\214\350\265\260</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Helvetica Neue,Helvetica,Arial,sans-serif'; font-size:14px; color:#333333; background-color:#ffffff;\">\346\204\237\350\247\211\345\210\260\350\203\214\345\220\216\345\271\275\345\271\275\347\232\204\347\233\256\345\205\211\347\233\264\346\265\201\345\206\267\346\261\227</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Helvetica Neue,Helvetica,Arial,sans-serif'; font-size:14px; color:#333333; background-color:#ffffff;\">\350\275\254\345\244\264\345\215\264\347\251\272\347\251\272\350\215\241\350\215\241\346\227\266</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Helvetica Neue,H"
                        "elvetica,Arial,sans-serif'; font-size:14px; color:#333333; background-color:#ffffff;\">\351\202\243\346\230\257\346\210\221\345\234\250\347\234\213\347\235\200\344\275\240</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Helvetica Neue,Helvetica,Arial,sans-serif'; font-size:14px; color:#333333; background-color:#ffffff;\">\346\210\221\344\274\232\344\270\200\347\233\264\347\234\213\347\235\200\344\275\240</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Helvetica Neue,Helvetica,Arial,sans-serif'; font-size:14px; color:#333333; background-color:#ffffff;\">\346\210\221\344\270\215\344\274\232\345\271\262\344\273\200\344\271\210 </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-fami"
                        "ly:'Helvetica Neue,Helvetica,Arial,sans-serif'; font-size:14px; color:#333333; background-color:#ffffff;\">\346\210\221\345\217\252\346\230\257\345\226\234\346\254\242\347\234\213\347\235\200\344\275\240\350\200\214\345\267\262</span></p></body></html>", 0));
        labelpicture->setText(QApplication::translate("M0Wig", "TextLabel", 0));
        picture->setText(QApplication::translate("M0Wig", "\346\213\215\347\205\247", 0));
        savepicture->setText(QApplication::translate("M0Wig", "\344\277\235\345\255\230", 0));
        labelmonitor->setText(QApplication::translate("M0Wig", "TextLabel", 0));
        monitor->setText(QApplication::translate("M0Wig", "\345\274\200\345\220\257\347\233\221\346\216\247", 0));
    } // retranslateUi

};

namespace Ui {
    class M0Wig: public Ui_M0Wig {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_M0WIG_H
