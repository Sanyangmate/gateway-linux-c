/****************************************************************************
** Meta object code from reading C++ file 'mbappwig.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "C:/Users/12137/Desktop/gateway_app/mbappwig.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mbappwig.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MbappWig_t {
    QByteArrayData data[7];
    char stringdata[73];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MbappWig_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MbappWig_t qt_meta_stringdata_MbappWig = {
    {
QT_MOC_LITERAL(0, 0, 8), // "MbappWig"
QT_MOC_LITERAL(1, 9, 9), // "goToM0Wig"
QT_MOC_LITERAL(2, 19, 0), // ""
QT_MOC_LITERAL(3, 20, 10), // "dataUpSlot"
QT_MOC_LITERAL(4, 31, 15), // "groupButtonSlot"
QT_MOC_LITERAL(5, 47, 11), // "timeoutSlot"
QT_MOC_LITERAL(6, 59, 13) // "flashDataSlot"

    },
    "MbappWig\0goToM0Wig\0\0dataUpSlot\0"
    "groupButtonSlot\0timeoutSlot\0flashDataSlot"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MbappWig[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   39,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    1,   40,    2, 0x08 /* Private */,
       4,    1,   43,    2, 0x08 /* Private */,
       5,    0,   46,    2, 0x08 /* Private */,
       6,    0,   47,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::QByteArray,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MbappWig::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MbappWig *_t = static_cast<MbappWig *>(_o);
        switch (_id) {
        case 0: _t->goToM0Wig(); break;
        case 1: _t->dataUpSlot((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 2: _t->groupButtonSlot((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->timeoutSlot(); break;
        case 4: _t->flashDataSlot(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (MbappWig::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MbappWig::goToM0Wig)) {
                *result = 0;
            }
        }
    }
}

const QMetaObject MbappWig::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_MbappWig.data,
      qt_meta_data_MbappWig,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MbappWig::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MbappWig::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MbappWig.stringdata))
        return static_cast<void*>(const_cast< MbappWig*>(this));
    return QWidget::qt_metacast(_clname);
}

int MbappWig::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void MbappWig::goToM0Wig()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
