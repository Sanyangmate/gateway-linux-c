/****************************************************************************
** Meta object code from reading C++ file 'mudpwig.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "C:/Users/12137/Desktop/gateway_app/mudpwig.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mudpwig.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MudpWig_t {
    QByteArrayData data[11];
    char stringdata[131];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MudpWig_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MudpWig_t qt_meta_stringdata_MudpWig = {
    {
QT_MOC_LITERAL(0, 0, 7), // "MudpWig"
QT_MOC_LITERAL(1, 8, 10), // "searchSlot"
QT_MOC_LITERAL(2, 19, 0), // ""
QT_MOC_LITERAL(3, 20, 13), // "readyReadSlot"
QT_MOC_LITERAL(4, 34, 12), // "lineEditShow"
QT_MOC_LITERAL(5, 47, 14), // "connectTcpSlot"
QT_MOC_LITERAL(6, 62, 13), // "sendHeartSlot"
QT_MOC_LITERAL(7, 76, 11), // "timeoutSlot"
QT_MOC_LITERAL(8, 88, 16), // "receiveHeartSlot"
QT_MOC_LITERAL(9, 105, 12), // "timeoutSlot1"
QT_MOC_LITERAL(10, 118, 12) // "sendJsonSlot"

    },
    "MudpWig\0searchSlot\0\0readyReadSlot\0"
    "lineEditShow\0connectTcpSlot\0sendHeartSlot\0"
    "timeoutSlot\0receiveHeartSlot\0timeoutSlot1\0"
    "sendJsonSlot"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MudpWig[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   59,    2, 0x08 /* Private */,
       3,    0,   60,    2, 0x08 /* Private */,
       4,    1,   61,    2, 0x08 /* Private */,
       5,    0,   64,    2, 0x08 /* Private */,
       6,    0,   65,    2, 0x08 /* Private */,
       7,    0,   66,    2, 0x08 /* Private */,
       8,    0,   67,    2, 0x08 /* Private */,
       9,    0,   68,    2, 0x08 /* Private */,
      10,    0,   69,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MudpWig::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MudpWig *_t = static_cast<MudpWig *>(_o);
        switch (_id) {
        case 0: _t->searchSlot(); break;
        case 1: _t->readyReadSlot(); break;
        case 2: _t->lineEditShow((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->connectTcpSlot(); break;
        case 4: _t->sendHeartSlot(); break;
        case 5: _t->timeoutSlot(); break;
        case 6: _t->receiveHeartSlot(); break;
        case 7: _t->timeoutSlot1(); break;
        case 8: _t->sendJsonSlot(); break;
        default: ;
        }
    }
}

const QMetaObject MudpWig::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_MudpWig.data,
      qt_meta_data_MudpWig,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MudpWig::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MudpWig::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MudpWig.stringdata))
        return static_cast<void*>(const_cast< MudpWig*>(this));
    return QWidget::qt_metacast(_clname);
}

int MudpWig::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
