/****************************************************************************
** Meta object code from reading C++ file 'mqtthandler.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "C:/Users/12137/Desktop/gateway_app/mqtthandler.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mqtthandler.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MqttHandler_t {
    QByteArrayData data[13];
    char stringdata[132];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MqttHandler_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MqttHandler_t qt_meta_stringdata_MqttHandler = {
    {
QT_MOC_LITERAL(0, 0, 11), // "MqttHandler"
QT_MOC_LITERAL(1, 12, 12), // "dataUpSignal"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 12), // "ctrlUpSignal"
QT_MOC_LITERAL(4, 39, 11), // "doConnected"
QT_MOC_LITERAL(5, 51, 12), // "doSubscribed"
QT_MOC_LITERAL(6, 64, 5), // "topic"
QT_MOC_LITERAL(7, 70, 3), // "qos"
QT_MOC_LITERAL(8, 74, 14), // "doDataReceived"
QT_MOC_LITERAL(9, 89, 14), // "QMQTT::Message"
QT_MOC_LITERAL(10, 104, 7), // "message"
QT_MOC_LITERAL(11, 112, 11), // "detectState"
QT_MOC_LITERAL(12, 124, 7) // "publish"

    },
    "MqttHandler\0dataUpSignal\0\0ctrlUpSignal\0"
    "doConnected\0doSubscribed\0topic\0qos\0"
    "doDataReceived\0QMQTT::Message\0message\0"
    "detectState\0publish"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MqttHandler[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   49,    2, 0x06 /* Public */,
       3,    1,   52,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    0,   55,    2, 0x0a /* Public */,
       5,    2,   56,    2, 0x0a /* Public */,
       8,    1,   61,    2, 0x0a /* Public */,
      11,    0,   64,    2, 0x0a /* Public */,
      12,    1,   65,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QByteArray,    2,
    QMetaType::Void, QMetaType::QByteArray,    2,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::UChar,    6,    7,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void,
    QMetaType::UShort, 0x80000000 | 9,   10,

       0        // eod
};

void MqttHandler::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MqttHandler *_t = static_cast<MqttHandler *>(_o);
        switch (_id) {
        case 0: _t->dataUpSignal((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 1: _t->ctrlUpSignal((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 2: _t->doConnected(); break;
        case 3: _t->doSubscribed((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const quint8(*)>(_a[2]))); break;
        case 4: _t->doDataReceived((*reinterpret_cast< QMQTT::Message(*)>(_a[1]))); break;
        case 5: _t->detectState(); break;
        case 6: { quint16 _r = _t->publish((*reinterpret_cast< const QMQTT::Message(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< quint16*>(_a[0]) = _r; }  break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 4:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QMQTT::Message >(); break;
            }
            break;
        case 6:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QMQTT::Message >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (MqttHandler::*_t)(const QByteArray & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MqttHandler::dataUpSignal)) {
                *result = 0;
            }
        }
        {
            typedef void (MqttHandler::*_t)(const QByteArray & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MqttHandler::ctrlUpSignal)) {
                *result = 1;
            }
        }
    }
}

const QMetaObject MqttHandler::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_MqttHandler.data,
      qt_meta_data_MqttHandler,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MqttHandler::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MqttHandler::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MqttHandler.stringdata))
        return static_cast<void*>(const_cast< MqttHandler*>(this));
    return QObject::qt_metacast(_clname);
}

int MqttHandler::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void MqttHandler::dataUpSignal(const QByteArray & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MqttHandler::ctrlUpSignal(const QByteArray & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
