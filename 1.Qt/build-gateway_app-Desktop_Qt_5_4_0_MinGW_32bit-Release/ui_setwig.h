/********************************************************************************
** Form generated from reading UI file 'setwig.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SETWIG_H
#define UI_SETWIG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SetWig
{
public:
    QPushButton *pushButton;

    void setupUi(QWidget *SetWig)
    {
        if (SetWig->objectName().isEmpty())
            SetWig->setObjectName(QStringLiteral("SetWig"));
        SetWig->resize(400, 300);
        pushButton = new QPushButton(SetWig);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(150, 220, 75, 23));

        retranslateUi(SetWig);

        QMetaObject::connectSlotsByName(SetWig);
    } // setupUi

    void retranslateUi(QWidget *SetWig)
    {
        SetWig->setWindowTitle(QApplication::translate("SetWig", "Form", 0));
        pushButton->setText(QApplication::translate("SetWig", "\350\256\276\347\275\256\347\225\214\351\235\242", 0));
    } // retranslateUi

};

namespace Ui {
    class SetWig: public Ui_SetWig {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETWIG_H
