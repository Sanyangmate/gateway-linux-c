#include <stdio.h>
#include "aiot_message_api.h"
#include "core_string.h"
#include "core_os.h"

#define USER_DEFINE_PREFIX "/%s/%s/user/%s"
aiot_msg_t* core_origin_msg_init()
{
    aiot_msg_t* message = core_os_malloc(sizeof(aiot_msg_t));
    if(message == NULL) {
        return NULL;
    }
    memset(message, 0, sizeof(aiot_msg_t));
    message->topic = NULL;
    message->payload = NULL;
    message->payload_lenth = 0;
    message->qos = 0;
    message->rrpc_id = NULL;
    message->cid = -1;

    return message;
}

static int32_t core_origin_msg_deinit(aiot_msg_t* message)
{

    if(message->topic != NULL) {
        core_os_free(message->topic);
    }

    if(message->payload != NULL) {
        core_os_free(message->payload);
    }

    if(message->rrpc_id != NULL) {
        core_os_free(message->rrpc_id);
    }

    core_os_free(message);

    return STATE_SUCCESS;
}

aiot_msg_t* aiot_msg_create_raw(const char *topic, const uint8_t *payload, uint32_t payload_lenth)
{
    aiot_msg_t* message;
    if(topic == NULL) {
        return NULL;
    }

    message = core_origin_msg_init();
    if(message == NULL) {
        return NULL;
    }

    /* 拷贝topic */
    if(STATE_SUCCESS != core_strdup(NULL, &message->topic, topic, NULL)) {
        core_origin_msg_deinit(message);
        return NULL;
    }

    /* 拷贝payload */
    if(payload != NULL && payload_lenth != 0) {
        message->payload = core_os_malloc(payload_lenth);
        if(message->payload == NULL) {
            core_origin_msg_deinit(message);
            return NULL;
        }
        memcpy(message->payload, payload, payload_lenth);
        message->payload_lenth = payload_lenth;
    }

    return message;
}

aiot_msg_t* aiot_msg_create_user_define(const char *productKey, const char *deviceName, const char *sub_topic, const uint8_t *payload, uint32_t payload_lenth)
{
    aiot_msg_t* message;
    const char* src[] = {productKey, deviceName, sub_topic};
    if(productKey == NULL || deviceName == NULL ||sub_topic == NULL) {
        return NULL;
    }

    message = core_origin_msg_init();
    if(message == NULL) {
        return NULL;
    }

    if(STATE_SUCCESS != core_sprintf(NULL, &message->topic, USER_DEFINE_PREFIX, (char **)src, sizeof(src) / sizeof(char*), NULL)) {
        core_origin_msg_deinit(message);
        return NULL;
    }

    /* 拷贝payload */
    if(payload != NULL && payload_lenth != 0) {
        message->payload = core_os_malloc(payload_lenth);
        if(message->payload == NULL) {
            core_origin_msg_deinit(message);
            return NULL;
        }
        memcpy(message->payload, payload, payload_lenth);
        message->payload_lenth = payload_lenth;
    }

    return message;
}

aiot_msg_t* aiot_msg_create_rrpc_reply(const aiot_msg_t *rrpc, const uint8_t *payload, uint32_t payload_lenth)
{
    aiot_msg_t* message;
    char *src[2];
    if(rrpc == NULL) {
        return NULL;
    }

    message = core_origin_msg_init();
    if(message == NULL) {
        return NULL;
    }

    /* 拷贝topic */
    src[0] = rrpc->rrpc_id;
    src[1] = rrpc->topic;
    if(STATE_SUCCESS != core_sprintf(NULL, &message->topic, "/ext/rrpc/%s%s", src, 2, NULL)) {
        core_origin_msg_deinit(message);
        return NULL;
    }
    message->cid = rrpc->cid;

    /* 拷贝payload */
    if(payload != NULL && payload_lenth != 0) {
        message->payload = core_os_malloc(payload_lenth);
        if(message->payload == NULL) {
            core_origin_msg_deinit(message);
            return NULL;
        }
        memcpy(message->payload, payload, payload_lenth);
        message->payload_lenth = payload_lenth;
    }

    return message;
}

aiot_msg_t* aiot_msg_clone(const aiot_msg_t* message)
{
    aiot_msg_t *clone = NULL;
    if(message == NULL || message->topic == NULL) {
        return NULL;
    }

    clone = core_origin_msg_init();
    if(clone == NULL) {
        return NULL;
    }
    memset(clone, 0, sizeof(aiot_msg_t));
    clone->qos = message->qos;
    clone->payload_lenth = message->payload_lenth;
    clone->rrpc_id = message->rrpc_id;
    clone->id = message->id;
    if(message->topic != NULL && STATE_SUCCESS != core_strdup(NULL, &clone->topic, message->topic, NULL)) {
        core_origin_msg_deinit(clone);
        return NULL;
    }

    if(message->payload != NULL) {
        clone->payload = core_os_malloc(message->payload_lenth);
        if(clone->payload == NULL) {
            core_origin_msg_deinit(clone);
            return NULL;
        }
        memcpy(clone->payload, message->payload, message->payload_lenth);
        clone->payload_lenth = message->payload_lenth;
    }

    if(message->rrpc_id != NULL && STATE_SUCCESS != core_strdup(NULL, &clone->rrpc_id, message->rrpc_id, NULL)) {
        core_origin_msg_deinit(clone);
        return NULL;
    }

    return clone;
}

int32_t aiot_msg_delete(aiot_msg_t* message)
{
    if(message == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    core_origin_msg_deinit(message);
    return STATE_SUCCESS;
}

int32_t aiot_msg_set_qos(aiot_msg_t* message, uint8_t qos)
{
    if(message == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    message->qos = qos;
    return STATE_SUCCESS;
}