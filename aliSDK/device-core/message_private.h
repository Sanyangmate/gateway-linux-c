

#ifndef _AIOT_MESSAGE_PRIVATE_H_
#define _AIOT_MESSAGE_PRIVATE_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>
#include "aiot_message_api.h"

aiot_msg_t* core_origin_msg_init();


#if defined(__cplusplus)
}
#endif

#endif