
/**
 * @file aiot_linkconfig_api.h
 * @brief 设备连接参数设置api头文件，包含服务器host，多连接、mqtt协议参数等配置项
 * @date 2021-12-16
 *
 * @copyright Copyright (C) 2015-2025 Alibaba Group Holding Limited
 *
 * @details
 */
#ifndef _AIOT_LINK_CONFIG_H_
#define _AIOT_LINK_CONFIG_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>
#include "aiot_message_api.h"
#include "aiot_sysdep_api.h"

typedef enum {
    MQTT_PROTOCOL,
    HTTP_PROTOCOL,
} aiot_protocol_type_t;
struct aiot_linkconfig_t;

/**
 * @brief 描述设备建连所需参数的结构体
 */
typedef struct {
    /* 协议处理的方法实现 */
    aiot_protocol_type_t protocol;
    /* 对应协议的配置 */
    void *proto_config;
    /* 多连接配置, 起始connect_id */
    int32_t start_cid;
    /* 多连接配置，通道的个数 */
    int32_t channel_num;
} aiot_linkconfig_t;

/**
 * @brief 初始化默认的连接配置参数,生成配置参数句柄
 *
 * @return void*
 * @retval 非NULL 设备句柄
 * @retval NULL 初始化失败, 一般是内存分配失败导致
 *
 */
aiot_linkconfig_t *aiot_linkconfig_init(aiot_protocol_type_t type);

/**
 * @brief 删除连接配置句柄，回收资源
 * @param[in] config 配置句柄
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_linkconfig_deinit(aiot_linkconfig_t **config);

/**
 * @brief 设置连接的host, port
 * @param[in] config 配置句柄
 * @param[in] host 服务器地址
 * @param[in] port 服务端口号
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_linkconfig_host(aiot_linkconfig_t *config, const char *host, uint16_t port);

/**
 * @brief 设备网络连接的类型
 * @param[in] config 配置句柄
 * @param[in] option 网络安全类型 <br/>
 *          AIOT_SYSDEP_NETWORK_CRED_NONE        不实用加密 <br/>
 *          AIOT_SYSDEP_NETWORK_CRED_SVRCERT_CA, 使用CA证书加密, 默认值 <br/>
 *          AIOT_SYSDEP_NETWORK_CRED_SVRCERT_PSK,使用PSK方式加密 <br/>
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_linkconfig_cred_option(aiot_linkconfig_t *config, aiot_sysdep_network_cred_option_t option);

/**
 * @brief 设置建连的TLS版本
 * @details
 *
 * @param[in] config 配置句柄
 * @param[in] version tls的版本号
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_linkconfig_tls_version(aiot_linkconfig_t *config, aiot_sysdep_networkt_tls_version_t version);

/**
 * @brief 设置国密连接
 * @details
 *
 * @param[in] config 配置句柄
 * @param[in] gmssl
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_linkconfig_tls_gm(aiot_linkconfig_t *config, uint8_t gm_ssl);

/**
 * @brief 设置服务器的根证书
 * @details
 *      有默认的服务器根证书，只有需要自定义证书认证的设备才需要设置
 * @param[in] config 配置句柄
 * @param[in] cert   证书内容
 * @param[in] len    证书长度
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_linkconfig_root_cert(aiot_linkconfig_t *config, const char *cert, uint32_t len);

/**
 * @brief 设置设备端证书和密钥
 * @details
 *      只有在使用x509认证时才需要设置
 * @param[in] config 配置句柄
 * @param[in] client_cert 证书内容
 * @param[in] cert_len    证书长度
 * @param[in] client_privkey 密钥内容
 * @param[in] privkey_len    密钥长度
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_linkconfig_client_cert_privkey(aiot_linkconfig_t *config, const char *client_cert, uint32_t cert_len, const char *client_privkey, uint32_t privkey_len);

/**
 * @brief 设置建连超时的时间
 * @param[in] config 配置句柄
 * @param[in] timeout_ms 超时时间, 默认值：10000ms
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_linkconfig_connect_timeout(aiot_linkconfig_t *config, uint32_t timeout_ms);

/**
 * @brief 设置接收超时的时间
 * @param[in] config 配置句柄
 * @param[in] timeout_ms 超时时间, 默认值：5000ms
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_linkconfig_recv_timeout(aiot_linkconfig_t *config, uint32_t timeout_ms);

/**
 * @brief 设置发送超时的时间
 * @param[in] config 配置句柄
 * @param[in] timeout_ms 超时时间, 默认值：5000ms
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_linkconfig_send_timeout(aiot_linkconfig_t *config, uint32_t timeout_ms);

/**
 * @brief 设置是否自动重连, 默认为开启自动重连
 * @param[in] config 配置句柄
 * @param[in] enable 【0，关闭自动重连】,关闭自动重连后需要手动触发重连
 *                   【1，打开自动重连】,SDK重连会有逼退算法，重连的间隔时间为2～120s
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_linkconfig_reconnect_enable(aiot_linkconfig_t *config, uint8_t enable);

/**
 * @brief 设置多连接：允许建连多个与云平台的连接
 * @param[in] config 配置句柄
 * @param[in] start_cid  连接的唯一标识符，起始值，一般从填0.
 * @param[in] num 设置多个连接的数量，消息量大的时候避免限流可以多建连几个，取值范围【1～100】
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_linkconfig_multi_channel(aiot_linkconfig_t *config, uint32_t start_cid, uint32_t num);

/**
 * @brief 设备连接阿里云物联网平台时的扩展clientid
 * @param[in] config 配置句柄
 * @param[in] extern_clientid 扩展的clientid, 若需要上报模组商id和模组id以及os信息, 按以下格式填写: "mid=<模组ID>,pid=<模组商ID>,os=<操作系统>"
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_linkconfig_mqtt_extclientid(aiot_linkconfig_t *config, const char *extern_clientid);

/**
 * @brief MQTT建联时, CONNECT报文中的最长心跳间隔
 * @param[in] config 配置句柄
 * @param[in] keep_alive_s 取值范围为30 ~ 1200s
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_linkconfig_mqtt_keepalive(aiot_linkconfig_t *config, uint16_t keep_alive_s);

/**
 * @brief MQTT建联时, CONNECT报文中的clean session参数
 * @details
 * 1. 设备上线时如果clean session为0, 那么上线前服务器推送QoS1的消息会在此时推送给设备
 * 2. 设备上线时如果clean session为1, 那么上线前服务器推送的QoS1的消息会被丢弃
 * @param[in] config 配置句柄
 * @param[in] clean_session 是否清除mqtt会话内容
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 */
int32_t aiot_linkconfig_mqtt_cleansession(aiot_linkconfig_t *config, uint8_t clean_session);

/**
 * @brief 配置设备主动发送PINGREQ报文发送时间间隔. (心跳发送间隔)
 * @details
 *  维持连接，设备会主动定时发送心跳，若连续三次的心跳丢失，设备会主动断开并进行重连
 * @param[in] config 配置句柄
 * @param[in] interval_ms 设备发送心跳的时间间隔，默认值25s
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 */
int32_t aiot_linkconfig_mqtt_heartbeat_interval(aiot_linkconfig_t *config, uint32_t interval_ms);

/**
 * @brief 动态注册参数配置，设置产品密钥及实例id
 * @details
 *  设备动态注册需要产品密钥，企业型实例还需要实例id
 * @param[in] config 配置句柄
 * @param[in] product_secret 产品密钥
 * @param[in] instance_id 企业实例id，如为公共实例，输入NULL即可
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 */
int32_t aiot_linkconfig_dynamic_register(aiot_linkconfig_t *config, const char *product_secret, const char *instance_id);

/**
 * @brief 透传给mqtt的建连认证信息, 免白动态注册后需要使用
 * @details
 *  设备建连一般使用设备认证信息进行建连，不需要调用该接口。
 *
 *  免白动态注册时，会返回mqtt鉴权的信息username、password、clientid。
 *  调用该接口完成参数配置，即可完成设备建连，不需要使用设备认证信息。
 * @param[in] config 配置句柄
 * @param[in] username mqtt协议建连消息中的username
 * @param[in] password mqtt协议建连消息中的password
 * @param[in] clientid mqtt协议建连消息中的clientid
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 */
int32_t aiot_linkconfig_mqtt_auth(aiot_linkconfig_t *config, const char *username, const char *password, const char *clientid);

#if defined(__cplusplus)
}
#endif

#endif