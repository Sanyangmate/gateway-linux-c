/**
 * @file device_bootstrap.c
 * @brief 设备就近接入
 * @date 2021-12-27
 *
 * @copyright Copyright (C) 2015-2025 Alibaba Group Holding Limited
 *
 */
#include <stdio.h>
#include "aiot_state_api.h"
#include "aiot_device_api.h"
#include "device_private.h"
#include "core_string.h"
#include "device_module.h"
#include "core_http.h"
#include "core_log.h"
#include "core_os.h"

#define TAG "BOOTSTRAP"

extern const char *ali_ca_cert;
typedef struct {
    void *http_handle;
    core_http_response_t response;
    uint32_t response_body_len;
    char *host;
    uint16_t port;
    uint32_t send_timeout_ms;
    uint32_t recv_timeout_ms;
    uint32_t timeout_ms;
} bootstrap_t;

#define BOOTSTRAP_MODULE_NAME                      "Bootstrap"

#define BOOTSTRAP_DEFAULT_HOST                     "iot-auth-global.aliyuncs.com"
#define BOOTSTRAP_DEFAULT_PORT                     (443)
#define BOOTSTRAP_DEFAULT_TIMEOUT_MS               (5 * 1000)

#define BOOTSTRAP_PATH                             "/auth/bootstrap"

#define BOOTSTRAP_DEINIT_INTERVAL_MS               (100)
#define BOOTSTRAP_RESPONSE_BODY_LEN                (192)

static bootstrap_t* core_bootstrap_init()
{
    bootstrap_t *bootstrap = core_os_malloc(sizeof(bootstrap_t));
    memset(bootstrap, 0, sizeof(bootstrap_t));
    bootstrap->http_handle = NULL;
    bootstrap->host = BOOTSTRAP_DEFAULT_HOST;
    bootstrap->port = BOOTSTRAP_DEFAULT_PORT;
    bootstrap->send_timeout_ms = BOOTSTRAP_DEFAULT_TIMEOUT_MS;
    bootstrap->recv_timeout_ms = BOOTSTRAP_DEFAULT_TIMEOUT_MS;
    bootstrap->timeout_ms = BOOTSTRAP_DEFAULT_TIMEOUT_MS;
    bootstrap->response_body_len = 1024;

    return bootstrap;
}

static void core_bootstrap_deinit(bootstrap_t* bootstrap)
{
    if(bootstrap->http_handle != NULL) {
        core_http_deinit(&bootstrap->http_handle);
    }

    if(bootstrap->response.content != NULL) {
        core_os_free(bootstrap->response.content);
    }

    core_os_free(bootstrap);
}

static void core_bootstrap_http_recv_callback(void *handle, const aiot_http_recv_t *packet, void *userdata)
{
    bootstrap_t *bootstrap = (bootstrap_t *)userdata;

    switch (packet->type) {
    case AIOT_HTTPRECV_STATUS_CODE: {
        bootstrap->response.code = packet->data.status_code.code;
    }
    break;
    case AIOT_HTTPRECV_HEADER: {
        if ((strlen(packet->data.header.key) == strlen("Content-Length")) &&
                (memcmp(packet->data.header.key, "Content-Length", strlen(packet->data.header.key)) == 0)) {
            core_str2uint(packet->data.header.value, (uint8_t)strlen(packet->data.header.value),
                          &bootstrap->response.content_total_len);
        }
    }
    break;
    case AIOT_HTTPRECV_BODY: {
        uint8_t *content = core_os_malloc(bootstrap->response.content_len +
                                          packet->data.body.len + 1);
        if (content == NULL) {
            return;
        }
        memset(content, 0, bootstrap->response.content_len + packet->data.body.len + 1);
        if (content != NULL) {
            memcpy(content, bootstrap->response.content, bootstrap->response.content_len);
            core_os_free(bootstrap->response.content);
        }
        memcpy(content + bootstrap->response.content_len, packet->data.body.buffer, packet->data.body.len);
        bootstrap->response.content = content;
        bootstrap->response.content_len = bootstrap->response.content_len + packet->data.body.len;
    }
    break;
    default: {

    }
    break;
    }
}

static int32_t core_bootstrap_send_post_request(device_handle_t *device, aiot_protocol_type_t type, bootstrap_t *bootstrap)
{
    int32_t res = STATE_SUCCESS;
    char *content = NULL;
    char *content_fmt = "productKey=%s&deviceName=%s&clientId=%s.%s&resources=%s";
    char *protocol = (type == MQTT_PROTOCOL) ? "mqtt" : "http";
    char *content_src[] = { device->product_key, device->device_name, device->product_key, device->device_name, protocol};
    core_http_request_t request;

    memset(&request, 0, sizeof(core_http_request_t));
    res = core_sprintf(NULL, &content, content_fmt, content_src, sizeof(content_src) / sizeof(char *),
                       NULL);
    if (res < STATE_SUCCESS) {
        return res;
    }
    request.method = "POST";
    request.path = BOOTSTRAP_PATH;
    request.header = "Accept: text/xml,text/javascript,text/html,application/json\r\n" \
                     "Content-Type: application/x-www-form-urlencoded;charset=utf-8\r\n";
    request.content = (uint8_t *)content;
    request.content_len = (uint32_t)strlen(content);

    res = core_http_send(bootstrap->http_handle, &request);
    core_os_free(content);
    if (res < STATE_SUCCESS) {
        return res;
    }

    return STATE_SUCCESS;
}

static int32_t core_bootstrap_recv(bootstrap_t *bootstrap)
{
    int32_t res = STATE_SUCCESS;
    uint64_t starttime = core_os_time();

    while (1) {
        if (core_os_time() - starttime >= bootstrap->timeout_ms) {
            break;
        }

        res = core_http_recv(bootstrap->http_handle);
        if (res < STATE_SUCCESS) {
            break;
        }
    }

    return res;
}

static int32_t core_bootstrap_connection_info(bootstrap_t *bootstrap, char **host, uint16_t *port)
{
    int32_t res = STATE_SUCCESS;
    char *tmp_host = NULL, *host_key = "host", *port_key = "port";
    char *host_value = NULL, *port_value = NULL;
    uint32_t tmp_port = 0, host_value_len = 0, port_value_len = 0;

    if (bootstrap->response.code != 200) {
        return STATE_HTTP_HEADER_INVALID;
    }

    if (((res = core_json_value((char *)bootstrap->response.content, bootstrap->response.content_len,
                                host_key, strlen(host_key), &host_value, &host_value_len)) < STATE_SUCCESS) ||
            ((res = core_json_value((char *)bootstrap->response.content, bootstrap->response.content_len,
                                    port_key, strlen(port_key), &port_value, &port_value_len)) < STATE_SUCCESS)) {
        ALOG_ERROR(TAG, STATE_MESSAGE_PARSE_ERROR, "msg(%.*s) json parse error\r\n", bootstrap->response.content_len, bootstrap->response.content);
        return STATE_HTTP_HEADER_INVALID;
    }

    tmp_host = core_os_malloc(host_value_len + 1);
    if (tmp_host == NULL) {
        ALOG_ERROR(TAG, STATE_SYS_DEPEND_MALLOC_FAILED, "malloc failed \r\n");
        return STATE_SYS_DEPEND_MALLOC_FAILED;
    }
    memset(tmp_host, 0, host_value_len + 1);
    memcpy(tmp_host, host_value, host_value_len);

    core_str2uint(port_value, port_value_len, &tmp_port);

    *host = tmp_host;
    *port = tmp_port;

    return STATE_SUCCESS;
}

int32_t aiot_device_bootstrap_request(void *device, aiot_protocol_type_t type, uint32_t timeout_ms, aiot_bootstrap_info_t *info)
{
    aiot_sysdep_network_cred_t cred;
    int32_t res = STATE_SUCCESS;
    device_handle_t *dev = (device_handle_t *)device;
    char *host = NULL;
    uint16_t port = 0;
    bootstrap_t *bootstrap = core_bootstrap_init();
    /* 边界条件判断 */
    if(dev == NULL || info == NULL || dev->product_key == NULL || dev->device_name == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_bootstrap_request input null, please check\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    ALOG_INFO(TAG, "%s|%s bootstrap \r\n", dev->product_key, dev->device_name);

    /* 发送请求 */
    bootstrap->http_handle = core_http_init();
    if (bootstrap->http_handle == NULL) {
        core_bootstrap_deinit(bootstrap);
        ALOG_ERROR(TAG, STATE_SYS_DEPEND_MALLOC_FAILED, "malloc failed \r\n");
        return STATE_SYS_DEPEND_MALLOC_FAILED;
    }

    bootstrap->send_timeout_ms = timeout_ms;
    bootstrap->recv_timeout_ms = timeout_ms;
    bootstrap->timeout_ms = timeout_ms;
    memset(&cred, 0, sizeof(aiot_sysdep_network_cred_t));
    cred.option = AIOT_SYSDEP_NETWORK_CRED_SVRCERT_CA;  /* 使用RSA证书校验MQTT服务端 */
    cred.max_tls_fragment = 16384; /* 最大的分片长度为16K, 其它可选值还有4K, 2K, 1K, 0.5K */
    cred.sni_enabled = 1;                               /* TLS建连时, 支持Server Name Indicator */
    cred.x509_server_cert = ali_ca_cert;                 /* 用来验证MQTT服务端的RSA根证书 */
    cred.x509_server_cert_len = strlen(ali_ca_cert);     /* 用来验证MQTT服务端的RSA根证书长度 */
    if (((res = core_http_setopt(bootstrap->http_handle, CORE_HTTPOPT_HOST,
                                 (void *)bootstrap->host)) < STATE_SUCCESS) ||
            ((res = core_http_setopt(bootstrap->http_handle, CORE_HTTPOPT_PORT,
                                     (void *)&bootstrap->port)) < STATE_SUCCESS) ||
            ((res = core_http_setopt(bootstrap->http_handle, CORE_HTTPOPT_NETWORK_CRED,
                                     (void *)&cred)) < STATE_SUCCESS) ||
            ((res = core_http_setopt(bootstrap->http_handle, CORE_HTTPOPT_CONNECT_TIMEOUT_MS,
                                     (void *)&bootstrap->timeout_ms)) < STATE_SUCCESS) ||
            ((res = core_http_setopt(bootstrap->http_handle, CORE_HTTPOPT_SEND_TIMEOUT_MS,
                                     (void *)&bootstrap->send_timeout_ms)) < STATE_SUCCESS) ||
            ((res = core_http_setopt(bootstrap->http_handle, CORE_HTTPOPT_RECV_TIMEOUT_MS,
                                     (void *)&bootstrap->recv_timeout_ms)) < STATE_SUCCESS) ||
            ((res = core_http_setopt(bootstrap->http_handle, CORE_HTTPOPT_BODY_BUFFER_MAX_LEN,
                                     (void *)&bootstrap->response_body_len)) < STATE_SUCCESS) ||
            ((res = core_http_setopt(bootstrap->http_handle, CORE_HTTPOPT_RECV_HANDLER,
                                     (void *)core_bootstrap_http_recv_callback)) < STATE_SUCCESS) ||
            ((res = core_http_setopt(bootstrap->http_handle, CORE_HTTPOPT_USERDATA,
                                     (void *)bootstrap)) < STATE_SUCCESS)) {
        core_bootstrap_deinit(bootstrap);
        return res;
    }

    res = core_http_connect(bootstrap->http_handle);
    if (res < STATE_SUCCESS) {
        core_bootstrap_deinit(bootstrap);
        return res;
    }

    res = core_bootstrap_send_post_request(dev, type, bootstrap);
    if (res < STATE_SUCCESS) {
        core_bootstrap_deinit(bootstrap);
        return res;
    }

    /* 接收返回报文 */
    res = core_bootstrap_recv(bootstrap);
    if(res != STATE_HTTP_READ_BODY_FINISHED) {
        core_bootstrap_deinit(bootstrap);
        return res;
    }
    /* 解析返回报文 */
    res = core_bootstrap_connection_info(bootstrap, &host, &port);
    if (res < STATE_SUCCESS) {
        core_bootstrap_deinit(bootstrap);
        return res;
    }
    memset(info, 0, sizeof(aiot_bootstrap_info_t));
    strncpy(info->host, host, sizeof(info->host) - 1);
    info->port = port;
    ALOG_INFO(TAG, "%s|%s bootstrap result %s:%d\r\n", dev->product_key, dev->device_name, info->host, info->port);

    core_os_free(host);
    core_bootstrap_deinit(bootstrap);
    return STATE_SUCCESS;
}