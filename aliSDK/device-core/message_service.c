
#include <stdio.h>
#include <string.h>
#include "protocol_config.h"
#include "core_string.h"
#include "aiot_state_api.h"
#include "aiot_device_api.h"
#include "misc.h"
#include "core_log.h"
#include "core_os.h"
#include "message_service.h"

#define TAG "MSG_SERVICE"
extern aiot_protocol_t mqtt_protocol;
extern aiot_protocol_t http_protocol;

/* 传输通道状态变化回调函数 */
static void trans_event_callback(void *handle, const aiot_protocol_event_t *event, void *userdata)
{
    core_channel_status_t status;
    core_msg_service_handle_t *service_handle = NULL;
    core_trans_channel_t *channel = (core_trans_channel_t *)userdata;
    if(event == NULL || userdata == NULL) {
        return;
    }
    service_handle = (core_msg_service_handle_t *)channel->content;
    core_os_mutex_lock(service_handle->run_lock);
    switch (event->type) {
    case AIOT_TRANSEVT_RECONNECT:
    case AIOT_TRANSEVT_CONNECT: {
        if(channel->online == 0)
        {
            /* 更新channel、service_handle状态，这里调用has_connected是为了标识子设备建连 */
            service_handle->has_connected = 1;
            channel->online = 1;
            service_handle->online_num++;
            ALOG_INFO(TAG, "cid[%d] connect, online channel %d \r\n", channel->cid, service_handle->online_num);
            status.type = (aiot_device_status_type_t)event->type;
            status.error_code = event->error_code;
            status.cid = channel->cid;
            status.online_num = service_handle->online_num;
            service_handle->status_callback(service_handle, &status, service_handle->userdata);
        }
    }
    break;
    case AIOT_TRANSEVT_DISCONNECT: {
        if(channel->online == 1) {
            /* 更新channel、service_handle状态 */
            channel->online = 0;
            service_handle->online_num--;
            ALOG_INFO(TAG, "cid[%d] disconnect, online channel %d \r\n", channel->cid, service_handle->online_num);
            status.type = (aiot_device_status_type_t)event->type;
            status.error_code = event->error_code;
            status.cid = channel->cid;
            status.online_num = service_handle->online_num;
            service_handle->status_callback(service_handle, &status, service_handle->userdata);
        }
    }
    break;
    }
    core_os_mutex_unlock(service_handle->run_lock);
}


/* 传输通道消息回调函数 */
static void trans_recv_callback(void *handle, const aiot_msg_t *message, void *userdata)
{
    core_msg_service_handle_t *service_handle = NULL;
    core_trans_channel_t *channel = (core_trans_channel_t *)userdata;
    aiot_msg_t *msg = (aiot_msg_t *)message; /* 因可能需要修改msg内容，增加强制转换 */
    char rrpc_prefix[] = "/ext/rrpc/";
    char *real_topic = NULL;
    int32_t real_topic_len = 0;
    if(message == NULL || userdata == NULL) {
        return;
    }
    service_handle = (core_msg_service_handle_t *)channel->content;

    /* 检查是否为rrpc消息，为rrpc消息脱去前缀，并且记录消息的cid及rrpc_id  */
    if(0 == strncmp(msg->topic, rrpc_prefix, strlen(rrpc_prefix))) {
        msg->cid = channel->cid;
        msg->rrpc_id = get_topic_level(msg->topic, 3);
        if(msg->rrpc_id != NULL) {
            real_topic_len = strlen(msg->topic) - strlen(rrpc_prefix) - strlen(msg->rrpc_id);
            real_topic = core_os_malloc(real_topic_len + 1);
            memcpy(real_topic, msg->topic + strlen(rrpc_prefix) + strlen(msg->rrpc_id), real_topic_len);
            *(real_topic + real_topic_len) = 0;
            core_os_free(msg->topic);
            msg->topic = real_topic;
        }
    }

    /* 执行消息回调 */
    if(service_handle->message_callback != NULL) {
        service_handle->message_callback(service_handle, msg, service_handle->userdata);
    }
}

/* 传输通道消息ACK回调 */
static void trans_result_callback(void *handle, const aiot_protocol_result_t *event, void *userdata)
{
    core_msg_service_handle_t *service_handle = NULL;
    core_trans_channel_t *channel = (core_trans_channel_t *)userdata;
    aiot_device_msg_result_t result;

    if(event == NULL || userdata == NULL) {
        return;
    }
    service_handle = (core_msg_service_handle_t *)channel->content;

    /* 事件转换，执行回调函数 */
    result.type = (aiot_device_msg_result_type_t)event->type;
    result.message_id = channel->cid << 16 | event->message_id;
    result.code = event->code;
    if(service_handle->result_callback != NULL) {
        service_handle->result_callback(service_handle, &result, service_handle->userdata);
    }
}

/* 添加传输通道进消息服务 */
static int32_t append_transport(core_msg_service_handle_t *service,  uint32_t cid)
{
    char ext_clientid[256];
    /* 生成传输节点添加进消息服务的链表中 */
    core_trans_channel_t *channel = core_os_malloc(sizeof(core_trans_channel_t));
    if (channel == NULL) {
        ALOG_ERROR(TAG, STATE_DEVICE_UNKOWN_ERROR, "device message service unkown error\r\n");
        return STATE_DEVICE_UNKOWN_ERROR;
    }

    /* channel初始化 */
    memset(channel, 0, sizeof(core_trans_channel_t));
    CORE_INIT_LIST_HEAD(&channel->linked_node);
    channel->online = 0;
    channel->cid = cid;
    channel->content = service;
    channel->config = protocol_config_copy(service->proto_config);

    /* clientid添加cid参数 */
    memset(ext_clientid, 0, sizeof(ext_clientid));
    snprintf(ext_clientid, sizeof(ext_clientid), "cid=%d", cid);
    protocol_config_add_ext_clientid(channel->config, ext_clientid);

    /* 添加通道回调函数的消息服务对象及通道对象 */
    channel->config->userdata = channel;
    channel->trans_handle = service->protocol->init(channel->config);

    core_os_mutex_lock(service->trans_mutex);
    core_list_add_tail(&channel->linked_node, &service->transport_list);
    core_os_mutex_unlock(service->trans_mutex);
    return STATE_SUCCESS;
}

static int32_t release_all_transports(core_msg_service_handle_t *service_handle)
{
    core_trans_channel_t *node = NULL, *next = NULL;
    core_os_mutex_lock(service_handle->trans_mutex);
    core_list_for_each_entry_safe(node, next, &service_handle->transport_list, linked_node,
                                  core_trans_channel_t) {
        core_list_del(&node->linked_node);
        service_handle->protocol->deinit(&node->trans_handle);
        protocol_config_deinit(&node->config);
        core_os_free(node);
    }
    core_os_mutex_unlock(service_handle->trans_mutex);

    return STATE_SUCCESS;
}

/* 使用轮询的方式查找有效的传输通道 */
static core_trans_channel_t* _core_find_valid_transport(core_msg_service_handle_t *service)
{
    core_trans_channel_t *trans = NULL, *result = NULL;
    core_os_mutex_lock(service->trans_mutex);
    if(service->online_num == 0) {
        core_os_mutex_unlock(service->trans_mutex);
        return NULL;
    }

    if(service->current_trans == NULL || service->current_trans->online == 0) {
        service->current_trans = core_list_first_entry(&service->transport_list, core_trans_channel_t, linked_node);
    }

    trans = service->current_trans;
    while(trans) {
        trans = core_list_next_entry(trans, linked_node, core_trans_channel_t);
        /* 忽略表头 */
        if(&trans->linked_node == &service->transport_list) {
            continue;
        }

        /* 通道在线，允许发送 */
        if(trans->online == 1) {
            result = trans;
            break;
        }

        /* 链表循环一圈没有找到可发送的连接，判断当前节点是否可以发送 */
        if(trans == service->current_trans) {
            break;
        }
    }
    service->current_trans = result;
    core_os_mutex_unlock(service->trans_mutex);
    return result;
}

int32_t core_msg_find_valid_cid(void *handle)
{
    core_msg_service_handle_t *service = (core_msg_service_handle_t *)handle;
    core_trans_channel_t *trans = NULL;

    trans = _core_find_valid_transport(service);
    if(trans == NULL) {
        return DEFAULT_CID;
    }

    return trans->cid;
}

/* 指定cid传输，如果cid无效，返回NULL */
static core_trans_channel_t* _core_find_transport_by_cid(core_msg_service_handle_t *service, int32_t cid)
{
    core_trans_channel_t *node = NULL, *target = NULL;
    core_os_mutex_lock(service->trans_mutex);
    core_list_for_each_entry(node, &service->transport_list, linked_node, core_trans_channel_t) {
        /* 通道在线才能发出 */
        if (node->online == 1 && node->cid == cid) {
            target = node;
            break;
        }
    }
    core_os_mutex_unlock(service->trans_mutex);

    return target;
}

/* 消息传输服务初始化 */
void *core_msg_service_init(aiot_linkconfig_t *config)
{
    core_msg_service_handle_t *service_handle;
    if(config == NULL) {
        return NULL;
    }

    service_handle = core_os_malloc(sizeof(core_msg_service_handle_t));
    if(NULL == service_handle) {
        return NULL;
    }

    CORE_INIT_LIST_HEAD(&service_handle->transport_list);
    service_handle->current_trans = NULL;
    service_handle->trans_mutex = core_os_mutex_init();
    service_handle->online_num = 0;
    if(config->protocol == MQTT_PROTOCOL) {
        service_handle->protocol = &mqtt_protocol;
    } else if(config->protocol == HTTP_PROTOCOL) {
        service_handle->protocol = &http_protocol;
    }
    service_handle->proto_config = protocol_config_copy(config->proto_config);
    service_handle->start_cid = config->start_cid;
    service_handle->channel_num = config->channel_num;
    service_handle->has_connected = 0;
    service_handle->proto_config->recv_callback = trans_recv_callback;
    service_handle->proto_config->event_callback = trans_event_callback;
    service_handle->proto_config->result_callback = trans_result_callback;
    service_handle->proto_config->userdata = service_handle;
    service_handle->thread = NULL;
    service_handle->thread_exit = 0;
    service_handle->run_lock = core_os_mutex_init();

    return service_handle;
}

/* 设置消息传输服务事件回调函数 */
int32_t core_msg_service_set_event_callback(void *handle, aiot_device_msg_callback_t message_callback, core_channel_status_callback_t status_callback, aiot_device_msg_result_callback_t result_callback, void *userdata)
{
    core_msg_service_handle_t *service_handle = (core_msg_service_handle_t *)handle;
    if(service_handle == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    service_handle->message_callback = message_callback;
    service_handle->status_callback = status_callback;
    service_handle->result_callback = result_callback;
    service_handle->userdata = userdata;

    return STATE_SUCCESS;
}

static int32_t _core_msg_serice_connect_sync(core_msg_service_handle_t *service)
{
    int32_t res = STATE_SUCCESS;
    core_trans_channel_t *node = NULL, *target_node = NULL;
    core_os_mutex_lock(service->trans_mutex);
    core_list_for_each_entry(node, &service->transport_list, linked_node, core_trans_channel_t) {
        /* 每次只建连一个通道 */
        if (node->online == 0) {
            target_node = node;
            break;
        }
    }
    core_os_mutex_unlock(service->trans_mutex);

    if(target_node != NULL) {
        ALOG_INFO(TAG, "node online %d, connect cid %d\r\n", node->online, node->cid);
        res = service->protocol->connect(node->trans_handle);
        if(res == STATE_SUCCESS) {
            service->has_connected = 1;
        }
    }

    return res;
}

/* 消息传输服务建连线程，单次只执行一个建连 */
static void *_core_msg_service_thread(void *argv)
{
    core_msg_service_handle_t *service = (core_msg_service_handle_t *)argv;
    int32_t failed_counter = 0;
    int32_t res = STATE_SUCCESS;
    int32_t last_time = 0;
    while(service->thread_exit) {
        /* 连接失败，增加退避(1s-10s) */
        if(core_os_time() - last_time < failed_counter * 1000) {
            core_os_sleep(1000);
        }
        last_time = core_os_time();
        /* 发起建连 */
        res = _core_msg_serice_connect_sync(service);
        if(res == STATE_SUCCESS) {
            failed_counter = 0;
        } else if(failed_counter < 10) {
            failed_counter++;
        }

        /* 当所有的通道都已建连，退出线程 */
        if(service->channel_num == service->online_num) {
            service->thread_exit = 0;
        }
    }

    return NULL;
}

/* sync [1, 同步建连] [0, 异步建连]*/
int32_t core_msg_service_connect(void *handle, int8_t sync)
{
    core_msg_service_handle_t *service_handle = (core_msg_service_handle_t *)handle;
    int32_t res = STATE_SUCCESS;
    int32_t i = 0;

    if(service_handle == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    if(service_handle->has_connected == 0) {
        /* 多连接 */
        if(service_handle->channel_num > 1) {
            for(i = 0; i < service_handle->channel_num; i++) {
                append_transport(service_handle, i + service_handle->start_cid);
            }
        } else {
            append_transport(service_handle, 0);
        }
    }
    if(sync) {
        res = _core_msg_serice_connect_sync(service_handle);
        if(service_handle->online_num > 0) {
            /* 多通道也支持异步 */
            if(service_handle->channel_num > 0) {
                service_handle->thread_exit = 1;
                service_handle->thread = core_os_thread_init("CONNECT_ASYNC", _core_msg_service_thread, service_handle);
            }
            return STATE_SUCCESS;
        } else {
            return res;
        }
    } else {
        service_handle->thread_exit = 1;
        service_handle->thread = core_os_thread_init("CONNECT_ASYNC", _core_msg_service_thread, service_handle);
        return STATE_SUCCESS;
    }

}

/* 选择传输通道向物联网平台传输消息 */
int32_t core_msg_service_send(void *handle, const aiot_msg_t *message)
{
    core_msg_service_handle_t *service_handle = (core_msg_service_handle_t *)handle;
    core_trans_channel_t *trans = NULL;
    int32_t res = 0;
    if(service_handle == NULL || service_handle->has_connected != 1) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    if(message->cid != DEFAULT_CID) {
        /* 指定cid回复 */
        trans = _core_find_transport_by_cid(service_handle, message->cid);
    } else {
        /* 查找有效的通道发送 */
        trans = _core_find_valid_transport(service_handle);
    }

    /* 找到有效的传输通道进行消息传输 */
    if(trans != NULL) {
        res = service_handle->protocol->send_message(trans->trans_handle, message);
        if(res > STATE_SUCCESS) {
            /* Qos1消息，返回消息id*/
            return trans->cid << 16 | res;
        }
        return res;
    } else {
        ALOG_ERROR(TAG, STATE_DEVICE_UNKOWN_ERROR, "device message service unkown error\r\n");
        return STATE_DEVICE_UNKOWN_ERROR;
    }
}

/* sub消息 */
int32_t core_msg_service_sub_topic(void *handle, const char *topic, uint8_t qos, int32_t cid)
{
    core_msg_service_handle_t *service_handle = (core_msg_service_handle_t *)handle;
    core_trans_channel_t *trans = NULL;
    int32_t res = 0;
    if(service_handle == NULL || service_handle->has_connected != 1) {
        ALOG_ERROR(TAG, STATE_DEVICE_UNKOWN_ERROR, "device message service unkown error\r\n");
        return STATE_DEVICE_UNKOWN_ERROR;
    }

    if(cid != DEFAULT_CID) {
        /* 指定cid回复 */
        trans = _core_find_transport_by_cid(service_handle, cid);
    } else {
        /* 查找有效的通道发送 */
        trans = _core_find_valid_transport(service_handle);
    }

    if(trans != NULL) {
        res = service_handle->protocol->sub_topic(trans->trans_handle, topic, qos);
        if(res > STATE_SUCCESS) {
            /* Qos1消息，返回消息id*/
            return trans->cid << 16 | res;
        }
        return res;
    } else {
        ALOG_ERROR(TAG, STATE_DEVICE_UNKOWN_ERROR, "device message service unkown error\r\n");
        return STATE_DEVICE_UNKOWN_ERROR;
    }
}

/* unsub消息 */
int32_t core_msg_service_unsub_tpoic(void *handle, const char *topic, uint8_t qos, int32_t cid)
{
    core_msg_service_handle_t *service_handle = (core_msg_service_handle_t *)handle;
    core_trans_channel_t *trans = NULL;
    int32_t res = 0;
    if(service_handle == NULL || service_handle->has_connected != 1) {
        return STATE_USER_INPUT_NULL_POINTER;
    }
    if(cid != DEFAULT_CID) {
        /* 指定cid回复 */
        trans = _core_find_transport_by_cid(service_handle, cid);
    } else {
        /* 查找有效的通道发送 */
        trans = _core_find_valid_transport(service_handle);
    }

    if(trans != NULL) {
        res = service_handle->protocol->unsub_topic(trans->trans_handle, topic, qos);
        if(res > STATE_SUCCESS) {
            /* Qos1消息，返回消息id*/
            return trans->cid << 16 | res;
        }
        return res;
    } else {
        ALOG_ERROR(TAG, STATE_DEVICE_UNKOWN_ERROR, "device message service unkown error\r\n");
        return STATE_DEVICE_UNKOWN_ERROR;
    }
}

/* 断开消息服务 */
int32_t core_msg_service_disconnect(void *handle)
{
    core_msg_service_handle_t *service_handle = (core_msg_service_handle_t *)handle;
    core_trans_channel_t *node = NULL, *next = NULL;
    if(service_handle == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    /* 断连前先退出异步建连操作 */
    service_handle->thread_exit = 0;
    if(service_handle->thread != NULL) {
        core_os_thread_join(&service_handle->thread);
    }

    core_os_mutex_lock(service_handle->trans_mutex);
    /* 将所有的通道断连 */
    core_list_for_each_entry_safe(node, next, &service_handle->transport_list,
                                  linked_node, core_trans_channel_t) {
        service_handle->protocol->disconnect(node->trans_handle);
    }
    core_os_mutex_unlock(service_handle->trans_mutex);

    return STATE_SUCCESS;
}

/* 回收消息服务资源 */
int32_t core_msg_service_deinit(void **handle)
{
    core_msg_service_handle_t **service_handle_ptr = (core_msg_service_handle_t **)handle;
    core_msg_service_handle_t *service_handle = NULL;
    if(handle == NULL || *handle == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    service_handle = *service_handle_ptr;
    /* 删除服务前先断开连接 */
    if(service_handle->online_num > 0) {
        core_msg_service_disconnect(service_handle);
    }

    /* 回收transport资源 */
    release_all_transports(service_handle);
    core_os_mutex_lock(service_handle->trans_mutex);
    core_os_mutex_unlock(service_handle->trans_mutex);
    core_os_mutex_deinit(&service_handle->trans_mutex);

    core_os_mutex_lock(service_handle->run_lock);
    core_os_mutex_unlock(service_handle->run_lock);
    core_os_mutex_deinit(&service_handle->run_lock);

    /* 其它资源回收 */
    protocol_config_deinit(&service_handle->proto_config);
    core_os_free(service_handle);
    service_handle = NULL;
    *handle = NULL;
    return STATE_SUCCESS;
}


