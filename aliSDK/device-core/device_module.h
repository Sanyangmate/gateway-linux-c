

#ifndef _DEVICE_MODULE_H_
#define _DEVICE_MODULE_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>
#include "device_private.h"

void*   module_init();
int32_t module_add_element(void *handle, module_type_t key, module_ops_t *ops);
int32_t module_delete_element(void *handle, module_type_t key);
void*   module_get_element(void *handle, module_type_t key);
int32_t module_status_event_notify(void *handle, aiot_device_status_t event);
int32_t module_channel_status_event_notify(void *handle, const core_channel_status_t *event);
int32_t module_deinit(void **handle);

#if defined(__cplusplus)
}
#endif

#endif