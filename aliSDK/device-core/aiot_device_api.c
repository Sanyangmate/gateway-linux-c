/**
 * @file aiot_device_api.c
 * @brief 设备管理模块，管理设备的配置、行为、及运行时参数
 * @date 2021-12-27
 *
 * @copyright Copyright (C) 2015-2025 Alibaba Group Holding Limited
 *
 */
#include <stdio.h>
#include "aiot_state_api.h"
#include "aiot_device_api.h"
#include "device_private.h"
#include "core_string.h"
#include "message_service.h"
#include "device_module.h"
#include "core_log.h"
#include "core_os.h"
#include "aiot_sysdep_api.h"

#define TAG "DEVICE"

/* 将对应topic从消息回调列表中删除 */
static int32_t _core_device_sublist_remove(device_handle_t *device, const char *topic)
{
    core_device_sub_node_t *node = NULL, *next = NULL;
    int32_t topic_len = 0;
    if(device == NULL || topic == NULL) {
        return -1;
    }
    topic_len = strlen(topic);

    core_os_mutex_lock(device->sub_list_mutex);
    core_list_for_each_entry_safe(node, next, &device->sub_list, linked_node, core_device_sub_node_t) {
        if ((strlen(node->topic) == topic_len) && memcmp(node->topic, topic, topic_len) == 0) {
            /* exist topic */
            ALOG_INFO(TAG, "[%s][%s] sublist remove %s\r\n", device->product_key, device->device_name, topic);
            core_list_del(&node->linked_node);
            core_os_free(node->topic);
            core_os_free(node);
        }
    }
    core_os_mutex_unlock(device->sub_list_mutex);

    return STATE_SUCCESS;
}

/* 将对应topic插入消息回调列表*/
static int32_t _core_device_sublist_insert(device_handle_t *device, const char *topic,
        aiot_device_msg_callback_t callback, void *userdata)
{
    core_device_sub_node_t *node = NULL, *next = NULL;
    int32_t topic_len = 0;
    if(device == NULL || topic == NULL) {
        return -1;
    }
    topic_len = strlen(topic);

    core_os_mutex_lock(device->sub_list_mutex);
    core_list_for_each_entry_safe(node, next, &device->sub_list, linked_node, core_device_sub_node_t) {
        if ((strlen(node->topic) == topic_len) && memcmp(node->topic, topic, topic_len) == 0) {
            /* exist topic */
            node->callback = callback;
            node->userdata = userdata;
            core_os_mutex_unlock(device->sub_list_mutex);
            return STATE_SUCCESS;
        }
    }

    /* new topic */
    node = core_os_malloc(sizeof(core_device_sub_node_t));
    if (node == NULL) {
        core_os_mutex_unlock(device->sub_list_mutex);
        ALOG_ERROR(TAG, STATE_SYS_DEPEND_MALLOC_FAILED, "malloc failed \r\n");
        return STATE_SYS_DEPEND_MALLOC_FAILED;
    }
    memset(node, 0, sizeof(core_device_sub_node_t));
    CORE_INIT_LIST_HEAD(&node->linked_node);
    node->topic = core_os_malloc(topic_len + 1);
    if(node->topic == NULL) {
        core_os_free(node);
        core_os_mutex_unlock(device->sub_list_mutex);
        ALOG_ERROR(TAG, STATE_SYS_DEPEND_MALLOC_FAILED, "malloc failed \r\n");
        return STATE_SYS_DEPEND_MALLOC_FAILED;
    }

    memcpy(node->topic, topic, topic_len);
    node->topic[topic_len] = 0;
    node->callback = callback;
    node->userdata = userdata;
    core_list_add_tail(&node->linked_node, &device->sub_list);
    core_os_mutex_unlock(device->sub_list_mutex);

    return STATE_SUCCESS;
}

/* 消息回调列表初始化 */
static void _core_device_sublist_init(device_handle_t *device)
{
    device->sub_list_mutex = core_os_mutex_init();
    CORE_INIT_LIST_HEAD(&device->sub_list);
}

/* 消息回调列表删除，资源回收 */
static void _core_device_sublist_destroy(device_handle_t *device)
{
    core_device_sub_node_t *node = NULL, *next = NULL;

    core_os_mutex_lock(device->sub_list_mutex);
    core_list_for_each_entry_safe(node, next, &device->sub_list, linked_node, core_device_sub_node_t) {
        core_list_del(&node->linked_node);
        core_os_free(node->topic);
        core_os_free(node);
    }
    core_os_mutex_unlock(device->sub_list_mutex);
    core_os_mutex_deinit(&device->sub_list_mutex);
}

void *aiot_device_create(const char *product_key, const char *device_name) {
    device_handle_t *dev = NULL;

    if(product_key == NULL || device_name == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_create input null, please check product_key, device_name\r\n");
        return NULL;
    }

    dev = core_os_malloc(sizeof(device_handle_t));
    if(NULL == dev) {
        ALOG_ERROR(TAG, STATE_SYS_DEPEND_MALLOC_FAILED, "malloc failed \r\n");
        return NULL;
    }
    memset(dev, 0, sizeof(device_handle_t));
    dev->message_service = NULL;
    dev->linkconfig.proto_config = NULL;
    dev->ops = &normal_ops;
    core_strdup(NULL, &dev->product_key, product_key, "");
    core_strdup(NULL, &dev->device_name, device_name, "");
    dev->alink_id_mutex = core_os_mutex_init();
    dev->owner_cid = DEFAULT_CID;

    _core_device_sublist_init(dev);

    return dev;
}

int32_t aiot_device_set_device_secret(void *device, const char *device_secret)
{
    device_handle_t *dev = (device_handle_t *)device;
    if(dev == NULL || device_secret == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_set_device_secret input null, please check device, device_secret\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    core_strdup(NULL, &dev->device_secret, device_secret, "");

    return STATE_SUCCESS;
}

int32_t aiot_device_set_linkconfig(void *device, aiot_linkconfig_t *config)
{
    device_handle_t *dev = (device_handle_t *)device;
    if(dev == NULL || config == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_set_linkconfig input null, please check device, config\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    /* 拷贝连接参数 */
    if(dev->linkconfig.proto_config != NULL) {
        protocol_config_deinit((aiot_protocol_config_t **)&dev->linkconfig.proto_config);
    }
    dev->linkconfig.protocol = config->protocol;
    dev->linkconfig.proto_config = protocol_config_copy(config->proto_config);
    dev->linkconfig.start_cid = config->start_cid;
    dev->linkconfig.channel_num = config->channel_num;

    return STATE_SUCCESS;
}

int32_t aiot_device_connect_async(void *device) {
    device_handle_t *dev = (device_handle_t *)device;
    int32_t res = STATE_SUCCESS;
    if(dev == NULL || dev->product_key == NULL || dev->device_name == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_connect_async input null , please check\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    if(dev->has_connected) {
        ALOG_ERROR(TAG, STATE_DEVICE_REPEATED_CONNECT, "device reconnect error\r\n");
        return STATE_DEVICE_REPEATED_CONNECT;
    }

    /* 校验连接参数是否缺失 */
    if(STATE_SUCCESS != core_device_check_linkconfig(dev)) {
        ALOG_ERROR(TAG, STATE_DEVICE_MISSING_CONNECT_ARGS, "aiot_device_connect miss linkconfig\r\n");
        return STATE_DEVICE_MISSING_CONNECT_ARGS;
    }

    ALOG_INFO(TAG, "device %s.%s connecting\r\n", dev->product_key, dev->device_name);

    if((res = dev->ops->connect_async(dev)) < STATE_SUCCESS) {
        ALOG_ERROR(TAG, res, "device %s.%s connect failed\r\n", dev->product_key, dev->device_name);
        return res;
    }

    dev->has_connected = 1;

    return STATE_SUCCESS;
}

int32_t aiot_device_connect(void *device) {
    device_handle_t *dev = (device_handle_t *)device;
    int32_t res = STATE_SUCCESS;
    if(dev == NULL || dev->product_key == NULL || dev->device_name == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_connect input null , please check\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    if(dev->has_connected) {
        ALOG_ERROR(TAG, STATE_DEVICE_REPEATED_CONNECT, "device reconnect error\r\n");
        return STATE_DEVICE_REPEATED_CONNECT;
    }

    /* 校验连接参数是否缺失 */
    if(STATE_SUCCESS != core_device_check_linkconfig(dev)) {
        ALOG_ERROR(TAG, STATE_DEVICE_MISSING_CONNECT_ARGS, "aiot_device_connect miss linkconfig\r\n");
        return STATE_DEVICE_MISSING_CONNECT_ARGS;
    }

    ALOG_INFO(TAG, "%s.%s connecting\r\n", dev->product_key, dev->device_name);
    if((res = dev->ops->connect(dev)) < STATE_SUCCESS) {
        ALOG_ERROR(TAG, res, "%s.%s connect failed\r\n", dev->product_key, dev->device_name);
        return res;
    }

    dev->has_connected = 1;
    return STATE_SUCCESS;
}

int32_t aiot_device_send_message(void *device, const aiot_msg_t *message)
{
    device_handle_t *dev = (device_handle_t *)device;
    int32_t res = STATE_SUCCESS;

    if(device == NULL || message == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_send_message input null , please check message\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    if(dev->online == 0) {
        ALOG_ERROR(TAG, STATE_DEVICE_OFFLINE, "device offline unable send message\r\n");
        return STATE_DEVICE_OFFLINE;
    }

    // ALOG_INFO(TAG, "msg > : %s \r\n", message->topic);
    res = normal_ops.send_message(device, message);
    if(res < STATE_SUCCESS) {
        ALOG_ERROR(TAG, res, "send msg(%s) failed \r\n", message->topic);
    }

    return res;
}

int32_t aiot_device_set_event_callback(void *device, aiot_device_msg_callback_t msg_recv_callback, aiot_device_status_callback_t status_callback, aiot_device_msg_result_callback_t msg_result_callback, void *userdata)
{
    device_handle_t *dev = (device_handle_t *)device;
    if(dev == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    dev->recv_callback = msg_recv_callback;
    dev->status_callback = status_callback;
    dev->result_callback = msg_result_callback;
    dev->userdata = userdata;

    return STATE_SUCCESS;
}

int32_t aiot_device_register_topic_filter(void *device, const char *topic, aiot_device_msg_callback_t callback, uint8_t sub, void *userdata)
{
    device_handle_t *dev = (device_handle_t *)device;
    if(device == NULL || topic == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_register_topic_filter input null , please check topic\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    if(callback != NULL) {
        _core_device_sublist_insert(device, topic, callback, userdata);
    }

    if(sub) {
        if(dev->online == 0) {
            ALOG_ERROR(TAG, STATE_DEVICE_OFFLINE, "device offline unable sub\r\n");
            return STATE_DEVICE_OFFLINE;
        }

        ALOG_INFO(TAG, "sub >: %s\r\n", topic);
        return dev->ops->sub_topic(dev, topic, 1, DEFAULT_CID);
    }


    return STATE_SUCCESS;
}

int32_t aiot_device_deregister_topic_filter(void *device, const char *topic, uint8_t unsub)
{
    device_handle_t *dev = (device_handle_t *)device;
    if(device == NULL || topic == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_deregister_topic_filter input null , please check topic\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    _core_device_sublist_remove(device, topic);

    if(unsub) {
        if(dev->online == 0) {
            ALOG_ERROR(TAG, STATE_DEVICE_OFFLINE, "device offline unable unsub\r\n");
            return STATE_DEVICE_OFFLINE;
        }

        ALOG_INFO(TAG, "unsub >: %s\r\n", topic);
        return dev->ops->unsub_topic(dev, topic, 1, DEFAULT_CID);
    }

    return STATE_SUCCESS;
}

int32_t aiot_device_disconnect(void *device)
{
    device_handle_t *dev = (device_handle_t *)device;
    if(device == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }
    if(dev->has_connected != 1) {
        ALOG_ERROR(TAG, STATE_DEVICE_REPEATED_DISCONNECT, "device has disconnected\r\n");
        return STATE_DEVICE_REPEATED_DISCONNECT;
    }

    dev->has_connected = 0;

    ALOG_INFO(TAG, "%s.%s user call disconnect\r\n", dev->product_key, dev->device_name);
    return dev->ops->disconnect(dev);
}

int32_t aiot_device_delete(void **device)
{
    device_handle_t *dev;
    if(device == NULL || *device == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }
    dev = *(device_handle_t **)device;

    if(dev->has_connected == 1) {
        dev->ops->disconnect(dev);
    }

    if(dev->module_manager != NULL) {
        module_deinit(&dev->module_manager);
    }

    if(dev->product_key != NULL) {
        core_os_free(dev->product_key);
    }
    if(dev->device_name != NULL) {
        core_os_free(dev->device_name);
    }
    if(dev->device_secret != NULL) {
        core_os_free(dev->device_secret);
    }

    _core_device_sublist_destroy(dev);

    core_os_mutex_deinit(&dev->alink_id_mutex);

    if(dev->linkconfig.proto_config != NULL) {
        protocol_config_deinit((aiot_protocol_config_t **)&dev->linkconfig.proto_config);
    }

    core_os_free(dev);
    *device = NULL;
    return STATE_SUCCESS;
}

char *aiot_device_name(void *device)
{
    device_handle_t *dev = (device_handle_t *)device;
    if(device == NULL) {
        return NULL;
    }
    return dev->device_name;
}

char *aiot_device_product_key(void *device)
{
    device_handle_t *dev = (device_handle_t *)device;
    if(device == NULL) {
        return NULL;
    }
    return dev->product_key;
}

int32_t aiot_device_online(void *device)
{
    device_handle_t *dev = (device_handle_t *)device;
    if(device == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    return dev->online;
}
