#include <stdio.h>
#include "device_private.h"
#include "aiot_state_api.h"
#include "core_string.h"
#include "device_module.h"
#include "message_service.h"
#include "core_log.h"
#include "core_os.h"
#include "cJSON.h"

#define TAG "DEVICE"

static int32_t _core_device_topic_compare(char *topic, uint32_t topic_len, const char *cmp_topic, uint32_t cmp_topic_len)
{
    uint32_t idx = 0, cmp_idx = 0;

    for (idx = 0, cmp_idx = 0; idx < topic_len; idx++) {
        /* compare topic alreay out of bounds */
        if (cmp_idx >= cmp_topic_len) {
            /* compare success only in case of the left string of topic is "/#" */
            if ((topic_len - idx == 2) && (memcmp(&topic[idx], "/#", 2) == 0)) {
                return STATE_SUCCESS;
            } else {
                return STATE_MQTT_TOPIC_COMPARE_FAILED;
            }
        }

        /* if topic reach the '#', compare success */
        if (topic[idx] == '#') {
            return STATE_SUCCESS;
        }

        if (topic[idx] == '+') {
            /* wildcard + exist */
            for (; cmp_idx < cmp_topic_len; cmp_idx++) {
                if (cmp_topic[cmp_idx] == '/') {
                    /* if topic already reach the bound, compare topic should not contain '/' */
                    if (idx + 1 == topic_len) {
                        return STATE_MQTT_TOPIC_COMPARE_FAILED;
                    } else {
                        break;
                    }
                }
            }
        } else {
            /* compare each character */
            if (topic[idx] != cmp_topic[cmp_idx]) {
                return STATE_MQTT_TOPIC_COMPARE_FAILED;
            }
            cmp_idx++;
        }
    }

    /* compare topic should be reach the end */
    if (cmp_idx < cmp_topic_len) {
        return STATE_MQTT_TOPIC_COMPARE_FAILED;
    }
    return STATE_SUCCESS;
}

core_device_sub_node_t _core_device_research_callback(device_handle_t *device, const char *topic)
{
    core_device_sub_node_t result;
    core_device_sub_node_t *node = NULL, *next = NULL;

    memset(&result, 0, sizeof(result));
    core_os_mutex_lock(device->sub_list_mutex);

    core_list_for_each_entry_safe(node, next, &device->sub_list, linked_node, core_device_sub_node_t) {
        int32_t ret = _core_device_topic_compare(node->topic, strlen(node->topic), topic, strlen(topic));
        if (STATE_SUCCESS == ret) {
            /* exist topic */
            result.callback = node->callback;
            result.userdata = node->userdata;
            break;
        }
    }
    core_os_mutex_unlock(device->sub_list_mutex);

    return result;
}

char *core_device_secret(void *device)
{
    device_handle_t *dev = (device_handle_t *)device;
    if(device == NULL) {
        return NULL;
    }
    return dev->device_secret;
}

int32_t core_device_get_next_alink_id(void *device)
{
    device_handle_t *dev = (device_handle_t *)device;
    int32_t alink_id = 0;
    if(device == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    core_os_mutex_lock(dev->alink_id_mutex);
    dev->alink_id++;
    if(dev->alink_id <= 0) {
        dev->alink_id = 1;
    }

    alink_id = dev->alink_id;
    core_os_mutex_unlock(dev->alink_id_mutex);



    return alink_id;
}

int32_t _cJSON_ADDInt2StrToObject(void *root, char *key, int32_t value)
{
    cJSON *item = (cJSON *)root;
    char value_str[12] = { 0 };
    memset(value_str, 0, sizeof(value_str));

    core_int2str(value, value_str, NULL);
    cJSON_AddStringToObject(item, key, value_str);

    return STATE_SUCCESS;
}

/* 高级组件动态添加 */
int32_t core_device_module_register(void *device, module_type_t key, module_ops_t *ops)
{
    device_handle_t *dev = (device_handle_t *)device;
    if(device == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    if(dev->module_manager == NULL) {
        dev->module_manager = module_init();
        if(dev->module_manager == NULL) {
            ALOG_ERROR(TAG, STATE_DEVICE_UNKOWN_ERROR, "device module manager unkown error\r\n");
            return STATE_DEVICE_UNKOWN_ERROR;
        }
    }

    return module_add_element(dev->module_manager, key, ops);
}
int32_t core_device_module_unregister(void *device, module_type_t key)
{
    device_handle_t *dev = (device_handle_t *)device;
    if(device == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    if(dev->module_manager == NULL) {
        ALOG_ERROR(TAG, STATE_DEVICE_UNKOWN_ERROR, "device module manager unkown error\r\n");
        return STATE_DEVICE_UNKOWN_ERROR;
    }

    return module_delete_element(dev->module_manager, key);
}
void*   core_device_module_get(void *device, module_type_t key)
{
    device_handle_t *dev = (device_handle_t *)device;
    if(device == NULL) {
        return NULL;
    }

    if(dev->module_manager == NULL) {
        return NULL;
    }

    return module_get_element(dev->module_manager, key);
}

int32_t core_device_check_linkconfig(device_handle_t *device)
{
    aiot_protocol_config_t* proto_config = NULL;
    /* 没有连接参数，直接返回错误 */
    if(device->linkconfig.proto_config == NULL) {
        return STATE_DEVICE_MISSING_CONNECT_ARGS;
    }
    proto_config = (aiot_protocol_config_t *)device->linkconfig.proto_config;

    /* 使用密钥三元组进行认证连接 */
    if(device->device_secret != NULL) {
        core_strdup(NULL, &proto_config->product_key, device->product_key, "");
        core_strdup(NULL, &proto_config->device_name, device->device_name, "");
        core_strdup(NULL, &proto_config->device_secret, device->device_secret, "");
        return STATE_SUCCESS;
    }

    /* 使用MQTT自带的username, password进行认证连接 */
    else if(proto_config->username != NULL && proto_config->password) {
        return STATE_SUCCESS;
    }

    /* 使用x509证书进行认证 */
    else if(proto_config->cred.x509_client_cert != NULL && proto_config->cred.x509_client_privkey != NULL) {
        return STATE_SUCCESS;
    }

    return STATE_DEVICE_MISSING_CONNECT_ARGS;
}

int32_t core_device_set_compr_data(void *device, core_device_compress_data_t compress)
{
    device_handle_t *dev = (device_handle_t *)device;
    if(device == NULL) {
        return STATE_PORT_INPUT_NULL_POINTER;
    }

    dev->compress = compress;
    return STATE_SUCCESS;
}

int32_t core_device_set_decompr_data(void *device, core_device_compress_data_t decompress)
{
    device_handle_t *dev = (device_handle_t *)device;
    if(device == NULL) {
        return STATE_PORT_INPUT_NULL_POINTER;
    }

    dev->decompress = decompress;
    return STATE_SUCCESS;
}

int32_t core_device_get_valid_cid(void *device)
{
    device_handle_t *dev = (device_handle_t *)device;
    if(dev->online == 1) {
        return core_msg_find_valid_cid(dev->message_service);
    } else {
        return DEFAULT_CID;
    }
}

static void _core_device_rev_callback(void *device, const aiot_msg_t *message, void *userdata)
{
    device_handle_t *dev = (device_handle_t *)userdata;
    if(dev == NULL) {
        return;
    }

    dev->ops->message_handle(dev, message);
}
static void _core_device_status_callback(void *handle, const core_channel_status_t *event, void *userdata)
{
    device_handle_t *dev = (device_handle_t *)userdata;
    aiot_device_status_t status;
    if(dev == NULL) {
        return;
    }

    switch (event->type) {
    case AIOT_TRANSEVT_RECONNECT:
    case AIOT_TRANSEVT_CONNECT: {
        if(event->online_num == 1)
        {
            status.type = (aiot_device_status_type_t)event->type;
            status.error_code = event->error_code;
            /* 通知设备上线 */
            dev->ops->status_handle(dev, &status);
        }
    }
    break;
    case AIOT_TRANSEVT_DISCONNECT: {
        if(event->online_num == 0)
        {
            status.type = (aiot_device_status_type_t)event->type;
            status.error_code = event->error_code;
            /* 通知设备离线 */
            dev->ops->status_handle(dev, &status);
        }
    }
    break;
    }

    /* 有高级能力使用，通知高级模块通道状态变化 */
    if(dev->module_manager != NULL) {
        module_channel_status_event_notify(dev->module_manager, event);
    }
}

static void _core_device_result_callback(void *handle, const aiot_device_msg_result_t *result, void *userdata)
{
    device_handle_t *dev = (device_handle_t *)userdata;
    if(dev == NULL) {
        return;
    }

    dev->ops->result_handle(dev, result);
}

int32_t normal_connect(void *device)
{
    device_handle_t *dev = (device_handle_t *)device;
    int32_t res;
    if(dev == NULL || dev->linkconfig.proto_config == NULL) {
        ALOG_ERROR(TAG, STATE_DEVICE_MISSING_CONNECT_ARGS, "device connect miss linkconfig\r\n");
        return STATE_DEVICE_MISSING_CONNECT_ARGS;
    }

    if(dev->message_service != NULL) {
        core_msg_service_deinit(&dev->message_service);
    }

    dev->message_service = core_msg_service_init(&dev->linkconfig);
    if(dev->message_service == NULL) {
        ALOG_ERROR(TAG, STATE_DEVICE_UNKOWN_ERROR, "device message service unkown error\r\n");
        return STATE_DEVICE_UNKOWN_ERROR;
    }

    core_msg_service_set_event_callback(dev->message_service, _core_device_rev_callback, _core_device_status_callback, _core_device_result_callback, dev);
    /* 与服务器建立连接 */
    res = core_msg_service_connect(dev->message_service, 1);
    if (res < STATE_SUCCESS) {
        /* 尝试建立连接失败, 销毁MQTT实例, 回收资源 */
        core_msg_service_deinit(&dev->message_service);
        return res;
    }

    return STATE_SUCCESS;
}

int32_t normal_connect_async(void *device)
{
    device_handle_t *dev = (device_handle_t *)device;
    int32_t res;
    if(dev == NULL || dev->linkconfig.proto_config == NULL) {
        ALOG_ERROR(TAG, STATE_DEVICE_MISSING_CONNECT_ARGS, "device connect miss linkconfig\r\n");
        return STATE_DEVICE_MISSING_CONNECT_ARGS;
    }

    if(dev->message_service != NULL) {
        core_msg_service_deinit(&dev->message_service);
    }

    dev->message_service = core_msg_service_init(&dev->linkconfig);
    if(dev->message_service == NULL) {
        ALOG_ERROR(TAG, STATE_DEVICE_UNKOWN_ERROR, "device message service unkown error\r\n");
        return STATE_DEVICE_UNKOWN_ERROR;
    }

    core_msg_service_set_event_callback(dev->message_service, _core_device_rev_callback, _core_device_status_callback, _core_device_result_callback, dev);
    /* 与服务器建立连接 */
    res = core_msg_service_connect(dev->message_service, 0);
    if (res < STATE_SUCCESS) {
        /* 尝试建立连接失败, 销毁MQTT实例, 回收资源 */
        core_msg_service_deinit(&dev->message_service);
        return res;
    }

    return STATE_SUCCESS;
}

int32_t normal_disconnect(void *device)
{
    device_handle_t *dev = (device_handle_t *)device;
    if(device == NULL || dev->message_service == NULL) {
        ALOG_ERROR(TAG, STATE_DEVICE_REPEATED_DISCONNECT, "device has disconnected\r\n");
        return STATE_DEVICE_REPEATED_DISCONNECT;
    }
    dev->has_connected = 0;
    dev->online = 0;
    core_msg_service_disconnect(dev->message_service);

    return core_msg_service_deinit(&dev->message_service);
}

int32_t normal_send_message(void *device, const aiot_msg_t *message)
{
    device_handle_t *dev = (device_handle_t *)device;
    int32_t res = STATE_SUCCESS;
    aiot_msg_t *new_msg = NULL;
    const aiot_msg_t *send_msg = message;
    if(device == NULL || message == NULL) {
        ALOG_ERROR(TAG, STATE_DEVICE_MISSING_CONNECT_ARGS, "device connect miss linkconfig\r\n");
        return STATE_DEVICE_MISSING_CONNECT_ARGS;
    }

    /* 检查是否需要压缩 */
    if(dev->compress.handler != NULL) {
        res = dev->compress.handler(dev->compress.context, message, &new_msg);
        if(res == STATE_COMPRESS_SUCCESS) {
            send_msg = new_msg;
        } else if(res < STATE_SUCCESS) {
            return res;
        }
    }
    if(dev->gateway_device != NULL) {
        res = dev->ops->send_message(device, send_msg);
    } else {
        res = core_msg_service_send(dev->message_service, send_msg);
    }

    if(new_msg != NULL) {
        aiot_msg_delete(new_msg);
    }
    return res;
}
int32_t normal_sub_topic(void *device, const char *topic, uint8_t qos, int32_t cid)
{
    device_handle_t *dev = (device_handle_t *)device;
    if(device == NULL || topic == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    return core_msg_service_sub_topic(dev->message_service, topic, 1, cid);
}
int32_t normal_unsub_topic(void *device, const char *topic, uint8_t qos, int32_t cid)
{
    device_handle_t *dev = (device_handle_t *)device;
    if(device == NULL || topic == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    return core_msg_service_unsub_tpoic(dev->message_service, topic, 1, cid);
}
void normal_message_handle(void *device, const aiot_msg_t *message)
{
    device_handle_t *dev = (device_handle_t *)device;
    aiot_msg_t *new_msg = NULL;
    const aiot_msg_t *recv_msg = message;
    int32_t res = STATE_SUCCESS;
    if(dev == NULL) {
        return;
    }
    /* 查找对应的设备，执行相应的操作 */


    /* 设备维度的流控 */

    /* 执行消息解压缩转换 */
    if(dev->decompress.handler != NULL) {
        res = dev->decompress.handler(dev->decompress.context, message, &new_msg);
        if(res == STATE_COMPRESS_SUCCESS) {
            recv_msg = new_msg;
        } else if(res < STATE_SUCCESS) {
            return;
        }
    }

    /* 执行注册的回调函数 */
    core_device_sub_node_t result = _core_device_research_callback(dev, message->topic);
    if(result.callback != NULL) {
        result.callback(dev, recv_msg, result.userdata);
    }

    if(dev->recv_callback != NULL) {
        dev->recv_callback(dev, recv_msg, dev->userdata);
    }

    if(new_msg != NULL) {
        aiot_msg_delete(new_msg);
    }
}
void normal_status_handle(void *device, const aiot_device_status_t *event)
{
    device_handle_t *dev = (device_handle_t *)device;
    if(dev == NULL) {
        return;
    }

    /* 标准在线状态 */
    if(event->type == AIOT_DEVICE_STATUS_CONNECT || event->type == AIOT_DEVICE_STATUS_RECONNECT) {
        dev->has_connected = 1;
        dev->online = 1;
        ALOG_INFO(TAG, "%s|%s connect success\r\n", dev->product_key, dev->device_name);
    } else {
        dev->online = 0;
        ALOG_INFO(TAG, "%s|%s disconnected\r\n", dev->product_key, dev->device_name);
    }

    /* 有高级能力使用，通知高级模块状态变化 */
    if(dev->module_manager != NULL) {
        module_status_event_notify(dev->module_manager, *event);
    }

    if(dev->status_callback != NULL) {
        dev->status_callback(dev, event, dev->userdata);
    }
}
void normal_result_handle(void *device, const aiot_device_msg_result_t *result)
{
    device_handle_t *dev = (device_handle_t *)device;
    if(dev == NULL) {
        return;
    }

    if(dev->result_callback != NULL) {
        dev->result_callback(dev, result, dev->userdata);
    }
}

device_operation_t normal_ops = {
    .connect = normal_connect,
    .connect_async = normal_connect_async,
    .disconnect = normal_disconnect,
    .send_message = normal_send_message,
    .sub_topic = normal_sub_topic,
    .unsub_topic = normal_unsub_topic,
    .message_handle = normal_message_handle,
    .status_handle = normal_status_handle,
    .result_handle = normal_result_handle,
};