

#ifndef _PROTOCOL_CONFIG_H_
#define _PROTOCOL_CONFIG_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>
#include "aiot_sysdep_api.h"
#include "aiot_message_api.h"

/**
 * @brief 描述消息传输协议实现方法的结构体
 */
typedef struct {
    /**
     * @brief 消息传递初始化
     */
    void *(*init)(void *proto_config);
    /**
     * @brief 建连认证
     */
    int32_t (*connect)(void *handle);
    /**
     * @brief 建连认证
     */
    int32_t (*send_message)(void *handle, const aiot_msg_t *message);
    /**
     * @brief 订阅该topic消息
     */
    int32_t (*sub_topic)(void *handle, const char *topic, uint8_t qos);
    /**
     * @brief 取消该topic消息订阅
     */
    int32_t (*unsub_topic)(void *handle, const char *topic, uint8_t qos);
    /**
     * @brief 断开连接
     */
    int32_t (*disconnect)(void *handle);
    /**
     * @brief 消息传递反初始化
     */
    void (*deinit)(void **handle);
} aiot_protocol_t;

/**
 * @brief PROTOCOL内部事件类型
 */
typedef enum {
    /**
     * @brief 当实例第一次连接网络成功时, 触发此事件
     */
    AIOT_TRANSEVT_CONNECT,
    /**
     * @brief 当实例断开网络连接后重连成功时, 触发此事件
     */
    AIOT_TRANSEVT_RECONNECT,
    /**
     * @brief 当实例断开网络连接时, 触发此事件
     */
    AIOT_TRANSEVT_DISCONNECT
} aiot_protocol_event_type_t;

/**
 * @brief 内部事件
 */
typedef struct {
    /**
     * @brief 内部事件类型. 更多信息请参考@ref aiot_protocol_event_type_t
     *
     */
    aiot_protocol_event_type_t type;
    /**
     * @brief 事件数据联合体
     */
    int32_t error_code;
} aiot_protocol_event_t;

typedef enum {
    TRANS_RESULT_TYPE_MESSAGE,
    TRANS_RESULT_TYPE_REGISTER,
    TRANS_RESULT_TYPE_UNREGISTER,
} aiot_protocol_result_type_t;

typedef struct {
    aiot_protocol_result_type_t type;
    uint16_t message_id;
    int32_t code;
} aiot_protocol_result_t;

typedef void (*aiot_protocol_recv_callback_t)(void *handle, const aiot_msg_t *packet, void *userdata);
typedef void (*aiot_protocol_event_callback_t)(void *handle, const aiot_protocol_event_t *event, void *userdata);
typedef void (*aiot_protocol_result_callback_t)(void *handle, const aiot_protocol_result_t *event, void *userdata);

/**
 * @brief 描述消息传递所需参数配置结构体
 */
typedef struct {
    /* 基础配置信息 */
    char *product_key;
    char *device_name;
    char *device_secret;
    char *host;
    uint16_t port;
    aiot_sysdep_network_cred_t cred;
    aiot_protocol_recv_callback_t recv_callback;
    aiot_protocol_event_callback_t event_callback;
    aiot_protocol_result_callback_t result_callback;
    void *userdata;

    /* 通用配置信息 */
    /* 建连超时时间 */
    int32_t connect_timeout_ms;
    /* 发送超时时间 */
    int32_t send_timeout_ms;
    /* 接收超时时间 */
    int32_t recv_timeout_ms;

    /* 动态注册使用的连接配置 */
    char *product_secret;
    char *instance_id;

    /* MQTT 特有配置 */
    char *username;
    char *password;
    char *clientid;
    char *extend_clientid;
    char *security_mode;
    uint16_t keep_alive_s;
    uint8_t reconnect_enable;
    uint8_t clean_session;
    uint32_t heartbeat_interval_ms;
} aiot_protocol_config_t;

aiot_protocol_config_t *protocol_config_init();
aiot_protocol_config_t *protocol_config_copy(aiot_protocol_config_t *src);
int32_t protocol_config_deinit(aiot_protocol_config_t **config);
int32_t protocol_config_add_ext_clientid(aiot_protocol_config_t *config, const char *extern_clientid);

#if defined(__cplusplus)
}
#endif

#endif