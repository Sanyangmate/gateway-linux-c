#include <stdio.h>
#include <string.h>
#include "protocol_config.h"
#include "aiot_state_api.h"
#include "core_string.h"
#include "core_os.h"

static const uint32_t LINK_DEFUALT_CONNECT_TIMEOUT_MS = 10 * 1000;
static const uint32_t LINK_DEFUALT_SEND_TIMEOUT_MS = 5 * 1000;
static const uint32_t LINK_DEFUALT_RECV_TIMEOUT_MS = 5 * 1000;

static const uint8_t MQTT_DEFAULT_CLEAN_SESSION = 1;
static const uint32_t MQTT_DEFAULT_KEEP_ALIVE_S = 1200;
static const uint32_t MQTT_DEFAULT_HEARTBEAT_INTERVAL_MS = 25 * 1000;
static const uint8_t  MQTT_DEFAULT_RECONNECT = 1;


/* 位于external/ali_ca_cert.c中的服务器证书 */
extern const char *ali_ca_cert;

aiot_protocol_config_t *protocol_config_init()
{
    aiot_protocol_config_t *config = (aiot_protocol_config_t *)core_os_malloc(sizeof(aiot_protocol_config_t));

    memset(config, 0, sizeof(aiot_protocol_config_t));
    config->product_key = NULL;
    config->device_name = NULL;
    config->device_secret = NULL;
    config->host = NULL;
    /* 创建SDK的安全凭据, 用于建立TLS连接 */
    memset(&config->cred, 0, sizeof(aiot_sysdep_network_cred_t));
    config->cred.option = AIOT_SYSDEP_NETWORK_CRED_SVRCERT_CA;  /* 使用RSA证书校验MQTT服务端 */
    config->cred.force_version = AIOT_SYSDEP_NETWORK_TLS_12;
    config->cred.max_tls_fragment = 16384; /* 最大的分片长度为16K, 其它可选值还有4K, 2K, 1K, 0.5K */
    config->cred.sni_enabled = 1;                               /* TLS建连时, 支持Server Name Indicator */
    config->cred.x509_server_cert = ali_ca_cert;                 /* 用来验证MQTT服务端的RSA根证书 */
    config->cred.x509_server_cert_len = strlen(ali_ca_cert);     /* 用来验证MQTT服务端的RSA根证书长度 */

    config->recv_callback = NULL;
    config->event_callback = NULL;
    config->userdata = NULL;

    /* timeout config */
    config->connect_timeout_ms = LINK_DEFUALT_CONNECT_TIMEOUT_MS;
    config->send_timeout_ms = LINK_DEFUALT_SEND_TIMEOUT_MS;
    config->recv_timeout_ms = LINK_DEFUALT_RECV_TIMEOUT_MS;

    /* MQTT config */
    config->clean_session = MQTT_DEFAULT_CLEAN_SESSION;
    config->keep_alive_s = MQTT_DEFAULT_KEEP_ALIVE_S;
    config->heartbeat_interval_ms = MQTT_DEFAULT_HEARTBEAT_INTERVAL_MS;
    config->reconnect_enable = MQTT_DEFAULT_RECONNECT;

    return config;
}

aiot_protocol_config_t *protocol_config_copy(aiot_protocol_config_t *src)
{
    aiot_protocol_config_t *config = protocol_config_init();
    memcpy(config, src, sizeof(aiot_protocol_config_t));

    if(src->product_key != NULL) {
        config->product_key = NULL;
        core_strdup(NULL, &config->product_key, src->product_key, "");
    }
    if(src->device_name != NULL) {
        config->device_name = NULL;
        core_strdup(NULL, &config->device_name, src->device_name, "");
    }
    if(src->device_secret != NULL) {
        config->device_secret = NULL;
        core_strdup(NULL, &config->device_secret, src->device_secret, "");
    }
    if(src->host != NULL) {
        config->host = NULL;
        core_strdup(NULL, &config->host, src->host, "");
    }
    if(src->product_secret != NULL) {
        config->product_secret = NULL;
        core_strdup(NULL, &config->product_secret, src->product_secret, "");
    }
    if(src->instance_id != NULL) {
        config->instance_id = NULL;
        core_strdup(NULL, &config->instance_id, src->instance_id, "");
    }
    if(src->username != NULL) {
        config->username = NULL;
        core_strdup(NULL, &config->username, src->username, "");
    }
    if(src->password != NULL) {
        config->password = NULL;
        core_strdup(NULL, &config->password, src->password, "");
    }
    if(src->clientid != NULL) {
        config->clientid = NULL;
        core_strdup(NULL, &config->clientid, src->clientid, "");
    }
    if(src->extend_clientid != NULL) {
        config->extend_clientid = NULL;
        core_strdup(NULL, &config->extend_clientid, src->extend_clientid, "");
    }
    if(src->security_mode != NULL) {
        config->security_mode = NULL;
        core_strdup(NULL, &config->security_mode, src->security_mode, "");
    }


    return config;
}

int32_t protocol_config_deinit(aiot_protocol_config_t **config)
{
    if(config == NULL || *config == NULL) {
        return -1;
    }

    aiot_protocol_config_t *proto_config = *config;
    if(proto_config->product_key != NULL) {
        core_os_free(proto_config->product_key);
    }
    if(proto_config->device_name != NULL) {
        core_os_free(proto_config->device_name);
    }
    if(proto_config->device_secret != NULL) {
        core_os_free(proto_config->device_secret);
    }
    if(proto_config->host != NULL) {
        core_os_free(proto_config->host);
    }
    if(proto_config->product_secret != NULL) {
        core_os_free(proto_config->product_secret);
    }
    if(proto_config->instance_id != NULL) {
        core_os_free(proto_config->instance_id);
    }
    if(proto_config->username != NULL) {
        core_os_free(proto_config->username);
    }
    if(proto_config->password != NULL) {
        core_os_free(proto_config->password);
    }
    if(proto_config->clientid != NULL) {
        core_os_free(proto_config->clientid);
    }
    if(proto_config->extend_clientid != NULL) {
        core_os_free(proto_config->extend_clientid);
    }
    if(proto_config->security_mode != NULL) {
        core_os_free(proto_config->security_mode);
    }

    core_os_free(*config);
    *config = NULL;

    return STATE_SUCCESS;
}

int32_t protocol_config_add_ext_clientid(aiot_protocol_config_t *config, const char *extern_clientid)
{
    char extclient[256];
    if(config == NULL || extern_clientid == NULL || strlen(extern_clientid) > 256) {
        return -1;
    }

    memset(extclient, 0, sizeof(extclient));
    if(config->extend_clientid == NULL) {
        snprintf(extclient, sizeof(extclient), "%s", extern_clientid);
    } else {
        snprintf(extclient, sizeof(extclient), "%s,%s", config->extend_clientid, extern_clientid);
        core_os_free(config->extend_clientid);
        config->extend_clientid = NULL;
    }

    core_strdup(NULL, &config->extend_clientid, extclient, NULL);

    return STATE_SUCCESS;
}