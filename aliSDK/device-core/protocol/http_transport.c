#include "aiot_linkconfig_api.h"
#include "protocol_config.h"
#include "core_http_client.h"
#include "aiot_state_api.h"
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include "core_log.h"
#include "core_os.h"

typedef struct {
    void *http_handle;
    aiot_protocol_config_t *config;
} aiot_http_protocol_callback_t;

#define TAG "HTTP"

/* HTTP事件回调函数, 当SDK读取到网络报文时被触发, 报文描述类型见core/aiot_http_api.h */
static void http_protocol_recv_callback(void *handle, const aiot_http_recv_t *packet, void *userdata)
{
    switch (packet->type) {
    case AIOT_HTTPRECV_STATUS_CODE: {
        /* TODO: 以下代码如果不被注释, SDK收到HTTP报文时, 会通过这个用户回调打印HTTP状态码, 如404, 200, 302等 */
        /* ALOG_INFO(TAG, "status code: %d\n", packet->data.status_code.code); */
    }
    break;

    case AIOT_HTTPRECV_HEADER: {
        /* TODO: 以下代码如果不被注释, SDK收到HTTP报文时, 会通过这个用户回调打印HTTP首部, 如Content-Length等 */
        /* ALOG_INFO(TAG, "key: %s, value: %s\n", packet->data.header.key, packet->data.header.value);  */
    }
    break;

    /* TODO: 如果需要处理云平台的HTTP回应报文, 修改这里, 现在只是将回应打印出来 */
    case AIOT_HTTPRECV_BODY: {
        ALOG_INFO(TAG, "%.*s\r\n", packet->data.body.len, packet->data.body.buffer);
    }
    break;

    default: {
    }
    break;

    }
}

/* HTTP事件回调函数, 当网络连接/重连/断开时被触发, 事件定义见core/aiot_http_api.h */
static void http_protocol_event_callback(void *handle, const aiot_http_event_t *event, void *userdata)
{
    aiot_http_protocol_callback_t *http_protocol = (aiot_http_protocol_callback_t *)userdata;
    aiot_protocol_event_t trans_event;
    int32_t res = STATE_SUCCESS;
    trans_event.type = AIOT_TRANSEVT_DISCONNECT;
    trans_event.error_code = 0;

    if(http_protocol->config->event_callback != NULL) {
        http_protocol->config->event_callback(http_protocol, &trans_event, http_protocol->config->userdata);
    }

    if (event->type == AIOT_HTTPEVT_TOKEN_INVALID) {
        ALOG_INFO(TAG, "token invalid, invoke iot_http_auth to get new token\n");
        res = aiot_http_auth(handle);
        if(res == STATE_SUCCESS && http_protocol->config->event_callback != NULL) {
            trans_event.type = AIOT_TRANSEVT_RECONNECT;
            http_protocol->config->event_callback(http_protocol, &trans_event, http_protocol->config->userdata);
        }
    }
}

static void *http_protocol_init(void *proto_config)
{
    uint8_t long_connection = 0;
    aiot_http_protocol_callback_t *http_protocol = core_os_malloc(sizeof(aiot_http_protocol_callback_t));
    aiot_protocol_config_t *config = (aiot_protocol_config_t *)proto_config;
    void *http_handle = aiot_http_init();
    http_protocol->http_handle = http_handle;
    http_protocol->config = config;
    /* 配置HTTP服务器地址 */
    aiot_http_setopt(http_handle, AIOT_HTTPOPT_HOST, (void *)config->host);
    /* 配置HTTP服务器端口 */
    aiot_http_setopt(http_handle, AIOT_HTTPOPT_PORT, (void *)&config->port);
    /* 配置设备productKey */
    aiot_http_setopt(http_handle, AIOT_HTTPOPT_PRODUCT_KEY, (void *)config->product_key);
    /* 配置设备deviceName */
    aiot_http_setopt(http_handle, AIOT_HTTPOPT_DEVICE_NAME, (void *)config->device_name);
    /* 配置设备deviceSecret */
    aiot_http_setopt(http_handle, AIOT_HTTPOPT_DEVICE_SECRET, (void *)config->device_secret);
    /* 配置网络连接的安全凭据, 上面已经创建好了 */
    aiot_http_setopt(http_handle, AIOT_HTTPOPT_NETWORK_CRED, (void *)&config->cred);
    /* 配置HTTP默认消息接收回调函数 */
    aiot_http_setopt(http_handle, AIOT_HTTPOPT_RECV_HANDLER, (void *)http_protocol_recv_callback);
    /* 配置HTTP事件回调函数 */
    aiot_http_setopt(http_handle, AIOT_HTTPOPT_EVENT_HANDLER, (void *)http_protocol_event_callback);
    /* 配置回调函数参数 */
    aiot_http_setopt(http_handle, AIOT_HTTPOPT_USERDATA, http_protocol);

    /* 超时配置 */
    aiot_http_setopt(http_handle, AIOT_HTTPOPT_CONNECT_TIMEOUT_MS, &config->connect_timeout_ms);
    aiot_http_setopt(http_handle, AIOT_HTTPOPT_SEND_TIMEOUT_MS, &config->send_timeout_ms);
    aiot_http_setopt(http_handle, AIOT_HTTPOPT_RECV_TIMEOUT_MS, &config->recv_timeout_ms);
    aiot_http_setopt(http_handle, AIOT_HTTPOPT_LONG_CONNECTION, &long_connection);

    return http_protocol;
}

static int32_t http_protocol_connect(void *handle)
{
    aiot_http_protocol_callback_t *http_protocol = (aiot_http_protocol_callback_t *)handle;
    int32_t res = aiot_http_auth(http_protocol->http_handle);
    if (res < STATE_SUCCESS) {
        /* 尝试建立连接失败, 销毁HTTP实例, 回收资源 */
        ALOG_ERROR(TAG, res, "please check variables like http_host, produt_key, device_name, device_secret\r\n");
        return res;
    }

    aiot_protocol_event_t trans_event;
    trans_event.type = AIOT_TRANSEVT_CONNECT;
    trans_event.error_code = 0;

    if(http_protocol->config->event_callback != NULL) {
        http_protocol->config->event_callback(http_protocol, &trans_event, http_protocol->config->userdata);
    }

    return res;
}

static int32_t http_protocol_send_message(void *handle, const aiot_msg_t *message)
{
    aiot_http_protocol_callback_t *http_protocol = (aiot_http_protocol_callback_t *)handle;

    aiot_http_send(http_protocol->http_handle, message->topic, message->payload, message->payload_lenth);
    return aiot_http_recv(http_protocol->http_handle);
}

static int32_t http_protocol_sub_topic(void *handle, const char *topic, uint8_t qos)
{
    ALOG_ERROR(TAG, STATE_NONSUPPORT, "HTTP  protocol non support sub\r\n");
    return STATE_NONSUPPORT;
}

static int32_t http_protocol_unsub_topic(void *handle, const char *topic, uint8_t qos)
{
    ALOG_ERROR(TAG, STATE_NONSUPPORT, "HTTP  protocol non support unsub\r\n");
    return STATE_NONSUPPORT;
}

static int32_t http_protocol_disonnect(void *handle)
{
    return STATE_SUCCESS;
}

static void http_protocol_deinit(void **handle)
{
    aiot_http_protocol_callback_t **http_protocol_ptr = (aiot_http_protocol_callback_t **)handle;
    aiot_http_protocol_callback_t *http_protocol = *http_protocol_ptr;

    aiot_http_deinit(&http_protocol->http_handle);

    core_os_free(http_protocol);
    http_protocol = NULL;
}

aiot_protocol_t http_protocol = {
    .init = http_protocol_init,
    .connect = http_protocol_connect,
    .send_message = http_protocol_send_message,
    .sub_topic = http_protocol_sub_topic,
    .unsub_topic = http_protocol_unsub_topic,
    .disconnect = http_protocol_disonnect,
    .deinit = http_protocol_deinit,
};