#include <stdio.h>
#include <string.h>
#include "aiot_linkconfig_api.h"
#include "protocol_config.h"
#include "core_mqtt_client.h"
#include "aiot_state_api.h"
#include "core_list.h"
#include "core_log.h"
#include "core_os.h"
#include "aiot_message_api.h"

#define TAG  "MQTT"

typedef struct {
    void *mqtt_handle;
    aiot_protocol_config_t *config;
    uint8_t process_thread_running;
    uint8_t recv_thread_running;
    void* process_thread;
    void* recv_thread;
} aiot_mqtt_protocol_callback_t;

/* MQTT事件回调函数, 当网络连接/重连/断开时被触发, 事件定义见core/aiot_mqtt_api.h */
static void mqtt_protocol_event_callback(void *handle, const aiot_mqtt_event_t *event, void *userdata)
{
    aiot_mqtt_protocol_callback_t *protocol_handle = (aiot_mqtt_protocol_callback_t *)userdata;
    aiot_protocol_event_t trans_event;
    trans_event.type = (aiot_protocol_event_type_t)event->type;
    trans_event.error_code = event->data.disconnect;

    if(protocol_handle->config->event_callback != NULL) {
        protocol_handle->config->event_callback(protocol_handle, &trans_event, protocol_handle->config->userdata);
    }
}

/* MQTT默认消息处理回调, 当SDK从服务器收到MQTT消息时, 且无对应用户回调处理时被调用 */
static void mqtt_protocol_recv_callback(void *handle, const aiot_mqtt_recv_t *packet, void *userdata)
{
    aiot_mqtt_protocol_callback_t *mqtt_protocol = (aiot_mqtt_protocol_callback_t *)userdata;
    switch (packet->type) {
    case AIOT_MQTTRECV_HEARTBEAT_RESPONSE: {
        ALOG_INFO(TAG, "heartbeat response\n");
    }
    break;

    case AIOT_MQTTRECV_SUB_ACK: {
        aiot_protocol_result_t result;
        result.type = TRANS_RESULT_TYPE_REGISTER;
        result.message_id = packet->data.sub_ack.packet_id;
        result.code = packet->data.sub_ack.res;
        if(mqtt_protocol->config->result_callback != NULL) {
            mqtt_protocol->config->result_callback(mqtt_protocol, &result, mqtt_protocol->config->userdata);
        }
        ALOG_DEBUG(TAG, "suback, res: -0x%04X, packet id: %d, max qos: %d\n",
                   -packet->data.sub_ack.res, packet->data.sub_ack.packet_id, packet->data.sub_ack.max_qos);
    }
    break;

    case AIOT_MQTTRECV_UNSUB_ACK: {
        aiot_protocol_result_t result;
        result.type = TRANS_RESULT_TYPE_UNREGISTER;
        result.message_id = packet->data.unsub_ack.packet_id;
        result.code = 0;
        if(mqtt_protocol->config->result_callback != NULL) {
            mqtt_protocol->config->result_callback(mqtt_protocol, &result, mqtt_protocol->config->userdata);
        }
        ALOG_DEBUG(TAG, "unsuback, res: -0x%04X, packet id: %d, max qos: %d\n",
                   -packet->data.sub_ack.res, packet->data.sub_ack.packet_id, packet->data.sub_ack.max_qos);
    }
    break;

    case AIOT_MQTTRECV_PUB: {
        char *topic = NULL;
        aiot_msg_t *msg = NULL;
        topic = core_os_malloc(packet->data.pub.topic_len + 1);
        memcpy(topic, packet->data.pub.topic, packet->data.pub.topic_len);
        *(topic + packet->data.pub.topic_len) = 0;
        msg = aiot_msg_create_raw(topic, packet->data.pub.payload, packet->data.pub.payload_len);
        aiot_msg_set_qos(msg, packet->data.pub.qos);

        if(mqtt_protocol->config->recv_callback != NULL) {
            mqtt_protocol->config->recv_callback(mqtt_protocol, msg, mqtt_protocol->config->userdata);
        }

        core_os_free(topic);
        aiot_msg_delete(msg);
    }
    break;

    case AIOT_MQTTRECV_PUB_ACK: {
        aiot_protocol_result_t result;
        result.type = TRANS_RESULT_TYPE_MESSAGE;
        result.message_id = packet->data.pub_ack.packet_id;
        result.code = 0;
        if(mqtt_protocol->config->result_callback != NULL) {
            mqtt_protocol->config->result_callback(mqtt_protocol, &result, mqtt_protocol->config->userdata);
        }
        ALOG_DEBUG(TAG, "puback, packet id: %d\n", packet->data.pub_ack.packet_id);
    }
    break;

    default: {

    }
    }

}

/* 执行aiot_mqtt_process的线程, 包含心跳发送和QoS1消息重发 */
static void *process_thread(void *args)
{
    int32_t res = STATE_SUCCESS;
    aiot_mqtt_protocol_callback_t *mqtt_protocol = (aiot_mqtt_protocol_callback_t *)args;

    while (mqtt_protocol->process_thread_running) {
        res = aiot_mqtt_process(mqtt_protocol->mqtt_handle);
        if (res == STATE_USER_INPUT_EXEC_DISABLED) {
            break;
        }
        core_os_sleep(100);
    }
    return NULL;
}

/* 执行aiot_mqtt_recv的线程, 包含网络自动重连和从服务器收取MQTT消息 */
static void *recv_thread(void *args)
{
    int32_t res = STATE_SUCCESS;
    aiot_mqtt_protocol_callback_t *mqtt_protocol = (aiot_mqtt_protocol_callback_t *)args;

    while (mqtt_protocol->recv_thread_running) {
        res = aiot_mqtt_recv(mqtt_protocol->mqtt_handle);
        if (res < STATE_SUCCESS) {
            if (res == STATE_USER_INPUT_EXEC_DISABLED) {
                break;
            }
        }
        core_os_sleep(10);
    }
    return NULL;
}


static void *mqtt_protocol_init(void *proto_config)
{
    aiot_mqtt_protocol_callback_t *mqtt_protocol = core_os_malloc(sizeof(aiot_mqtt_protocol_callback_t));
    aiot_protocol_config_t *config = (aiot_protocol_config_t *)proto_config;
    void *mqtt_handle = aiot_mqtt_init();
    mqtt_protocol->mqtt_handle = mqtt_handle;
    mqtt_protocol->config = config;
    mqtt_protocol->process_thread = NULL;
    mqtt_protocol->recv_thread = NULL;
    /* 配置MQTT服务器地址 */
    aiot_mqtt_setopt(mqtt_handle, AIOT_MQTTOPT_HOST, (void *)config->host);
    /* 配置MQTT服务器端口 */
    aiot_mqtt_setopt(mqtt_handle, AIOT_MQTTOPT_PORT, (void *)&config->port);
    if(config->username != NULL && config->password != NULL && config->clientid != NULL) {
        /* 配置自定义认证凭据 或者 免白动态注册返回凭据*/
        aiot_mqtt_setopt(mqtt_handle, AIOT_MQTTOPT_USERNAME, (void *)config->username);
        aiot_mqtt_setopt(mqtt_handle, AIOT_MQTTOPT_PASSWORD, (void *)config->password);
        aiot_mqtt_setopt(mqtt_handle, AIOT_MQTTOPT_CLIENTID, (void *)config->clientid);
    } else if(config->cred.x509_client_cert != NULL && config->cred.x509_client_privkey != NULL) {
        /* 配置设备productKey */
        aiot_mqtt_setopt(mqtt_handle, AIOT_MQTTOPT_PRODUCT_KEY, (void *)config->product_key);
        /* 配置设备deviceName */
        aiot_mqtt_setopt(mqtt_handle, AIOT_MQTTOPT_DEVICE_NAME, (void *)config->device_name);
        aiot_mqtt_setopt(mqtt_handle, AIOT_MQTTOPT_DEVICE_SECRET, (void *)"");
    } else {
        /* 配置设备productKey */
        aiot_mqtt_setopt(mqtt_handle, AIOT_MQTTOPT_PRODUCT_KEY, (void *)config->product_key);
        /* 配置设备deviceName */
        aiot_mqtt_setopt(mqtt_handle, AIOT_MQTTOPT_DEVICE_NAME, (void *)config->device_name);
        /* 配置设备deviceSecret */
        aiot_mqtt_setopt(mqtt_handle, AIOT_MQTTOPT_DEVICE_SECRET, (void *)config->device_secret);
        if(config->extend_clientid != NULL) {
            aiot_mqtt_setopt(mqtt_handle, AIOT_MQTTOPT_EXTEND_CLIENTID, (void *)config->extend_clientid);
        }
    }

    /* 配置网络连接的安全凭据, 上面已经创建好了 */
    aiot_mqtt_setopt(mqtt_handle, AIOT_MQTTOPT_NETWORK_CRED, (void *)&config->cred);
    /* 配置MQTT默认消息接收回调函数 */
    aiot_mqtt_setopt(mqtt_handle, AIOT_MQTTOPT_RECV_HANDLER, (void *)mqtt_protocol_recv_callback);
    /* 配置MQTT事件回调函数 */
    aiot_mqtt_setopt(mqtt_handle, AIOT_MQTTOPT_EVENT_HANDLER, (void *)mqtt_protocol_event_callback);

    /* 配置回调函数参数 */
    aiot_mqtt_setopt(mqtt_handle, AIOT_MQTTOPT_USERDATA, mqtt_protocol);

    /* 超时配置 */
    aiot_mqtt_setopt(mqtt_handle, AIOT_MQTTOPT_CONNECT_TIMEOUT_MS, &config->connect_timeout_ms);
    aiot_mqtt_setopt(mqtt_handle, AIOT_MQTTOPT_SEND_TIMEOUT_MS, &config->send_timeout_ms);
    aiot_mqtt_setopt(mqtt_handle, AIOT_MQTTOPT_RECV_TIMEOUT_MS, &config->recv_timeout_ms);

    /* MQTT连接配置 */
    aiot_mqtt_setopt(mqtt_handle, AIOT_MQTTOPT_KEEPALIVE_SEC, &config->keep_alive_s);
    aiot_mqtt_setopt(mqtt_handle, AIOT_MQTTOPT_RECONN_ENABLED, &config->reconnect_enable);
    aiot_mqtt_setopt(mqtt_handle, AIOT_MQTTOPT_CLEAN_SESSION, &config->clean_session);
    aiot_mqtt_setopt(mqtt_handle, AIOT_MQTTOPT_HEARTBEAT_INTERVAL_MS, &config->heartbeat_interval_ms);

    return mqtt_protocol;
}

static int32_t mqtt_protocol_connect(void *handle)
{
    aiot_mqtt_protocol_callback_t *mqtt_protocol = (aiot_mqtt_protocol_callback_t *)handle;
    int32_t res = aiot_mqtt_connect(mqtt_protocol->mqtt_handle);
    if (res < STATE_SUCCESS) {
        /* 尝试建立连接失败, 销毁MQTT实例, 回收资源 */
        ALOG_ERROR(TAG, res, "please check variables like mqtt_host, produt_key, device_name, device_secret\r\n");
        return res;
    } else {
        mqtt_protocol->process_thread_running = 1;
        mqtt_protocol->process_thread = core_os_thread_init("MQTT_PROCESS", process_thread, mqtt_protocol);
        if (mqtt_protocol->process_thread == NULL) {
            ALOG_ERROR(TAG, STATE_PORT_PTHREAD_CREATE_FAILED, "pthread_create recv_thread failed\r\n");
            return STATE_PORT_PTHREAD_CREATE_FAILED;
        }

        mqtt_protocol->recv_thread_running = 1;
        /* 创建一个单独的线程用于执行aiot_mqtt_recv, 它会循环收取服务器下发的MQTT消息, 并在断线时自动重连 */
        mqtt_protocol->recv_thread = core_os_thread_init("MQTT_RECV", recv_thread, mqtt_protocol);
        if (mqtt_protocol->recv_thread == NULL) {
            ALOG_ERROR(TAG, STATE_PORT_PTHREAD_CREATE_FAILED, "pthread_create recv_thread failed\r\n");
            return STATE_PORT_PTHREAD_CREATE_FAILED;
        }
    }

    return res;
}

static int32_t mqtt_protocol_send_message(void *handle, const aiot_msg_t *message)
{
    aiot_mqtt_protocol_callback_t *mqtt_protocol = (aiot_mqtt_protocol_callback_t *)handle;

    return aiot_mqtt_pub(mqtt_protocol->mqtt_handle, message->topic, message->payload, message->payload_lenth, message->qos);
}

static int32_t mqtt_protocol_sub_topic(void *handle, const char *topic, uint8_t qos)
{
    aiot_mqtt_protocol_callback_t *mqtt_protocol = (aiot_mqtt_protocol_callback_t *)handle;
    return aiot_mqtt_sub(mqtt_protocol->mqtt_handle, (char *)topic, NULL, qos, NULL);
}

static int32_t mqtt_protocol_unsub_topic(void *handle, const char *topic, uint8_t qos)
{
    aiot_mqtt_protocol_callback_t *mqtt_protocol = (aiot_mqtt_protocol_callback_t *)handle;
    return aiot_mqtt_unsub(mqtt_protocol->mqtt_handle, (char *)topic);
}

static int32_t mqtt_protocol_disonnect(void *handle)
{
    int32_t res = STATE_SUCCESS;
    aiot_mqtt_protocol_callback_t *mqtt_protocol = (aiot_mqtt_protocol_callback_t *)handle;
    mqtt_protocol->process_thread_running = 0;
    mqtt_protocol->recv_thread_running = 0;

    res = aiot_mqtt_disconnect(mqtt_protocol->mqtt_handle);
    core_os_thread_join(&mqtt_protocol->process_thread);
    core_os_thread_join(&mqtt_protocol->recv_thread);

    return res;
}

static void mqtt_protocol_deinit(void **handle)
{
    aiot_mqtt_protocol_callback_t **mqtt_protocol_ptr = (aiot_mqtt_protocol_callback_t **)handle;
    aiot_mqtt_protocol_callback_t *mqtt_protocol = *mqtt_protocol_ptr;

    aiot_mqtt_disconnect(mqtt_protocol->mqtt_handle);
    aiot_mqtt_deinit(&mqtt_protocol->mqtt_handle);

    core_os_free(mqtt_protocol);
    mqtt_protocol = NULL;
}

aiot_protocol_t mqtt_protocol = {
    .init = mqtt_protocol_init,
    .connect = mqtt_protocol_connect,
    .send_message = mqtt_protocol_send_message,
    .sub_topic = mqtt_protocol_sub_topic,
    .unsub_topic = mqtt_protocol_unsub_topic,
    .disconnect = mqtt_protocol_disonnect,
    .deinit = mqtt_protocol_deinit,
};
