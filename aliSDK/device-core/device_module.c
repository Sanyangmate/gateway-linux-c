#include <stdint.h>
#include "device_module.h"
#include "core_list.h"
#include "aiot_state_api.h"
#include "core_os.h"

typedef struct {
    int32_t type;
    void *handle;
    uint64_t process_stamp;
    module_ops_t ops;
} module_t;

typedef struct {
    /* 高频调用数据结构，采用数组直接寻址 */
    module_t *module_table[MODULE_MAX];
    /* 需要对数组进行操作时先要获取锁 */
    void *table_mutex;
    /* 已经注册在工作的module个数 */
    int32_t work_module_num;
    /* 处理module逻辑的线程 */
    void *thread;
    /* 线程退出的标志 */
    int8_t thread_exit;
} module_manager_t;

static void *_module_daemon_process_thread(void *argv) {
    int i = 0;
    module_t *module = NULL;
    module_t *module_table[MODULE_MAX];
    uint64_t time_now = 0;
    module_manager_t *manager = (module_manager_t *)argv;
    if(manager == NULL) {
        return NULL;
    }

    while(manager->thread_exit) {
        core_os_mutex_lock(manager->table_mutex);
        memcpy(module_table, manager->module_table, sizeof(module_t *) * MODULE_MAX);
        core_os_mutex_unlock(manager->table_mutex);
        /* 调用高级组件的处理回调函数 */
        for(i = 0; i < MODULE_MAX; i++ ) {
            if(module_table[i] != NULL ) {
                module = module_table[i];
                time_now = core_os_time();
                if(module->ops.on_process != NULL && time_now - module->process_stamp >= module->ops.internal_ms) {
                    module->process_stamp = time_now;
                    module->ops.on_process(module->handle);
                }
            }
        }
        core_os_sleep(100);
    }

    return NULL;
}

void module_start_daemon_thread(module_manager_t *manager)
{
    manager->thread_exit = 1;
    manager->thread = core_os_thread_init("MODULE_DAEMON", _module_daemon_process_thread, manager);
}
void module_stop_daemon_thread(module_manager_t *manager)
{
    manager->thread_exit = 0;
    if(manager->thread != NULL) {
        core_os_thread_join(&manager->thread);
    }
}

void* module_init()
{
    module_manager_t *manager = (module_manager_t *)core_os_malloc(sizeof(module_manager_t));
    if(manager == NULL) {
        return NULL;
    }
    memset(manager, 0, sizeof(module_manager_t));

    manager->table_mutex = core_os_mutex_init();
    if(manager->table_mutex == NULL) {
        core_os_free(manager);
        return NULL;
    }

    return manager;
}

int32_t module_add_element(void *handle, module_type_t key, module_ops_t *ops)
{
    module_t *module = NULL;
    module_manager_t *manager = (module_manager_t *)handle;
    if(manager == NULL || key >= MODULE_MAX || ops == NULL || ops->on_init == NULL || ops->on_deinit == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    if(manager->module_table[key] != NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    module = core_os_malloc(sizeof(module_t));
    if(module == NULL) {
        return STATE_SYS_DEPEND_MALLOC_FAILED;
    }

    memset(module, 0, sizeof(module_t));
    module->ops = *ops;
    module->type = key;
    module->handle = ops->on_init();

    core_os_mutex_lock(manager->table_mutex);
    manager->module_table[key] = module;
    manager->work_module_num++;
    /* 如果线程未开启，开启线程 */
    if(manager->thread_exit == 0 && ops->on_process != NULL) {
        module_start_daemon_thread(manager);
    }
    core_os_mutex_unlock(manager->table_mutex);

    return STATE_SUCCESS;
}

int32_t module_delete_element(void *handle, module_type_t key)
{
    module_t *module = NULL;
    module_manager_t *manager = (module_manager_t *)handle;
    if(manager == NULL || key >= MODULE_MAX ) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    if(manager->module_table[key] != NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    module = manager->module_table[key];
    module->ops.on_deinit(module->handle);

    core_os_free(module);
    core_os_mutex_lock(manager->table_mutex);
    manager->module_table[key] = NULL;
    manager->work_module_num--;
    /* 如果线程未开启，开启线程 */
    if(manager->work_module_num == 0 && manager->thread_exit == 1) {
        module_stop_daemon_thread(manager);
    }
    core_os_mutex_unlock(manager->table_mutex);

    return STATE_SUCCESS;
}
void* module_get_element(void *handle, module_type_t key)
{
    module_t *module = NULL;
    module_manager_t *manager = (module_manager_t *)handle;
    if(manager == NULL || key >= MODULE_MAX || manager->module_table[key] == NULL) {
        return NULL;
    }

    module = manager->module_table[key];

    return module->handle;
}

int32_t module_status_event_notify(void *handle, aiot_device_status_t event)
{
    int32_t i = 0;
    module_t *module = NULL;
    module_manager_t *manager = (module_manager_t *)handle;
    if(manager == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    core_os_mutex_lock(manager->table_mutex);
    /* 调用高级组件的状态变化回调函数 */
    for(i = 0; i < MODULE_MAX; i++ ) {
        if(manager->module_table[i] != NULL) {
            module = manager->module_table[i];
            if(module->ops.on_status != NULL) {
                module->ops.on_status(module->handle, event);
            }
        }
    }
    core_os_mutex_unlock(manager->table_mutex);

    return STATE_SUCCESS;
}
int32_t module_channel_status_event_notify(void *handle, const core_channel_status_t *event)
{
    int32_t i = 0;
    module_t *module = NULL;
    module_manager_t *manager = (module_manager_t *)handle;
    if(manager == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    core_os_mutex_lock(manager->table_mutex);
    /* 调用高级组件的状态变化回调函数 */
    for(i = 0; i < MODULE_MAX; i++ ) {
        if(manager->module_table[i] != NULL) {
            module = manager->module_table[i];
            if(module->ops.on_channel_status != NULL) {
                module->ops.on_channel_status(module->handle, event);
            }
        }
    }
    core_os_mutex_unlock(manager->table_mutex);

    return STATE_SUCCESS;
}

int32_t module_deinit(void **handle)
{
    int i = 0;
    module_t *module = NULL;
    module_manager_t *manager = NULL;
    if(handle == NULL || *handle == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    manager = *(module_manager_t **)handle;
    /* 回收线程资源 */
    module_stop_daemon_thread(manager);
    core_os_mutex_lock(manager->table_mutex);
    /* 删除高级组件 */
    for(i = 0; i < MODULE_MAX; i++ ) {
        if(manager->module_table[i] != NULL ) {
            module = manager->module_table[i];
            module->ops.on_deinit(module->handle);
            core_os_free(module);
            manager->module_table[i] = NULL;
        }
    }
    core_os_mutex_unlock(manager->table_mutex);
    core_os_mutex_deinit(&manager->table_mutex);

    core_os_free(manager);
    *handle = NULL;

    return STATE_SUCCESS;
}