#ifndef _CORE_ADAPTER_H_
#define _CORE_ADAPTER_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include "core_stdinc.h"
#include "aiot_sysdep_api.h"


/* 网络适配模块方法 */
typedef struct {
    int32_t (*establish)(void *handle);
    int32_t (*recv)(void *handle, uint8_t *buffer, uint32_t len, uint32_t timeout_ms, core_sysdep_addr_t *addr);
    int32_t (*send)(void *handle, uint8_t *buffer, uint32_t len, uint32_t timeout_ms, core_sysdep_addr_t *addr);
    int32_t (*deinit)(void *handle);
} adapter_operation_t;

/* 网络适配模块核心数据结构 */
typedef struct {
    void *network_handle;
    core_sysdep_socket_type_t socket_type;
    aiot_sysdep_network_cred_t *cred;
    char *host;
    char backup_ip[16];
    uint16_t port;
    uint32_t connect_timeout_ms;
    core_sysdep_psk_t psk;
    void *tls_handle;
    adapter_operation_t *ops;
} adapter_network_handle_t;

void*   core_network_init(void);
int32_t core_network_setopt(void *handle, core_sysdep_network_option_t option, void *data);
int32_t core_network_establish(void *handle);
int32_t core_network_recv(void *handle, uint8_t *buffer, uint32_t len, uint32_t timeout_ms,
                          core_sysdep_addr_t *addr);
int32_t core_network_send(void *handle, uint8_t *buffer, uint32_t len, uint32_t timeout_ms,
                          core_sysdep_addr_t *addr);
int32_t core_network_deinit(void **handle);
void core_network_set_origin_portfile(aiot_sysdep_portfile_t *portfile);
aiot_sysdep_portfile_t *aiot_sysdep_get_adapter_portfile();

#if defined(__cplusplus)
}
#endif

#endif
