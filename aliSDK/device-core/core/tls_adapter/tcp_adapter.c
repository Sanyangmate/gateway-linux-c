#include "core_log.h"
#include "core_os.h"
#include "core_adapter.h"

int32_t core_tcp_establish(void *handle)
{
    adapter_network_handle_t *adapter_handle = (adapter_network_handle_t *)handle;
    aiot_sysdep_portfile_t *g_origin_portfile = aiot_sysdep_get_portfile();

    return g_origin_portfile->core_sysdep_network_establish(adapter_handle->network_handle);
}
int32_t core_tcp_recv(void *handle, uint8_t *buffer, uint32_t len, uint32_t timeout_ms,
                          core_sysdep_addr_t *addr)
{
    adapter_network_handle_t *adapter_handle = (adapter_network_handle_t *)handle;
    aiot_sysdep_portfile_t *g_origin_portfile = aiot_sysdep_get_portfile();
    return g_origin_portfile->core_sysdep_network_recv(adapter_handle->network_handle, buffer, len, timeout_ms, addr);
}
int32_t core_tcp_send(void *handle, uint8_t *buffer, uint32_t len, uint32_t timeout_ms,
                          core_sysdep_addr_t *addr)
{
    adapter_network_handle_t *adapter_handle = (adapter_network_handle_t *)handle;
    aiot_sysdep_portfile_t *g_origin_portfile = aiot_sysdep_get_portfile();
    return g_origin_portfile->core_sysdep_network_send(adapter_handle->network_handle, buffer, len, timeout_ms, addr);
}
int32_t core_tcp_deinit(void *handle)
{
    return STATE_SUCCESS;
}

static adapter_operation_t tcp_adapter = {
    .establish = core_tcp_establish,
    .recv = core_tcp_recv,
    .send = core_tcp_send,
    .deinit = core_tcp_deinit,
};

adapter_operation_t *core_tcp_adapter()
{
    return &tcp_adapter;
}