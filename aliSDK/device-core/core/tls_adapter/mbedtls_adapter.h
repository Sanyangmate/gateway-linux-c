#ifndef _MBEDTLS_ADAPTER_H_
#define _MBEDTLS_ADAPTER_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include "core_adapter.h"

adapter_operation_t *core_mbedtls_adapter();

#if defined(__cplusplus)
}
#endif

#endif
