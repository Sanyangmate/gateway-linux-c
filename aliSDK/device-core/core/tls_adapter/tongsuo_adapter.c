#ifdef GMSSL
#include "tongsuo_adapter.h"
#include "core_log.h"
#include "core_os.h"
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

#define TAG "TONGSUO"

typedef struct {
    int sock;
    SSL_CTX *ssl_ctx;
    SSL *conn;
} core_sysdep_tls_t;

static uint8_t _host_is_ip(char *host)
{
    uint32_t idx = 0;

    if (strlen(host) >= 16) {
        return 0;
    }

    for (idx = 0; idx < strlen(host); idx++) {
        if ((host[idx] != '.') && (host[idx] < '0' || host[idx] > '9')) {
            return 0;
        }
    }

    return 1;
}

static int32_t core_tls_establish(void *handle)
{
    adapter_network_handle_t *adapter_handle = (adapter_network_handle_t *)handle;
    core_sysdep_tls_t *tongsuo = NULL;
    const SSL_METHOD *method = NULL;
    BIO * cbio = NULL;
    X509 * cert = NULL;
    X509_STORE* cert_store = NULL;
    struct hostent *hp;
    struct sockaddr_in server, client;
    socklen_t addr_len = sizeof(client);
    int32_t res = STATE_SUCCESS;

    tongsuo = core_os_malloc(sizeof(core_sysdep_tls_t));
    if(tongsuo == NULL) {
        return STATE_PORT_MALLOC_FAILED;
    }
    memset(tongsuo, 0, sizeof(core_sysdep_tls_t));

    ALOG_INFO(TAG, "tls(SM) connecting with server(host='%s', port=[%d])\r\n", adapter_handle->host, adapter_handle->port);

    /* Create context used by both client and server */
    method = TLS_client_method();
    tongsuo->ssl_ctx = SSL_CTX_new(method);
    if (tongsuo->ssl_ctx == NULL) {
        return -1;
    }

    /* 设置服务端证书校验 */
    SSL_CTX_set_verify(tongsuo->ssl_ctx, SSL_VERIFY_PEER, NULL);
    cbio = BIO_new_mem_buf(adapter_handle->cred->x509_server_cert, strlen(adapter_handle->cred->x509_server_cert));
    cert = PEM_read_bio_X509(cbio, NULL, 0, NULL);
    cert_store = SSL_CTX_get_cert_store(tongsuo->ssl_ctx);
    X509_STORE_add_cert(cert_store, cert);
    X509_free(cert);
    BIO_free(cbio);

    /* 设置为TLS 1.3， 套件为TLS_SM4_GCM_SM3 */
    SSL_CTX_set_min_proto_version(tongsuo->ssl_ctx, TLS1_3_VERSION);
    SSL_CTX_set_max_proto_version(tongsuo->ssl_ctx, TLS1_3_VERSION);
    SSL_CTX_set_ciphersuites(tongsuo->ssl_ctx, "TLS_SM4_GCM_SM3");

    /* 完成socket建连 */
    tongsuo->sock = socket(AF_INET, SOCK_STREAM, 0);
    if(tongsuo->sock == -1) {
        return STATE_PORT_TLS_SOCKET_CREATE_FAILED;
    }
    hp = gethostbyname(adapter_handle->host);
    server.sin_addr = *((struct in_addr *)hp->h_addr_list[0]);
    server.sin_family = AF_INET;
    server.sin_port = htons(adapter_handle->port);
   
    if (connect(tongsuo->sock, (struct sockaddr*) &server, sizeof(server)) != 0) {
        ALOG_INFO(TAG, "TCP connection Failed\n");
        return -1;
    } else {
        res = getsockname(tongsuo->sock, (struct sockaddr *)&client, &addr_len);
        if(res == -1) {
            ALOG_INFO(TAG, "get socket name failed. errno: %d, error: %s", errno, strerror(errno));
            return -1;
        }
        ALOG_INFO(TAG, "TCP connection to server successful\n");
        ALOG_INFO(TAG, "local port: %u\n", ntohs(client.sin_port));
    }

    /* Create client SSL structure using dedicated client socket */
    tongsuo->conn = SSL_new(tongsuo->ssl_ctx);
    SSL_set_fd(tongsuo->conn, tongsuo->sock);

    if(_host_is_ip(adapter_handle->host) == 0) {
        /* Set hostname for SNI */
        SSL_set_tlsext_host_name(tongsuo->conn, adapter_handle->host);
        /* Configure server hostname check */
        SSL_set1_host(tongsuo->conn, adapter_handle->host);
    }

    res = SSL_connect(tongsuo->conn);
    if (res == 1) {
        ALOG_INFO(TAG, "SSL connection to server successful\r\n");
    } else {
        ALOG_INFO(TAG, "SSL connection failed,res %d\r\n", res);
        ERR_print_errors_fp(stderr);
    }
    adapter_handle->tls_handle = tongsuo;

    /*
    SSL_SESSION *ss = SSL_get_session(tongsuo->conn);
    BIO *b = BIO_new_fp(stdout, BIO_NOCLOSE | BIO_FP_TEXT);
    SSL_SESSION_print(b, ss);
    */

    return STATE_SUCCESS;
}

static int32_t core_tls_recv(void *handle, uint8_t *buffer, uint32_t len, uint32_t timeout_ms,
                             core_sysdep_addr_t *addr)
{
    adapter_network_handle_t *adapter_handle = (adapter_network_handle_t *)handle;
    core_sysdep_tls_t *tls = (core_sysdep_tls_t *)adapter_handle->tls_handle;
    int32_t recv_bytes = 0;
    size_t recv_len = 0;
    uint64_t timestart_ms = 0, timenow_ms = 0;
    fd_set recv_sets;
    int32_t res = STATE_SUCCESS;
    struct timeval timestart, timenow;

    FD_ZERO(&recv_sets);
    FD_SET(tls->sock, &recv_sets);

    /* Start Time */
    gettimeofday(&timestart, NULL);
    timestart_ms = timestart.tv_sec * 1000 + timestart.tv_usec / 1000;
    timenow_ms = timestart_ms;

    do {
        gettimeofday(&timenow, NULL);
        timenow_ms = timenow.tv_sec * 1000 + timenow.tv_usec / 1000;

        if (timenow_ms - timestart_ms >= timenow_ms ||
                timeout_ms - (timenow_ms - timestart_ms) > timeout_ms) {
            break;
        }

        res = SSL_read_ex(tls->conn, (uint8_t *)buffer + recv_bytes, len - recv_bytes, &recv_len);
        if (res >= 0) {
            recv_bytes += recv_len;
            /* printf("recv_bytes: %d, len: %d\n",recv_bytes,len); */
            if (recv_bytes == len) {
                break;
            }
        } else {
            // perror("_core_sysdep_network_recv, nwk recv error: ");
            // return STATE_PORT_NETWORK_RECV_FAILED;
            break;
        }
    } while (((timenow_ms - timestart_ms) < timeout_ms) && (recv_bytes < len));

    /* ALOG_HEX(TAG, LOG_LEVEL_INFO, '<', buffer, recv_bytes); */
    /* printf("%s: recv over\n",__FUNCTION__); */
    return recv_bytes;
}
static int32_t core_tls_send(void *handle, uint8_t *buffer, uint32_t len, uint32_t timeout_ms,
                             core_sysdep_addr_t *addr)
{
    int32_t res = 0, send_len = 0, total_sent = 0;
    size_t sent_len = 0;
    adapter_network_handle_t *adapter_handle = (adapter_network_handle_t *)handle;
    core_sysdep_tls_t *tls = (core_sysdep_tls_t *)adapter_handle->tls_handle;

    while(len) {
        send_len = len;
        res = SSL_write(tls->conn, (uint8_t *)(buffer + total_sent), send_len);
        if(res <= 0) {
            return STATE_PORT_TLS_SEND_FAILED;
        }else {
            sent_len = send_len;
        }
        total_sent += sent_len;
        len -= sent_len;
    }
    /* ALOG_HEX(TAG, LOG_LEVEL_INFO, '>', buffer, total_sent); */

    return total_sent;
}
static int32_t core_tls_deinit(void *handle)
{
    adapter_network_handle_t *adapter_handle = (adapter_network_handle_t *)handle;
    core_sysdep_tls_t *tls = (core_sysdep_tls_t *)adapter_handle->tls_handle;

    if(tls != NULL) {
        if (tls->conn != NULL) {
            SSL_shutdown(tls->conn);
            SSL_free(tls->conn);
        }
        SSL_CTX_free(tls->ssl_ctx);

        if (tls->sock != -1) {
            close(tls->sock);
            tls->sock = -1;
        }

        core_os_free(tls);
        adapter_handle->tls_handle = NULL;
    }

    return STATE_SUCCESS;
}

static adapter_operation_t tongsuo_adapter = {
    .establish = core_tls_establish,
    .recv = core_tls_recv,
    .send = core_tls_send,
    .deinit = core_tls_deinit,
};

adapter_operation_t *core_tls_adapter()
{
    return &tongsuo_adapter;
}
#endif