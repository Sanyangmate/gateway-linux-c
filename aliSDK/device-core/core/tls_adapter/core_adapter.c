#include "core_adapter.h"
#include "aiot_state_api.h"
#include "core_log.h"
#include "mbedtls_adapter.h"
#include "tcp_adapter.h"
#include "tongsuo_adapter.h"

static aiot_sysdep_portfile_t *g_origin_portfile = NULL;
static aiot_sysdep_portfile_t g_aiot_portfile;

#define TAG "TLS"

void *core_network_init(void)
{
    adapter_network_handle_t *adapter_handle = NULL;

    adapter_handle = g_origin_portfile->core_sysdep_malloc(sizeof(adapter_network_handle_t), "TLS");
    if (adapter_handle == NULL) {
        return NULL;
    }
    memset(adapter_handle, 0, sizeof(adapter_network_handle_t));
    adapter_handle->network_handle = g_origin_portfile->core_sysdep_network_init();
    adapter_handle->ops = core_tcp_adapter();
    return adapter_handle;
}

int32_t core_network_setopt(void *handle, core_sysdep_network_option_t option, void *data)
{
    adapter_network_handle_t *adapter_handle = (adapter_network_handle_t *)handle;
    int32_t res = STATE_SUCCESS;

    if (handle == NULL || data == NULL) {
        return STATE_PORT_INPUT_NULL_POINTER;
    }

    if (option >= CORE_SYSDEP_NETWORK_MAX) {
        return STATE_PORT_INPUT_OUT_RANGE;
    }
    res = g_origin_portfile->core_sysdep_network_setopt(adapter_handle->network_handle, option, data);

    switch (option) {
    case CORE_SYSDEP_NETWORK_SOCKET_TYPE: {
        adapter_handle->socket_type = *(core_sysdep_socket_type_t *)data;
    }
    break;
    case CORE_SYSDEP_NETWORK_HOST: {
        adapter_handle->host = g_origin_portfile->core_sysdep_malloc(strlen(data) + 1, "TLS");
        if (adapter_handle->host == NULL) {
            ALOG_ERROR(TAG, STATE_PORT_MALLOC_FAILED, "core_sysdep_malloc failed\r\n");
            return STATE_PORT_MALLOC_FAILED;
        }
        memset(adapter_handle->host, 0, strlen(data) + 1);
        memcpy(adapter_handle->host, data, strlen(data));
    }
    break;
    case CORE_SYSDEP_NETWORK_BACKUP_IP: {
        memcpy(adapter_handle->backup_ip, data, strlen(data));
    }
    break;
    case CORE_SYSDEP_NETWORK_PORT: {
        adapter_handle->port = *(uint16_t *)data;
    }
    break;
    case CORE_SYSDEP_NETWORK_CONNECT_TIMEOUT_MS: {
        adapter_handle->connect_timeout_ms = *(uint32_t *)data;
    }
    break;

    case CORE_SYSDEP_NETWORK_CRED: {
        adapter_handle->cred = g_origin_portfile->core_sysdep_malloc(sizeof(aiot_sysdep_network_cred_t), "TLS");
        if (adapter_handle->cred == NULL) {
            ALOG_ERROR(TAG, STATE_PORT_MALLOC_FAILED, "core_sysdep_malloc failed\r\n");
            return STATE_PORT_MALLOC_FAILED;
        }
        memset(adapter_handle->cred, 0, sizeof(aiot_sysdep_network_cred_t));
        memcpy(adapter_handle->cred, data, sizeof(aiot_sysdep_network_cred_t));
        if(adapter_handle->cred->gm_ssl) {
            #ifdef GMSSL
            adapter_handle->ops = core_tls_adapter();
            #else
            g_origin_portfile->core_sysdep_free(adapter_handle->cred);
            adapter_handle->cred = NULL;
            return STATE_PORT_INPUT_OUT_RANGE;
            #endif
        } else if (adapter_handle->cred != NULL && adapter_handle->cred->option != AIOT_SYSDEP_NETWORK_CRED_NONE) {
            adapter_handle->ops = core_mbedtls_adapter();
        } else {
            adapter_handle->ops = core_tcp_adapter();
        }
    }
    break;
    case CORE_SYSDEP_NETWORK_PSK: {
        core_sysdep_psk_t *psk = (core_sysdep_psk_t *)data;
        adapter_handle->psk.psk_id = g_origin_portfile->core_sysdep_malloc(strlen(psk->psk_id) + 1, "TLS");
        if (adapter_handle->psk.psk_id == NULL) {
            ALOG_ERROR(TAG, STATE_PORT_MALLOC_FAILED, "core_sysdep_malloc failed\r\n");
            return STATE_PORT_MALLOC_FAILED;
        }
        memset(adapter_handle->psk.psk_id, 0, strlen(psk->psk_id) + 1);
        memcpy(adapter_handle->psk.psk_id, psk->psk_id, strlen(psk->psk_id));
        adapter_handle->psk.psk = g_origin_portfile->core_sysdep_malloc(strlen(psk->psk) + 1, "TLS");
        if (adapter_handle->psk.psk == NULL) {
            g_origin_portfile->core_sysdep_free(adapter_handle->psk.psk_id);
            ALOG_ERROR(TAG, STATE_PORT_MALLOC_FAILED, "core_sysdep_malloc failed\r\n");
            return STATE_PORT_MALLOC_FAILED;
        }
        memset(adapter_handle->psk.psk, 0, strlen(psk->psk) + 1);
        memcpy(adapter_handle->psk.psk, psk->psk, strlen(psk->psk));
    }
    break;

    default: {
        ALOG_ERROR(TAG, STATE_ADAPTER_ERROR, "adapter_network_setopt unkown option %d\r\n", option);
    }
    break;
    }

    return res;
}

int32_t core_network_establish(void *handle)
{
    adapter_network_handle_t *adapter_handle = (adapter_network_handle_t *)handle;
    if (handle == NULL) {
        return STATE_PORT_INPUT_NULL_POINTER;
    }

    return adapter_handle->ops->establish(adapter_handle);
}
int32_t core_network_recv(void *handle, uint8_t *buffer, uint32_t len, uint32_t timeout_ms,
                          core_sysdep_addr_t *addr)
{
    adapter_network_handle_t *adapter_handle = (adapter_network_handle_t *)handle;
    if (handle == NULL) {
        return STATE_PORT_INPUT_NULL_POINTER;
    }

    return adapter_handle->ops->recv(handle, buffer, len, timeout_ms, addr);
}
int32_t core_network_send(void *handle, uint8_t *buffer, uint32_t len, uint32_t timeout_ms,
                          core_sysdep_addr_t *addr)
{
    adapter_network_handle_t *adapter_handle = (adapter_network_handle_t *)handle;
    if (handle == NULL) {
        return STATE_PORT_INPUT_NULL_POINTER;
    }

    return adapter_handle->ops->send(handle, buffer, len, timeout_ms, addr);
}

int32_t core_network_deinit(void **handle)
{
    adapter_network_handle_t *adapter_handle = NULL;

    if (handle == NULL || *handle == NULL) {
        return STATE_PORT_INPUT_NULL_POINTER;
    }
    adapter_handle = *(adapter_network_handle_t **)handle;

    if (adapter_handle->host != NULL) {
        g_origin_portfile->core_sysdep_free(adapter_handle->host);
        adapter_handle->host = NULL;
    }
    if(adapter_handle->ops != NULL) {
        adapter_handle->ops->deinit(adapter_handle);
    }
    if (adapter_handle->psk.psk_id != NULL) {
        g_origin_portfile->core_sysdep_free(adapter_handle->psk.psk_id);
        adapter_handle->psk.psk_id = NULL;
    }
    if (adapter_handle->psk.psk != NULL) {
        g_origin_portfile->core_sysdep_free(adapter_handle->psk.psk);
        adapter_handle->psk.psk = NULL;
    }

    if (adapter_handle->cred != NULL) {
        g_origin_portfile->core_sysdep_free(adapter_handle->cred);
        adapter_handle->cred = NULL;
    }

    ALOG_INFO(TAG, "adapter_network_deinit\r\n");
    g_origin_portfile->core_sysdep_network_deinit(&adapter_handle->network_handle);
    g_origin_portfile->core_sysdep_free(adapter_handle);
    *handle = NULL;

    return STATE_SUCCESS;
}

void core_network_set_origin_portfile(aiot_sysdep_portfile_t *portfile)
{
    g_origin_portfile = portfile;
}

aiot_sysdep_portfile_t *aiot_sysdep_get_adapter_portfile()
{
    if(g_origin_portfile == NULL) {
        return NULL;
    }
    g_aiot_portfile = *g_origin_portfile;
    g_aiot_portfile.core_sysdep_network_init = core_network_init;
    g_aiot_portfile.core_sysdep_network_setopt = core_network_setopt;
    g_aiot_portfile.core_sysdep_network_establish = core_network_establish;
    g_aiot_portfile.core_sysdep_network_recv = core_network_recv;
    g_aiot_portfile.core_sysdep_network_send = core_network_send;
    g_aiot_portfile.core_sysdep_network_deinit = core_network_deinit;
    return &g_aiot_portfile;
}