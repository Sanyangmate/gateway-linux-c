#ifndef _TONGSUO_ADAPTER_H_
#define _TONGSUO_ADAPTER_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include "core_adapter.h"

adapter_operation_t *core_tls_adapter();

#if defined(__cplusplus)
}
#endif

#endif
