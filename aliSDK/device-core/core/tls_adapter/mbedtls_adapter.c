#include "core_adapter.h"
#include "aiot_state_api.h"
#include "core_log.h"
#include "core_os.h"

/*
 *  CORE_ADAPTER_MBEDTLS_ENABLED 不是一个用户需要关心的编译开关
 *
 *  大多数情况下, 就保持它如下的设置即可
 *  只有少数时候, SDK的用户关心对接层代码的ROM尺寸, 并且也没有选择用TLS连接服务器
 *  那时才会出现, 将 CORE_ADAPTER_MBEDTLS_ENABLED 宏定义关闭的改动, 以减小对接尺寸
 *
 *  我们不建议去掉 #define CORE_ADAPTER_MBEDTLS_ENABLED 这行代码
 *  虽然物联网平台接收TCP方式的连接, 但我们不推荐这样做, TLS是更安全的通信方式
 *
 */
#define CORE_ADAPTER_MBEDTLS_ENABLED
// #define CORE_ADAPTER_MBEDTLS_PRINT_KEYLOG


#ifdef CORE_ADAPTER_MBEDTLS_ENABLED
#include "mbedtls_adapter.h"
#include "mbedtls_config.h"
#include "mbedtls/net_sockets.h"
#include "mbedtls/ssl.h"
#include "mbedtls/ctr_drbg.h"
#include "mbedtls/debug.h"
#include "mbedtls/platform.h"
#include "mbedtls/timing.h"
#include "crypto.h"
#include "mbedtls/entropy.h"

#define TAG "MBEDTLS"
static aiot_sysdep_portfile_t *g_origin_portfile = NULL;

typedef struct {
    mbedtls_net_context          net_ctx;
    mbedtls_ssl_context          ssl_ctx;
    mbedtls_ssl_config           ssl_config;
    mbedtls_timing_delay_context timer_delay_ctx;
    mbedtls_x509_crt             x509_server_cert;
    mbedtls_x509_crt             x509_client_cert;
    mbedtls_pk_context           x509_client_pk;
} core_sysdep_mbedtls_t;

#define MBEDTLS_MEM_INFO_MAGIC  (0x12345678)

static uint32_t g_mbedtls_total_mem_used = 0;
static uint32_t g_mbedtls_max_mem_used = 0;
typedef struct {
    int32_t magic;
    int32_t size;
} mbedtls_mem_info_t;

static uint8_t _host_is_ip(char *host)
{
    uint32_t idx = 0;

    if (strlen(host) >= 16) {
        return 0;
    }

    for (idx = 0; idx < strlen(host); idx++) {
        if ((host[idx] != '.') && (host[idx] < '0' || host[idx] > '9')) {
            return 0;
        }
    }

    return 1;
}

static void *_core_mbedtls_calloc(size_t n, size_t size)
{
    uint8_t *buf = NULL;
    mbedtls_mem_info_t *mem_info = NULL;

    if (n == 0 || size == 0) {
        return NULL;
    }
    buf = (uint8_t *)core_os_malloc(n * size + sizeof(mbedtls_mem_info_t));
    if (NULL == buf) {
        ALOG_ERROR(TAG, STATE_SYS_DEPEND_MALLOC_FAILED, "error -- mbedtls malloc: %d failed\r\n", (int32_t)size);
        return NULL;
    } else {
        memset(buf, 0, n * size + sizeof(mbedtls_mem_info_t));
    }

    mem_info = (mbedtls_mem_info_t *)buf;
    mem_info->magic = MBEDTLS_MEM_INFO_MAGIC;
    mem_info->size = n * size;
    buf += sizeof(mbedtls_mem_info_t);

    g_mbedtls_total_mem_used += mem_info->size;
    if (g_mbedtls_total_mem_used > g_mbedtls_max_mem_used) {
        g_mbedtls_max_mem_used = g_mbedtls_total_mem_used;
    }

    /* ALOG_DEBUG(TAG, "INFO -- mbedtls malloc: %d  total used: %d  max used: %d\r\n", size, g_mbedtls_total_mem_used, g_mbedtls_max_mem_used); */

    return buf;
}

static void _core_mbedtls_free(void *ptr)
{
    mbedtls_mem_info_t *mem_info = NULL;
    if (NULL == ptr) {
        return;
    }

    mem_info = (mbedtls_mem_info_t *)((uint8_t *)ptr - sizeof(mbedtls_mem_info_t));
    if (mem_info->magic != MBEDTLS_MEM_INFO_MAGIC) {
        ALOG_ERROR(TAG, STATE_ADAPTER_ERROR, "Warning - invalid mem info magic: %d\r\n", mem_info->magic);
        return;
    }

    g_mbedtls_total_mem_used -= mem_info->size;

    /* ALOG_DEBUG(TAG, "INFO -- mbedtls free: %d  total used: %d  max used: %d\r\n", size, g_mbedtls_total_mem_used, g_mbedtls_max_mem_used); */

    core_os_free(mem_info);
}

static int32_t _core_mbedtls_random(void *handle, uint8_t *output, size_t output_len)
{
    core_os_rand(output, output_len);
    return 0;
}

static void _core_mbedtls_debug(void *ctx, int32_t level, const char *file, int32_t line, const char *str)
{
    ((void) level);
    if (NULL != ctx) {
        ALOG_INFO(TAG, "[mbedtls] %s\r\n", str);
    }
}


static int32_t _core_mbedtls_net_send(void *ctx, const uint8_t *buf, size_t len)
{
    int32_t ret = g_origin_portfile->core_sysdep_network_send(ctx, (uint8_t *)buf, len, 5000, NULL);
    return ret;
}

static int32_t _core_mbedtls_net_recv(void *ctx, uint8_t *buf, size_t len)
{
    int32_t ret = g_origin_portfile->core_sysdep_network_recv(ctx, buf, len, 5000, NULL);
    if (ret < 0) {
        return (MBEDTLS_ERR_NET_RECV_FAILED);
    } else {
        return ret;
    }
}

static int32_t _core_mbedtls_net_recv_timeout(void *ctx, uint8_t *buf, size_t len,
        uint32_t timeout)
{
    int32_t ret = g_origin_portfile->core_sysdep_network_recv(ctx, buf, len, timeout, NULL);
    if (ret < 0) {
        return (MBEDTLS_ERR_NET_RECV_FAILED);
    } else {
        return ret;
    }
}

#ifdef CORE_ADAPTER_MBEDTLS_PRINT_KEYLOG
static void nss_keylog_export( void *p_expkey,
                        mbedtls_ssl_key_export_type secret_type,
                        const unsigned char *secret,
                        size_t secret_len,
                        const unsigned char client_random[32],
                        const unsigned char server_random[32],
                        mbedtls_tls_prf_types tls_prf_type )
{
    char nss_keylog_line[ 200 ];
    size_t const client_random_len = 32;
    size_t len = 0;
    size_t j;

    /* We're only interested in the TLS 1.2 master secret */
    if( secret_type != MBEDTLS_SSL_KEY_EXPORT_TLS12_MASTER_SECRET )
        return;

    ((void) p_expkey);
    ((void) server_random);
    ((void) tls_prf_type);

    len += sprintf( nss_keylog_line + len,
                    "%s", "CLIENT_RANDOM " );

    for( j = 0; j < client_random_len; j++ )
    {
        len += sprintf( nss_keylog_line + len,
                        "%02x", client_random[j] );
    }

    len += sprintf( nss_keylog_line + len, " " );

    for( j = 0; j < secret_len; j++ )
    {
        len += sprintf( nss_keylog_line + len,
                        "%02x", secret[j] );
    }

    len += sprintf( nss_keylog_line + len, "\n" );
    nss_keylog_line[ len ] = '\0';

    ALOG_INFO(TAG, "\n" );
    ALOG_INFO(TAG,  "---------------- NSS KEYLOG -----------------\n" );
    ALOG_INFO(TAG,  "%s", nss_keylog_line );
    ALOG_INFO(TAG,  "---------------------------------------------\n" );
}
#endif

static int32_t core_mbedtls_establish(void *handle)
{
    adapter_network_handle_t *adapter_handle = (adapter_network_handle_t *)handle;
    core_sysdep_mbedtls_t *mbedtls = NULL;
    int32_t res = 0;
    g_origin_portfile = aiot_sysdep_get_portfile();
    g_mbedtls_total_mem_used = g_mbedtls_max_mem_used = 0;
    mbedtls = (core_sysdep_mbedtls_t *)core_os_malloc(sizeof(core_sysdep_mbedtls_t));
    if (mbedtls == NULL) {
        return STATE_PORT_MALLOC_FAILED;
    }
    memset(mbedtls, 0, sizeof(*mbedtls));
    adapter_handle->tls_handle = mbedtls;

    res = g_origin_portfile->core_sysdep_network_establish(adapter_handle->network_handle);
    if(res != STATE_SUCCESS) {
        return res;
    }

    psa_crypto_init();
    mbedtls_debug_set_threshold(0);
    mbedtls_platform_set_calloc_free(_core_mbedtls_calloc, _core_mbedtls_free);

    mbedtls_ssl_init(&mbedtls->ssl_ctx);
    mbedtls_ssl_config_init(&mbedtls->ssl_config);

    ALOG_INFO(TAG, "tls connecting with server(host='%s', port=[%d])\r\n", adapter_handle->host, adapter_handle->port);

    if (adapter_handle->socket_type == CORE_SYSDEP_SOCKET_TCP_CLIENT) {
        res = mbedtls_ssl_config_defaults(&mbedtls->ssl_config, MBEDTLS_SSL_IS_CLIENT,
                                          MBEDTLS_SSL_TRANSPORT_STREAM, MBEDTLS_SSL_PRESET_DEFAULT);
    } else if (adapter_handle->socket_type == CORE_SYSDEP_SOCKET_UDP_CLIENT) {
        res = mbedtls_ssl_config_defaults(&mbedtls->ssl_config, MBEDTLS_SSL_IS_CLIENT,
                                          MBEDTLS_SSL_TRANSPORT_DATAGRAM, MBEDTLS_SSL_PRESET_DEFAULT);
    }

    if (res < 0) {
        ALOG_ERROR(TAG, STATE_ADAPTER_ERROR, "mbedtls_ssl_config_defaults error, res: -0x%04x\r\n", -1 * res);
        return res;
    }

    mbedtls_ssl_conf_authmode( &mbedtls->ssl_config, MBEDTLS_SSL_VERIFY_REQUIRED );
    if (adapter_handle->cred->option == AIOT_SYSDEP_NETWORK_CRED_SVRCERT_CA) {
        if (adapter_handle->cred->x509_server_cert == NULL && adapter_handle->cred->x509_server_cert_len == 0) {
            ALOG_ERROR(TAG, STATE_PORT_TLS_INVALID_SERVER_CERT, "invalid x509 server cert\r\n");
            return STATE_PORT_TLS_INVALID_SERVER_CERT;
        }
        mbedtls_x509_crt_init(&mbedtls->x509_server_cert);

        res = mbedtls_x509_crt_parse(&mbedtls->x509_server_cert,
                                     (const uint8_t *)adapter_handle->cred->x509_server_cert, (size_t)adapter_handle->cred->x509_server_cert_len + 1);
        if (res < 0) {
            ALOG_ERROR(TAG, STATE_PORT_TLS_INVALID_SERVER_CERT, "mbedtls_x509_crt_parse server cert error, res: -0x%04x\r\n", -1 * res);
            return STATE_PORT_TLS_INVALID_SERVER_CERT;
        }

        if (adapter_handle->cred->x509_client_cert != NULL && adapter_handle->cred->x509_client_cert_len > 0 &&
                adapter_handle->cred->x509_client_privkey != NULL && adapter_handle->cred->x509_client_privkey_len > 0) {
            mbedtls_x509_crt_init(&mbedtls->x509_client_cert);
            mbedtls_pk_init(&mbedtls->x509_client_pk);
            res = mbedtls_x509_crt_parse(&mbedtls->x509_client_cert,
                                         (const uint8_t *)adapter_handle->cred->x509_client_cert, (size_t)adapter_handle->cred->x509_client_cert_len + 1);
            if (res < 0) {
                ALOG_ERROR(TAG, STATE_PORT_TLS_INVALID_CLIENT_CERT, "mbedtls_x509_crt_parse client cert error, res: -0x%04x\r\n", -1 * res);
                return STATE_PORT_TLS_INVALID_CLIENT_CERT;
            }
            res = mbedtls_pk_parse_key(&mbedtls->x509_client_pk,
                                       (const uint8_t *)adapter_handle->cred->x509_client_privkey,
                                       (size_t)adapter_handle->cred->x509_client_privkey_len + 1, NULL, 0, _core_mbedtls_random, NULL);
            if (res < 0) {
                ALOG_ERROR(TAG, STATE_PORT_TLS_INVALID_CLIENT_KEY, "mbedtls_pk_parse_key client pk error, res: -0x%04x\r\n", -1 * res);
                return STATE_PORT_TLS_INVALID_CLIENT_KEY;
            }
            res = mbedtls_ssl_conf_own_cert(&mbedtls->ssl_config, &mbedtls->x509_client_cert,
                                            &mbedtls->x509_client_pk);
            if (res < 0) {
                ALOG_ERROR(TAG, STATE_PORT_TLS_INVALID_CLIENT_CERT, "mbedtls_ssl_conf_own_cert error, res: -0x%04x\r\n", -1 * res);
                return STATE_PORT_TLS_INVALID_CLIENT_CERT;
            }
        }
        mbedtls_ssl_conf_ca_chain(&mbedtls->ssl_config, &mbedtls->x509_server_cert, NULL);
    } else if (adapter_handle->cred->option == AIOT_SYSDEP_NETWORK_CRED_SVRCERT_PSK) {
        static const int32_t ciphersuites[1] = {MBEDTLS_TLS_PSK_WITH_AES_128_GCM_SHA256};
        res = mbedtls_ssl_conf_psk(&mbedtls->ssl_config,
                                   (const uint8_t *)adapter_handle->psk.psk, (size_t)strlen(adapter_handle->psk.psk),
                                   (const uint8_t *)adapter_handle->psk.psk_id, (size_t)strlen(adapter_handle->psk.psk_id));
        if (res < 0) {
            ALOG_ERROR(TAG, STATE_PORT_TLS_CONFIG_PSK_FAILED, "mbedtls_ssl_conf_psk error, res: -0x%04x\r\n", -1 * res);
            return STATE_PORT_TLS_CONFIG_PSK_FAILED;
        }

        mbedtls_ssl_conf_ciphersuites(&mbedtls->ssl_config, ciphersuites);
    } else {
        ALOG_ERROR(TAG, STATE_PORT_TLS_INVALID_CRED_OPTION, "unsupported security option\r\n");
        return STATE_PORT_TLS_INVALID_CRED_OPTION;
    }

    /*
        mbedtls_ssl_conf_rng( &mbedtls->ssl_config, mbedtls_ctr_drbg_random, &ctr_drbg );
    */
    mbedtls_ssl_conf_rng(&mbedtls->ssl_config, _core_mbedtls_random, NULL);
    mbedtls_ssl_conf_dbg(&mbedtls->ssl_config, _core_mbedtls_debug, stdout);
    if(adapter_handle->cred->force_version == AIOT_SYSDEP_NETWORK_TLS_13) {
        mbedtls_ssl_conf_max_version(&mbedtls->ssl_config, MBEDTLS_SSL_MAJOR_VERSION_3,
                                     MBEDTLS_SSL_MINOR_VERSION_4);
        mbedtls_ssl_conf_min_version(&mbedtls->ssl_config, MBEDTLS_SSL_MAJOR_VERSION_3,
                                     MBEDTLS_SSL_MINOR_VERSION_4);
        mbedtls_ssl_conf_tls13_key_exchange_modes( &mbedtls->ssl_config, MBEDTLS_SSL_TLS1_3_KEY_EXCHANGE_MODE_ALL );
    } else {
        mbedtls_ssl_conf_max_version(&mbedtls->ssl_config, MBEDTLS_SSL_MAJOR_VERSION_3,
                                     MBEDTLS_SSL_MINOR_VERSION_3);
        mbedtls_ssl_conf_min_version(&mbedtls->ssl_config, MBEDTLS_SSL_MAJOR_VERSION_3,
                                     MBEDTLS_SSL_MINOR_VERSION_3);
    }
    mbedtls_ssl_conf_handshake_timeout(&mbedtls->ssl_config, (MBEDTLS_SSL_DTLS_TIMEOUT_DFL_MIN * 2),
                                       (MBEDTLS_SSL_DTLS_TIMEOUT_DFL_MIN * 2 * 4));

    res = mbedtls_ssl_setup(&mbedtls->ssl_ctx, &mbedtls->ssl_config);
    if (res < 0) {
        ALOG_ERROR(TAG, STATE_ADAPTER_ERROR, "mbedtls_ssl_setup error, res: -0x%04x\r\n", -1 * res);
        return res;
    }

    if (_host_is_ip(adapter_handle->host) == 0) {
        res = mbedtls_ssl_set_hostname(&mbedtls->ssl_ctx, adapter_handle->host);
        if (res < 0) {
            ALOG_ERROR(TAG, STATE_ADAPTER_ERROR, "mbedtls_ssl_set_hostname error, res: -0x%04x\r\n", -1 * res);
            return res;
        }
    }

#ifdef CORE_ADAPTER_MBEDTLS_PRINT_KEYLOG
    mbedtls_ssl_set_export_keys_cb(&mbedtls->ssl_ctx, nss_keylog_export, NULL);
#endif
    mbedtls_ssl_set_bio(&mbedtls->ssl_ctx, adapter_handle->network_handle, _core_mbedtls_net_send,
                        _core_mbedtls_net_recv, _core_mbedtls_net_recv_timeout);
    mbedtls_ssl_conf_read_timeout(&mbedtls->ssl_config, adapter_handle->connect_timeout_ms);

    while ((res = mbedtls_ssl_handshake(&mbedtls->ssl_ctx)) != 0) {
        if ((res != MBEDTLS_ERR_SSL_WANT_READ) && (res != MBEDTLS_ERR_SSL_WANT_WRITE)) {
            ALOG_ERROR(TAG, STATE_ADAPTER_ERROR, "mbedtls_ssl_handshake error, res: -0x%04x\r\n", -1 * res);
            if (res == MBEDTLS_ERR_SSL_INVALID_RECORD) {
                res = STATE_PORT_TLS_INVALID_RECORD;
            } else {
                res = STATE_PORT_TLS_INVALID_HANDSHAKE;
            }
            return res;
        }
    }

    res = mbedtls_ssl_get_verify_result(&mbedtls->ssl_ctx);
    if (res < 0) {
        ALOG_ERROR(TAG, STATE_ADAPTER_ERROR, "mbedtls_ssl_get_verify_result error, res: -0x%04x\r\n", -1 * res);
        return res;
    }

    ALOG_INFO(TAG, "success to establish mbedtls connection, (cost %d bytes in total, max used %d bytes)\r\n",
              g_mbedtls_total_mem_used, g_mbedtls_max_mem_used);
    return 0;
}
static int32_t core_mbedtls_recv(void *handle, uint8_t *buffer, uint32_t len, uint32_t timeout_ms,
                          core_sysdep_addr_t *addr)
{
    int32_t res = 0;
    int32_t recv_bytes = 0;
    adapter_network_handle_t *adapter_handle = (adapter_network_handle_t *)handle;
    core_sysdep_mbedtls_t *mbedtls = NULL;
    if (handle == NULL) {
        return STATE_PORT_INPUT_NULL_POINTER;
    }

    mbedtls = (core_sysdep_mbedtls_t *)adapter_handle->tls_handle;
    mbedtls_ssl_conf_read_timeout(&mbedtls->ssl_config, timeout_ms);
    do {
        res = mbedtls_ssl_read(&mbedtls->ssl_ctx, buffer + recv_bytes, len - recv_bytes);
        if (res < 0) {
            if (res == MBEDTLS_ERR_SSL_TIMEOUT) {
                break;
            } else if (res != MBEDTLS_ERR_SSL_WANT_READ &&
                       res != MBEDTLS_ERR_SSL_WANT_WRITE &&
                       res != MBEDTLS_ERR_SSL_CLIENT_RECONNECT &&
                       res != -0x7700) {
                if (recv_bytes == 0) {
                    if (res == MBEDTLS_ERR_SSL_PEER_CLOSE_NOTIFY) {
                        ALOG_INFO(TAG, "tls connection closed, res: -0x%04x\r\n", -1 * res);
                        return STATE_PORT_TLS_RECV_CONNECTION_CLOSED;
                    } else if (res == MBEDTLS_ERR_SSL_INVALID_RECORD) {
                        ALOG_ERROR(TAG, STATE_PORT_TLS_INVALID_RECORD, "mbedtls_ssl_read failed, -0x%04x\r\n", -1 * res);
                        return STATE_PORT_TLS_INVALID_RECORD;
                    } else {
                        ALOG_ERROR(TAG, STATE_PORT_TLS_RECV_FAILED, "mbedtls_ssl_read failed, -0x%04x\r\n", -1 * res);
                        return STATE_PORT_TLS_RECV_FAILED;
                    }
                }
                break;
            }
        } else if (res == 0) {
            break;
        } else {
            recv_bytes += res;
        }
    } while (recv_bytes < len);

    return recv_bytes;
}
static int32_t core_mbedtls_send(void *handle, uint8_t *buffer, uint32_t len, uint32_t timeout_ms,
                          core_sysdep_addr_t *addr)
{
    int32_t res = 0;
    int32_t send_bytes = 0;
    uint64_t timestart_ms = 0, timenow_ms = 0;
    adapter_network_handle_t *adapter_handle = (adapter_network_handle_t *)handle;
    core_sysdep_mbedtls_t *mbedtls = NULL;
    if (handle == NULL) {
        return STATE_PORT_INPUT_NULL_POINTER;
    }
    mbedtls = (core_sysdep_mbedtls_t *)adapter_handle->tls_handle;
    /* Start Time */
    timestart_ms = core_os_time();
    timenow_ms = timestart_ms;

    do {
        timenow_ms = core_os_time();

        if (timenow_ms - timestart_ms >= timenow_ms ||
                timeout_ms - (timenow_ms - timestart_ms) > timeout_ms) {
            break;
        }

        res = mbedtls_ssl_write(&mbedtls->ssl_ctx, buffer + send_bytes, len - send_bytes);
        if (res < 0) {
            if (res != MBEDTLS_ERR_SSL_WANT_READ &&
                    res != MBEDTLS_ERR_SSL_WANT_WRITE) {
                if (send_bytes == 0) {
                    if (res == MBEDTLS_ERR_SSL_PEER_CLOSE_NOTIFY) {
                        ALOG_INFO(TAG, "tls connection closed, res: -0x%04x\r\n", -1 * res);
                        return STATE_PORT_TLS_SEND_CONNECTION_CLOSED;
                    } else if (res == MBEDTLS_ERR_SSL_INVALID_RECORD) {
                        ALOG_ERROR(TAG, STATE_PORT_TLS_INVALID_RECORD, "mbedtls_ssl_write failed,-0x%04x\r\n", -1 * res);
                        return STATE_PORT_TLS_INVALID_RECORD;
                    } else {
                        ALOG_ERROR(TAG, STATE_PORT_TLS_SEND_FAILED, "mbedtls_ssl_write failed, -0x%04x\r\n", -1 * res);
                        return STATE_PORT_TLS_SEND_FAILED;
                    }
                }
                break;
            } else {
                core_os_sleep(100);
            }
        } else if (res == 0) {
            break;
        } else {
            send_bytes += res;
        }
    } while (((timenow_ms - timestart_ms) < timeout_ms) && (send_bytes < len));

    return send_bytes;
}
static int32_t core_mbedtls_deinit(void *handle)
{
    adapter_network_handle_t *adapter_handle = (adapter_network_handle_t *)handle;
    core_sysdep_mbedtls_t *mbedtls = NULL;

    if (handle == NULL) {
        return STATE_PORT_INPUT_NULL_POINTER;
    }
    mbedtls = (core_sysdep_mbedtls_t *)adapter_handle->tls_handle;

    if(mbedtls != NULL) {
        mbedtls_ssl_close_notify(&mbedtls->ssl_ctx);
        mbedtls_x509_crt_free(&mbedtls->x509_server_cert);
        mbedtls_x509_crt_free(&mbedtls->x509_client_cert);
        mbedtls_pk_free(&mbedtls->x509_client_pk);
        mbedtls_ssl_free(&mbedtls->ssl_ctx);
        mbedtls_ssl_config_free(&mbedtls->ssl_config);
        core_os_free(mbedtls);
        adapter_handle->tls_handle = NULL;
        g_mbedtls_total_mem_used = g_mbedtls_max_mem_used = 0;
    }

    return STATE_SUCCESS;
}

static adapter_operation_t mbedtls_adapter = {
    .establish = core_mbedtls_establish,
    .recv = core_mbedtls_recv,
    .send = core_mbedtls_send,
    .deinit = core_mbedtls_deinit,
};

adapter_operation_t *core_mbedtls_adapter()
{
    return &mbedtls_adapter;
}
#endif