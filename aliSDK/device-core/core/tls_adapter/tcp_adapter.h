#ifndef _TCP_ADAPTER_H_
#define _TCP_ADAPTER_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include "core_adapter.h"

adapter_operation_t *core_tcp_adapter();

#if defined(__cplusplus)
}
#endif

#endif
