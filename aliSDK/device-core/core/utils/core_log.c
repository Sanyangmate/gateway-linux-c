#include <stdio.h>
#include <string.h>
#include "core_log.h"
#include "core_os.h"
#include "core_time.h"


static int32_t log_level = LOG_DEFAULT_LEVEL;
typedef struct {
    uint64_t time_start;
    uint64_t time_interval;
    uint64_t timestamp;
    uint8_t  log_stamp;
    uint8_t  log_date;
} core_log_t;
static core_log_t g_core_log = {
    .time_start = 0,
    .time_interval = 0,
    .timestamp = 0,
    .log_stamp = 1,
    .log_date = 0
};

static int32_t default_log_print(int32_t code, char *message)
{
    printf("%s", message);
    return 0;
}

static aiot_state_logcb_t g_logcb_handler = default_log_print;
int32_t aiot_state_set_logcb(aiot_state_logcb_t handler)
{
    if(handler == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }
    g_logcb_handler = handler;
    return 0;
}


int32_t aiot_state_set_log_level(log_level_t level)
{
    if(level < LOG_LEVEL_NONE || level > LOG_LEVEL_DEBUG) {
        return STATE_USER_INPUT_OUT_RANGE;
    }

    log_level = level;

    return STATE_SUCCESS;
}
static uint64_t _core_log_get_timestamp(aiot_sysdep_portfile_t *sysdep)
{
    uint64_t timenow = core_os_time();

    /*NTP同步过时间，判断系统时间是否已更新，没更新进入if分支使用网络时间log*/
    if (g_core_log.timestamp != 0 && g_core_log.timestamp > timenow)
    {
        if (timenow >= g_core_log.time_start) {
            g_core_log.time_interval += timenow - g_core_log.time_start;
        } else {
            /* loss (max_time - g_core_log.time_start) ms */
            g_core_log.time_interval += timenow;
        }
        g_core_log.time_start = timenow;
        timenow = g_core_log.timestamp + g_core_log.time_interval;
    }

    return timenow;
}

void _core_log_append_date(aiot_sysdep_portfile_t *sysdep, uint64_t timestamp, char *buffer)
{
    core_date_t date;

    memset(&date, 0, sizeof(core_date_t));
    core_utc2date(timestamp, 8, &date);

    sprintf(buffer, "%d/%02d/%02d %02d:%02d:%02d:%03d"
            , date.year, date.mon, date.day
            , date.hour, date.min, date.sec, date.msec);
}


void core_log_set_timestamp(aiot_sysdep_portfile_t *sysdep, uint64_t timestamp)
{
    g_core_log.timestamp = timestamp;
    g_core_log.time_start = core_os_time();
    g_core_log.time_interval = 0;
}

uint64_t core_log_get_timestamp(aiot_sysdep_portfile_t *sysdep)
{
    return _core_log_get_timestamp(sysdep);
}

void core_log(const char *tag, const int level, const char *fmt, ...)
{
    va_list ap;
    int32_t ret = 0;
    uint64_t time = core_os_time();
    char utc[40] = { 0 };
    char buffer[CORE_LOG_MAXLEN];
    if(level <= LOG_LEVEL_NONE || level > LOG_LEVEL_DEBUG || level > log_level) {
        return;
    }

    if(g_logcb_handler == NULL || tag == NULL || fmt == NULL) {
        return;
    }
    _core_log_append_date(NULL, time, utc);

    ret = snprintf(buffer, sizeof(buffer), "[%s][%s] ", utc, tag);
    va_start(ap, fmt);
    ret += vsnprintf(buffer + ret, CORE_LOG_MAXLEN - 1 - ret, fmt, ap);
    va_end(ap);

    if(ret > 0) {
        g_logcb_handler(0, buffer);
    }
}
void core_log_res(const char *tag, int32_t code, const int level, const char *fmt, ...)
{
    va_list ap;
    int32_t ret = 0;
    char utc[40] = { 0 };
    uint64_t time = core_os_time();
    char buffer[CORE_LOG_MAXLEN];
    if(level <= LOG_LEVEL_NONE || level > LOG_LEVEL_DEBUG || level > log_level) {
        return;
    }

    if(g_logcb_handler == NULL || tag == NULL || fmt == NULL) {
        return;
    }

    _core_log_append_date(NULL, time, utc);
    ret = snprintf(buffer, sizeof(buffer), "[%s][%s] ", utc, tag);
    if(ret < 0) {
        return;
    }
    va_start(ap, fmt);
    ret += vsnprintf(buffer + ret, CORE_LOG_MAXLEN - 1 - ret, fmt, ap);
    va_end(ap);
    if(ret < 0) {
        return;
    }

    /* 统一添加打印后缀 \r\n */
    while(ret > 0 && (buffer[ret - 1] == '\r' || buffer[ret - 1] == '\n')) {
        buffer[ret - 1] = 0;
        ret--;
    }

    ret += snprintf(buffer + ret, CORE_LOG_MAXLEN - 1 - ret, ", code: -0x%04x\r\n", -1 * code);

    if(ret > 0) {
        g_logcb_handler(0, buffer);
    }
}

void core_log_hex(const char *tag, int level, char prefix, uint8_t *buffer, uint32_t len)
{
    uint32_t idx = 0, line_idx = 0, ch_idx = 0, code_len = 0;
    char hexdump[128] = {0};

    if(level <= LOG_LEVEL_NONE || level > LOG_LEVEL_DEBUG || level > log_level) {
        return;
    }

    if (g_logcb_handler == NULL || len == 0) {
        return;
    }

    g_logcb_handler(0, "\r\n");
    code_len = snprintf(hexdump, sizeof(hexdump), "[%s] ", tag);

    for (idx = 0; idx < len;) {
        memset(hexdump + code_len, ' ', 71);
        ch_idx = 2;
        hexdump[code_len + 0] = prefix;
        hexdump[code_len + 51] = '|';
        hexdump[code_len + 52] = ' ';
        for (line_idx = idx; ((line_idx - idx) < 16) && (line_idx < len); line_idx++) {
            if ((line_idx - idx) == 8) {
                ch_idx++;
            }
            core_hex2str((uint8_t *)&buffer[line_idx], 1, &hexdump[code_len + ch_idx], 0);
            hexdump[code_len + ch_idx + 2] = ' ';
            if (buffer[line_idx] >= 0x20 && buffer[line_idx] <= 0x7E) {
                hexdump[code_len + 53 + (line_idx - idx)] = buffer[line_idx];
            } else {
                hexdump[code_len + 53 + (line_idx - idx)] = '.';
            }
            ch_idx += 3;
        }
        hexdump[code_len + 69] = '\r';
        hexdump[code_len + 70] = '\n';
        idx += (line_idx - idx);
        g_logcb_handler(0, hexdump);
    }
    g_logcb_handler(0, "\r\n");
}
