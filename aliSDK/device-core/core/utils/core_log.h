#ifndef _CORE_LOG_H_
#define _CORE_LOG_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdlib.h>
#include "core_stdinc.h"
#include "core_string.h"
#include "aiot_state_api.h"
#include "aiot_sysdep_api.h"
#include "stdarg.h"

#define CORE_LOG_MAXLEN (1024)

void core_log(const char *tag, const int level, const char *fmt, ...);
void core_log_res(const char *tag, int32_t code, const int level, const char *fmt, ...);
void core_log_hex(const char *tag, int level, char prefix, uint8_t *buf, uint32_t len);

#ifdef LOG_ENABLE
#define ALOG_ERROR(tag, code, ...)              core_log_res(tag, code, LOG_LEVEL_ERROR, __VA_ARGS__)
#define ALOG_INFO(tag, ...)                     core_log(tag, LOG_LEVEL_INFO, __VA_ARGS__)
#define ALOG_DEBUG(tag, ...)                    core_log(tag, LOG_LEVEL_DEBUG, __VA_ARGS__)
#define ALOG_HEX(tag, level, prefix, buf, len)  core_log_hex(tag, level, prefix, buf, len)
#else
#define ALOG_ERROR(...)
#define ALOG_INFO(...)
#define ALOG_DEBUG(...)
#define ALOG_HEX(...)
#endif

#if defined(__cplusplus)
}
#endif

#endif

