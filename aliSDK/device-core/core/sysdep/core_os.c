#include "core_os.h"
#include "aiot_sysdep_api.h"

static aiot_sysdep_portfile_t *sysdep = NULL;

void core_os_set_origin_portfile(aiot_sysdep_portfile_t *portfile)
{
    sysdep = portfile;
}
void *core_os_malloc(uint32_t size)
{
    if(sysdep == NULL) {
        return NULL;
    }
    return sysdep->core_sysdep_malloc(size, "");
}
void core_os_free(void *ptr)
{
    if(sysdep == NULL) {
        return;
    }
    return sysdep->core_sysdep_free(ptr);
}
uint64_t core_os_time(void) {
    if(sysdep == NULL) {
        return 0;
    }
    return sysdep->core_sysdep_time();
}
void core_os_sleep(uint64_t time_ms)
{
    if(sysdep == NULL) {
        return;
    }
    return sysdep->core_sysdep_sleep(time_ms);
}
void core_os_rand(uint8_t *output, uint32_t output_len)
{
    if(sysdep == NULL) {
        return;
    }
    return sysdep->core_sysdep_rand(output, output_len);
}
void *core_os_mutex_init(void)
{
    if(sysdep == NULL) {
        return NULL;
    }
    return sysdep->core_sysdep_mutex_init();
}
void core_os_mutex_lock(void *mutex)
{
    if(sysdep == NULL) {
        return;
    }
    return sysdep->core_sysdep_mutex_lock(mutex);
}
int32_t core_os_mutex_lock_with_timeout(void *mutex, int32_t time_ms)
{
    if(sysdep == NULL) {
        return 0;
    }
    sysdep->core_sysdep_mutex_lock(mutex);
    return 0;
}
void core_os_mutex_unlock(void *mutex)
{
    if(sysdep == NULL) {
        return;
    }
    return sysdep->core_sysdep_mutex_unlock(mutex);
}
void core_os_mutex_deinit(void **mutex)
{
    if(sysdep == NULL) {
        return;
    }
    return sysdep->core_sysdep_mutex_deinit(mutex);
}

void *core_os_thread_init(char *task_name, void *(*work)(void *), void *argv)
{
    if(sysdep == NULL) {
        return NULL;
    }
    return sysdep->core_sysdep_thread_init(task_name, work, argv);
}
void core_os_thread_join(void **thread_ptr)
{
    if(sysdep == NULL) {
        return;
    }
    return sysdep->core_sysdep_thread_join(thread_ptr);
}
void core_os_thread_deinit(void **thread_ptr)
{
    if(sysdep == NULL) {
        return;
    }
    return sysdep->core_sysdep_thread_deinit(thread_ptr);
}