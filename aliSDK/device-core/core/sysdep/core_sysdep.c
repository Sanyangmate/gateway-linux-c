#include "aiot_state_api.h"
#include "core_adapter.h"
#include "core_os.h"
#include "cJSON.h"


void *cjson_malloc(size_t size){
    return core_os_malloc(size);
}

void aiot_sysdep_init(aiot_sysdep_portfile_t *portfile)
{
    cJSON_Hooks hooks;
    core_network_set_origin_portfile(portfile);
    core_os_set_origin_portfile(portfile);
    hooks.free_fn = core_os_free;
    hooks.malloc_fn = cjson_malloc;
    cJSON_InitHooks(&hooks);
}

