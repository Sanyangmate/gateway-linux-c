#ifndef _CORE_OS_H_
#define _CORE_OS_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include "core_stdinc.h"
#include "aiot_sysdep_api.h"
void core_os_set_origin_portfile(aiot_sysdep_portfile_t *portfile);
void *core_os_malloc(uint32_t size);
void core_os_free(void *ptr);
uint64_t core_os_time(void);
void core_os_sleep(uint64_t time_ms);
void core_os_rand(uint8_t *output, uint32_t output_len);
void *core_os_mutex_init(void);
void core_os_mutex_lock(void *mutex);
int32_t core_os_mutex_lock_with_timeout(void *mutex, int32_t time_ms);
void core_os_mutex_unlock(void *mutex);
void core_os_mutex_deinit(void **mutex);
void *core_os_thread_init(char *task_name, void *(*work)(void *), void *argv);
void core_os_thread_join(void **thread_ptr);
void core_os_thread_deinit(void **thread_ptr);

#if defined(__cplusplus)
}
#endif

#endif

