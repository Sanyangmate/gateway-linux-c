#ifndef _CORE_TIME_H_
#define _CORE_TIME_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include "core_stdinc.h"
#include "aiot_sysdep_api.h"

/* 将时间戳转换为UTC时间 */
void _core_log_append_date(aiot_sysdep_portfile_t *sysdep, uint64_t timestamp, char *buffer);
/* 设置日志时间戳 */
void core_log_set_timestamp(aiot_sysdep_portfile_t *sysdep, uint64_t timestamp);
/* 获取日志时间戳 */
uint64_t core_log_get_timestamp(aiot_sysdep_portfile_t *sysdep);

#if defined(__cplusplus)
}
#endif

#endif

