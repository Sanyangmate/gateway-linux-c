/**
 * @file aiot_device_api.h
 * @brief 设备基础功能头文件, 提供设备的连接与消息相关的能力，包含：设备建连，设备消息收发，设备订阅topic，设备动态注册，设备就近接入
 * @date 2022-01-20
 *
 * @copyright Copyright (C) 2020-2025 Alibaba Group Holding Limited
 *
 */
#ifndef _AIOT_DEVICE_CLIENT_API_H_
#define _AIOT_DEVICE_CLIENT_API_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>
#include "aiot_sysdep_api.h"
#include "aiot_message_api.h"
#include "aiot_linkconfig_api.h"
#include "aiot_state_api.h"

/**
 * @brief 设备报文接收回调函数原型， 可通过 @ref aiot_device_set_event_callback 设置
 *
 * @param[in] device 设备实例句柄
 * @param[in] message 设备报文结构体, 存放收到的报文，数据结构参考 @ref aiot_msg_t
 * @param[in] userdata 用户上下文， 可通过 @ref aiot_device_set_event_callback 设置
 *
 * @return void
 */
typedef void (*aiot_device_msg_callback_t)(void *device, const aiot_msg_t *message, void *userdata);

/**
 * @brief device状态变化类型
 */
typedef enum {
    /**
     * @brief 当实例第一次连接网络成功时, 触发此事件
     */
    AIOT_DEVICE_STATUS_CONNECT,
    /**
     * @brief 当实例断开网络连接后重连成功时, 触发此事件
     */
    AIOT_DEVICE_STATUS_RECONNECT,
    /**
     * @brief 当实例断开网络连接时, 触发此事件
     */
    AIOT_DEVICE_STATUS_DISCONNECT
} aiot_device_status_type_t;

/**
 * @brief 设备状态变化时，描述设备状态的结构体
 */
typedef struct {
    /**
     * @brief 内部状态事件类型
     *
     */
    aiot_device_status_type_t type;
    /**
     * @brief 错误码
     */
    uint32_t error_code;
} aiot_device_status_t;

/**
 * @brief 设备状态变化时回调函数原型, 用户定义回调函数后，可通过 @ref aiot_device_set_event_callback 设置
 *
 * @param[in] device 设备实例句柄
 * @param[in] event  描述设备状态变化事件, 数据结构 @ref aiot_device_status_t
 * @param[in] userdata 用户上下文 可通过 @ref aiot_device_set_event_callback 设置
 *
 * @return void
 */
typedef void (*aiot_device_status_callback_t)(void *device, const aiot_device_status_t *event, void *userdata);

/**
 * @brief 设备消息ack的类型
 */
typedef enum {
    /**
     * @brief 消息发送的结果
     */
    AIOT_DEVICE_SEND_MESSAGE_RESULT,
    /**
     * @brief 订阅感兴趣的消息的结果
     */
    AIOT_DEVICE_REGISTER_TOPIC_RESULT,
    /**
     * @brief 取消订阅消息的结果
     */
    AIOT_DEVICE_UNREGISTER_TOPIC_RESULT,
} aiot_device_msg_result_type_t;

/**
 * @brief 设备消息发送后，返回的消息发送结果，包含pub/sub消息的ACK
 */
typedef struct {
    /**
     * @brief 返回的ack类型
     */
    aiot_device_msg_result_type_t type;
    /**
     * @brief 消息ID
     */
    int32_t message_id;
    /**
     * @brief 结果，0：成功；  其它：错误码
     */
    int32_t code;
} aiot_device_msg_result_t;

/**
 * @brief 设备接收到消息ACK时的回调函数原型， 可通过 @ref aiot_device_set_event_callback 设置
 *
 * @param[in] device 设备实例句柄
 * @param[in] result 描述pub/sub消息的结果, 数据结构参考 @ref aiot_device_msg_result_t
 * @param[in] userdata 用户上下文， 可通过 @ref aiot_device_set_event_callback 设置
 *
 * @return void
 */
typedef void (*aiot_device_msg_result_callback_t)(void *device, const aiot_device_msg_result_t *result, void *userdata);

/**
 * @brief 初始化设备并设置默认参数
 * @param[in] product_key 设备认证信息中的产品key
 * @param[in] device_name 设备认证信息中的设备名
 * @return void*
 * @retval 非NULL 设备句柄
 * @retval NULL 初始化失败, 一般是内存分配失败导致
 *
 */
void *aiot_device_create(const char *product_key, const char *device_name);

/**
 * @brief 设置设备建连使用的设备密钥
 * @param[in] device 设备的句柄
 * @param[in] device_secret 设备认证信息中的设备密钥
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_device_set_device_secret(void *device, const char *device_secret);

/**
 * @brief 设置设备连接的参数
 * @param[in] device 设备句柄
 * @param[in] config 建连参数的句柄，初始化及配置参考 @ref aiot_linkconfig_api.h
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_device_set_linkconfig(void *device, aiot_linkconfig_t *config);

/**
 * @brief 设备与云平台进行连接，同步阻塞的
 * @param[in] device 设备句柄
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_device_connect(void *device);

/**
 * @brief 设备与云平台进行连接，异步非阻塞的
 * @param[in] device 设备句柄
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_device_connect_async(void *device);

/**
 * @brief 设备向云平台发送消息
 *
 * @param[in] device 设备句柄
 * @param[in] message 需要发送的消息, 消息可由高级模块生成，也可由用户自定义生成 @ref aiot_message_api.h
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_device_send_message(void *device, const aiot_msg_t *message);

/**
 * @brief 设置设备事件：包含消息回调，状态回调
 * @param[in] device 设备句柄
 * @param[in] msg_recv_callback 消息回调函数(所有消息都会有这个回调执行，如果只需要监听部分消息，可以使用@ref aiot_device_register_topic_filter 订阅感兴趣的topic)
 * @param[in] status_callback  设备状态变化回调函数
 * @param[in] msg_result_callback  设备接收到ack回调函数
 * @param[in] userdata 执行回调时返回给用户上下文
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 */
int32_t aiot_device_set_event_callback(void *device, aiot_device_msg_callback_t msg_recv_callback, aiot_device_status_callback_t status_callback, aiot_device_msg_result_callback_t msg_result_callback, void *userdata);

/**
 * @brief 设备注册感兴趣的topic，接收到该topic的消息，会执行对应回调
 * @param[in] device 设备句柄
 * @param[in] topic 待注册的topic
 * @param[in] callback 接收到该topic消息后的回调函数
 * @param[in] sub  是否需要向云端发送订阅消息，设备只需要订阅一次即可，重启后一样有效
 *                 [1, 向云端发送订阅消息]
 *                 [0, 不向云端发送订阅消息，接收到该消息一样会执行回调]
 * @param[in] userdata 执行回调时返回给用户
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_device_register_topic_filter(void *device, const char *topic, aiot_device_msg_callback_t callback, uint8_t sub, void *userdata);

/**
 * @brief 设备注销topic，接收到该topic的消息，将不再执行回调
 * @param[in] device 设备句柄
 * @param[in] topic 待注销的topic
 * @param[in] unsub  是否需要向云端发送订阅消息，设备只需要订阅一次即可，重启后一样有效
 *                  [1, 向云端发送取消订阅消息]
 *                  [0, 不向云端发送取消订阅消息]
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_device_deregister_topic_filter(void *device, const char *topic, uint8_t unsub);

/**
 * @brief 设备主动与平台断开连接
 * @param[in] device 设备实例句柄
 *
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 */
int32_t aiot_device_disconnect(void *device);

/**
 * @brief 释放设备实例的资源
 *
 * @param[in] device 指向device实例句柄的指针
 *
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_device_delete(void **device);

/**
 * @brief 就近接入返回的接入信息结构体
 */
typedef struct {
    char host[128];
    uint16_t port;
} aiot_bootstrap_info_t;

/**
 * @brief 请求就近接入信息(host，port)
 *
 * @param[in] device 指向device实例句柄的指针
 * @param[in] type   就近接入的协议类型，目前仅支持MQTT_PROTOCOL
 * @param[in] timeout_ms 请求超时的时间，[ >= 0 ]:为设置的具体超时时间ms；
 * @param[out] info  返回的接入信息结构体
 *
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_device_bootstrap_request(void *device, aiot_protocol_type_t type, uint32_t timeout_ms, aiot_bootstrap_info_t *info);

/**
 * @brief 设备动态获取密钥(设备需先预注册)
 *
 * @param[in] device 指向device实例句柄的指针
 * @param[in] linkconfig 动态注册连接云的参数配置
 * @param[out] out_device_seceret  返回的设备密钥
 * @param[in] out_secret_len  保存密钥的缓存长度，该长度建议设置为128
 *
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_device_dynamic_secret(void *device, aiot_linkconfig_t *linkconfig, char *out_device_seceret, int32_t out_secret_len);

/**
 * @brief 设备动态注册，返回信息结构体，设备可用这些信息建连
 */
typedef struct {
    char username[128];
    char password[128];
    char client_id[128];
} aiot_dynamic_register_info_t;
/**
 * @brief 设备动态注册(设备无需预注册)，该请求会返回设备建连认证信息
 *
 * @param[in] device 指向device实例句柄的指针
 * @param[in] linkconfig 动态注册连接云的参数配置
 * @param[out] info 设备建连认证信息, 可通过 @ref aiot_linkconfig_mqtt_auth 设置进连接参数中
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_device_dynamic_register(void *device, aiot_linkconfig_t *linkconfig, aiot_dynamic_register_info_t *info);

/**
 * @brief 查询设备的product_key
 *
 * @param[in] device 指向device实例句柄的指针
 *
 */
char *aiot_device_product_key(void *device);

/**
 * @brief 查询设备的device_name
 *
 * @param[in] device 指向device实例句柄的指针
 *
 */
char *aiot_device_name(void *device);

/**
 * @brief 查询设备的状态
 *
 * @param[in] device 指向device实例句柄的指针
 * @return int32_t  1:在线 0:离线
 */
int32_t aiot_device_online(void *device);

#if defined(__cplusplus)
}
#endif

#endif