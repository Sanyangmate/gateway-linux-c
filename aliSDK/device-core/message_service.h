#ifndef _CORE_MESSAGE_SERVICE_API_H_
#define _CORE_MESSAGE_SERVICE_API_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>
#include <message_private.h>
#include <aiot_linkconfig_api.h>
#include "device_private.h"
#include "core_list.h"
#include "protocol_config.h"

/* 单个通道的结构体 */
typedef struct {
    /* 传输模块句柄 */
    void *trans_handle;
    /* 连接的cid */
    uint8_t cid;
    /* 传输模块上下文，由上层模块指定 */
    void *content;
    /* 传输模块的连接配置参数 */
    aiot_protocol_config_t *config;

    /* 连接在线状态 */
    uint8_t online;
    /* 链表节点 */
    struct core_list_head linked_node;
} core_trans_channel_t;

/* 消息服务模块核心结构体 */
typedef struct {
    /* 协议处理的方法实现*/
    aiot_protocol_t *protocol;
    /* 协议连接的配置，包含多连接配置及单个连接的详细配置 */
    aiot_protocol_config_t *proto_config;
    /* 多连接配置, 起始connect_id */
    int32_t start_cid;
    /* 多连接配置，通道的个数 */
    int32_t channel_num;
    /* 接收到消息后的回调处理 */
    aiot_device_msg_callback_t message_callback;
    /* 传输层状态变化后的回调处理 */
    core_channel_status_callback_t status_callback;
    /* 消息发送后的结果回调处理 */
    aiot_device_msg_result_callback_t result_callback;
    /* 执行回调处理后，回传给用户的参数 */
    void *userdata;

    /* ------------以上为配置项，以下为运行时参数 ----------------- */
    /* 处于连接状态下的通道个数 */
    uint32_t online_num;
    /* 是否已建连过 */
    uint8_t  has_connected;
    /* 传输层的通道列表，多连接时用于管理多连接 */
    struct core_list_head transport_list;
    /* 当前用于发送消息的节点，用于轮询发送，快速找到下个传输节点 */
    core_trans_channel_t *current_trans;
    void *trans_mutex;
    /* 异步建连线程句柄 */
    void *thread;
    /* 异步建连线程退出标志 */
    int8_t thread_exit;
    /* 多线程执行时，锁定运行时变量 */
    void *run_lock;
} core_msg_service_handle_t;

void *core_msg_service_init(aiot_linkconfig_t *config);
int32_t core_msg_service_set_event_callback(void *handle, aiot_device_msg_callback_t message_callback, core_channel_status_callback_t status_callback, aiot_device_msg_result_callback_t result_callback, void *userdata);
int32_t core_msg_service_connect(void *handle, int8_t sync);
int32_t core_msg_service_send(void *handle, const aiot_msg_t *message);
int32_t core_msg_service_sub_topic(void *handle, const char *topic, uint8_t qos, int32_t cid);
int32_t core_msg_service_unsub_tpoic(void *handle, const char *topic, uint8_t qos, int32_t cid);
int32_t core_msg_service_disconnect(void *handle);
int32_t core_msg_service_deinit(void **handle);
int32_t core_msg_find_valid_cid(void *handle);


#if defined(__cplusplus)
}
#endif

#endif