/**
 * @file aiot_linkconfig_api.c
 * @brief 连接物联网平台的配置管理，包含tls/mqtt/http连接等参数配置
 * @date 2021-12-27
 *
 * @copyright Copyright (C) 2015-2025 Alibaba Group Holding Limited
 *
 */

#include "aiot_linkconfig_api.h"
#include "core_os.h"
#include "aiot_state_api.h"
#include "core_string.h"
#include "protocol_config.h"

aiot_linkconfig_t *aiot_linkconfig_init(aiot_protocol_type_t type)
{
    aiot_linkconfig_t *config = (aiot_linkconfig_t *)core_os_malloc(sizeof(aiot_linkconfig_t));

    if(config == NULL) {
        return NULL;
    }
    if(type != MQTT_PROTOCOL && type != HTTP_PROTOCOL) {
        return NULL;
    }

    config->protocol = type;
    config->proto_config = protocol_config_init();
    config->start_cid = 0;
    config->channel_num = 1;

    return config;
}

int32_t aiot_linkconfig_deinit(aiot_linkconfig_t **config)
{
    aiot_linkconfig_t *link_config;
    if(config == NULL || *config == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    link_config = *config;
    protocol_config_deinit((aiot_protocol_config_t **)&link_config->proto_config);
    core_os_free(*config);
    *config = NULL;

    return STATE_SUCCESS;
}

int32_t aiot_linkconfig_host(aiot_linkconfig_t *config, const char *host, uint16_t port)
{
    aiot_protocol_config_t *proto_config = NULL;
    if(config == NULL || host == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    proto_config = (aiot_protocol_config_t *)config->proto_config;

    if(proto_config->host != NULL) {
        core_os_free(proto_config->host);
        proto_config->host = NULL;
    }
    core_strdup(NULL, &proto_config->host, host, "");
    proto_config->port = port;

    return STATE_SUCCESS;
}

int32_t aiot_linkconfig_cred_option(aiot_linkconfig_t *config, aiot_sysdep_network_cred_option_t option)
{
    aiot_protocol_config_t *proto_config = NULL;
    if(config == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }
    proto_config = (aiot_protocol_config_t *)config->proto_config;

    if(option >= AIOT_SYSDEP_NETWORK_CRED_MAX) {
        return STATE_USER_INPUT_OUT_RANGE;
    }
    proto_config->cred.option = option;

    return STATE_SUCCESS;
}

int32_t aiot_linkconfig_tls_version(aiot_linkconfig_t *config, aiot_sysdep_networkt_tls_version_t version)
{
    aiot_protocol_config_t *proto_config = NULL;
    if(config == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }
    proto_config = (aiot_protocol_config_t *)config->proto_config;

    proto_config->cred.force_version = version;

    return STATE_SUCCESS;
}

int32_t aiot_linkconfig_tls_gm(aiot_linkconfig_t *config, uint8_t gm_ssl)
{
    aiot_protocol_config_t *proto_config = NULL;
    if(config == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }
    proto_config = (aiot_protocol_config_t *)config->proto_config;

    proto_config->cred.gm_ssl = gm_ssl;

    return STATE_SUCCESS;
}

int32_t aiot_linkconfig_root_cert(aiot_linkconfig_t *config, const char *cert, uint32_t len)
{
    aiot_protocol_config_t *proto_config = NULL;
    if(config == NULL || cert == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }
    proto_config = (aiot_protocol_config_t *)config->proto_config;

    proto_config->cred.x509_server_cert = cert;
    proto_config->cred.x509_server_cert_len = len;

    return STATE_SUCCESS;
}
int32_t aiot_linkconfig_client_cert_privkey(aiot_linkconfig_t *config, const char *client_cert, uint32_t cert_len, const char *client_privkey, uint32_t privkey_len)
{
    aiot_protocol_config_t *proto_config = NULL;
    if(config == NULL || client_cert == NULL || client_privkey == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }
    proto_config = (aiot_protocol_config_t *)config->proto_config;

    proto_config->cred.sni_enabled = 1;
    proto_config->cred.x509_client_cert = client_cert;
    proto_config->cred.x509_client_cert_len = cert_len;
    proto_config->cred.x509_client_privkey = client_privkey;
    proto_config->cred.x509_client_privkey_len = privkey_len;

    return STATE_SUCCESS;
}


int32_t aiot_linkconfig_connect_timeout(aiot_linkconfig_t *config, uint32_t timeout_ms)
{
    aiot_protocol_config_t *proto_config = NULL;
    if(config == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }
    proto_config = (aiot_protocol_config_t *)config->proto_config;

    proto_config->connect_timeout_ms = timeout_ms;

    return STATE_SUCCESS;
}

int32_t aiot_linkconfig_send_timeout(aiot_linkconfig_t *config, uint32_t timeout_ms)
{
    aiot_protocol_config_t *proto_config = NULL;
    if(config == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }
    proto_config = (aiot_protocol_config_t *)config->proto_config;

    proto_config->send_timeout_ms = timeout_ms;

    return STATE_SUCCESS;
}

int32_t aiot_linkconfig_recv_timeout(aiot_linkconfig_t *config, uint32_t timeout_ms)
{
    aiot_protocol_config_t *proto_config = NULL;
    if(config == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }
    proto_config = (aiot_protocol_config_t *)config->proto_config;

    proto_config->recv_timeout_ms = timeout_ms;

    return STATE_SUCCESS;
}

int32_t aiot_linkconfig_reconnect_enable(aiot_linkconfig_t *config, uint8_t enable)
{
    aiot_protocol_config_t *proto_config = NULL;
    if(config == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }
    proto_config = (aiot_protocol_config_t *)config->proto_config;

    proto_config->reconnect_enable = enable;
    return STATE_SUCCESS;
}

int32_t aiot_linkconfig_multi_channel(aiot_linkconfig_t *config, uint32_t start_cid, uint32_t num)
{
    if(config == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    if(num == 0 || num > 100) {
        return STATE_USER_INPUT_OUT_RANGE;
    }

    config->start_cid = start_cid;
    config->channel_num = num;
    return STATE_SUCCESS;
}

int32_t aiot_linkconfig_mqtt_extclientid(aiot_linkconfig_t *config, const char *extern_clientid)
{
    if(config == NULL || extern_clientid == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    return protocol_config_add_ext_clientid(config->proto_config, extern_clientid);
}

int32_t aiot_linkconfig_mqtt_keepalive(aiot_linkconfig_t *config, uint16_t keep_alive_s)
{
    aiot_protocol_config_t *proto_config = NULL;
    if(config == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }
    proto_config = (aiot_protocol_config_t *)config->proto_config;

    proto_config->keep_alive_s = keep_alive_s;
    return STATE_SUCCESS;
}

int32_t aiot_linkconfig_mqtt_cleansession(aiot_linkconfig_t *config, uint8_t clean_session)
{
    aiot_protocol_config_t *proto_config = NULL;
    if(config == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }
    proto_config = (aiot_protocol_config_t *)config->proto_config;

    proto_config->clean_session = clean_session;
    return STATE_SUCCESS;
}

int32_t aiot_linkconfig_mqtt_heartbeat_interval(aiot_linkconfig_t *config, uint32_t interval_ms)
{
    aiot_protocol_config_t *proto_config = NULL;
    if(config == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }
    proto_config = (aiot_protocol_config_t *)config->proto_config;

    proto_config->heartbeat_interval_ms = interval_ms;
    return STATE_SUCCESS;
}

int32_t aiot_linkconfig_dynamic_register(aiot_linkconfig_t *config, const char *product_secret, const char *instance_id)
{
    aiot_protocol_config_t *proto_config = NULL;
    if(config == NULL || product_secret == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }
    proto_config = (aiot_protocol_config_t *)config->proto_config;

    core_strdup(NULL, &proto_config->product_secret, product_secret, "");
    if(instance_id != NULL) {
        core_strdup(NULL, &proto_config->instance_id, instance_id, "");
    }

    return STATE_SUCCESS;
}

int32_t aiot_linkconfig_mqtt_auth(aiot_linkconfig_t *config, const char *username, const char *password, const char *clientid)
{
    aiot_protocol_config_t *proto_config = NULL;
    if(config == NULL || username == NULL || password == NULL || clientid == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }
    proto_config = (aiot_protocol_config_t *)config->proto_config;

    if(proto_config->username != NULL) {
        core_os_free(proto_config->username);
        proto_config->clientid = NULL;
    }
    if(proto_config->password != NULL) {
        core_os_free(proto_config->password);
        proto_config->clientid = NULL;
    }
    if(proto_config->clientid != NULL) {
        core_os_free(proto_config->clientid);
        proto_config->clientid = NULL;
    }

    core_strdup(NULL, &proto_config->username, username, "");
    core_strdup(NULL, &proto_config->password, password, "");
    core_strdup(NULL, &proto_config->clientid, clientid, "");

    return STATE_SUCCESS;
}