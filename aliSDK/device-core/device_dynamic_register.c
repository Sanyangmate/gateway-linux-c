/**
 * @file device_dynamic_register.c
 * @brief 设备动态注册
 * @date 2021-12-27
 *
 * @copyright Copyright (C) 2015-2025 Alibaba Group Holding Limited
 *
 */
#include <stdio.h>
#include "aiot_state_api.h"
#include "aiot_device_api.h"
#include "device_private.h"
#include "core_string.h"
#include "device_module.h"
#include "core_http.h"
#include "core_mqtt_client.h"
#include "core_os.h"
#include "protocol_config.h"

typedef struct {
    uint32_t code;
    uint8_t *content;
    uint32_t content_len;
    uint32_t content_total_len;
} core_mqtt_response_t;

#define TAG "DYNREG"

/* 定义dynreg模块内部的会话句柄结构体, SDK用户不可见, 只能得到void *handle类型的指针 */
typedef struct {
    char       *host;                               /* 会话目标服务器域名 */
    uint16_t    port;                               /* 会话目标服务器端口 */
    char       *product_key;
    char       *product_secret;
    char       *device_name;
    uint8_t     flag_nowhitelist;                   /* 是否使用免白名单功能 */
    void       *mqtt_handle;
    char
    *instance_id;                        /* 实例ID，当用户使用自购实例，且使用免白名单方式时，需设置实例ID */
    uint32_t    recv_timeout_ms;                    /* 从协议栈收包时最长等待时间 */
    uint32_t    send_timeout_ms;                    /* 向协议栈写入时最长花费时间 */
    uint32_t    timeout_ms;
    uint8_t     flag_completed;
    /* 结果缓存 */
    char        device_secret[128];
    char        username[128];
    char        password[128];
    char        clientID[128];
} dynreg_handle_t;

#define DYNREG_DEFAULT_TIMEOUT_MS               (5 * 1000)

dynreg_handle_t *core_dynamic_register_init()
{
    dynreg_handle_t *dynreg = core_os_malloc(sizeof(dynreg_handle_t));
    memset(dynreg, 0, sizeof(dynreg_handle_t));

    dynreg->timeout_ms = DYNREG_DEFAULT_TIMEOUT_MS;
    dynreg->send_timeout_ms = DYNREG_DEFAULT_TIMEOUT_MS;
    dynreg->recv_timeout_ms = DYNREG_DEFAULT_TIMEOUT_MS;
    return dynreg;
}

void core_dynamic_register_deinit(dynreg_handle_t *dynreg)
{
    core_os_free(dynreg);
}
static void core_dynreg_recv_callback(void *handle, const aiot_mqtt_recv_t *packet, void *userdata)
{
    dynreg_handle_t *dynreg = (dynreg_handle_t *)userdata;
    switch (packet->type) {
    case AIOT_MQTTRECV_PUB: {
        char *topic = packet->data.pub.topic;
        uint32_t topic_len = packet->data.pub.topic_len;
        char *payload = (char *)packet->data.pub.payload;
        uint32_t payload_len = packet->data.pub.payload_len;
        const char *topic_register = "/ext/register";
        const char *topic_regnwl = "/ext/regnwl";

        if (strlen(topic_register) == topic_len && 0 == memcmp(topic_register, topic, topic_len)) {
            const char *key_ds = "deviceSecret";
            char *ds = NULL;
            uint32_t ds_len = 0;

            core_json_value(payload, payload_len, key_ds, strlen(key_ds),
                            &ds, &ds_len);

            if (ds == NULL || ds_len == 0) {
                ALOG_ERROR(TAG, STATE_MESSAGE_PARSE_ERROR, "msg(%.*s) json parse error\r\n", topic_len, topic);
                break;
            }

            dynreg->flag_completed = 1;
            memcpy(dynreg->device_secret, ds, ds_len);
        } else if (strlen(topic_regnwl) == topic_len && 0 == memcmp(topic_regnwl, topic, topic_len)) {
            const char *key_clientid = "clientId";
            const char *key_devicetoken = "deviceToken";
            char *client_id = NULL;
            char *device_token = NULL;
            uint32_t client_id_len = 0;
            uint32_t device_token_len = 0;
            char *conn_clientid = NULL;
            char *conn_username = NULL;
            char *conn_username_fmt[] = { dynreg->device_name, dynreg->product_key };

            core_json_value(payload, payload_len, key_clientid, strlen(key_clientid), &client_id, &client_id_len);
            core_json_value(payload, payload_len, key_devicetoken, strlen(key_devicetoken), &device_token, &device_token_len);

            if (client_id == NULL || device_token == NULL || client_id_len == 0 || device_token_len == 0) {
                ALOG_ERROR(TAG, STATE_MESSAGE_PARSE_ERROR, "msg(%.*s) json parse error\r\n", topic_len, topic);
                break;
            }

            dynreg->flag_completed = 1;
            *(client_id + client_id_len) = 0;
            *(device_token + device_token_len) = 0;
            core_sprintf(NULL, &conn_clientid, "%s|authType=connwl,securemode=-2,_ss=1,ext=3,_v="CORE_AUTH_SDK_VERSION"|",
                         &client_id, 1, NULL);
            core_sprintf(NULL,  &conn_username, "%s&%s",
                         conn_username_fmt, sizeof(conn_username_fmt) / sizeof(char *), NULL);

            strncpy(dynreg->clientID, conn_clientid, sizeof(dynreg->clientID));
            strncpy(dynreg->username, conn_username, sizeof(dynreg->username));
            memcpy(dynreg->password, device_token, device_token_len);
            core_os_free(conn_clientid);
            core_os_free(conn_username);
        }
    }
    break;
    default:
        break;
    }
}
int32_t core_dynamic_register_request(dynreg_handle_t *dynreg, aiot_linkconfig_t *linkconfig)
{
    int32_t res = STATE_SUCCESS;
    char *auth_clientid = NULL;
    char *auth_username = NULL;
    char auth_password[65] = {0};
    char *sign_input = NULL;
    uint32_t random_num = 0;
    char random[11] = {0};
    char *auth_type = NULL;
    uint8_t reconnect = 0;
    aiot_protocol_config_t* proto_config = NULL;

    dynreg->mqtt_handle = aiot_mqtt_init();
    if(dynreg->mqtt_handle == NULL) {
        ALOG_ERROR(TAG, STATE_SYS_DEPEND_MALLOC_FAILED, "malloc failed \r\n");
        return STATE_SYS_DEPEND_MALLOC_FAILED;
    }

    auth_type = dynreg->flag_nowhitelist ? "regnwl" : "register";
    core_os_rand((uint8_t *)&random_num, 4);
    core_uint2str(random_num, random, NULL);

    /* assamble clientid, username and password */
    {
        uint8_t has_instance_id = (dynreg->flag_nowhitelist && dynreg->instance_id != NULL) ? 1 : 0;
        char *client_fmt = (has_instance_id) ?
                           "%s.%s|random=%s,authType=%s,securemode=2,signmethod=hmacsha256,instanceId=%s|" :
                           "%s.%s|random=%s,authType=%s,securemode=2,signmethod=hmacsha256|";
        char *client_src[] = { dynreg->device_name, dynreg->product_key,
                               random, auth_type, dynreg->instance_id
                             };
        char *username_fmt = "%s&%s";
        char *username_src[] = { dynreg->device_name, dynreg->product_key };
        char *sign_input_fmt = "deviceName%sproductKey%srandom%s";
        uint8_t sign_output[32] = {0};

        core_sprintf(NULL, &auth_clientid, client_fmt, client_src,
                     has_instance_id ? 5 : 4, NULL);
        core_sprintf(NULL, &auth_username, username_fmt, username_src,
                     sizeof(username_src) / sizeof(char *), NULL);
        core_sprintf(NULL, &sign_input, sign_input_fmt, client_src,
                     3, NULL);
        core_hmac_sha256((const uint8_t *)sign_input, (uint32_t)strlen(sign_input),
                         (const uint8_t *)dynreg->product_secret,
                         (uint32_t)strlen(dynreg->product_secret), sign_output);
        core_hex2str(sign_output, sizeof(sign_output), auth_password, 0);
    }

    proto_config = (aiot_protocol_config_t *)linkconfig->proto_config;
    if (((res = aiot_mqtt_setopt(dynreg->mqtt_handle, AIOT_MQTTOPT_HOST,
                                 (void *)proto_config->host)) < STATE_SUCCESS) ||
            ((res = aiot_mqtt_setopt(dynreg->mqtt_handle, AIOT_MQTTOPT_PORT,
                                     (void *)&proto_config->port)) < STATE_SUCCESS) ||
            ((res = aiot_mqtt_setopt(dynreg->mqtt_handle, AIOT_MQTTOPT_CLIENTID,
                                     (void *)auth_clientid)) < STATE_SUCCESS) ||
            ((res = aiot_mqtt_setopt(dynreg->mqtt_handle, AIOT_MQTTOPT_USERNAME,
                                     (void *)auth_username)) < STATE_SUCCESS) ||
            ((res = aiot_mqtt_setopt(dynreg->mqtt_handle, AIOT_MQTTOPT_PASSWORD,
                                     (void *)auth_password)) < STATE_SUCCESS) ||
            ((res = aiot_mqtt_setopt(dynreg->mqtt_handle, AIOT_MQTTOPT_NETWORK_CRED,
                                     (void *)&proto_config->cred)) < STATE_SUCCESS) ||
            ((res = aiot_mqtt_setopt(dynreg->mqtt_handle, AIOT_MQTTOPT_SEND_TIMEOUT_MS,
                                     (void *)&dynreg->send_timeout_ms)) < STATE_SUCCESS) ||
            ((res = aiot_mqtt_setopt(dynreg->mqtt_handle, AIOT_MQTTOPT_RECV_TIMEOUT_MS,
                                     (void *)&dynreg->recv_timeout_ms)) < STATE_SUCCESS) ||
            ((res = aiot_mqtt_setopt(dynreg->mqtt_handle, AIOT_MQTTOPT_RECV_HANDLER,
                                     (void *)core_dynreg_recv_callback)) < STATE_SUCCESS) ||
            ((res = aiot_mqtt_setopt(dynreg->mqtt_handle, AIOT_MQTTOPT_USERDATA,
                                     (void *)dynreg)) < STATE_SUCCESS) ||
            ((res = aiot_mqtt_setopt(dynreg->mqtt_handle, AIOT_MQTTOPT_RECONN_ENABLED,
                                     (void *)&reconnect)) < STATE_SUCCESS)) {
        aiot_mqtt_deinit(&dynreg->mqtt_handle);
        core_os_free(auth_clientid);
        core_os_free(auth_username);
        core_os_free(sign_input);
        return res;
    }

    res = aiot_mqtt_connect(dynreg->mqtt_handle);
    if (res < STATE_SUCCESS) {
        aiot_mqtt_deinit(&dynreg->mqtt_handle);
    }

    core_os_free(auth_clientid);
    core_os_free(auth_username);
    core_os_free(sign_input);

    return STATE_SUCCESS;
}

static int32_t core_dynreg_recv(dynreg_handle_t *dynreg)
{
    int32_t res = STATE_SUCCESS;
    uint64_t starttime = core_os_time();

    while (1) {
        if (core_os_time() - starttime >= dynreg->timeout_ms) {
            ALOG_ERROR(TAG, STATE_DEVICE_DYNREG_TIMEOUT, "device dynamic register timeout\r\n");
            res = STATE_DEVICE_DYNREG_TIMEOUT;
            break;
        }
        if((res = aiot_mqtt_recv(dynreg->mqtt_handle)) < 0) {
            break;
        }

        if(dynreg->flag_completed) {
            res = STATE_SUCCESS;
            break;
        }
    }

    return res;
}

int32_t aiot_device_dynamic_secret(void *device, aiot_linkconfig_t *linkconfig, char *out_device_seceret, int32_t out_secret_len)
{
    device_handle_t *dev = (device_handle_t *)device;
    dynreg_handle_t *dynreg = NULL;
    int32_t res = STATE_SUCCESS;
    aiot_protocol_config_t* proto_config = NULL;
    /* 边界条件判断 */
    if(dev == NULL || linkconfig == NULL || dev->product_key == NULL || dev->device_name == NULL || out_device_seceret == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    dynreg = core_dynamic_register_init();
    if(dynreg == NULL) {
        return STATE_SYS_DEPEND_MALLOC_FAILED;
    }
    proto_config = (aiot_protocol_config_t *)linkconfig->proto_config;

    dynreg->flag_nowhitelist = 0;
    dynreg->device_name = dev->device_name;
    dynreg->product_key = dev->product_key;
    dynreg->instance_id = proto_config->instance_id;
    dynreg->product_secret = proto_config->product_secret;

    ALOG_INFO(TAG, "%s|%s dynamic get secret \r\n", dev->product_key, dev->device_name);

    /* 发送请求 */
    res = core_dynamic_register_request(dynreg, linkconfig);
    if(res != STATE_SUCCESS) {
        core_dynamic_register_deinit(dynreg);
        ALOG_INFO(TAG, "%s|%s dynamic get secret failed\r\n", dev->product_key, dev->device_name);
        return res;
    }
    /* 等待接收 */
    res = core_dynreg_recv(dynreg);
    if(res != STATE_SUCCESS) {
        core_dynamic_register_deinit(dynreg);
        ALOG_INFO(TAG, "%s|%s dynamic get secret timeout\r\n", dev->product_key, dev->device_name);
        return res;
    }

    /* 获取结果 */
    memset(out_device_seceret, 0, out_secret_len);
    strncpy(out_device_seceret, dynreg->device_secret, out_secret_len);
    ALOG_INFO(TAG, "%s|%s dynamic get secret  success\r\n", dev->product_key, dev->device_name);

    aiot_mqtt_deinit(&dynreg->mqtt_handle);
    core_dynamic_register_deinit(dynreg);
    return STATE_SUCCESS;
}

int32_t aiot_device_dynamic_register(void *device, aiot_linkconfig_t *linkconfig, aiot_dynamic_register_info_t *info)
{
    device_handle_t *dev = (device_handle_t *)device;
    dynreg_handle_t *dynreg = NULL;
    int32_t res = STATE_SUCCESS;
    aiot_protocol_config_t* proto_config = NULL;
    /* 边界条件判断 */
    if(dev == NULL || linkconfig == NULL || dev->product_key == NULL || dev->device_name == NULL || info == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_dynamic_register input null , please check\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    dynreg = core_dynamic_register_init();
    if(dynreg == NULL) {
        ALOG_ERROR(TAG, STATE_SYS_DEPEND_MALLOC_FAILED, "malloc failed \r\n");
        return STATE_SYS_DEPEND_MALLOC_FAILED;
    }
    proto_config = (aiot_protocol_config_t *)linkconfig->proto_config;

    dynreg->flag_nowhitelist = 1;
    dynreg->device_name = dev->device_name;
    dynreg->product_key = dev->product_key;
    dynreg->instance_id = proto_config->instance_id;
    dynreg->product_secret = proto_config->product_secret;

    ALOG_INFO(TAG, "%s|%s dynamic register\r\n", dev->product_key, dev->device_name);

    /* 发送请求 */
    res = core_dynamic_register_request(dynreg, linkconfig);
    if(res != STATE_SUCCESS) {
        core_dynamic_register_deinit(dynreg);
        ALOG_INFO(TAG, "%s|%s dynamic register failed\r\n", dev->product_key, dev->device_name);
        return res;
    }
    /* 等待接收 */
    res = core_dynreg_recv(dynreg);
    if(res != STATE_SUCCESS) {
        core_dynamic_register_deinit(dynreg);
        ALOG_INFO(TAG, "%s|%s dynamic register timeout\r\n", dev->product_key, dev->device_name);
        return res;
    }

    /* 获取结果 */
    memset(info, 0, sizeof(aiot_dynamic_register_info_t));

    strncpy(info->username, dynreg->username, sizeof(info->username));
    strncpy(info->password, dynreg->password, sizeof(info->password));
    strncpy(info->client_id, dynreg->clientID, sizeof(info->client_id));
    ALOG_INFO(TAG, "%s|%s dynamic register success\r\n", dev->product_key, dev->device_name);
    /* ALOG_INFO(TAG, "username %s, password %s, clientid %d\r\n", info->username, info->password, info->client_id); */

    aiot_mqtt_deinit(&dynreg->mqtt_handle);
    core_dynamic_register_deinit(dynreg);
    return STATE_SUCCESS;
}
