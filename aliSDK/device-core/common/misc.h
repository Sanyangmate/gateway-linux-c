

#ifndef _MISC_H_
#define _MISC_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>

char* get_topic_level(char *topic, uint8_t level);



#if defined(__cplusplus)
}
#endif

#endif