#include <stdio.h>
#include "misc.h"
#include "core_os.h"



char* get_topic_level(char *topic, uint8_t level)
{
    uint32_t i = 0;
    uint16_t level_curr = 0;
    char *p_open = NULL;
    char *p_close = NULL;
    char *p_name = NULL;
    uint16_t name_len = 0;
    int32_t topic_len = strlen(topic);

    for (i = 0; i < (topic_len - 1); i++) {
        if (topic[i] == '/') {
            level_curr++;
            if (level_curr == level && p_open == NULL) {
                p_open = topic + i + 1;
            }

            if (level_curr == (level + 1) && p_close == NULL) {
                p_close = topic + i;
            }
        }
    }

    if (p_open == NULL) {
        return NULL;
    }
    if (p_close == NULL) {
        p_close = topic + topic_len;
    }

    name_len = p_close - p_open;
    p_name = core_os_malloc(name_len + 1);
    if (p_name == NULL) {
        return NULL;
    }
    memset(p_name, 0, name_len + 1);
    memcpy(p_name, p_open, name_len);

    return p_name;
}
