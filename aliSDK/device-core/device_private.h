#ifndef _AIOT_DEVICE_PRIVATE_API_H_
#define _AIOT_DEVICE_PRIVATE_API_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>
#include "aiot_sysdep_api.h"
#include "message_private.h"
#include "aiot_device_api.h"
#include "core_list.h"

#define  ALINK_ID_MAX
#define  DEFAULT_CID        -1

/* 设备订阅消息节点 */
typedef struct {
    /* 订阅的topic名称 */
    char *topic;
    /* 该topic对应的回调函数 */
    aiot_device_msg_callback_t callback;
    /* 回调执行的上下文 */
    void *userdata;
    /* 链表节点 */
    struct core_list_head linked_node;
} core_device_sub_node_t;

typedef struct {
    /**
     * @brief 内部状态事件类型
     *
     */
    aiot_device_status_type_t type;
    /**
     * @brief 错误码
     */
    uint32_t error_code;
    /**
     * @brief 通道id
     */
    uint32_t cid;
    /**
     * @brief 通道在线总数
     */
    uint32_t online_num;
} core_channel_status_t;
/**
 * @brief 通道变化的回调函数
 */
typedef void (*core_channel_status_callback_t)(void *device, const core_channel_status_t *event, void *userdata);


/* 设备与连接消息模块交互的接口原型 */
typedef struct {
    /* 设备同步建连的接口原型 */
    int32_t (*connect)(void *device);
    /* 设备异步建连的接口原型 */
    int32_t (*connect_async)(void *device);
    /* 设备异步断连的接口原型 */
    int32_t (*disconnect)(void *device);
    /* 设备发送消息的接口原型 */
    int32_t (*send_message)(void *device, const aiot_msg_t *message);
    /* 设备向物联网平台订阅消息的接口原型 */
    int32_t (*sub_topic)(void *device, const char *topic, uint8_t qos, int32_t cid);
    /* 设备向物联网平台取消订阅消息的接口原型 */
    int32_t (*unsub_topic)(void *device, const char *topic, uint8_t qos, int32_t cid);
    /* 设备消息回调接口原型 */
    void    (*message_handle)(void *device, const aiot_msg_t *message);
    /* 设备连接状态回调接口原型 */
    void    (*status_handle)(void *device, const aiot_device_status_t *event);
    /* 设备消息ack回调接口原型 */
    void    (*result_handle)(void *device, const aiot_device_msg_result_t *result);
} device_operation_t;

#define STATE_COMPRESS_SUCCESS    1   /* prepare回调压缩模块，不需要压缩/解压缩 */
typedef int32_t (*core_device_compress_callback_t)(void *context, const aiot_msg_t *src, aiot_msg_t **dest);
typedef struct {
    core_device_compress_callback_t handler;
    void *context;
} core_device_compress_data_t;

/* 默认的直连设备操作接口 */
extern device_operation_t normal_ops;

/* 设备管理模块核心结构体实现 */
typedef struct {
    /* 设备认证信息 */
    char *product_key;
    char *device_name;
    char *device_secret;
    /* 订阅处理链表 */
    struct core_list_head sub_list;
    void *sub_list_mutex;
    /* 设备回调函数相关 */
    aiot_device_msg_callback_t recv_callback;
    aiot_device_status_callback_t status_callback;
    aiot_device_msg_result_callback_t result_callback;
    void *userdata;
    /* 压缩解压缩回调相关 */
    core_device_compress_data_t compress;
    core_device_compress_data_t decompress;

    /* 高级模块管理 */
    void *module_manager;
    /* 全局唯一的消息id */
    int32_t alink_id;
    void *alink_id_mutex;
    /* 标志设备是否已经建连，避免重复建连 */
    int32_t has_connected;
    /* 在线状态标志 */
    int8_t  online;
    /* 设备操作方法 */
    const device_operation_t *ops;
    /* 连接参数备份 */
    aiot_linkconfig_t linkconfig;
    /* 消息传输服务相关 */
    void *message_service;
    /* 子设备使用该参数，关联的网关设备 */
    void *gateway_device;
    /* 子设备使用：归属的cid */
    int32_t owner_cid;
} device_handle_t;

/* 组件类型，需要确保全局唯一 */
typedef enum {
    MODULE_DM,
    MODULE_OTA,
    MODULE_GATEWAY,
    MODULE_LOGPOST,
    MODULE_SHADOW,
    MODULE_NTP,
    MODULE_REMOTE_CONFIG,
    MODULE_DEVINFO,
    MODULE_COMPRESS,
    MODULE_MAX,
} module_type_t;

/* 单个组件操作结构体 */
typedef struct {
    int32_t internal_ms;
    void *(*on_init)();
    void (*on_process)(void *handle);
    void (*on_status)(void *handle, aiot_device_status_t event);
    void (*on_channel_status)(void *handle, const core_channel_status_t *event);
    void (*on_deinit)(void *handle);
} module_ops_t;

/* 高级组件动态添加 */
int32_t core_device_module_register(void *device, module_type_t key, module_ops_t *ops);
int32_t core_device_module_unregister(void *device, module_type_t key);
void*   core_device_module_get(void *device, module_type_t key);

/* 设备初步检查连接参数是否正常 */
int32_t core_device_check_linkconfig(device_handle_t *device);

/* 组件查询device参数交口 */
char *core_device_secret(void *device);
int32_t core_device_get_next_alink_id(void *device);

/* 主要用于alink_id作为字符串传入json，公用的接口 */
int32_t _cJSON_ADDInt2StrToObject(void *root, char *key, int32_t value);

/* 设置压缩回调接口 */
int32_t core_device_set_compr_data(void *device, core_device_compress_data_t compress);
int32_t core_device_set_decompr_data(void *device, core_device_compress_data_t decompress);

/* 传输层获取有效的通道 */
int32_t core_device_get_valid_cid(void *device);
#if defined(__cplusplus)
}
#endif

#endif