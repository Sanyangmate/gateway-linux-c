#ifndef _DEMO_CONFIG_H_
#define _DEMO_CONFIG_H_

#if 1 //就这

    /* TODO: 替换为自己的接入信息*/
    // 在MQTT 连接参数的 改：mqttHostUrl a1Siq9fRZ4K.iot-as-mqtt.cn-shanghai.aliyuncs.com
    #define INSTANCE_ID      "a1Siq9fRZ4K"
    #define HOST             INSTANCE_ID".iot-as-mqtt.cn-shanghai.aliyuncs.com"
    #define PORT             443

    /* TODO: 替换直连设备认证信息:产品名、设备名、设备密钥、产品密钥 */
    #define PRODUCT_KEY       "a1Siq9fRZ4K"
    #define DEVICE_NAME       "gatewayAli"
    #define DEVICE_SECRET     "578c1ea77ea28426b56d72e25d75fcea"
    #define PRODUCT_SECRET    "h4IQZ5****L075Zi"          /* 仅动态注册功能需要使用 */

#endif

/* 网关子设备demo TODO: 替换网关设备认证信息 */
#define GW_PRODUCT_KEY     "a1kt****O8H"
#define GW_DEVICE_NAME     "GATEWAY_****_001"
#define GW_DEVICE_SECRET   "d414****c32e2"

/* 网关子设备demo TODO: 替换子设备认证信息 */
#define SUBDEV_LIST {                                                    \
    {                                                                    \
        .product_key = "a1o****qvg7",                                    \
        .device_name = "SUBDEV_****_001",                                \
        .device_secret = "af1e********55a822931af372cbc6e7",             \
    },                                                                   \
    {                                                                    \
        .product_key = "a1o****qvg7",                                    \
        .device_name = "SUBDEV_****_002",                                \
        .device_secret = "f15*********688b7dd5e9d7e8ee0b51",             \
    },                                                                   \
    {                                                                    \
        .product_key = "a1o****qvg7",                                    \
        .device_name = "SUBDEV_****_003",                                \
        .device_secret = "eca3*******b3f056f3b6abe06538648",             \
    },                                                                   \
}

/* TODO: 网关与子设备动态注册demo: 子设备的信息列表 */
#define SUBDEV_DYNREG_LIST {                                             \
    {                                                                    \
        .product_key = "gb80****8MP",                                    \
        .device_name = "STAND_********_d001",                            \
        .product_secret = "rqml*******l2oS2",                            \
    },                                                                   \
    {                                                                    \
        .product_key = "gb80****8MP",                                    \
        .device_name = "STAND_********_d002",                            \
        .product_secret = "rqml*******l2oS2",                            \
    },                                                                   \
    {                                                                    \
        .product_key = "gb80****8MP",                                    \
        .device_name = "STAND_********_d003",                            \
        .product_secret = "rqml*******l2oS2",                            \
    },                                                                   \
}

#endif

