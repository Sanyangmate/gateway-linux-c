#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "aiot_device_api.h"
#include "demo_config.h"
#include <stdlib.h>
#include "cJSON.h"
#include "msg_queue_peer.h"
#include "shmem.h"
#include "dataType.h"

/* MY API */

//上报延时时间
#define DELAY_TIME 3

// 共享内存,存入几个数据
#define NUM_DATA_STM32 4
#define NUM_DATA_SLAVE 3

//共享内存API 识别标签
struct shm_param para_stm32;
struct shm_param para_slave;

//共享内存基地址
void *pBase_Shm_Data_stm32;
void *pBase_Shm_Data_slave;

//共享内存数据起始地址
stm32NodeVal_t *pData_Shm_stm32;
mbSlaveNodeVal_t *pData_Shm_slave;

char *create_My_pub_payload(void);
int init_ShareMemory(void);
int send_QueueMsg(queueMsg_t *queueMsg);

/* SDK API */
void demo_status_callback(void *device, const aiot_device_status_t *status, void *userdata);
void demo_msg_callback(void *device, const aiot_msg_t *message, void *userdata);
void demo_result_callback(void *device, const aiot_device_msg_result_t *result, void *userdata);
void demo_user_topic_callback(void *device, const aiot_msg_t *message, void *userdata);

int main(int argc, char *argv[])
{
    /* 初始化IPC */
    if (init_ShareMemory() < 0)
    {
        perror("init_ShareMemory err");
        return -1;
    }

    int32_t res = STATE_SUCCESS;
    /* 配置SDK的底层依赖 */
    aiot_sysdep_init(aiot_sysdep_get_portfile());

    /* 创建设备 */
    void *device_client = aiot_device_create(PRODUCT_KEY, DEVICE_NAME);
    if (device_client == NULL)
    {
        printf("device_client failed\n");
        return -1;
    }
    /* 设置设备密钥 */
    aiot_device_set_device_secret(device_client, DEVICE_SECRET);

    /* 设置设备消息回调及状态变化回调 */
    aiot_device_set_event_callback(device_client,
                                   demo_msg_callback,
                                   demo_status_callback,
                                   demo_result_callback,
                                   NULL);

    /* 连接配置参数初始化 */
    aiot_linkconfig_t *config = aiot_linkconfig_init(MQTT_PROTOCOL);
    /* 设置服务器的host、port */
    aiot_linkconfig_host(config, HOST, PORT);

    /* 设置设备连接参数 */
    aiot_device_set_linkconfig(device_client, config);
    /* 设备建连 */
    res = aiot_device_connect(device_client);
    if (res < STATE_SUCCESS)
    {
        /* 尝试建立连接失败, 销毁MQTT实例, 回收资源 */
        aiot_linkconfig_deinit(&config);
        aiot_device_delete(&device_client);
        printf("aiot_device_connect failed: -0x%04X\n\r\n", -res);
        return -1;
    }
    printf("\t[aiot_device_connect ok!]\n");

    /* 订阅消息，设置该消息回调函数，也支持只设备回调 */
    char *sub_topic = "/" PRODUCT_KEY "/" DEVICE_NAME "/user/get";
    int32_t msg_id =
        aiot_device_register_topic_filter(/* 设置订阅的 回调函数 */
                                          device_client, sub_topic,
                                          demo_user_topic_callback, 1, NULL);
    if (msg_id >= 0)
    {
        printf("aiot_device_register_topic_filter topic %s msg_id %d\r\n", sub_topic, msg_id);
    }

    while (1)
    {
        /* 创建消息 */
        char *pub_topic =
            "/sys/" PRODUCT_KEY "/" DEVICE_NAME "/thing/event/property/post"; /* 用的 */

        /* 生成自己要上报的字符串 */
        char *pub_payload = create_My_pub_payload();

        /* 生成 AliJson 的字符串*/
        aiot_msg_t *pub_message =
            aiot_msg_create_raw(pub_topic, (uint8_t *)pub_payload, strlen(pub_payload));

        /* 发送消息 */
        aiot_device_send_message(device_client, /* AliJson */ pub_message);

        /* 删除消息 */
        aiot_msg_delete(pub_message);

        /* 释放空间 */
        free(pub_payload);

        /* 延时阻塞 */
        sleep(DELAY_TIME);
    }

    res = aiot_device_disconnect(device_client);
    if (res < STATE_SUCCESS)
    {
        printf("aiot_device_disconnect failed: -0x%04X\n", -res);
        return -1;
    }
    /* 销毁设备实例, 一般不会运行到这里 */
    aiot_device_delete(&device_client);
    aiot_linkconfig_deinit(&config);
    return 0;
}

char *create_My_pub_payload(void)
{
    int FanSwitch = 0;   /* 风扇  bool */
    int LightSwitch = 0; /* 开关灯 bool */

    float BatteryPercentage = 0;      /* 电池百分比，后期再处理 */
    float EnvironmentHumidity = 0;    /* 湿度  */
    float EnvironmentTemperature = 0; /* 温度 */
    int LightLux = 0;                 /* 光照强度 int */
    int CO2Content = 0;               /* CO2含量 int */

#if 0 //测试的数据
    FanSwitch = LightSwitch = 1;
    BatteryPercentage = EnvironmentHumidity = EnvironmentTemperature = LightLux = CO2Content = 152.3;
#else //真实的数据
    for (int i = 0; i < NUM_DATA_STM32; i++)
    {
        switch ((pData_Shm_stm32 + i)->key)
        {
        //温度
        case 101:
            EnvironmentTemperature = (pData_Shm_stm32 + i)->data.f_val;
            break;

        //湿度
        case 102:
            EnvironmentHumidity = (pData_Shm_stm32 + i)->data.f_val;
            break;

        //电池电压
        case 103:
            BatteryPercentage = (pData_Shm_stm32 + i)->data.f_val;
            break;

        //led
        case 104:
            LightSwitch = (pData_Shm_stm32 + i)->data.b_val;
            break;
        }
    }

    for (int i = 0; i < NUM_DATA_SLAVE; i++)
    {
        switch ((pData_Shm_slave + i)->key)
        {
        //光照强度
        case 201:
            LightLux = (pData_Shm_slave + i)->data.i_val;
            break;

        //co2浓度
        case 202:
            CO2Content = (pData_Shm_slave + i)->data.i_val;
            break;

        //风扇开关
        case 203:
            FanSwitch = (pData_Shm_slave + i)->data.b_val;
            break;
        }
    }
#endif

    char *buf = (char *)malloc(350);
    if (NULL == buf)
    {
        printf("<err> create json err!!<LineL:%d>\n", __LINE__);
        return NULL;
    }

    sprintf(buf,
            "{\"id\": \"1\",\"params\": {\"BatteryPercentage\": %f,\"CO2Content\": %d,\"EnvironmentHumidity\": %f,\"EnvironmentTemperature\": %f,\"LightLux\": %d,\"FanSwitch\": %d,\"LightSwitch\": %d},\"version\": \"1.0\"}",
            BatteryPercentage,
            CO2Content,
            EnvironmentHumidity,
            EnvironmentTemperature,
            LightLux,
            FanSwitch,
            LightSwitch);

    return buf;
}

void demo_status_callback(void *device, const aiot_device_status_t *status, void *userdata)
{
    /* TODO: 设备状态变化的回调函数，用户可根据业务需求执行对应业务，回调不可长时间阻塞, 回调中不可调用设备删除操作 */
    switch (status->type)
    {
    /* SDK因为用户调用了aiot_device_connect()接口, 与服务器建立连接已成功 */
    case AIOT_DEVICE_STATUS_CONNECT:
    {
        printf("AIOT_DEVICE_STATUS_CONNECT\n");
    }
    break;

    /* SDK因为网络状况被动断连后, 自动发起重连已成功 */
    case AIOT_DEVICE_STATUS_RECONNECT:
    {
        printf("AIOT_DEVICE_STATUS_CONNECT\n");
    }
    break;

    /* SDK因为网络的状况而被动断开了连接, network是底层读写失败, heartbeat是没有按预期得到服务端心跳应答 */
    case AIOT_DEVICE_STATUS_DISCONNECT:
    {
        printf("AIOT_DEVICE_STATUS_CONNECT: %d\n", status->error_code);
    }
    break;
    }
}

void demo_msg_callback(void *device, const aiot_msg_t *message, void *userdata)
{
    /* 设备接收到消息消息，默认只做打印处理 */
    printf("[message] <<, topic: %s\n", message->topic);
    printf("[------------message--------] <<, payload: \n%.*s\n", message->payload_lenth, message->payload);

    // TODO 处理下发的指令
    // {"method":"thing.service.property.set","id":"1318932750","params":{"LightSwitch":0,"FanSwitch":1},"version":"1.0.0"}

    cJSON *root = cJSON_Parse((char *)message->payload);
    if (root == NULL)
    {
        printf("Error before:  Parse get Cmd!!![%s]\n", cJSON_GetErrorPtr());
        return;
    }

    cJSON *params = cJSON_GetObjectItem(root, "params");
    cJSON *fan_switch = cJSON_GetObjectItem(params, "FanSwitch"); //本身是bool
    cJSON *light_switch = cJSON_GetObjectItem(params, "LightSwitch");

    //如果下发了这个消息
    //控制风扇的，就发到消息队列
    if (fan_switch != NULL)
    {
        queueMsg_t queueMsg = {1};                              //long
        queueMsg.ctlDeviceState_1.b_val = fan_switch->valueint; //数据
        strcpy(queueMsg.who, Str_Who_AliSDK);                   //who 用于消息队列
        strcpy(queueMsg.deviceName, Str_DeviceName_Slave);      //设备归属
        queueMsg.cmdType = 1;                                   //下发控制
        queueMsg.key = 203;                                     //key

        //正常发送命令给消息队列
        if (msg_queue_send(QueueMsg_Recv, &queueMsg, sizeof(queueMsg), /*立即发送*/ 0) < 0)
        {
            printf("LINE:%d msg_queue_send error", __LINE__);

            //如果同时有下一条也不会执行
            //因为这个消息队列错了，下了也好不了
            goto end;
        }
        else
            puts("QueueMsg send ok from Qt Cmd");
    }

    //可以延时半秒再发第二条，为了让单片机多点时间响应
    usleep(500000); // 500毫秒 = 500000微秒

    //如果下发了这个消息
    //控制led的，就发到消息队列
    if (light_switch != NULL)
    {
        queueMsg_t queueMsg = {1};                                //long
        queueMsg.ctlDeviceState_1.b_val = light_switch->valueint; //数据
        strcpy(queueMsg.who, Str_Who_AliSDK);                     //who 用于消息队列
        strcpy(queueMsg.deviceName, Str_DeviceName_Stm32);        //设备归属
        queueMsg.cmdType = 1;                                     //下发控制
        queueMsg.key = 104;                                       //key

        //正常发送命令给消息队列
        if (msg_queue_send(QueueMsg_Recv, &queueMsg, sizeof(queueMsg), /*立即发送*/ 0) < 0)
        {
            printf("LINE:%d msg_queue_send error", __LINE__);

            //错了直接结束
            goto end;
        }
        else
            puts("QueueMsg send ok from Qt Cmd");
    }

end:
    cJSON_Delete(root);
}

void demo_result_callback(void *device, const aiot_device_msg_result_t *result, void *userdata)
{
    switch (result->type)
    {
    /* 发送qos消息，收到后执行的回调 */
    case AIOT_DEVICE_SEND_MESSAGE_RESULT:
    {
        printf("send message result, message id: %d, code -0x%04X\n", result->message_id, -result->code);
    }
    break;
    /* 注册订阅topic后得到回复执行的回调 */
    case AIOT_DEVICE_REGISTER_TOPIC_RESULT:
    {
        printf("register, message id: %d, code code -0x%04X\n", result->message_id, -result->code);
    }
    break;
    default:
        break;
    }
}

/* 自定义topic下行消息处理示例, 示例只做打印处理 */
void demo_user_topic_callback(void *device, const aiot_msg_t *message, void *userdata)
{
    printf("[thing_message] <<, topic: %s\n", message->topic);
    printf("[thing_message] <<, payload: %.*s\n", message->payload_lenth, message->payload);
}

int init_ShareMemory(void)
{
    int ret, size;

    //stm32存放数据的共享内存
    size = sizeof(int) + (NUM_DATA_STM32) * (sizeof(stm32NodeVal_t));
    ret = shm_init(&para_stm32, Sheme_ID_stm32, size);
    if (ret < 0)
    {
        perror("Stm32 Data shareMemory malloc Err!");
        return -1;
    }
    pBase_Shm_Data_stm32 = shm_getaddr(&para_stm32);      //基地址
    pData_Shm_stm32 = pBase_Shm_Data_stm32 + sizeof(int); //数据开始的地址

    //slave存放数据的共享内存
    size = sizeof(int) + (NUM_DATA_SLAVE) * (sizeof(mbSlaveNodeVal_t));
    ret = shm_init(&para_slave, Sheme_ID_slave, size);
    if (ret < 0)
    {
        perror("Slave Data shareMemory malloc Err!");
        return -1;
    }
    pBase_Shm_Data_slave = shm_getaddr(&para_slave);      //基地址
    pData_Shm_slave = pBase_Shm_Data_slave + sizeof(int); //数据开始的地址

    return 0;
}

int send_QueueMsg(queueMsg_t *queueMsg)
{
    //正常发送命令给消息队列
    if (msg_queue_send(QueueMsg_Recv, queueMsg, sizeof(queueMsg_t), /*立即发送*/ 0) < 0)
    {
        perror("msg_queue_send error");
        return -1;
    }
    else
    {
        puts("QueueMsg send ok from Qt Cmd");
        return 0;
    }
}