# 简介

阿里云的[物联网平台](https://www.aliyun.com/product/iot)提供安全可靠的设备连接通信能力, 帮助用户将海量设备数据采集上云

设备可通过集成 `LinkSDK` 具备连接物联网平台服务器的能力, 以及进而使用物联网平台的其它高级能力

---
这个SDK以C99编写, 开源发布, 支持多种操作系统和硬件平台.

# 了解SDK架构

<img src="https://linkkit-export.oss-cn-shanghai.aliyuncs.com/5.x/ext_doc_src/sdk_5.x_arch.png" width="800">

## 源码目录结构说明
SDK是服务于设备，让设备更好的连接云平台及使用云平台的能力，所以SDK的能力设计都是围绕着设备开展的，大部分方法(函数)都是以设备作为操作句柄实现的。  
`/device-core`  
设备的核心模块，必选的功能模块，能力包括：
+ 设备认证:支持设备使用三元组认证及x509认证
+ 设备连接：支持设备使用mqtt协议、http协议连接云平台，http协议只能支持上行消息，推荐使用mqtt协议。
+ 设备消息收发：消息的上下行，同步消息(rrpc)及广播消息
+ 设备就近接入：引导设备就近连接服务器
+ 设备动态注册：从云端获取共享密钥, 批量烧录设备统一固件
  
`/device-modules`  
设备的组件模块，可选的功能模块，组件内模块支持裁剪，编译前删除不使用即可，可以减少资源开销。
+ data-model 物模型：使用属性、事件、服务构成的物模型描述设备，帮助用户快速进行设备的业务开发。
+ devinfo    设备标签：向云平台添加/删除设备的标签，便于对设备进行管理
+ gateway    网关与子设备：网关代理子设备注册、添加拓扑管理、上下线、消息通信
+ logpost    设备日志：将设备的日志上报到云端，以便存储查询分析
+ ntp        时间同步：设备从云平台获取标准时间
+ ota        设备升级：设备通过云平台进行升级
+ shadow     设备影子：将设备的状态上报和云端的指令下发缓存在云端JSON文档中，多为弱网场景使用
+ tunnel     安全隧道：包含远程登录功能，支持控制台ssh登录设备进行维护，安全隧道功能也可用于访问端与设备端进行流式数据传输
+ remote-config 远程配置：

`/external`  
SDK所依赖的外部开源的库，包含cjson、mbedtls、nopoll等  

`/utils`  
SDK实现的内部的工具库, 包含文件下载工具及签名校验工具  

`portfiles`  
SDK跨平台移植的接口实现，目前仅支持linux


# 环境说明

---
*如果用户使用安装有`GNU Make`的`Linux`主机开发环境, 可在SDK源码根目录运行*

# 使用说明
用户下载后，解压后，进入SDK根目录
## 编译
```
mkdir build && cd build
cmake ..
make -j
```

## 以静态库编译
```
mkdir build && cd build
cmake  -DBUILD_SHARED_LIBS=OFF ..
make -j
```

## 设置交叉编译
修改CMakeLists.txt，设置交叉编译工具链，再执行上述编译方式
```
set(CMAKE_C_COMPILER arm-linux-gnueabihf-gcc)
```

## 输出内容:
* demo执行文件路径：`./output/bin/`
* 库文件路径：`./output/lib/`
* 头文件路径：`./output/include/`

# demo简介
|demo|功能|
|----|----|
|device_basic_demo|演示设备基础的建连，发送接收消息，包含自定义消息的使用|
|device_bootstrap_demo|演示设备就近接入功能的使用|
|device_dynamic_register_demo|演示设备就近接入功能的使用|
|ota_basic_demo|演示设备OTA功能的使用|
|dm_basic_demo|演示设备使用物模型功能|
|logpost_basic_demo|演示设备上报本地日志的功能|
|tunnel_basic_demo|演示设备使用远程登录的功能|
|shadow_basic_demo|演示设备使用设备影子的功能|
|gateway_***_demo|演示网关与子设备相关功能的使用|

试用demo功能需先修改demo配置文件demo_config.h，写入用户的设备信息及接入信息。
运行demo，如设备基础功能demo
```
./output/bin/device_basic_demo
```

# 设计原则

+ 用户须知的API函数接口和数据结构, 在`xxx/aiot_xxx_api.h`头文件中列出, 以`aiot_xxx_yyy`风格命名
+ 组件能力的使用范例, 在`xxx/demos/xxx_{basic,posix}_demo.c`中
+ 组件的API函数原型, 遵循统一的设计模式
    - `aiot_device_***`: 设备核心模块接口
    - `aiot_device_xxx_***`: 设备的组件xxx接口
+ API的返回值: 是1个`int32_t`的非正数整型, 也叫**状态码**, `0`表成功, 其它值表达运行状态
    * `retval = aiot_xxx_yyy()`方式获取返回值
    * 所有返回值唯一对应内部运行分支, 含义详见`aiot_state_api.h`或`aiot_xxx_api.h`
    * 所有组件的返回值的值域互不重叠, 共同分别分布在`0x0000 - 0xFFFF`
