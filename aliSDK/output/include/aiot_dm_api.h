/**
 * @file aiot_dm_api.h
 * @brief 设备物模型模块头文件, 提供了物模型数据格式的上云能力, 包括属性, 事件, 服务和物模型二进制格式的数据上下行能力
 * @date 2022-01-20
 *
 * @copyright Copyright (C) 2020-2025 Alibaba Group Holding Limited
 *
 */

#ifndef _AIOT_DM_API_H_
#define _AIOT_DM_API_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>

/**
 * @brief data-model模块接收消息类型枚举
 *
 * @details
 *
 * 这个枚举类型包括了dm模块支持接收的所有数据类型, 不同的消息类型将对于不同的消息结构体
 * 用户可查看网页文档<a href="https://help.aliyun.com/document_detail/89301.html">设备属性/事件/服务</a>进一步了解各种数据类型
 *
 */
typedef enum {
    /**
     * @brief 上报属性/实践后服务器返回的应答消息, 消息数据结构体参考@ref aiot_dm_msg_generic_reply_t
     */
    AIOT_DMRECV_POST_REPLY,

    /**
     * @brief 上报属性/实践后服务器返回的应答消息, 消息数据结构体参考@ref aiot_dm_msg_generic_reply_t
     */
    AIOT_DMRECV_BATCH_POST_REPLY,

    /**
     * @brief 服务器下发的属性设置消息, 消息数据结构体参考@ref aiot_dm_msg_property_set_t
     */
    AIOT_DMRECV_PROPERTY_SET,

    /**
     * @brief 服务器下发的异步服务调用消息, 消息数据结构体参考@ref aiot_dm_msg_service_invoke_t
     */
    AIOT_DMRECV_SERVICE_INVOKE,

    /**
     * @brief 上报属性/实践后服务器返回的应答消息, 消息数据结构体参考@ref aiot_dm_msg_generic_reply_t
     */
    AIOT_DMRECV_GET_DESIRED_REPLY,

    /**
     * @brief 上报属性/实践后服务器返回的应答消息, 消息数据结构体参考@ref aiot_dm_msg_generic_reply_t
     */
    AIOT_DMRECV_DELETE_DESIRED_REPLY,

    /**
     * @brief 服务器对设备上报的二进制数据应答, 消息数据结构体参考@ref aiot_dm_msg_raw_data_t
     */
    AIOT_DMRECV_RAW_REPLY,

    /**
     * @brief 服务器下发的物模型二进制数据, 消息数据结构体参考@ref aiot_dm_msg_raw_data_t
     */
    AIOT_DMRECV_RAW_DOWN,

    /**
     * @brief 消息数量最大值, 不可用作消息类型
     */
    AIOT_DMRECV_MAX,
} aiot_dm_msg_type_t;

/**
 * @brief <b>云端通用应答</b>消息结构体, 设备端上报@ref aiot_device_dm_property_post, @ref aiot_device_dm_event_post 或者@ref aiot_device_dm_get_desired 等消息后, 服务器会应答此消息
 */
typedef struct {
    uint32_t msg_id;
    /**
     * @brief 设备端错误码, 200-请求成功, 更多错误码码查看<a href="https://help.aliyun.com/document_detail/120329.html">设备端错误码</a>
     */
    uint32_t code;
    /**
     * @brief 指向云端应答数据的指针
     */
    char *data;
    /**
     * @brief 云端应答数据的长度
     */
    uint32_t data_len;
    /**
     * @brief 指向状态消息字符串的指针, 当设备端上报请求成功时对应的应答消息为"success", 若请求失败则应答消息中包含错误信息
     */
    char *message;
    /**
     * @brief 消息字符串的长度
     */
    uint32_t message_len;
} aiot_dm_msg_generic_reply_t;

/**
 * @brief <b>属性设置</b>消息结构体
 */
typedef struct {
    /**
     * @brief 服务器下发的属性数据, 为字符串形式的JSON结构体, 此字符串<b>不</b>以结束符'\0'结尾, 如<i>"{\"LightSwitch\":0}"</i>
     */
    char       *params;
    /**
     * @brief 属性数据的字符串长度
     */
    uint32_t    params_len;
} aiot_dm_msg_property_set_t;

/**
 * @brief <b>同步服务调用</b>消息结构体, 用户收到同步服务后, 必须在超时时间(默认7s)内进行应答
 */
typedef struct {
    /**
     * @brief 服务标示符, 字符串内容由用户定义的物模型决定
     */
    char       *service_id;
    /**
     * @brief 服务调用的输入参数数据, 为字符串形式的JSON结构体, 此字符串<b>不</b>以结束符'\0'结尾, 如<i>"{\"LightSwitch\":0}"</i>
     */
    char       *params;
    /**
     * @brief 输入参数的字符串长度
     */
    uint32_t    params_len;
} aiot_dm_msg_service_invoke_t;


/**
 * @brief <b>物模型二进制数据</b>消息结构体, 服务器的JSON格式物模型数据将通过物联网平台的JavaScript脚本转化为二进制数据, 用户在接收此消息前应确保已正确启用云端解析脚本
 */
typedef struct {
    /**
     * @brief 指向接受数据缓冲区的指针
     */
    uint8_t    *data;
    /**
     * @brief 二进制数据的长度
     */
    uint32_t    data_len;
} aiot_dm_msg_raw_data_t;

/**
 * @brief data-model模块接收消息的结构体
 */
typedef struct {
    /* 原始的消息对象，回复的时候需要使用 */
    void *context;
    /* 解析后的数据类型 */
    aiot_dm_msg_type_t type;
    /**
     * @brief 解析后消息数据联合体, 不同的消息类型将使用不同的消息结构体
     */
    union {
        aiot_dm_msg_generic_reply_t        generic_reply;
        aiot_dm_msg_property_set_t         property_set;
        aiot_dm_msg_service_invoke_t       service_invoke;
        aiot_dm_msg_raw_data_t             raw_data;
    } data;
} aiot_dm_msg_t;

/**
 * @brief 设备物模型消息回调函数原型，用户定义回调函数后，可通过 @ref aiot_device_dm_set_msg_callback 配置
 *
 * @param[in] device 设备句柄
 * @param[in] msg    接收到的物模型消息,消息的数据结构 @ref aiot_dm_msg_t
 * @param[in] userdata 用户设置的上下文，可通过 @ref aiot_device_dm_set_msg_callback 配置
 */
typedef void (*dm_msg_callback_t)(void *device, const aiot_dm_msg_t *msg, void *userdata);

/**
 * @brief 设置设备接收到物模型消息后的回调函数
 *
 * @param[in] device 设备句柄
 * @param[in] msg_callback 回调函数
 * @param[in] userdata 执行回调函数后的上下文指针
 *
 * @return int32_t
 * @retval STATE_SUCCESS 参数配置成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval 其它 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_device_dm_set_msg_callback(void *device, dm_msg_callback_t msg_callback, void *userdata);

/**
 * @brief 物模型属性消息上报
 *
 * @param[in] device 设备句柄
 * @param[in] params 字符串形式的JSON结构体, <b>必须以结束符'\0'结尾</b>. 包含用户要上报的属性数据, 如<i>"{\"LightSwitch\":0}"</i>
 * @param[in] post_reply 消息上报后，是否需要回复, [1]:需要回复   [0]:不需要回复
 *
 * @return int32_t
 * @retval >=STATE_SUCCESS 消息发送成功, 返回消息id
 * @retval STATE_USER_INPUT_NULL_POINTER 入参<i>device</i>或<i>params</i>为NULL
 * @retval 其它 参考@ref aiot_state_api.h 中对应的错误码说明
 *
 */
int32_t aiot_device_dm_property_post(void *device, char *params, int8_t post_reply);

/**
 * @brief 物模型事件消息上报
 *
 * @param[in] device 设备句柄
 * @param[in] event_id 事件标示符, <b>必须为以结束符'\0'结尾的字符串</b>
 * @param[in] params 字符串形式的JSON结构体, <b>必须以结束符'\0'结尾</b>. 包含用户要上报的事件数据, 如<i>"{\"ErrorNum\":0}"</i>
 * @param[in] post_reply 消息上报后，是否需要回复, [1]:需要回复   [0]:不需要回复
 *
 * @return int32_t
 * @retval >=STATE_SUCCESS 消息发送成功, 返回消息id
 * @retval STATE_USER_INPUT_NULL_POINTER 入参<i>device</i>或<i>event_id</i>或<i>params</i>为NULL
 * @retval 其它 参考@ref aiot_state_api.h 中对应的错误码说明
 *
 */
int32_t aiot_device_dm_event_post(void *device, char *event_id, char *params, int8_t post_reply);

/**
 * @brief 批量上报物模型属性/事件
 *
 * @param[in] device 设备句柄
 * @param[in] params 字符串形式的JSON结构体, <b>必须以结束符'\0'结尾</b>. 包含用户要批量上报的属性和事件数据, 如 {"properties":{"Power": [ { "value": "on", "time": 1524448722000 },
 *  { "value": "off", "time": 1524448722001 } ], "WF": [ { "value": 3, "time": 1524448722000 }]}, "events": {"alarmEvent": [{ "value": { "Power": "on", "WF": "2"},
 *  "time": 1524448722000}]}}
 * @param[in] post_reply 消息上报后，是否需要回复, [1]:需要回复   [0]:不需要回复
 *
 * @return int32_t
 * @retval >=STATE_SUCCESS 消息发送成功, 返回消息id
 * @retval STATE_USER_INPUT_NULL_POINTER 入参<i>device</i>或<i>params</i>为NULL
 * @retval 其它 参考@ref aiot_state_api.h 中对应的错误码说明
 *
 */
int32_t aiot_device_dm_batch_post(void *device, char *params, int8_t post_reply);

/**
 * @brief 获取物模型期望属性
 *
 * @param[in] device 设备句柄
 * @param[in] params 字符串形式的JSON<b>数组</b>, <b>必须以结束符'\0'结尾</b>. 应包含用户要获取的期望属性的ID, 如<i>"[\"LightSwitch\"]"</i>
 *
 * @return int32_t
 * @retval >=STATE_SUCCESS 消息发送成功, 返回消息id
 * @retval STATE_USER_INPUT_NULL_POINTER 入参<i>device</i>或<i>params</i>为NULL
 * @retval 其它 参考@ref aiot_state_api.h 中对应的错误码说明
 *
 */
int32_t aiot_device_dm_get_desired(void *device, char *params);

/**
 * @brief 删除物模型期望属性
 *
 * @param[in] device 设备句柄
 * @param[in] params 字符串形式的JSON结构体, <b>必须以结束符'\0'结尾</b>. 应包含用户要删除的期望属性的ID和期望值版本号, 如<i>"{\"LightSwitch\":{\"version\":1},\"Color\":{}}"</i>
 *
 * @return int32_t
 * @retval >=STATE_SUCCESS 消息发送成功, 返回消息id
 * @retval STATE_USER_INPUT_NULL_POINTER 入参<i>device</i>或<i>params</i>为NULL
 * @retval 其它 参考@ref aiot_state_api.h 中对应的错误码说明
 *
 */
int32_t aiot_device_dm_delete_desired(void *device, char *params);

/**
 * @brief 物模型事件消息上报
 *
 * @param[in] device 设备句柄
 * @param[in] data  指向待发送二进制数据的指针
 * @param[in] data_len  待发送数据的长度
 *
 * @return int32_t
 * @retval >=STATE_SUCCESS 消息发送成功, 返回消息id
 * @retval STATE_USER_INPUT_NULL_POINTER 入参<i>device</i>或<i>params</i>为NULL
 * @retval 其它 参考@ref aiot_state_api.h 中对应的错误码说明
 *
 */
int32_t aiot_device_dm_raw_post(void *device, uint8_t *data, uint32_t data_len);

/**
 * @brief 属性设置应答
 *
 * @param[in] device 设备句柄
 * @param[in] context 消息的上下文，消息回调中有
 * @param[in] code  设备端状态码, 200-请求成功, 更多状态码查看<a href="https://help.aliyun.com/document_detail/89309.html">设备端通用code</a>
 * @param[in] data  设备端应答数据, 字符串形式的JSON结构体, <b>必须以结束符'\0'结尾</b>, 如<i>"{}"</i>表示应答数据为空
 *
 * @return int32_t
 * @retval ==STATE_SUCCESS 消息发送成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参<i>device</i>或<i>context</i>或<i>data</i>为NULL
 * @retval 其它 参考@ref aiot_state_api.h 中对应的错误码说明
 *
 */
int32_t aiot_device_dm_propertyset_reply(void *device, void *context, uint32_t code, char *data);

/**
 * @brief 服务调用应答
 *
 * @param[in] device 设备句柄
 * @param[in] context 消息的上下文，消息回调中有
 * @param[in] service_id 服务标示符, 标识了要响应服务
 * @param[in] code 设备端状态码, 200-请求成功, 更多状态码查看<a href="https://help.aliyun.com/document_detail/89309.html">设备端通用code</a>
 * @param[in] data  设备端应答数据, 字符串形式的JSON结构体, <b>必须以结束符'\0'结尾</b>, 如<i>"{}"</i>表示应答数据为空
 *
 * @return int32_t
 * @retval ==STATE_SUCCESS 消息发送成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参<i>device</i>或<i>context</i>或<i>data</i>为NULL
 * @retval 其它 参考@ref aiot_state_api.h 中对应的错误码说明
 *
 */
int32_t aiot_device_dm_service_reply(void *device, void *context, char *service_id, uint32_t code, char *data);

/**
 * @brief 二进制格式的同步服务应答
 *
 * @param[in] device 设备句柄
 * @param[in] context 消息的上下文，消息回调中有
 * @param[in] data 指向待发送二进制数据的指针
 * @param[in] data_len 待发送数据的长度
 *
 * @return int32_t
 * @retval STATE_SUCCESS 消息发送成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参<i>device</i>或<i>context</i>或<i>data</i>为NULL
 * @retval 其它 参考@ref aiot_state_api.h 中对应的错误码说明
 *
 */
int32_t aiot_device_dm_raw_reply(void *device, void *context, uint8_t *data, uint32_t data_len);

/**
 * @brief 深拷贝消息回复所需要上下文
 *
 * @param[in] context 设备句柄
 *
 * @return void *
 * @retval NULL 拷贝失败
 * @retval 其它 返回拷贝后的上下文指针
 */
void *aiot_dm_msg_context_clone(void *context);

/**
 * @brief 释放拷贝的上下文
 *
 * @param[in] context 设备句柄
 *
 * @return int32_t
 * @retval STATE_SUCCESS 成功释放
 */
int32_t aiot_dm_msg_context_free(void *context);

#if defined(__cplusplus)
}
#endif

#endif