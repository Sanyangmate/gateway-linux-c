/**
 * @file aiot_compress_api.h
 * @brief 设备时间同步模块头文件, 提供获取utc时间的能力
 * @date 2022-01-20
 *
 * @copyright Copyright (C) 2020-2025 Alibaba Group Holding Limited
 *
 */

#ifndef _AIOT_COMPRESS_API_H
#define _AIOT_COMPRESS_API_H

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>

/* TODO: 这一段列出compress模块使用的状态码, 每个状态码需要有doxygen的brief说明, 每个码之间有1个空行隔开 */
/**
 * @brief -0x2200~-0x22FF表达SDK在compress模块内的状态码
 */
#define STATE_COMPRESS_BASE                                             (-0x2200)

/**
 * @brief 设置的压缩等级超出范围，范围：1～9
 */
#define STATE_COMPRESS_LEVEL_INVALID                                    (-0x2201)

/**
 * @brief 压缩内部错误，压缩失败
 */
#define STATE_COMPRESS_COMPR_FAILED                                     (-0x2202)

/**
 * @brief 解压缩内部错误，解压缩失败
 */
#define STATE_COMPRESS_DECOMPR_FAILED                                   (-0x2203)

/**
 * @brief 重复添加需要压缩/解压缩的topic
 */
#define STATE_COMPRESS_APPEND_REPEATED                                  (-0x2204)

/**
 * @brief compress模块收到从网络上来的报文时, 通知用户的报文类型
 */
typedef enum {
    AIOT_COMPRESS_UPDATE_REPLY
} aiot_compress_recv_type_t;

/**
 * @brief compress模块收到从网络上来的报文时, 通知用户的报文内容
 */
typedef struct {
    /**
     * @brief 报文内容所对应的报文类型, 更多信息请参考@ref aiot_compress_recv_type_t
     */
    aiot_compress_recv_type_t  type;
    union {
        /**
         * @brief 上报需要压缩解压缩的topic列表的结果返回
         */
        struct {
            uint32_t id;
            uint32_t code;
            char *message;
        } update_reply;
    } data;
} aiot_compress_recv_t;

/**
 * @brief compress模块收到从网络上来的报文时, 通知用户所调用的数据回调函数
 *
 * @param[in] handle compress会话句柄
 * @param[in] packet compress消息结构体, 存放收到的compress报文内容
 * @param[in] userdata 用户上下文
 *
 * @return void
 */
typedef void (* compress_callback_t)(void *handle,
                                     const aiot_compress_recv_t *packet, void *userdata);


/**
 * @brief 设置压缩等级
 *
 * @param[in] device 设备句柄
 * @param[in] level 压缩等级1～9
 * @return int32_t
 * @retval STATE_SUCCESS 参数配置成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_device_compress_set_level(void *device, uint8_t level);

/**
 * @brief 设置压缩需要压缩的topic列表
 *
 * @param[in] device 设备句柄
 * @param[in] topic_list 需要压缩的topic表
 * @param[in] num  topic_list表成员个数
 * @return int32_t
 * @retval STATE_SUCCESS 参数配置成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_device_compress_set_compr_list(void *device, char **topic_list, int32_t topic_num);

/**
 * @brief 设置压缩需要解压缩的topic列表
 *
 * @param[in] device 设备句柄
 * @param[in] topic_list 需要解压缩的topic表
 * @param[in] num  topic_list表成员个数
 * @return int32_t
 * @retval STATE_SUCCESS 参数配置成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_device_compress_set_decompr_list(void *device, char **topic_list, int32_t topic_num);

/**
 * @brief 设置时间同步回调
 *
 * @param[in] device 设备句柄
 * @param[in] callback 消息回调函数
 * @param[in] userdata 执行回调消息的上下文
 * @return int32_t
 * @retval STATE_SUCCESS 参数配置成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_device_compress_set_callback(void *device, compress_callback_t callback, void *userdata);

/**
 * @brief 请求时间同步
 *
 * @param[in] device 设备句柄
 * @return int32_t
 * @retval >=STATE_SUCCESS  消息id
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_device_compress_update_topic(void *device);

#if defined(__cplusplus)
}
#endif

#endif