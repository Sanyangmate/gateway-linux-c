/**
 * @file aiot_gateway_api.h
 * @brief 设备的网关模块头文件，网关设备使用该模块接口。提供：代理子设备连接，代理子设备消息收发、topo关系管理，子设备管理、批量消息发送等功能
 * @date 2022-01-20
 *
 * @copyright Copyright (C) 2020-2025 Alibaba Group Holding Limited
 *
 */

#ifndef _GATEWAY_MODULE_H_
#define _GATEWAY_MODULE_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>

/**
 * @brief 错误码：网关批量代理子设备建连，子设备重复建连报
 *
 */
#define  STATE_GATEWAY_SUBDEV_REPEATED_CONNECT                    (-0x1301)

/**
 * @brief 错误码：网关批量代理子设备建连/断连，添加/删除topo等同步操作时，接收云端返回消息超时
 *
 */
#define  STATE_GATEWAY_SYNC_RECV_TIMEOUT                          (-0x1302)

/**
 * @brief 错误码：网关批量代理子设备建连/断连，添加/删除topo等同步操作时，接收云端返回错误消息
 *
 */
#define  STATE_GATEWAY_SYNC_RECV_ERROR                            (-0x1303)

/**
 * @brief 产品key的字符串最长长度
 */
#define  AIOT_PRODUCT_KEY_MAX_LENTH     64
/**
 * @brief 设备名称的字符串最长长度
 */
#define  AIOT_DEVICE_NAME_MAX_LENTH     64
/**
 * @brief 产品密钥的最长字符串长度
 */
#define  AIOT_PRODUCT_SECRET_MAX_LENTH  64
/**
 * @brief 设备密钥的字符串最长长度
 */
#define  AIOT_DEVICE_SECRET_MAX_LENTH   64

/**
 * @brief 子设备的元信息
 */
typedef struct {
    /**
     * @brief 子设备的产品key
     */
    char product_key[AIOT_PRODUCT_KEY_MAX_LENTH];
    /**
     * @brief 子设备名
     */
    char device_name[AIOT_DEVICE_NAME_MAX_LENTH];
    /**
     * @brief 子设备的产品密钥，在子设备动态注册时作为输入，子设备添加topo时可为空
     */
    char product_secret[AIOT_PRODUCT_SECRET_MAX_LENTH];
    /**
     * @brief 子设备的设备密钥，子设备添加topo时作为输入，子设备动态注册时作为输出。
     */
    char device_secret[AIOT_DEVICE_SECRET_MAX_LENTH];
} aiot_subdev_meta_info_t;

/**
 * @brief 网关设备接收的到消息类型
 */
typedef enum {
    /**
     * @brief 批量建连消息的回复报文
     */
    AIOT_GWRECV_CONNECT_REPLY,
    /**
     * @brief 批量断连消息的回复报文
     */
    AIOT_GWRECV_DISCONNECT_REPLY,
    /**
     * @brief 批量添加topo消息的回复报文
     */
    AIOT_GWRECV_ADD_TOPO_REPLY,
    /**
     * @brief 批量删除topo消息的回复报文
     */
    AIOT_GWRECV_DELETE_TOPO_REPLY,
    /**
     * @brief topo变更云端的通知消息
     */
    AIOT_GWRECV_TOPO_CHANGE_NOTICE,
} aiot_gateway_msg_type_t;

/**
 * @brief 接收消息中表示设备信息的数据结构
 */
typedef struct {
    char *product_key;
    char *device_name;
    char *device_secret;
} subdev_recv_t;

/**
 * @brief 回复消息的关键字段
 */
typedef struct {
    /**
     * @brief 消息ID，可以与发送的消息ID进行匹配
     */
    uint32_t id;
    /**
     * @brief 批量操作的状态，200表示成功
     */
    int32_t code;
    /**
     * @brief 如果操作失败，根据message可以查看失败原因
     */
    char *message;
} aiot_gateway_msg_reply_t;

/**
 * @brief topo变更的状态
 */
typedef struct {
    /**
     * @brief topo状态：0-创建  1-删除 2-恢复禁用  8-禁用
     */
    int32_t status;
} aiot_gateway_msg_topo_change_t;

/**
 * @brief 网关接收到的消息的完整数据结构
 */
typedef struct {
    /**
     * @brief 消息类型，具体类型参考@ref aiot_gateway_msg_type_t
     */
    aiot_gateway_msg_type_t type;
    /**
     * @brief 子设备列表 参考@ref subdev_recv_t
     */
    subdev_recv_t *subdev_table;
    /**
     * @brief 子设备个数
     */
    int32_t subdev_num;
    union {
        /**
         * @brief 回复消息的其它字段
         */
        aiot_gateway_msg_reply_t reply;
        /**
         * @brief topo变更消息的其它字段
         */
        aiot_gateway_msg_topo_change_t topo_change;
    } data;
} aiot_gateway_msg_t;

/**
 * @brief 设备物模型消息回调函数原型，用户定义回调函数后，可通过 @ref aiot_gateway_set_msg_callback 配置
 *
 * @param[in] device 设备句柄
 * @param[in] msg    接收到的物模型消息， 消息的数据结构 @ref aiot_gateway_msg_t
 * @param[in] userdata 用户设置的上下文，可通过 @ref aiot_gateway_set_msg_callback 配置
 */
typedef void (*gateway_msg_callback_t)(void *gateway_device, const aiot_gateway_msg_t *msg, void *userdata);

/**
 * @brief 设置网关消息的回调
 *
 * @param[in] gateway_device 网关设备设备句柄
 * @param[in] msg_callback 回调函数
 * @param[in] userdata 执行回调函数返回的上下文指针
 *
 * @return int32_t
 * @retval STATE_SUCCESS 参数配置成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_gateway_set_msg_callback(void *gateway_device, gateway_msg_callback_t msg_callback, void *userdata);

/**
 * @brief 设置子设备是否自动重连
 * @details 网关离线后，子设备也会设置为离线状态，等网关自动重连后，子设备是否由网关托管自动重连
 *
 * @param[in] gateway_device 设备句柄
 * @param[in] auto_reconnect 1: 网关代理子设备重连【默认值】  <br/>
 *                           0: 网关不代理子设备重连，网关离线后，子设备将从网关模块中删除，需要重新调用子设备建连接口 @ref aiot_gateway_batch_connect_subdev
 * @return int32_t
 * @retval STATE_SUCCESS 参数配置成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_gateway_set_auto_reconnect(void *gateway_device, int32_t auto_reconnect);

/**
 * @brief 设置批量同步接口调用的超时时间
 * @details 批量操作接口列表： <br/>
 *              @ref aiot_gateway_batch_connect_subdev <br/>
 *              @ref aiot_gateway_batch_disconnect_subdev <br/>
 *              @ref aiot_gateway_batch_add_topo <br/>
 *              @ref aiot_gateway_batch_delete_topo <br/>
 *              @ref aiot_gateway_dynamic_secret <br/>
 *              @ref aiot_gateway_dynamic_register <br/>
 *
 * @param[in] gateway_device 设备句柄
 * @param[in] timeout_ms 从调用到失败的超时时间，单位ms, 默认值：10000ms
 *
 * @return int32_t
 * @retval STATE_SUCCESS 参数配置成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_gateway_set_sync_msg_timeout(void *gateway_device, uint32_t timeout_ms);

/**
 * @brief 网关批量代理子设备建连(同步接口会阻塞), 超时时间设置 @ref aiot_gateway_set_sync_msg_timeout
 *
 * @param[in] gateway_device 网关设备句柄
 * @param[in] subdev_table   子设备句柄表
 * @param[in] subdev_num     子设备个数
 *
 * @return int32_t
 * @retval STATE_SUCCESS 代理子设备建连成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_gateway_batch_connect_subdev(void *gateway_device, void* subdev_table[], int32_t subdev_num);

/**
 * @brief 网关批量增加子设备topo(同步接口会阻塞), 超时时间设置 @ref aiot_gateway_set_sync_msg_timeout
 *
 * @param[in] gateway_device 网关设备句柄
 * @param[in] subdev_table   子设备元数据表
 * @param[in] subdev_num     子设备个数
 *
 * @return int32_t
 * @retval STATE_SUCCESS 添加topo成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_gateway_batch_add_topo(void *gateway_device, aiot_subdev_meta_info_t subdev_table[], int32_t subdev_num);

/**
 * @brief 网关批量删除子设备topo(同步接口会阻塞), 超时时间设置 @ref aiot_gateway_set_sync_msg_timeout
 *
 * @param[in] gateway_device 网关设备句柄
 * @param[in] subdev_table   子设备元数据表
 * @param[in] subdev_num     子设备个数
 *
 * @return int32_t
 * @retval STATE_SUCCESS 删除子设备topo关系成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_gateway_batch_delete_topo(void *gateway_device, aiot_subdev_meta_info_t subdev_table[], int32_t subdev_num);

/**
 * @brief 网关批量代理子设备动态注册(同步接口会阻塞), 超时时间设置 @ref aiot_gateway_set_sync_msg_timeout
 * @details 通过该接口获取子设备的密钥，需要先添加子设备与网关的topo关系
 *
 * @param[in] gateway_device 网关设备句柄
 * @param[in] subdev_table   子设备元数据表(product_key, device_name不能为空) <br/>
 *                           成功后密钥会保存在subdev_table[].device_secret中
 * @param[in] subdev_num     子设备个数
 *
 * @return int32_t
 * @retval STATE_SUCCESS 获取密钥成功，密钥存储在subdev_table.device_secret
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_gateway_dynamic_secret(void *gateway_device, aiot_subdev_meta_info_t subdev_table[], int32_t subdev_num);

/**
 * @brief 网关批量代理子设备抢注(同步接口会阻塞), 超时时间设置 @ref aiot_gateway_set_sync_msg_timeout
 * @details 当子设备使用多个网关时，更换网关后可通过该接口强制绑定该网关
 *
 * @param[in] gateway_device 网关设备句柄
 * @param[in] subdev_table   子设备元数据表中(product_key, device_name, product_secret不能为空) <br/>
 *                           成功后密钥会保存在subdev_table.device_secret中
 * @param[in] subdev_num     子设备个数
 *
 * @return int32_t
 * @retval STATE_SUCCESS 获取密钥成功，密钥存储在subdev_table.device_secret
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_gateway_dynamic_register(void *gateway_device, aiot_subdev_meta_info_t subdev_table[], int32_t subdev_num);

/**
 * @brief 网关批量代理子设备建连(异步接口不阻塞)
 *
 * @param[in] gateway_device 网关设备句柄
 * @param[in] subdev_table   子设备句柄表
 * @param[in] subdev_num     子设备个数
 *
 * @return int32_t
 * @retval >= STATE_SUCCESS 大于0时，表示消息发送成功，返回值为消息ID，回复消息类型 @ref AIOT_GWRECV_CONNECT_REPLY
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_gateway_batch_connect_subdev_async(void *gateway_device, void* subdev_table[], int32_t subdev_num);

/**
 * @brief 网关批量代理子设备断连(异步接口不阻塞)
 *
 * @param[in] gateway_device 网关设备句柄
 * @param[in] subdev_table   子设备句柄表
 * @param[in] subdev_num     子设备个数
 *
 * @return int32_t
 * @retval >= STATE_SUCCESS 大于0时，表示消息发送成功，返回值为消息ID，回复消息类型 @ref AIOT_GWRECV_DISCONNECT_REPLY
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_gateway_batch_disconnect_subdev(void *gateway_device, void* subdev_table[], int32_t subdev_num);

/**
 * @brief 网关批量增加子设备topo(异步接口不阻塞)
 *
 * @param[in] gateway_device 网关设备句柄
 * @param[in] subdev_table   子设备句柄表
 * @param[in] subdev_num     子设备个数
 *
 * @return int32_t
 * @retval >= STATE_SUCCESS 大于0时，表示消息发送成功，返回值为消息ID，回复消息类型 @ref AIOT_GWRECV_ADD_TOPO_REPLY
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_gateway_batch_add_topo_async(void *gateway_device, aiot_subdev_meta_info_t subdev_table[], int32_t subdev_num);

/**
 * @brief 网关批量删除子设备topo(异步接口不阻塞)
 *
 * @param[in] gateway_device 网关设备句柄
 * @param[in] subdev_table   子设备句柄表
 * @param[in] subdev_num     子设备个数
 *
 * @return int32_t
 * @retval >= STATE_SUCCESS 大于0时，表示消息发送成功，返回值为消息ID，回复消息类型 @ref AIOT_GWRECV_DELETE_TOPO_REPLY
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_gateway_batch_delete_topo_async(void *gateway_device, aiot_subdev_meta_info_t subdev_table[], int32_t subdev_num);
#if defined(__cplusplus)
}
#endif

#endif