/**
 * @file aiot_message_api.h
 * @brief 设备消息模块，包含：构建自定义消息、构建rrpc消息、消息拷贝、消息删除等功能
 * @date 2021-12-16
 *
 * @copyright Copyright (C) 2015-2025 Alibaba Group Holding Limited
 *
 * @details
 */

#ifndef _AIOT_MESSAGE_API_H_
#define _AIOT_MESSAGE_API_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>


/**
 * @brief 描述消息的数据结构
 */
typedef struct {
    /**
     * @brief 消息的话题，类似于命令字
     */
    char *topic;
    /**
     * @brief 消息的载体
     */
    uint8_t *payload;
    /**
     * @brief 消息的载体长度
     */
    uint32_t payload_lenth;
    /**
     * @brief 消息的质量等级，0：至多一次到达 1:至少一次到达
     */
    char qos;

    /* 接收消息：rrpc报文:云端对设备的同步调用，需要保存上下文信息以便消息返回 */
    /**
     * @brief rrpc的消息id
     */
    char *rrpc_id;
    /**
     * @brief 通道id
     */
    int32_t cid;
    /**
     * @brief Alink协议的消息id, 高级组件使用
     */
    uint32_t id;
} aiot_msg_t;

/**
 * @brief 生成透传的消息，topic和payload都使用参数内的
 *
 * @return aiot_msg_t*
 * @retval 非NULL 消息句柄
 * @retval NULL 初始化失败, 一般是内存分配失败导致
 *
 */
aiot_msg_t* aiot_msg_create_raw(const char *topic, const uint8_t *payload, uint32_t payload_lenth);

/**
 * @brief 生成自定义topic的消息
 * @param[in] productKey 设备认证信息中的产品类型
 * @param[in] deviceName 设备遏认证信息的设备名
 * @param[in] sub_topic  自定义topic中自定义的部分
 * @param[in] payload    消息载体
 * @param[in] payload_lenth 消息载体的长度
 * @return aiot_msg_t*
 * @retval 非NULL 消息句柄
 * @retval NULL 初始化失败, 一般是内存分配失败导致
 *
 */
aiot_msg_t* aiot_msg_create_user_define(const char *productKey, const char *deviceName, const char *sub_topic, const uint8_t *payload, uint32_t payload_lenth);

/**
 * @brief 生成rrpc回复的消息
 * @param[in] message         接收到的rrpc消息
 * @param[in] payload         消息载体
 * @param[in] payload_lenth   消息载体的长度
 * @return aiot_msg_t*
 * @retval 非NULL 消息句柄
 * @retval NULL 初始化失败, 一般是内存分配失败导致
 *
 */
aiot_msg_t* aiot_msg_create_rrpc_reply(const aiot_msg_t *message, const uint8_t *payload, uint32_t payload_lenth);

/**
 * @brief 深拷贝消息
 * @details
 *  使用示例：接收到消息时，如果消息处理时间过长，不建议在回调中处理。
 *  可以先对消息进行拷贝，在其它线程处理，处理后需调用 @ref aiot_msg_delete 删除消息
 * @param[in] message         源消息
 * @return aiot_msg_t*
 * @retval 非NULL 消息句柄
 * @retval NULL 初始化失败, 一般是内存分配失败导致
 *
 */
aiot_msg_t* aiot_msg_clone(const aiot_msg_t* message);

/**
 * @brief 删除消息，销毁消息资源
 * @param[in] message  待删除的消息
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_msg_delete(aiot_msg_t* message);

/**
 * @brief 设置消息发送的qos
 * @param[in] message 需要配置的消息
 * @param[in] qos [0, 不确认ack], [1, 需确认ack，无返回将重发]
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_msg_set_qos(aiot_msg_t* message, uint8_t qos);


#if defined(__cplusplus)
}
#endif

#endif