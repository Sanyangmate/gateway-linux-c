/**
 * @file aiot_tunnel_api.h
 * @brief 设备的隧道功能模块头文件，提供与物联网平台维持隧道连接的能力
 * @date 2022-01-20
 *
 * @copyright Copyright (C) 2020-2025 Alibaba Group Holding Limited
 *
 */
#ifndef AIOT_TUNNEL_API_H_
#define AIOT_TUNNEL_API_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>

/**
 * @brief 错误码：通用错误码
 */
#define  STATE_TUNNEL_BASE                                              (-0x1C80)
/**
 * @brief 错误码：隧道内部执行错误
 */
#define  STATE_TUNNEL_INNER_ERROR                                       (-0x1C81)
/**
 * @brief 错误码：隧道连接超时，发送超时等
 */
#define  STATE_TUNNEL_CONNECT_FAILED                                    (-0x1C82)
/**
 * @brief 错误码：隧道重复建连
 */
#define  STATE_TUNNEL_CONNECT_REPEATED                                  (-0x1C83)
/**
 * @brief 错误码：隧道重复断连
 */
#define  STATE_TUNNEL_DISCONNECT_REPEATED                               (-0x1C84)
/**
 * @brief 错误码：隧道被关闭或者已经过期
 */
#define  STATE_TUNNEL_TOKEN_EXPIRED                                     (-0x1C85)

/**
 * @brief tunnel内部事件类型
 */
typedef enum {
    /**
     * @brief 当tunnel实例连接代理通道成功, 触发此事件
     */
    AIOT_TUNNEL_EVT_CONNECT,
    /**
     * @brief 当tunnel实例从代理通道断开, 触发此事件
     */
    AIOT_TUNNEL_EVT_DISCONNECT,
    /**
     * @brief 隧道认证信息已经过期，需要重新连接
     */
    AIOT_TUNNEL_EVT_EXPIRED,
} aiot_tunnel_event_type;

/**
 * @brief 本地代理服务信息
 */
typedef struct {
    /**
     * @brief 服务IP地址/host
     */
    char                        *ip;
    /**
     * @brief 服务端口号
     */
    unsigned int                port;
} aiot_tunnel_proxy_params_t;

/**
 * @brief 隧道建连需要的参数
 */
typedef struct {
    char *host;
    uint16_t port;
    char *path;
    char *token;
} aiot_tunnel_connect_param_t;

/**
 * @brief 隧道内部事件回调函数原型, 可通过 @ref aiot_tunnel_set_event_callback 配置
 *
 * @param[in] handle 隧道的句柄
 * @param[in] event    接收到的事件 @ref aiot_tunnel_event_type
 * @param[in] userdata 用户设置的上下文，可通过 @ref aiot_tunnel_set_event_callback 配置
 */
typedef void (*aiot_tunnel_event_callback_t)(void *handle, const aiot_tunnel_event_type event, void *userdata);

/**
 * @brief 初始化隧道并设置默认参数
 *
 * @return void*
 * @retval 非NULL 隧道句柄
 * @retval NULL 初始化失败, 一般是内存分配失败导致
 *
 */
void *aiot_tunnel_init(char *tunnel_id);

/**
 * @brief 设置隧道的建连参数
 * @param[in] tunnel 隧道的句柄
 * @param[in] params 建连的参数，更多说明参考@ref aiot_tunnel_connect_param_t
 *
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_tunnel_set_connect_params(void *tunnel,const aiot_tunnel_connect_param_t *params);

/**
 * @brief 添加本地代理服务的session类型，如ssh、ftp等tcp服务
 * @param[in] tunnel 隧道的句柄
 * @param[in] proxy_type 本地代理服务类型
 * @param[in] params 本地代理服务的参数
 *
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_tunnel_add_proxy_service(void *tunnel, char *proxy_type, const aiot_tunnel_proxy_params_t *params);

/**
 * @brief 设置隧道的的事件回调，一般为隧道状态变更时调用
 * @param[in] tunnel 隧道的句柄
 * @param[in] callback 回调函数
 * @param[in] userdata 用户的上下文指针
 *
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_tunnel_set_event_callback(void *tunnel, aiot_tunnel_event_callback_t callback, void *userdata);

/**
 * @brief 隧道建连
 * @param[in] tunnel 隧道的句柄
 *
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_tunnel_connect(void *tunnel);

/**
 * @brief 隧道断连
 * @param[in] tunnel 隧道的句柄
 *
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_tunnel_disconnect(void *tunnel);

/**
 * @brief 删除隧道，回收内存资源
 * @param[in] tunnel 隧道的句柄的指针
 *
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
void aiot_tunnel_deinit(void **tunnel);

typedef struct {
    /**
     * @brief 新建session请求处理
     */
    int32_t (*session_new)(void *session, void *userdata);
    /**
     * @brief 断开session请求处理
     */
    int32_t (*session_release)(void *session, void *userdata);
    /**
     * @brief session接收到数据处理
     */
    int32_t (*session_recv_data)(void *session, uint8_t *data, uint32_t len, void *userdata);
} aiot_session_ops_t;

/**
 * @brief 添加本地自定义处理的服务
 * @param[in] tunnel 隧道的句柄
 * @param[in] proxy_type 本地服务类型
 * @param[in] ops 自定义回调函数集
 * @param[in] userdata 回调函数执行的上下文指针
 *
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_tunnel_add_userdefine_service(void *tunnel, char *proxy_type, aiot_session_ops_t *ops, void *userdata);

/**
 * @brief 用户主动断开session
 * @param[in] session 会话的句柄
 *
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t aiot_session_release(void *session);

/**
 * @brief 用户向session发送数据
 * @param[in] session 会话的句柄
 * @param[in] data    发送的数据内容
 * @param[in] len     发送的数据长度
 *
 * @return int32_t
 * @retval <STATE_SUCCESS 执行失败, 更多信息请参考@ref aiot_state_api.h
 * @retval >=STATE_SUCCESS 执行成功
 *
 */
int32_t  aiot_session_send_data(void *session, uint8_t *data, uint32_t len);

/**
 * @brief 获取session的类型
 * @param[in] session 会话的句柄
 */
char* aiot_session_type(void *session);

/**
 * @brief 获取sesssion_id
 * @param[in] session 会话的句柄
 */
char* aiot_session_id(void *session);


#endif /* __AIOT_TUNNEL_API_H_ */
