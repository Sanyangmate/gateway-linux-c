/**
 * @file aiot_devinfo_api.h
 * @brief 设备标签模块头文件, 提供更新和删除设备标签的能力
 * @date 2022-01-20
 *
 * @copyright Copyright (C) 2020-2025 Alibaba Group Holding Limited
 *
 */

#ifndef _AIOT_DEVINFO_API_H
#define _AIOT_DEVINFO_API_H

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>

/**
 * @brief 设备标签添加/删除后，物联网平台回复消息的结构体
 */
typedef struct {
    /**
     * @brief 消息标识符, uint32_t类型的整数, 与属性上报或事件上报的消息标示符一致
     */
    uint32_t msg_id;
    /**
     * @brief 设备端错误码, 200-请求成功, 更多错误码码查看<a href="https://help.aliyun.com/document_detail/120329.html">设备端错误码</a>
     */
    uint32_t code;
    /**
     * @brief 指向云端应答数据的指针
     */
    char *data;
    /**
     * @brief 云端应答数据的长度
     */
    uint32_t data_len;
    /**
     * @brief 指向状态消息字符串的指针, 当设备端上报请求成功时对应的应答消息为"success", 若请求失败则应答消息中包含错误信息
     */
    char *message;
    /**
     * @brief 消息字符串的长度
     */
    uint32_t message_len;
} aiot_devinfo_msg_reply_t;

/**
 * @brief 设备标签回复消息的回调函数原型，可通过 @ref aiot_device_devinfo_set_callback 配置
 *
 * @param[in] device 设备句柄
 * @param[in] msg    接收到的设备标签回复消息， 数据结构参考 @ref aiot_devinfo_msg_reply_t
 * @param[in] userdata 用户设置的上下文，可通过 @ref aiot_device_devinfo_set_callback 配置
 */
typedef void (*devinfo_callback_t)(void *device,
                                   const aiot_devinfo_msg_reply_t *recv, void *userdata);

/**
 * @brief 设置远程配置
 *
 * @param[in] device 设备句柄
 * @param[in] callback 消息回调函数
 * @param[in] userdata 执行回调消息的上下文
 * @return int32_t
 * @retval STATE_SUCCESS 参数配置成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_device_devinfo_set_callback(void *device, devinfo_callback_t callback, void *userdata);

/**
 * @brief 给设备添加标签
 *
 * @param[in] device 设备句柄
 * @param[in] key    标签的key
 * @param[in] value  标签的value
 * @return int32_t
 * @retval >=STATE_SUCCESS  消息id
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_device_devinfo_add(void *device, char *key, char *value);

/**
 * @brief 删除设备标签
 *
 * @param[in] device 设备句柄
 * @param[in] key    标签的key
 * @return int32_t
 * @retval >=STATE_SUCCESS  消息id
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_device_devinfo_delete(void *device, char *key);



#if defined(__cplusplus)
}
#endif

#endif