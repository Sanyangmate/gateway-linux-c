# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/hq/gatewayProj/aliSDK/device-core/aiot_device_api.c" "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/core.dir/device-core/aiot_device_api.c.o"
  "/home/hq/gatewayProj/aliSDK/device-core/aiot_linkconfig_api.c" "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/core.dir/device-core/aiot_linkconfig_api.c.o"
  "/home/hq/gatewayProj/aliSDK/device-core/aiot_message_api.c" "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/core.dir/device-core/aiot_message_api.c.o"
  "/home/hq/gatewayProj/aliSDK/device-core/common/misc.c" "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/core.dir/device-core/common/misc.c.o"
  "/home/hq/gatewayProj/aliSDK/device-core/core/core_http_client.c" "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/core.dir/device-core/core/core_http_client.c.o"
  "/home/hq/gatewayProj/aliSDK/device-core/core/core_mqtt_client.c" "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/core.dir/device-core/core/core_mqtt_client.c.o"
  "/home/hq/gatewayProj/aliSDK/device-core/core/sysdep/core_os.c" "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/core.dir/device-core/core/sysdep/core_os.c.o"
  "/home/hq/gatewayProj/aliSDK/device-core/core/sysdep/core_sysdep.c" "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/core.dir/device-core/core/sysdep/core_sysdep.c.o"
  "/home/hq/gatewayProj/aliSDK/device-core/core/tls_adapter/core_adapter.c" "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/core.dir/device-core/core/tls_adapter/core_adapter.c.o"
  "/home/hq/gatewayProj/aliSDK/device-core/core/tls_adapter/mbedtls_adapter.c" "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/core.dir/device-core/core/tls_adapter/mbedtls_adapter.c.o"
  "/home/hq/gatewayProj/aliSDK/device-core/core/tls_adapter/tcp_adapter.c" "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/core.dir/device-core/core/tls_adapter/tcp_adapter.c.o"
  "/home/hq/gatewayProj/aliSDK/device-core/core/utils/core_auth.c" "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/core.dir/device-core/core/utils/core_auth.c.o"
  "/home/hq/gatewayProj/aliSDK/device-core/core/utils/core_diag.c" "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/core.dir/device-core/core/utils/core_diag.c.o"
  "/home/hq/gatewayProj/aliSDK/device-core/core/utils/core_global.c" "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/core.dir/device-core/core/utils/core_global.c.o"
  "/home/hq/gatewayProj/aliSDK/device-core/core/utils/core_http.c" "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/core.dir/device-core/core/utils/core_http.c.o"
  "/home/hq/gatewayProj/aliSDK/device-core/core/utils/core_log.c" "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/core.dir/device-core/core/utils/core_log.c.o"
  "/home/hq/gatewayProj/aliSDK/device-core/core/utils/core_sha256.c" "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/core.dir/device-core/core/utils/core_sha256.c.o"
  "/home/hq/gatewayProj/aliSDK/device-core/core/utils/core_string.c" "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/core.dir/device-core/core/utils/core_string.c.o"
  "/home/hq/gatewayProj/aliSDK/device-core/device_bootstrap.c" "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/core.dir/device-core/device_bootstrap.c.o"
  "/home/hq/gatewayProj/aliSDK/device-core/device_dynamic_register.c" "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/core.dir/device-core/device_dynamic_register.c.o"
  "/home/hq/gatewayProj/aliSDK/device-core/device_module.c" "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/core.dir/device-core/device_module.c.o"
  "/home/hq/gatewayProj/aliSDK/device-core/device_private.c" "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/core.dir/device-core/device_private.c.o"
  "/home/hq/gatewayProj/aliSDK/device-core/message_service.c" "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/core.dir/device-core/message_service.c.o"
  "/home/hq/gatewayProj/aliSDK/device-core/protocol/http_transport.c" "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/core.dir/device-core/protocol/http_transport.c.o"
  "/home/hq/gatewayProj/aliSDK/device-core/protocol/mqtt_transport.c" "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/core.dir/device-core/protocol/mqtt_transport.c.o"
  "/home/hq/gatewayProj/aliSDK/device-core/protocol/protocol_config.c" "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/core.dir/device-core/protocol/protocol_config.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../device-modules"
  "../device-modules/shadow"
  "../device-modules/tunnel"
  "../device-modules/tunnel/src"
  "../device-modules/compress"
  "../device-modules/data-model"
  "../device-modules/devinfo"
  "../device-modules/gateway"
  "../device-modules/ntp"
  "../device-modules/logpost"
  "../device-modules/remote-config"
  "../device-modules/ota"
  "../external"
  "../external/nopoll"
  "../external/nopoll/library"
  "../external/nopoll/include"
  "../external/libdeflate"
  "../external/libdeflate/lib"
  "../external/libdeflate/lib/x86"
  "../external/libdeflate/lib/arm"
  "../external/mbedtls"
  "../external/mbedtls/library"
  "../external/mbedtls/include"
  "../external/mbedtls/include/psa"
  "../external/mbedtls/include/mbedtls"
  "../external/cjson"
  "../utils"
  "../portfiles"
  "../portfiles/aiot_port"
  "../device-core"
  "../device-core/common"
  "../device-core/protocol"
  "../device-core/core"
  "../device-core/core/utils"
  "../device-core/core/tls_adapter"
  "../device-core/core/sysdep"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
