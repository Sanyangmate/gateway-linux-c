# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/hq/gatewayProj/aliSDK/demos/device_basic_demo.c" "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/device_basic_demo.dir/demos/device_basic_demo.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../demos"
  "output/include"
  "../external/cjson"
  "../external/mbedtls/include"
  "../external/mbedtls/library"
  "../external/nopoll/include"
  "../external/libdeflate"
  "../external/libdeflate/lib"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/linksdk.dir/DependInfo.cmake"
  "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/cjson.dir/DependInfo.cmake"
  "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/mbedtls.dir/DependInfo.cmake"
  "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/nopoll.dir/DependInfo.cmake"
  "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/deflate.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
