# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/hq/gatewayProj/aliSDK/external/ali_ca_cert.c" "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/linksdk.dir/external/ali_ca_cert.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../device-modules"
  "../device-modules/shadow"
  "../device-modules/tunnel"
  "../device-modules/tunnel/src"
  "../device-modules/compress"
  "../device-modules/data-model"
  "../device-modules/devinfo"
  "../device-modules/gateway"
  "../device-modules/ntp"
  "../device-modules/logpost"
  "../device-modules/remote-config"
  "../device-modules/ota"
  "../external"
  "../external/nopoll"
  "../external/nopoll/library"
  "../external/nopoll/include"
  "../external/libdeflate"
  "../external/libdeflate/lib"
  "../external/libdeflate/lib/x86"
  "../external/libdeflate/lib/arm"
  "../external/mbedtls"
  "../external/mbedtls/library"
  "../external/mbedtls/include"
  "../external/mbedtls/include/psa"
  "../external/mbedtls/include/mbedtls"
  "../external/cjson"
  "../utils"
  "../portfiles"
  "../portfiles/aiot_port"
  "../device-core"
  "../device-core/common"
  "../device-core/protocol"
  "../device-core/core"
  "../device-core/core/utils"
  "../device-core/core/tls_adapter"
  "../device-core/core/sysdep"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/cjson.dir/DependInfo.cmake"
  "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/mbedtls.dir/DependInfo.cmake"
  "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/nopoll.dir/DependInfo.cmake"
  "/home/hq/gatewayProj/aliSDK/build/CMakeFiles/deflate.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
