/**
 * @file aiot_shadow_api.h
 * @brief 设备影子模块头文件, 提供更新, 删除, 获取设备影子的能力
 * @date 2022-01-20
 *
 * @copyright Copyright (C) 2020-2025 Alibaba Group Holding Limited
 *
 */

#ifndef __AIOT_SHADOW_API_H__
#define __AIOT_SHADOW_API_H__

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>

/**
 * @brief shadow模块收到从网络上来的报文时, 通知用户的报文类型
 */
typedef enum {
    /**
     * @brief 设备发送 @ref aiot_device_shadow_update, @ref aiot_device_shadow_clean_desired 或 @ref aiot_device_shadow_delete_reported 发送消息后, 云端返回的应答消息, \n
     * 消息数据结构体参考 @ref aiot_shadow_recv_generic_reply_t
     */
    AIOT_SHADOWRECV_GENERIC_REPLY,

    /**
     * @brief 设备在线时, 云端自动下发的影子内容, 消息数据结构体参考 @ref aiot_shadow_recv_control_t
     */
    AIOT_SHADOWRECV_CONTROL,

    /**
     * @brief 主动获取设备影子内容云端返回的影子内容, 消息数据结构体参考 @ref aiot_shadow_recv_get_reply_t
     */
    AIOT_SHADOWRECV_GET_REPLY,
} aiot_shadow_recv_type_t;

/**
 * @brief 设备调用 @ref aiot_device_shadow_update, @ref aiot_device_shadow_clean_desired 或 @ref aiot_device_shadow_delete_reported 发送消息后, 云端返回的应答消息
 */
typedef struct {
    /**
     * @brief 指向应答数据的指针
     */
    char *payload;

    /**
     * @brief 应答数据长度
     */
    uint32_t payload_len;

    /**
     * @brief 应答状态字符串, 云端处理成功则为<b>success</b>, 发送消息错误则为<b>error</b>, 错误信息和错误码放在在payload中
     */
    char *status;

    /**
     * @brief 应答报文对应的时间戳
     */
    uint64_t timestamp;
} aiot_shadow_recv_generic_reply_t;

/**
 * @brief 如果设备在线, 用户应用调用云端API<a href="https://help.aliyun.com/document_detail/69954.html#doc-api-Iot-UpdateDeviceShadow">UpdateDeviceShadow</a>后云端下推的消息
 */
typedef struct {
    /**
     * @brief 指向设备影子数据的指针
     */
    char *payload;

    /**
     * @brief 设备影子数据长度
     */
    uint32_t payload_len;

    /**
     * @brief 设备影子版本
     */
    uint64_t version;
} aiot_shadow_recv_control_t;

/**
 * @brief 设备调用 @ref aiot_device_shadow_get 发送消息后, 云端返回的设备影子数据
 */
typedef struct {
    /**
     * @brief 指向设备影子数据的指针
     */
    char *payload;

    /**
     * @brief 设备影子数据长度
     */
    uint32_t payload_len;

    /**
     * @brief 设备影子版本号
     */
    uint64_t version;
} aiot_shadow_recv_get_reply_t;

/**
 * @brief shadow模块收到从网络上来的报文时, 通知用户的报文内容
 */
typedef struct {
    /**
     * @brief 报文内容所对应的报文类型, 更多信息请参考@ref aiot_shadow_recv_type_t
     */
    aiot_shadow_recv_type_t  type;
    /**
     * @brief 消息数据联合体, 不同的消息类型将使用不同的消息结构体
     */
    union {
        aiot_shadow_recv_generic_reply_t generic_reply;
        aiot_shadow_recv_control_t control;
        aiot_shadow_recv_get_reply_t get_reply;
    } data;
} aiot_shadow_recv_t;

/**
 * @brief shadow模块收到从网络上来的报文时, 通知用户所调用的数据回调函数
 *
 * @param[in] handle shadow会话句柄
 * @param[in] recv shadow接受消息结构体, 存放收到的shadow报文内容
 * @param[in] userdata 指向用户上下文数据的指针, 这个指针由用户通过调用@ref aiot_device_shadow_set_callback 配置userdata
 *
 * @return void
 */
typedef void (* aiot_shadow_recv_callback_t)(void *device,
        const aiot_shadow_recv_t *recv, void *userdata);

/**
 * @brief 设置设备影子消息回调函数
 *
 * @param[in] device 设备句柄
 * @param[in] callback 消息回调
 * @param[in] userdata 执行回调消息的上下文
 * @return int32_t
 * @retval STATE_SUCCESS 参数配置成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval 其它 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_device_shadow_set_callback(void *device, aiot_shadow_recv_callback_t callback, void *userdata);

/**
 * @brief 更新设备影子中的reported数据
 *
 * @param[in] device 设备句柄
 * @param[in] reported 设备影子reported object字符串, <b>必须为以结束符'\0'结尾的字符串</b>, 如"{\"LightSwitch\": 1}"
 * @param[in] version 设备影子的目标版本, <b>必须大于设备影子的当前版本</b>, 若设置为-1将清空设备影子数据, 并将设备影子版本更新为0
 * @return int32_t
 * @retval STATE_SUCCESS 参数配置成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval 其它 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_device_shadow_update(void *device, char *reported, int64_t version);


/**
 * @brief 清除设备影子中的desired数据
 *
 * @param[in] device 设备句柄
 * @param[in] version 设备影子的目标版本, <b>必须大于设备影子的当前版本</b>
 * @return int32_t
 * @retval STATE_SUCCESS 参数配置成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval 其它 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_device_shadow_clean_desired(void *device, int64_t version);

/**
 * @brief 获取设备影子
 *
 * @param[in] device 设备句柄
 * @return int32_t
 * @retval STATE_SUCCESS 参数配置成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval 其它 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_device_shadow_get(void *device);

/**
 * @brief 用于<b>删除设备影子中的reported数据</b>
 *
 * @param[in] device 设备句柄
 * @param[in] reported 用户将要删除的reported数据, <b>必须为以结束符'\0'结尾的字符串</b>. \n
 *   若要删除全部reported数据, 则应填写"\"null\""字符串 \n
 *   若要删除部分reported数据, 则将对应的值定义为null, 如只清除LightSwitch的值应填写"{\"LightSwitch\":\"null\"}"
 * @param[in] version 设备影子的目标版本, <b>必须大于设备影子的当前版本</b>, 若设置为-1将清空设备影子数据, 并将设备影子版本更新为0
 * @return int32_t
 * @retval STATE_SUCCESS 参数配置成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval 其它 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_device_shadow_delete_reported(void *device, char *reported, int64_t version);

#if defined(__cplusplus)
}
#endif

#endif