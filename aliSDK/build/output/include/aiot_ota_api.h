/**
 * @file aiot_ota_api.h
 * @brief 设备升级模块头文件, 提供设备获取固件信息的能力
 * @date 2022-01-20
 *
 * @copyright Copyright (C) 2020-2025 Alibaba Group Holding Limited
 *
 */

#ifndef _OTA_MODULE_H_
#define _OTA_MODULE_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>

/**
 * @brief OTA过程中使用的digest方法类型, 分为MD5和SHA256两种
 *
 */
typedef enum {

    /**
     * @brief 收到的OTA固件的digest方法为MD5
     *
     */
    AIOT_OTA_DIGEST_MD5,

    /**
     * @brief 收到的OTA固件的digest方法为SHA256
     *
     */
    AIOT_OTA_DIGEST_SHA256,
    AIOT_OTA_DIGEST_MAX
} aiot_ota_digest_type_t;

/**
 * @brief OTA消息的结构体
 *
 */
typedef struct {
    /**
     * @brief 固件的版本信息. 如果为固件信息, 则这个version字段为固件的版本号. 如果为远程配置消息, 则为配置的configId
     *
     */
    char       *version;

    /*
     * @brief http下载固件所需的链接
     *
     */
    char       *http_url;

    /*
     * @brief 固件的大小, 单位为Byte
     *
     */
    uint32_t    size_total;

    /*
     * @brief 云端对固件计算数字签名算法,MD5/SHA256
     *
     */
    aiot_ota_digest_type_t digest_method;
    /*
     * @brief 云端对固件计算数字签名得出来的结果
     *
     */
    char       *expect_digest;

    /**
     * @brief 当前固件所都对应的模块
     *
     */
    char       *module;

    /**
     * @brief 当前下载信息中的扩展内容
     *
     */
    char       *extra_data;

    /**
     * @brief 是否为差分的升级包
     *
     */
    int32_t    is_diff;
} aiot_ota_msg_t;

/**
 * @brief 设备OTA消息回调函数原型，用户定义后, 可通过 @ref aiot_device_ota_set_callback 配置
 *
 * @param[in] device 设备句柄
 * @param[in] msg    接收到的消息 数据结构参考 @ref aiot_ota_msg_t
 * @param[in] userdata 用户设置的上下文，可通过 @ref aiot_device_ota_set_callback 配置
 */
typedef void (*ota_msg_callback_t)(void *device, const aiot_ota_msg_t *msg, void *userdata);

/**
 * @brief 设置OTA消息回调函数
 *
 * @param[in] device 设备句柄
 * @param[in] callback 回调函数
 * @param[in] userdata 执行回调函数后的上下文指针
 *
 * @return int32_t
 * @retval STATE_SUCCESS 参数配置成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_device_ota_set_callback(void *device, ota_msg_callback_t callback, void *userdata);

/**
 * @brief 设备版本信息上报
 *
 * @param[in] device 设备句柄
 * @param[in] module 模块版本信息上报时，参数为模块名 <br/>
 *                   设备版本上报时，参数置为NULL；
 * @param[in] version 版本号
 *
 * @return int32_t
 * @retval STATE_SUCCESS 参数配置成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_device_ota_report_version(void *device, char *module, char *version);

/**
 * @brief 设备升级状态上报
 *
 * @param[in] device 设备句柄
 * @param[in] module 模块版本状态上报时，参数为模块名 <br/>
 *                   设备版本状态上报时，参数置为NULL；
 * @param[in] state  取值：0～100，升级进度 <br/>
 *                   取值：-1， 升级失败 <br/>
 *                   取值：-2， 下载失败 <br/>
 *                   取值：-3， 校验失败 <br/>
 *                   取值：-4， 烧写失败 <br/>
 * @return int32_t
 * @retval STATE_SUCCESS 参数配置成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_device_ota_report_state(void *device, char *module, int32_t state);

/**
 * @brief 设备端主动向云端查询升级固件
 *
 * @param[in] device 设备句柄
 * @param[in] module 请求模块版本时，参数为模块名 <br/>
 *                   请求设备版本时，参数置为NULL；
 * @return int32_t
 * @retval STATE_SUCCESS 参数配置成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_device_ota_request_firmware(void *device, char *module);

/**
 * @brief 拷贝ota消息，用于异步执行ota操作
 * @param[in] msg  源消息
 * @return aiot_ota_msg_t*
 * @retval 非NULL 消息句柄
 * @retval NULL 初始化失败, 一般是内存分配失败导致
 *
 */
aiot_ota_msg_t* aiot_ota_msg_clone(const aiot_ota_msg_t *msg);

/**
 * @brief 释放ota消息资源
 * @param[in] msg  待删除的消息
 * @return void
 */
void aiot_ota_msg_free(aiot_ota_msg_t *msg);

#if defined(__cplusplus)
}
#endif

#endif