#include "core_log.h"
#include "core_time.h"
#include "core_string.h"
#include "core_os.h"
#include "aiot_state_api.h"
#include "device_private.h"
#include "aiot_message_api.h"
#include "ntp_message.h"
#include "aiot_ntp_api.h"

#define TAG "NTP"
typedef struct {
    ntp_callback_t callback;
    void *userdata;
    /* 时区 */
    int8_t zone;
} ntp_config_t;

typedef struct {
    void *device;
    ntp_config_t config;
} ntp_handle_t;

static ntp_handle_t *_ntp_get_handle(void *device);

int32_t aiot_device_ntp_set_zone(void *device, int8_t zone)
{
    ntp_handle_t *ntp_handle = _ntp_get_handle(device);;

    if(device == NULL || ntp_handle == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_ntp_set_zone input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    if(zone < -12 || zone > 12) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_OUT_RANGE, "aiot_device_ntp_set_zone error, please check zone %d\r\n", zone);
        return STATE_USER_INPUT_OUT_RANGE;
    }

    ALOG_INFO(TAG, "set time zone %d\r\n", zone);
    ntp_handle->config.zone = zone;

    return STATE_SUCCESS;
}

static void _ntp_response_callback(void *device, const aiot_msg_t *message, void *userdata)
{
    ntp_msg_t *ntp_resp = NULL;
    uint64_t utc_stamp = 0;
    core_date_t date;
    aiot_ntp_time_t recv;
    ntp_handle_t *ntp_handle = _ntp_get_handle(device);
    if(device == NULL || message == NULL || ntp_handle == NULL) {
        return;
    }

    ntp_resp = ntp_msg_parse_response(message);
    if(ntp_resp == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_PARSE_ERROR, "msg (%s) parse error\r\n", message->topic);
        return;
    }

    utc_stamp = (ntp_resp->serverRecvTime + ntp_resp->serverSendTime + core_os_time() - ntp_resp->deviceSendTime) / 2;
    core_log_set_timestamp(NULL, utc_stamp);
    core_utc2date(utc_stamp, ntp_handle->config.zone, &date);

    if(ntp_handle->config.callback != NULL) {
        memset(&recv, 0, sizeof(aiot_ntp_time_t));
        recv.timestamp = utc_stamp;
        recv.year = date.year;
        recv.mon = date.mon;
        recv.day = date.day;
        recv.hour = date.hour;
        recv.min = date.min;
        recv.sec = date.sec;
        recv.msec = date.msec;

        ALOG_INFO(TAG, "time sync: %d-%d-%d %d:%d:%d %d\r\n",
                  recv.year, recv.mon, recv.day,
                  recv.hour, recv.min, recv.sec, recv.msec);
        ntp_handle->config.callback(device, &recv, ntp_handle->config.userdata);
    }

    ntp_msg_delete(ntp_resp);
}

int32_t aiot_device_ntp_set_callback(void *device, ntp_callback_t callback, void *userdata)
{
    ntp_handle_t *ntp_handle = _ntp_get_handle(device);;

    if(device == NULL || callback == NULL || ntp_handle == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_ntp_set_callback input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    ntp_handle->config.callback = callback;
    ntp_handle->config.userdata = userdata;

    aiot_device_register_topic_filter(device, NTP_RESPONSE_TOPIC_FMT, _ntp_response_callback, 0, NULL);

    return STATE_SUCCESS;
}

int32_t aiot_device_ntp_request(void *device)
{
    aiot_msg_t *msg = NULL;
    int32_t res = STATE_SUCCESS;
    ntp_handle_t *ntp_handle = _ntp_get_handle(device);
    if(device == NULL || ntp_handle == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_ntp_request input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    /* 创建日志上传消息 */
    msg = ntp_msg_create_request(device);
    if(msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_CREATE_ERROR, "ntp message create error \r\n");
        return STATE_MESSAGE_CREATE_ERROR;
    }

    res = aiot_device_send_message(device, msg);

    aiot_msg_delete(msg);

    return res;
}

void *_ntp_init()
{
    ntp_handle_t *ntp_handle = (ntp_handle_t *)core_os_malloc(sizeof(ntp_handle_t));
    if(ntp_handle == NULL) {
        return NULL;
    }

    memset(ntp_handle, 0, sizeof(ntp_handle_t));
    return ntp_handle;
}
void _ntp_deinit(void *handle)
{
    core_os_free(handle);
}

/* 日志上报即插即用操作方法 */
static module_ops_t ntp_ops = {
    .internal_ms = -1,
    .on_init = _ntp_init,
    .on_process = NULL,
    .on_status = NULL,
    .on_deinit = _ntp_deinit,
};

static ntp_handle_t *_ntp_get_handle(void *device)
{
    ntp_handle_t *ntp_handle = core_device_module_get(device, MODULE_NTP);
    int32_t res = STATE_SUCCESS;

    if(ntp_handle == NULL) {
        res = core_device_module_register(device, MODULE_NTP, &ntp_ops);
        if(res != STATE_SUCCESS) {
            return NULL;
        } else {
            ntp_handle = core_device_module_get(device, MODULE_NTP);
            ntp_handle->device = device;
        }
    }
    return ntp_handle;
}