

#ifndef _NTP_MESSAGE_H_
#define _NTP_MESSAGE_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>
#include "aiot_message_api.h"
#include "aiot_ntp_api.h"

/* 时间同步请求topic */
#define NTP_REQUEST_TOPIC_FMT                "/ext/ntp/%s/%s/request"
/* 时间同步请求回复 */
#define NTP_RESPONSE_TOPIC_FMT               "/ext/ntp/+/+/response"

typedef struct {
    uint64_t deviceSendTime;
    uint64_t serverRecvTime;
    uint64_t serverSendTime;
} ntp_msg_t;

/* 创建主动获取日志开关状态消息 */
aiot_msg_t *ntp_msg_create_request(void *device);

/* 解析云端下发消息 */
ntp_msg_t *ntp_msg_parse_response(const aiot_msg_t *msg);

/* 删除ntp_msg_t消息 */
void ntp_msg_delete(ntp_msg_t *msg);

#if defined(__cplusplus)
}
#endif

#endif