/**
 * @file aiot_ntp_api.h
 * @brief 设备时间同步模块头文件, 提供获取utc时间的能力
 * @date 2022-01-20
 *
 * @copyright Copyright (C) 2020-2025 Alibaba Group Holding Limited
 *
 */

#ifndef _AIOT_NTP_API_H
#define _AIOT_NTP_API_H

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>

/**
 * @brief 时间结构体
 */
typedef struct {
    uint64_t timestamp;
    uint32_t year;
    uint32_t mon;
    uint32_t day;
    uint32_t hour;
    uint32_t min;
    uint32_t sec;
    uint32_t msec;
} aiot_ntp_time_t;

/**
 * @brief 设备时间同步回调函数原型，用户定义后, 可通过 @ref aiot_device_ntp_set_callback 配置
 *
 * @param[in] device 设备句柄
 * @param[in] recv_time  返回的互联网时间 @ref aiot_ntp_time_t
 * @param[in] userdata 用户设置的上下文，可通过 @ref aiot_device_ntp_set_callback 配置
 */
typedef void (*ntp_callback_t)(void *device, const aiot_ntp_time_t *recv_time, void *userdata);

/**
 * @brief 设置时区
 *
 * @param[in] device 设备句柄
 * @param[in] zone 时区 取值示例: 东8区, 取值为8; 西3区, 取值为-3
 * @return int32_t
 * @retval STATE_SUCCESS 参数配置成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_device_ntp_set_zone(void *device, int8_t zone);

/**
 * @brief 设置时间同步回调
 *
 * @param[in] device 设备句柄
 * @param[in] callback 消息回调函数
 * @param[in] userdata 执行回调消息的上下文
 * @return int32_t
 * @retval STATE_SUCCESS 参数配置成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_device_ntp_set_callback(void *device, ntp_callback_t callback, void *userdata);

/**
 * @brief 请求时间同步
 *
 * @param[in] device 设备句柄
 * @return int32_t
 * @retval >=STATE_SUCCESS  消息id
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_device_ntp_request(void *device);
#if defined(__cplusplus)
}
#endif

#endif