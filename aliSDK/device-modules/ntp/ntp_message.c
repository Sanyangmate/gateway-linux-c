#include "core_os.h"
#include "core_string.h"
#include "cJSON.h"
#include "device_private.h"
#include "aiot_state_api.h"
#include "aiot_message_api.h"
#include "ntp_message.h"

static aiot_msg_t *_ntp_msg_create_template(void *device, const char* fmt, cJSON *root)
{
    char *topic = NULL, *payload = NULL;
    char *src[] = { NULL, NULL };
    uint32_t src_counter = 2;
    aiot_msg_t *result = NULL;

    src[0] = aiot_device_product_key(device);
    src[1] = aiot_device_name(device);

    if(STATE_SUCCESS != core_sprintf(NULL, &topic, fmt, src, src_counter, "")) {
        return NULL;
    }

    payload = cJSON_PrintUnformatted(root);
    if (payload == NULL) {
        core_os_free(topic);
        return NULL;
    }

    result = aiot_msg_create_raw(topic, (uint8_t *)payload, strlen(payload));
    core_os_free(topic);
    core_os_free(payload);
    return result;
}

/* 创建主动获取日志开关状态消息 */
aiot_msg_t *ntp_msg_create_request(void *device)
{
    cJSON *root = NULL;
    aiot_msg_t *result = NULL;
    char time_str[21] = {0};

    root = cJSON_CreateObject();
    if (root == NULL) {
        return NULL;
    }
    core_uint642str(core_os_time(), time_str, NULL);

    cJSON_AddStringToObject(root, "deviceSendTime", time_str);

    result = _ntp_msg_create_template(device, NTP_REQUEST_TOPIC_FMT, root);
    cJSON_Delete(root);

    return result;
}

/* 解析云端下发消息 */
ntp_msg_t *ntp_msg_parse_response(const aiot_msg_t *msg)
{
    ntp_msg_t *recv = NULL;
    cJSON *root = NULL, *item = NULL;
    if(msg == NULL) {
        return NULL;
    }

    root = cJSON_ParseWithLength((char *)msg->payload, msg->payload_lenth);
    if(root == NULL) {
        return NULL;
    }

    recv = (ntp_msg_t *)core_os_malloc(sizeof(ntp_msg_t));
    if(recv == NULL) {
        cJSON_Delete(root);
        return NULL;
    }

    item = cJSON_GetObjectItem(root, "deviceSendTime");
    if(NULL == item || STATE_SUCCESS != core_str2uint64(item->valuestring, strlen(item->valuestring), &recv->deviceSendTime)) {
        cJSON_Delete(root);
        core_os_free(recv);
        return NULL;
    }

    item = cJSON_GetObjectItem(root, "serverRecvTime");
    if(NULL == item || STATE_SUCCESS != core_str2uint64(item->valuestring, strlen(item->valuestring), &recv->serverRecvTime)) {
        cJSON_Delete(root);
        core_os_free(recv);
        return NULL;
    }

    item = cJSON_GetObjectItem(root, "serverSendTime");
    if(NULL == item || STATE_SUCCESS != core_str2uint64(item->valuestring, strlen(item->valuestring), &recv->serverSendTime)) {
        cJSON_Delete(root);
        core_os_free(recv);
        return NULL;
    }

    cJSON_Delete(root);
    return recv;
}

/* 删除ntp_msg_t消息 */
void ntp_msg_delete(ntp_msg_t *msg)
{
    if(msg != NULL) {
        core_os_free(msg);
    }
}