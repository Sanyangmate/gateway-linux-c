#include "gateway_message.h"
#include "device_private.h"
#include "core_os.h"
#include "core_string.h"
#include "aiot_state_api.h"
#include "cJSON.h"
#include "cBASE64.h"
#include "aiot_message_api.h"

static aiot_msg_t *_gateway_msg_create_template(void *device, const char* fmt, cJSON *root)
{
    char *topic = NULL, *payload = NULL;
    char *src[] = { aiot_device_product_key(device), aiot_device_name(device) };
    uint32_t src_counter = 2;
    aiot_msg_t *result = NULL;

    if(STATE_SUCCESS != core_sprintf(NULL, &topic, fmt, src, src_counter, "")) {
        return NULL;
    }

    payload = cJSON_PrintUnformatted(root);
    if (payload == NULL) {
        core_os_free(topic);
        return NULL;
    }

    result = aiot_msg_create_raw(topic, (uint8_t *)payload, strlen(payload));
    core_os_free(topic);
    core_os_free(payload);
    return result;
}
aiot_msg_t *gateway_msg_create_batch_login(void* gateway_device, subdev_info_t* info_table[], int32_t num, int32_t auto_topo)
{
    cJSON *root = NULL, *params = NULL, *option = NULL, *device_list = NULL, *device = NULL;
    aiot_msg_t *result = NULL;
    int32_t id = 0;
    subdev_info_t* info = NULL;
    int32_t i = 0;

    root = cJSON_CreateObject();
    if (root == NULL) {
        return NULL;
    }


    option = cJSON_CreateObject();
    if (option == NULL) {
        cJSON_Delete(root);
        return NULL;
    }
    cJSON_AddItemToObject(root, "option", option);
    if(auto_topo == 1) {
        cJSON_AddTrueToObject(option, "auto_topo");
    } else {
        cJSON_AddFalseToObject(option, "auto_topo");
    }

    params = cJSON_CreateObject();
    if (params == NULL) {
        cJSON_Delete(root);
        return NULL;
    }

    id = core_device_get_next_alink_id(gateway_device);
    _cJSON_ADDInt2StrToObject(root, "id", id);
    cJSON_AddItemToObject(root, "params", params);

    device_list = cJSON_CreateArray();
    if (device_list == NULL) {
        cJSON_Delete(root);
        return NULL;
    }
    cJSON_AddItemToObject(params, "deviceList", device_list);
    for(i = 0; i < num; i++) {
        device = cJSON_CreateObject();
        if(device == NULL) {
            break;
        }
        info = info_table[i];
        cJSON_AddStringToObject(device, "productKey", info->product_key);
        cJSON_AddStringToObject(device, "deviceName", info->device_name);
        cJSON_AddStringToObject(device, "clientId", info->client_id);
        cJSON_AddStringToObject(device, "timestamp", info->timestamp);
        cJSON_AddStringToObject(device, "signMethod", info->sign_method);
        cJSON_AddStringToObject(device, "sign", info->sign);
        cJSON_AddStringToObject(device, "cleanSession", "true");
        /* 将设备添加进device_list */
        cJSON_AddItemToArray(device_list, device);
    }

    result = _gateway_msg_create_template(gateway_device, GATEWAY_SUBDEVICE_BATCH_LOGIN_TOPIC_FMT, root);
    result->id = id;
    cJSON_Delete(root);

    return result;
}

aiot_msg_t *gateway_msg_create_batch_logout(void* gateway_device, subdev_info_t* info_table[], int32_t num)
{
    cJSON *root = NULL, *params = NULL, *device = NULL;
    aiot_msg_t *result = NULL;
    int32_t id = 0;
    subdev_info_t* info = NULL;
    int32_t i = 0;

    root = cJSON_CreateObject();
    if (root == NULL) {
        return NULL;
    }

    params = cJSON_CreateArray();
    if (params == NULL) {
        cJSON_Delete(root);
        return NULL;
    }

    id = core_device_get_next_alink_id(gateway_device);
    _cJSON_ADDInt2StrToObject(root, "id", id);
    cJSON_AddItemToObject(root, "params", params);
    for(i = 0; i < num; i++) {
        device = cJSON_CreateObject();
        if(device == NULL) {
            break;
        }
        info = info_table[i];
        cJSON_AddStringToObject(device, "productKey", info->product_key);
        cJSON_AddStringToObject(device, "deviceName", info->device_name);
        /* 将设备添加进device_list */
        cJSON_AddItemToArray(params, device);
    }

    result = _gateway_msg_create_template(gateway_device, GATEWAY_SUBDEVICE_BATCH_LOGOUT_TOPIC_FMT, root);
    result->id = id;
    cJSON_Delete(root);

    return result;
}

aiot_msg_t *gateway_msg_create_topo_add(void* gateway_device, subdev_info_t* info_table[], int32_t num)
{
    cJSON *root = NULL, *params = NULL, *device = NULL;
    aiot_msg_t *result = NULL;
    int32_t id = 0;
    subdev_info_t *info = NULL;
    int i = 0;

    root = cJSON_CreateObject();
    if (root == NULL) {
        return NULL;
    }

    params = cJSON_CreateArray();
    if (params == NULL) {
        cJSON_Delete(root);
        return NULL;
    }

    id = core_device_get_next_alink_id(gateway_device);
    _cJSON_ADDInt2StrToObject(root, "id", id);
    cJSON_AddItemToObject(root, "params", params);
    for(i = 0; i < num; i++) {
        device = cJSON_CreateObject();
        if(device == NULL) {
            break;
        }
        info = info_table[i];
        cJSON_AddStringToObject(device, "productKey", info->product_key);
        cJSON_AddStringToObject(device, "deviceName", info->device_name);
        cJSON_AddStringToObject(device, "clientId", info->client_id);
        cJSON_AddStringToObject(device, "timestamp", info->timestamp);
        cJSON_AddStringToObject(device, "signMethod", info->sign_method);
        cJSON_AddStringToObject(device, "sign", info->sign);
        /* 将设备添加进device_list */
        cJSON_AddItemToArray(params, device);
    }
    result = _gateway_msg_create_template(gateway_device, GATEWAY_SUBDEVICE_ADD_TOPO_TOPIC_FMT, root);
    result->id = id;
    cJSON_Delete(root);

    return result;
}

aiot_msg_t *gateway_msg_create_topo_delete(void* gateway_device, subdev_info_t* info_table[], int32_t num)
{
    cJSON *root = NULL, *params = NULL, *device = NULL;
    aiot_msg_t *result = NULL;
    int32_t id = 0;
    subdev_info_t *info = NULL;
    int i = 0;

    root = cJSON_CreateObject();
    if (root == NULL) {
        return NULL;
    }

    params = cJSON_CreateArray();
    if (params == NULL) {
        cJSON_Delete(root);
        return NULL;
    }

    id = core_device_get_next_alink_id(gateway_device);
    _cJSON_ADDInt2StrToObject(root, "id", id);
    cJSON_AddItemToObject(root, "params", params);
    for(i = 0; i < num; i++) {
        device = cJSON_CreateObject();
        if(device == NULL) {
            break;
        }
        info = info_table[i];
        cJSON_AddStringToObject(device, "productKey", info->product_key);
        cJSON_AddStringToObject(device, "deviceName", info->device_name);
        /* 将设备添加进device_list */
        cJSON_AddItemToArray(params, device);
    }
    result = _gateway_msg_create_template(gateway_device, GATEWAY_SUBDEVICE_DELETE_TOPO_TOPIC_FMT, root);
    result->id = id;
    cJSON_Delete(root);

    return result;
}

aiot_msg_t *gateway_msg_create_secret_query(void* gateway_device, subdev_info_t* info_table[], int32_t num)
{
    cJSON *root = NULL, *params = NULL, *device = NULL;
    aiot_msg_t *result = NULL;
    int32_t id = 0;
    subdev_info_t *info = NULL;
    int i = 0;

    root = cJSON_CreateObject();
    if (root == NULL) {
        return NULL;
    }

    params = cJSON_CreateArray();
    if (params == NULL) {
        cJSON_Delete(root);
        return NULL;
    }

    id = core_device_get_next_alink_id(gateway_device);
    _cJSON_ADDInt2StrToObject(root, "id", id);
    cJSON_AddItemToObject(root, "params", params);
    for(i = 0; i < num; i++) {
        device = cJSON_CreateObject();
        if(device == NULL) {
            break;
        }
        info = info_table[i];
        cJSON_AddStringToObject(device, "productKey", info->product_key);
        cJSON_AddStringToObject(device, "deviceName", info->device_name);
        /* 将设备添加进device_list */
        cJSON_AddItemToArray(params, device);
    }
    result = _gateway_msg_create_template(gateway_device, GATEWAY_SUBDEVICE_REGISTER_TOPIC_FMT, root);
    result->id = id;
    cJSON_Delete(root);

    return result;
}

aiot_msg_t *gateway_msg_create_product_register(void* gateway_device, subdev_info_t* info_table[], int32_t num)
{
    cJSON *root = NULL, *params = NULL, *proxieds = NULL, *device = NULL;
    aiot_msg_t *result = NULL;
    int32_t id = 0;
    subdev_info_t *info = NULL;
    int i = 0;

    root = cJSON_CreateObject();
    if (root == NULL) {
        return NULL;
    }
    params = cJSON_CreateObject();
    if (params == NULL) {
        return NULL;
    }

    proxieds = cJSON_CreateArray();
    if (proxieds == NULL) {
        cJSON_Delete(root);
        return NULL;
    }

    id = core_device_get_next_alink_id(gateway_device);
    _cJSON_ADDInt2StrToObject(root, "id", id);
    cJSON_AddItemToObject(root, "params", params);
    cJSON_AddItemToObject(params, "proxieds", proxieds);
    for(i = 0; i < num; i++) {
        device = cJSON_CreateObject();
        if(device == NULL) {
            break;
        }
        info = info_table[i];
        cJSON_AddStringToObject(device, "productKey", info->product_key);
        cJSON_AddStringToObject(device, "deviceName", info->device_name);
        cJSON_AddStringToObject(device, "random", info->timestamp);
        cJSON_AddStringToObject(device, "signMethod", info->sign_method);
        cJSON_AddStringToObject(device, "sign", info->sign);
        /* 将设备添加进device_list */
        cJSON_AddItemToArray(proxieds, device);
    }

    result = _gateway_msg_create_template(gateway_device, GATEWAY_PRODUCT_REGISTER_TOPIC_FMT, root);
    result->id = id;
    cJSON_Delete(root);

    return result;
}

aiot_msg_t *gateway_msg_create_batch_post(void* gateway_device, aiot_msg_t *msg_table[], int32_t msg_num)
{
    cJSON *root = NULL, *params = NULL, *msg = NULL;
    aiot_msg_t *result = NULL;
    char *base64_payload = NULL;
    int32_t id = 0;
    int i = 0;

    root = cJSON_CreateObject();
    if (root == NULL) {
        return NULL;
    }
    params = cJSON_CreateArray();
    if (params == NULL) {
        return NULL;
    }

    id = core_device_get_next_alink_id(gateway_device);
    _cJSON_ADDInt2StrToObject(root, "id", id);
    cJSON_AddItemToObject(root, "params", params);
    for(i = 0; i < msg_num; i++) {
        msg = cJSON_CreateObject();
        if(msg == NULL) {
            break;
        }
        cJSON_AddStringToObject(msg, "topic", msg_table[i]->topic);
        base64_payload = base64encode(msg_table[i]->payload, msg_table[i]->payload_lenth, core_os_malloc);
        if(base64_payload == NULL) {
            break;
        }
        cJSON_AddStringToObject(msg, "payload", base64_payload);
        /* 将设备添加进device_list */
        cJSON_AddItemToArray(params, msg);
        core_os_free(base64_payload);
    }

    result = _gateway_msg_create_template(gateway_device, GATEWAY_BATCH_POST_TOPIC_FMT, root);
    result->id = id;
    cJSON_Delete(root);

    return result;
}

aiot_gateway_msg_t* gateway_msg_parse_batch_reply(const aiot_msg_t *msg)
{
    aiot_gateway_msg_t *recv = NULL;
    subdev_recv_t *dev_info = NULL;
    cJSON *root = NULL, *data = NULL, *device = NULL, *item = NULL;
    int32_t i = 0;
    if(msg == NULL) {
        return NULL;
    }

    root = cJSON_ParseWithLength((char *)msg->payload, msg->payload_lenth);
    if(root == NULL) {
        return NULL;
    }

    recv = (aiot_gateway_msg_t *)core_os_malloc(sizeof(aiot_gateway_msg_t));
    memset(recv, 0, sizeof(aiot_gateway_msg_t));

    if(NULL != (item = cJSON_GetObjectItem(root, "code"))) {
        recv->data.reply.code = item->valueint;
    } else {
        cJSON_Delete(root);
        core_os_free(recv);
        return NULL;
    }
    if(NULL != (item = cJSON_GetObjectItem(root, "id")) && item->valuestring != NULL) {
        core_str2uint(item->valuestring, strlen(item->valuestring), &recv->data.reply.id);
    } else {
        cJSON_Delete(root);
        core_os_free(recv);
        return NULL;
    }

    data = cJSON_GetObjectItem(root, "data");
    if(NULL == data) {
        cJSON_Delete(root);
        core_os_free(recv);
        return NULL;
    }

    recv->subdev_num = cJSON_GetArraySize(data);
    recv->subdev_table = core_os_malloc(recv->subdev_num * sizeof(subdev_recv_t));
    memset(recv->subdev_table, 0, recv->subdev_num * sizeof(subdev_recv_t));
    for(i = 0; i < cJSON_GetArraySize(data); i++) {
        device = cJSON_GetArrayItem(data, i);
        dev_info = &recv->subdev_table[i];
        if(NULL != (item = cJSON_GetObjectItem(device, "productKey")) && item->valuestring != NULL) {
            core_strdup(NULL, &dev_info->product_key, item->valuestring, "");
        }

        if(NULL != (item = cJSON_GetObjectItem(device, "deviceName")) && item->valuestring != NULL) {
            core_strdup(NULL, &dev_info->device_name, item->valuestring, "");
        }

        if(NULL != (item = cJSON_GetObjectItem(device, "deviceSecret")) && item->valuestring != NULL) {
            core_strdup(NULL, &dev_info->device_secret, item->valuestring, "");
        }
    }

    if(NULL != (item = cJSON_GetObjectItem(root, "message"))) {
        core_strdup(NULL, &recv->data.reply.message, item->valuestring, "");
    }

    cJSON_Delete(root);
    return recv;
}

aiot_gateway_msg_t* gateway_msg_parse_product_register_reply(const aiot_msg_t *msg)
{
    aiot_gateway_msg_t *recv = NULL;
    subdev_recv_t *dev_info = NULL;
    cJSON *root = NULL, *data = NULL, *successes = NULL, *device = NULL, *item = NULL;
    int32_t i = 0;
    if(msg == NULL) {
        return NULL;
    }

    root = cJSON_ParseWithLength((char *)msg->payload, msg->payload_lenth);
    if(root == NULL) {
        return NULL;
    }

    recv = (aiot_gateway_msg_t *)core_os_malloc(sizeof(aiot_gateway_msg_t));
    memset(recv, 0, sizeof(aiot_gateway_msg_t));

    if(NULL != (item = cJSON_GetObjectItem(root, "code"))) {
        recv->data.reply.code = item->valueint;
    } else {
        cJSON_Delete(root);
        core_os_free(recv);
        return NULL;
    }
    if(NULL != (item = cJSON_GetObjectItem(root, "id")) && item->valuestring != NULL) {
        core_str2uint(item->valuestring, strlen(item->valuestring), &recv->data.reply.id);
    } else {
        cJSON_Delete(root);
        core_os_free(recv);
        return NULL;
    }

    data = cJSON_GetObjectItem(root, "data");
    if(NULL == data) {
        cJSON_Delete(root);
        core_os_free(recv);
        return NULL;
    }

    successes = cJSON_GetObjectItem(data, "successes");
    if(NULL == successes) {
        cJSON_Delete(root);
        core_os_free(recv);
        return NULL;
    }

    recv->subdev_num = cJSON_GetArraySize(successes);
    recv->subdev_table = core_os_malloc(recv->subdev_num * sizeof(subdev_recv_t));
    memset(recv->subdev_table, 0, recv->subdev_num * sizeof(subdev_recv_t));
    for(i = 0; i < cJSON_GetArraySize(successes); i++) {
        device = cJSON_GetArrayItem(successes, i);
        dev_info = &recv->subdev_table[i];
        if(NULL != (item = cJSON_GetObjectItem(device, "productKey")) && item->valuestring != NULL) {
            core_strdup(NULL, &dev_info->product_key, item->valuestring, "");
        }

        if(NULL != (item = cJSON_GetObjectItem(device, "deviceName")) && item->valuestring != NULL) {
            core_strdup(NULL, &dev_info->device_name, item->valuestring, "");
        }

        if(NULL != (item = cJSON_GetObjectItem(device, "deviceSecret")) && item->valuestring != NULL) {
            core_strdup(NULL, &dev_info->device_secret, item->valuestring, "");
        }
    }

    if(NULL != (item = cJSON_GetObjectItem(root, "message"))) {
        core_strdup(NULL, &recv->data.reply.message, item->valuestring, "");
    }

    cJSON_Delete(root);
    return recv;
}
aiot_gateway_msg_t* gateway_msg_parse_topo_change(const aiot_msg_t *msg)
{
    aiot_gateway_msg_t *recv = NULL;
    subdev_recv_t *dev_info = NULL;
    cJSON *root = NULL, *params = NULL, *sub_list = NULL, *device = NULL, *item = NULL;
    int32_t i = 0;
    if(msg == NULL) {
        return NULL;
    }

    root = cJSON_ParseWithLength((char *)msg->payload, msg->payload_lenth);
    if(root == NULL) {
        return NULL;
    }

    recv = (aiot_gateway_msg_t *)core_os_malloc(sizeof(aiot_gateway_msg_t));
    memset(recv, 0, sizeof(aiot_gateway_msg_t));

    params = cJSON_GetObjectItem(root, "params");
    if(NULL == params) {
        cJSON_Delete(root);
        core_os_free(recv);
        return NULL;
    }

    sub_list = cJSON_GetObjectItem(params, "subList");
    if(NULL == sub_list) {
        cJSON_Delete(root);
        core_os_free(recv);
        return NULL;
    }

    recv->subdev_num = cJSON_GetArraySize(sub_list);
    recv->subdev_table = core_os_malloc(recv->subdev_num * sizeof(subdev_recv_t));
    memset(recv->subdev_table, 0, recv->subdev_num * sizeof(subdev_recv_t));
    for(i = 0; i < cJSON_GetArraySize(sub_list); i++) {
        device = cJSON_GetArrayItem(sub_list, i);
        dev_info = &recv->subdev_table[i];
        if(NULL != (item = cJSON_GetObjectItem(device, "productKey")) && item->valuestring != NULL) {
            core_strdup(NULL, &dev_info->product_key, item->valuestring, "");
        }

        if(NULL != (item = cJSON_GetObjectItem(device, "deviceName")) && item->valuestring != NULL) {
            core_strdup(NULL, &dev_info->device_name, item->valuestring, "");
        }

        if(NULL != (item = cJSON_GetObjectItem(device, "deviceSecret")) && item->valuestring != NULL) {
            core_strdup(NULL, &dev_info->device_secret, item->valuestring, "");
        }
    }

    if(NULL != (item = cJSON_GetObjectItem(params, "status"))) {
        recv->data.topo_change.status = item->valueint;
    }

    cJSON_Delete(root);
    return recv;
}


void gateway_msg_delete(aiot_gateway_msg_t *gateway_msg)
{
    int i = 0;
    subdev_recv_t *dev_info = NULL;
    if(gateway_msg == NULL) {
        return;
    }

    for(i = 0; i < gateway_msg->subdev_num; i++) {
        dev_info = gateway_msg->subdev_table + i;
        if(dev_info->product_key != NULL) {
            core_os_free(dev_info->product_key);
        }

        if(dev_info->device_name != NULL) {
            core_os_free(dev_info->device_name);
        }

        if(dev_info->device_secret != NULL) {
            core_os_free(dev_info->device_secret);
        }
    }
    core_os_free(gateway_msg->subdev_table);

    if(gateway_msg->type == AIOT_GWRECV_TOPO_CHANGE_NOTICE) {

    } else {
        if(gateway_msg->data.reply.message != NULL) {
            core_os_free(gateway_msg->data.reply.message);
        }

    }

    core_os_free(gateway_msg);
}