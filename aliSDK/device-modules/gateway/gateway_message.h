

#ifndef _GATEWAY_MESSAGE_H_
#define _GATEWAY_MESSAGE_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>
#include "aiot_message_api.h"
#include "aiot_gateway_api.h"

/* 子设备批量登录topic */
#define GATEWAY_SUBDEVICE_BATCH_LOGIN_TOPIC_FMT       "/ext/session/%s/%s/combine/batch_login"
#define GATEWAY_SUBDEVICE_BATCH_LOGIN_REPLY_TOPIC_FMT "/ext/session/+/+/combine/batch_login_reply"

/* 子设备批量登出topic */
#define GATEWAY_SUBDEVICE_BATCH_LOGOUT_TOPIC_FMT      "/ext/session/%s/%s/combine/batch_logout"
#define GATEWAY_SUBDEVICE_BATCH_LOGOUT_REPLY_TOPIC_FMT "/ext/session/+/+/combine/batch_logout_reply"

/* 子设备批量添加topo关系topic */
#define GATEWAY_SUBDEVICE_ADD_TOPO_TOPIC_FMT          "/sys/%s/%s/thing/topo/add"
#define GATEWAY_SUBDEVICE_ADD_TOPO_REPLY_TOPIC_FMT    "/sys/+/+/thing/topo/add_reply"

/* 子设备批量删除topo关系topic */
#define GATEWAY_SUBDEVICE_DELETE_TOPO_TOPIC_FMT       "/sys/%s/%s/thing/topo/delete"
#define GATEWAY_SUBDEVICE_DELETE_TOPO_REPLY_TOPIC_FMT "/sys/+/+/thing/topo/delete_reply"

/* topo关系变更通知 */
#define GATEWAY_SUBDEVICE_TOPO_CHANGED_TOPIC_FMT      "/sys/+/+/thing/topo/change"

/* 网关代理子设备查询设备密钥 */
#define GATEWAY_SUBDEVICE_REGISTER_TOPIC_FMT          "/sys/%s/%s/thing/sub/register"
#define GATEWAY_SUBDEVICE_REGISTER_REPLY_TOPIC_FMT    "/sys/+/+/thing/sub/register_reply"

/* 网关代理子设备动态注册设备 */
#define GATEWAY_PRODUCT_REGISTER_TOPIC_FMT          "/sys/%s/%s/thing/proxy/provisioning/product_register"
#define GATEWAY_PRODUCT_REGISTER_REPLY_TOPIC_FMT    "/sys/+/+/thing/proxy/provisioning/product_register_reply"

/* 批量消息上报 */
#define GATEWAY_BATCH_POST_TOPIC_FMT                "/sys/%s/%s/proxy/batch_post"
#define GATEWAY_BATCH_POST_REPLY_TOPIC_FMT          "/sys/+/+/proxy/batch_post_reply"
typedef struct {
    char *product_key;
    char *device_name;
    char *client_id;
    char *sign_method;
    char *sign;
    char *timestamp;
} subdev_info_t;

aiot_msg_t *gateway_msg_create_batch_login(void* gateway_device, subdev_info_t* info_table[], int32_t num, int32_t auto_topo);
aiot_msg_t *gateway_msg_create_batch_logout(void* gateway_device, subdev_info_t* info_table[], int32_t num);

aiot_msg_t *gateway_msg_create_topo_add(void* gateway_device, subdev_info_t* info_table[], int32_t num);
aiot_msg_t *gateway_msg_create_topo_delete(void* gateway_device, subdev_info_t* info_table[], int32_t num);

aiot_msg_t *gateway_msg_create_secret_query(void* gateway_device, subdev_info_t* info_table[], int32_t num);
aiot_msg_t *gateway_msg_create_product_register(void* gateway_device, subdev_info_t* info_table[], int32_t num);

aiot_msg_t *gateway_msg_create_batch_post(void* gateway_device, aiot_msg_t *msg_table[], int32_t msg_num);
aiot_gateway_msg_t* gateway_msg_parse_batch_reply(const aiot_msg_t *msg);
aiot_gateway_msg_t* gateway_msg_parse_product_register_reply(const aiot_msg_t *msg);
aiot_gateway_msg_t* gateway_msg_parse_topo_change(const aiot_msg_t *msg);
void gateway_msg_delete(aiot_gateway_msg_t *gateway_msg);

#if defined(__cplusplus)
}
#endif

#endif