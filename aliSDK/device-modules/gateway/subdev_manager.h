#ifndef _SUBDEV_MANAGER_H_
#define _SUBDEV_MANAGER_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>
#include "gateway_private.h"

int32_t core_subdev_manager_init(gateway_handle_t *gateway_handle);
int32_t core_subdev_manager_deinit(gateway_handle_t *gateway_handle);

/* 创建子设备对象并与网关模块关联 */
subdev_handle_t *core_subdev_init(gateway_handle_t *gateway_handle, void *sub_device);
/* 删除子设备对象并与网关设备解除关联 */
void core_subdev_deinit(gateway_handle_t *gateway_handle, subdev_handle_t *subdev_handle);

/* 添加子设备进相应的状态容器，会根据状态从其它容器移除 */
int32_t core_subdev_move_to_connnecting(gateway_handle_t *gateway_handle, subdev_handle_t *subdev_handle);
int32_t core_subdev_move_to_connnected(gateway_handle_t *gateway_handle, subdev_handle_t *subdev_handle);
int32_t core_subdev_move_to_disconnected(gateway_handle_t *gateway_handle, subdev_handle_t *subdev_handle);

/* 从状态容器中搜索子设备 */
subdev_handle_t *core_subdev_search_connnecting(gateway_handle_t *gateway_handle, char *product_key, char *device_name);
subdev_handle_t *core_subdev_search_connnected(gateway_handle_t *gateway_handle, char *product_key, char *device_name);
subdev_handle_t *core_subdev_search_disconnected(gateway_handle_t *gateway_handle, char *product_key, char *device_name);
subdev_handle_t *core_subdev_search_all(gateway_handle_t *gateway_handle, char *product_key, char *device_name);

/* 从状态容器中删除子设备 */
int32_t core_subdev_delete_connnecting(gateway_handle_t *gateway_handle, subdev_handle_t *subdev_handle);
int32_t core_subdev_delete_connnected(gateway_handle_t *gateway_handle, subdev_handle_t *subdev_handle);
int32_t core_subdev_delete_disconnected(gateway_handle_t *gateway_handle, subdev_handle_t *subdev_handle);
int32_t core_subdev_delete_all(gateway_handle_t *gateway_handle, subdev_handle_t *subdev_handle);

/* 从状态容器中获取表头设备 */
subdev_handle_t *core_subdev_front_connnecting(gateway_handle_t *gateway_handle);
subdev_handle_t *core_subdev_front_connnected(gateway_handle_t *gateway_handle);
subdev_handle_t *core_subdev_front_disconnected(gateway_handle_t *gateway_handle);

/* 查询状态容器子设备个数 */
int32_t core_subdev_size_connnecting(gateway_handle_t *gateway_handle);
int32_t core_subdev_size_connnected(gateway_handle_t *gateway_handle);
int32_t core_subdev_size_disconnected(gateway_handle_t *gateway_handle);
int32_t core_subdev_size_all(gateway_handle_t *gateway_handle);

#if defined(__cplusplus)
}
#endif

#endif