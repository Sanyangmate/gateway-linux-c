#include "device_private.h"
#include "aiot_state_api.h"
#include "subdev_manager.h"
#include "gateway_private.h"
#include "core_string.h"
#include "core_os.h"

/* 子设备的连接与断连只能使用网关的接口，此处直接返回错误 */
static int32_t subdev_connect(void *device)
{
    return -1;
}

static int32_t subdev_connect_async(void *device)
{
    return -1;
}

static int32_t subdev_disconnect(void *device)
{
    device_handle_t *sub_dev = (device_handle_t *)device;
    gateway_handle_t *gateway_handle = NULL;
    subdev_handle_t *subdev_handle = NULL;
    aiot_device_status_t status;
    int32_t res = 0;
    if(sub_dev == NULL) {
        return -1;
    }

    gateway_handle = core_gateway_get_handle(sub_dev->gateway_device);
    if(gateway_handle == NULL) {
        return -1;
    }

    /* 发送断连消息 */
    res = core_gateway_subdev_disconnect(sub_dev->gateway_device, (void **)&sub_dev, 1);

    /* 把设备从子设备管理列表中删除 */
    subdev_handle = core_subdev_search_all(gateway_handle, aiot_device_product_key(sub_dev), aiot_device_name(sub_dev));
    if(subdev_handle != NULL) {
        /* 子设备如果为在线状态的话，通知子设备状态变更 */
        if(subdev_handle->status == SUBDEV_STATUS_CONNECTED || subdev_handle->status == SUBDEV_STATUS_RECONNECTED ) {
            status.type = AIOT_DEVICE_STATUS_DISCONNECT;
            status.error_code = 0;
            normal_ops.status_handle(sub_dev, &status);
        }

        /* 回收子设备资源 */
        core_subdev_delete_all(gateway_handle, subdev_handle);
        core_subdev_deinit(gateway_handle, subdev_handle);
    }

    return res;
}

/* 发送需要返回的消息时，将packed_id与子设备对象关联 */
static int32_t _add_pend_ack_list(gateway_handle_t *gateway_handle, uint32_t packet_id, void *sub_dev)
{
    pend_ack_node_t *node = NULL;
    char *product_key = NULL, *device_name = NULL;

    product_key = aiot_device_product_key(sub_dev);
    device_name = aiot_device_name(sub_dev);
    if(product_key == NULL || device_name == NULL) {
        return -1;
    }
    node = (pend_ack_node_t *)core_os_malloc(sizeof(pend_ack_node_t));
    if(node == NULL) {
        return -1;
    }
    memset(node, 0, sizeof(*node));
    node->timestamp = core_os_time();
    node->packet_id = packet_id;
    core_strdup(NULL, &node->product_key, product_key, NULL);
    core_strdup(NULL, &node->device_name, device_name, NULL);

    /* 将关联节点添加进等待ack链表中 */
    core_os_mutex_lock(gateway_handle->pend_ack_mutex);
    core_list_add_tail(&node->linked_node, &gateway_handle->pend_ack_list);
    core_os_mutex_unlock(gateway_handle->pend_ack_mutex);

    return STATE_SUCCESS;
}
static int32_t subdev_send_message(void *device, const aiot_msg_t *message)
{
    device_handle_t *sub_dev = (device_handle_t *)device;
    gateway_handle_t *gateway_handle = NULL;
    aiot_msg_t *new_msg = NULL;
    int32_t res = 0;
    if(sub_dev == NULL || sub_dev->gateway_device == NULL || message == NULL) {
        return -1;
    }

    gateway_handle = core_gateway_get_handle(sub_dev->gateway_device);
    if(gateway_handle == NULL) {
        return -1;
    }

    if(message->cid == DEFAULT_CID && sub_dev->owner_cid != DEFAULT_CID) {
        new_msg = aiot_msg_clone(message);
        new_msg->cid = sub_dev->owner_cid;
        res = core_gateway_send_message(sub_dev->gateway_device, new_msg);
        aiot_msg_delete(new_msg);
    } else {
        res = core_gateway_send_message(sub_dev->gateway_device, message);
    }

    if(message->qos == 1 && res >= STATE_SUCCESS) {
        /* 将消息id与子设备的映射关系保存 */
        _add_pend_ack_list(gateway_handle, res, sub_dev);
    }

    return res;
}
static int32_t subdev_sub_topic(void *device, const char *topic, uint8_t qos, int32_t cid)
{
    device_handle_t *sub_dev = (device_handle_t *)device;
    gateway_handle_t *gateway_handle = NULL;
    int32_t res = 0;
    if(sub_dev == NULL || sub_dev->gateway_device == NULL || topic == NULL) {
        return -1;
    }

    gateway_handle = core_gateway_get_handle(sub_dev->gateway_device);
    if(gateway_handle == NULL) {
        return -1;
    }

    if(cid == DEFAULT_CID) {
        cid = sub_dev->owner_cid;
    }

    /* sub动作直接由网关代理 */
    res = gateway_handle->gateway_adapter->sub_topic(sub_dev->gateway_device, topic, 1, cid);
    if(res >= STATE_SUCCESS) {
        /* 将消息id与子设备的映射关系保存 */
        _add_pend_ack_list(gateway_handle, res, sub_dev);
    }

    return res;
}
static int32_t subdev_unsub_topic(void *device, const char *topic, uint8_t qos, int32_t cid)
{
    device_handle_t *sub_dev = (device_handle_t *)device;
    gateway_handle_t *gateway_handle = NULL;
    int32_t res = 0;
    if(device == NULL || sub_dev->gateway_device == NULL || topic == NULL) {
        return -1;
    }

    gateway_handle = core_gateway_get_handle(sub_dev->gateway_device);
    if(gateway_handle == NULL) {
        return -1;
    }

    if(cid == DEFAULT_CID) {
        cid = sub_dev->owner_cid;
    }

    /* unsub动作直接由网关代理 */
    res = gateway_handle->gateway_adapter->unsub_topic(sub_dev->gateway_device, topic, 1, cid);
    if(res >= STATE_SUCCESS) {
        /* 将消息id与子设备的映射关系保存 */
        _add_pend_ack_list(gateway_handle, res, sub_dev);
    }

    return res;
}

static device_operation_t subdev_adapter = {
    .connect = subdev_connect,
    .connect_async = subdev_connect_async,
    .disconnect = subdev_disconnect,
    .send_message = subdev_send_message,
    .sub_topic = subdev_sub_topic,
    .unsub_topic = subdev_unsub_topic,
};

/* 获取适配后的子设备的方法 */
const device_operation_t* subdev_adapter_get()
{
    /* 下行消息保留原有的方法 */
    subdev_adapter.message_handle = normal_ops.message_handle;
    subdev_adapter.result_handle = normal_ops.result_handle;
    subdev_adapter.status_handle = normal_ops.status_handle;

    return &subdev_adapter;
}