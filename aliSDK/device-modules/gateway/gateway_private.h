#ifndef _GATEWAY_PRIVATE_H_
#define _GATEWAY_PRIVATE_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>
#include "aiot_gateway_api.h"
#include "device_private.h"
#include "core_list.h"
#include "gateway_message.h"

#define MAX_BATCH_POST_SIZE  50

typedef enum {
    /* 子设备初始状态 */
    SUBDEV_STATUS_INIT,
    /* 子设备主动通过网关建连中 */
    SUBDEV_STATUS_CONNECTING,
    /* 子设备已成功通过网关建连 */
    SUBDEV_STATUS_CONNECTED,
    /* 网关代理子设备重连 */
    SUBDEV_STATUS_RECONNECTING,
    /* 网关代理子设备重连 */
    SUBDEV_STATUS_RECONNECTED,
    /* 网关代理子设备连接过，但是现在属于掉线状态 */
    SUBDEV_STATUS_DISCONNECTED,
} subdev_status_t;

typedef struct {
    /* 子设备的设备handle */
    device_handle_t *device;
    /* 子设备状态 */
    subdev_status_t status;
    /* 子设备初次建连时间戳 */
    uint64_t connect_stamp;
    /* 网关代理子设备建连时间戳 */
    uint64_t reconnect_stamp;
} subdev_handle_t;

typedef struct {
    gateway_msg_callback_t callback;
    void *userdata;
    int32_t msg_timeout;
    int32_t auto_topo;
    int32_t auto_reconnect;
} gateway_config_t;

/* 控制指令消息回复链表 */
typedef struct {
    uint64_t timestamp;
    aiot_gateway_msg_t *msg;
    struct core_list_head linked_node;
} resp_msg_node_t;

/* 控制指令消息回复链表 */
typedef struct {
    aiot_msg_t *msg;
    struct core_list_head linked_node;
} send_msg_node_t;

/* ack匹配节点 */
typedef struct {
    uint32_t packet_id;
    char *product_key;
    char *device_name;
    uint64_t timestamp;
    struct core_list_head linked_node;
} pend_ack_node_t;

typedef struct {
    /* 保存所属设备的指针 */
    void *device;
    gateway_config_t config;
    /* 已经通过网关建连成功子设备列表 */
    void *online_subdev_mgr;
    /* 正在建连子设备列表 */
    void *conn_subdev_mgr;
    /* 离线等待重连的设备 */
    void *offline_subdev_mgr;
    /* 子设备操作适配器 */
    const device_operation_t *subdev_adapter;
    /* 网关设备操作适配器 */
    const device_operation_t *gateway_adapter;
    /* 接收到的控制消息回复列表 */
    struct core_list_head recv_msg_list;
    void *recv_msg_mutex;
    int32_t msg_num;
    /* pub_ack、sub_ack回复匹配对应设备 */
    struct core_list_head pend_ack_list;
    void *pend_ack_mutex;
} gateway_handle_t;

gateway_handle_t *core_gateway_get_handle(void *device);

/* 生成并发送代理子设备批量登录消息 */
int32_t core_gateway_subdev_connect(void *gateway_device, subdev_handle_t *subdev_table[], int32_t subdev_num);

/* 生成并发送代理子设备批量登出消息 */
int32_t core_gateway_subdev_disconnect(void *gateway_device, void *subdev_table[], int32_t subdev_num);

/* 生成并发送代理子设备添加topo关系消息 */
int32_t core_gateway_add_topo(void *gateway_device, aiot_subdev_meta_info_t subdev_table[], int32_t subdev_num);

/* 生成并发送代理子设备删除topo关系消息 */
int32_t core_gateway_delete_topo(void *gateway_device, aiot_subdev_meta_info_t subdev_table[], int32_t subdev_num);

/* 生成并发送代理子设备动态注册消息 */
int32_t core_gateway_query_secret(void *gateway_device, aiot_subdev_meta_info_t subdev_table[], int32_t subdev_num);

/* 生成并发送代理子设备动态注册(抢注)消息 */
int32_t core_gateway_product_register(void *gateway_device, aiot_subdev_meta_info_t subdev_table[], int32_t subdev_num);

/* 读取发送控制指令后的返回消息 */
aiot_gateway_msg_t *core_pend_msg_from_list(gateway_handle_t *gateway_handle, int32_t id);

/* 网关设备/子设备发送消息，加入到批量发布队列中 */
int32_t core_gateway_send_message(void *gateway_device, const aiot_msg_t *msg);

/* 手动触发消息批量发布 */
int32_t core_gateway_flush_message(void *gateway_device);

#if defined(__cplusplus)
}
#endif

#endif