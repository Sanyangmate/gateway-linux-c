#ifndef _GATEWAY_ADAPTER_H_
#define _GATEWAY_ADAPTER_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>
#include "device_private.h"

const device_operation_t* gateway_adapter_get();

#if defined(__cplusplus)
}
#endif

#endif