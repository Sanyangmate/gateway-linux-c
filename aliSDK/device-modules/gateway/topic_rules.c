#include "topic_rules.h"
#include "string.h"


topic_rules_info_t sys_topic_rules[] = {
    {
        .prefix = "/sys",
        .pk_index = 1,
        .dn_index = 2,
    },
    {
        .prefix = "/ota",
        .pk_index = 3,
        .dn_index = 4,
    },
    {
        .prefix = "/shadow",
        .pk_index = 2,
        .dn_index = 3,
    },
    {
        .prefix = "/ext",
        .pk_index = 2,
        .dn_index = 3,
    },
    {
        .prefix = "/lora",
        .pk_index = 3,
        .dn_index = 4,
    },
    {
        .prefix = "/",
        .pk_index = 0,
        .dn_index = 1,
    },
};

int32_t topic_rules_get_device(const char *topic, char product_key[64], char device_name[64])
{
    topic_rules_info_t *result = NULL;
    uint16_t seg_index[TOPIC_SEGMENT_MAX_LEN + 1];
    uint8_t seg_num = 0;
    int32_t topic_len = 0;
    int i = 0;
    if(topic == NULL || product_key == NULL || device_name == NULL) {
        return -1;
    }

    /* 查找头部匹配的topic */
    for(i = 0; i < sizeof(sys_topic_rules) / sizeof(topic_rules_info_t); i++)
    {
        if(0 == strncmp(topic, sys_topic_rules[i].prefix, strlen(sys_topic_rules[i].prefix))) {
            result = &sys_topic_rules[i];
            break;
        }
    }

    /* 没有找到为未知topic，查找失败 */
    if(result == NULL) {
        return -1;
    }


    /* 解析topic一共有多少字段 */
    memset(seg_index, 0, sizeof(seg_index));
    topic_len = strlen(topic);
    for(i = 0; i < topic_len; i++) {
        if('/' == topic[i]) {
            seg_index[seg_num] = i;
            seg_num++;
            if(seg_num >= TOPIC_SEGMENT_MAX_LEN) {
                break;
            }
        }
    }
    seg_index[seg_num] = topic_len;

    /* 当topic的字段小于需要字段时，退出 */
    if(seg_num <= result->pk_index || seg_num <= result->dn_index) {
        return -1;
    }

    /* 当解析出来的product_key长度过长 */
    if(seg_index[result->pk_index + 1] - seg_index[result->pk_index] > 64) {
        return -1;
    }

    /* 当解析出来的device_name长度过长 */
    if(seg_index[result->dn_index + 1] - seg_index[result->dn_index] > 64) {
        return -1;
    }

    /* product_key 赋值 */
    memset(product_key, 0, 64);
    memcpy(product_key, topic + seg_index[result->pk_index] + 1, seg_index[result->pk_index + 1] - seg_index[result->pk_index] - 1);

    /* device_name 赋值 */
    memset(device_name, 0, 64);
    memcpy(device_name, topic + seg_index[result->dn_index] + 1, seg_index[result->dn_index + 1] - seg_index[result->dn_index] - 1);

    return 0;
}