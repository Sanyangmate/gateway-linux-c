#include "device_private.h"
#include "aiot_state_api.h"
#include "topic_rules.h"
#include "subdev_manager.h"
#include "gateway_private.h"
#include "core_os.h"

/* 网关消息处理方法 */
static void gateway_message_handle(void *device, const aiot_msg_t *message)
{
    int32_t res = 0;
    char product_key[64], device_name[64];
    subdev_handle_t *subdev_handle = NULL;
    gateway_handle_t *gateway_handle = NULL;
    if(device == NULL || message == NULL) {
        return;
    }

    /* 解析出PK、DN并且能找到代理在线的设备为子设备消息，否则为网关消息 */
    res = topic_rules_get_device(message->topic, product_key, device_name);
    if(res == 0) {
        /* 解析是否为子设备的消息 */
        gateway_handle = core_gateway_get_handle(device);
        if(gateway_handle != NULL) {
            subdev_handle = core_subdev_search_connnected(gateway_handle, product_key, device_name);
            if(subdev_handle != NULL) {
                gateway_handle->subdev_adapter->message_handle(subdev_handle->device, message);
                return;
            }
        }
    }

    /* 不属于子设备的消息，调用网关的方法 */
    normal_ops.message_handle(device, message);
}

static void gateway_delete_all_subdev(gateway_handle_t *gateway_handle, const aiot_device_status_t *event)
{
    void *device = NULL;
    subdev_handle_t *subdev_handle = NULL;
    /* 处理连接状态的子设备 */
    while((subdev_handle = core_subdev_front_connnected(gateway_handle)) != NULL) {
        /* 在线子设备直接告知已下线，将子设备添加进离线设备表，等待重连 */
        device = subdev_handle->device;
        core_subdev_delete_connnected(gateway_handle, subdev_handle);
        core_subdev_deinit(gateway_handle, subdev_handle);

        /* 告诉子设备已经离线 */
        gateway_handle->subdev_adapter->status_handle(device, event);
    }

    /* 处理原本就是离线状态的子设备*/
    while((subdev_handle = core_subdev_front_disconnected(gateway_handle)) != NULL) {
        core_subdev_delete_disconnected(gateway_handle, subdev_handle);
        core_subdev_deinit(gateway_handle, subdev_handle);
    }

    /* 处理正在建连的子设备 */
    while((subdev_handle = core_subdev_front_connnecting(gateway_handle)) != NULL) {
        core_subdev_delete_connnecting(gateway_handle, subdev_handle);
        core_subdev_deinit(gateway_handle, subdev_handle);
    }
}

static void gateway_status_handle(void *device, const aiot_device_status_t *event)
{
    gateway_handle_t *gateway_handle = NULL;
    subdev_handle_t *subdev_handle = NULL;
    if(device == NULL || event == NULL) {
        return;
    }

    gateway_handle = core_gateway_get_handle(device);
    if(gateway_handle == NULL) {
        return;
    }

    /* 在线变离线，将所有子设备都下线 */
    if(event->type == AIOT_DEVICE_STATUS_DISCONNECT) {
        if(gateway_handle->config.auto_reconnect) {
            /* 子设备由网关托管重连，先将子设备状态置为离线*/
            while((subdev_handle = core_subdev_front_connnected(gateway_handle)) != NULL) {
                /* 在线子设备直接告知已下线，将子设备添加进离线设备表，等待重连 */
                core_subdev_move_to_disconnected(gateway_handle, subdev_handle);
                gateway_handle->subdev_adapter->status_handle(subdev_handle->device, event);
            }

            while((subdev_handle = core_subdev_front_connnecting(gateway_handle)) != NULL) {
                if(subdev_handle->status == SUBDEV_STATUS_CONNECTING) {
                    /* 第一次建连直接删除，建连触发者会发现建连超时 */
                    core_subdev_delete_connnecting(gateway_handle, subdev_handle);
                    core_subdev_deinit(gateway_handle, subdev_handle);
                } else if(subdev_handle->status == SUBDEV_STATUS_RECONNECTING) {
                    /* 正在重连的设备，再次添加进离线表，等待重连 */
                    core_subdev_move_to_disconnected(gateway_handle, subdev_handle);
                }
            }
        } else {
            /* 将所有子设备标记为离线，并删除子设备 */
            gateway_delete_all_subdev(gateway_handle, event);
        }
    }

    /* 网关设备下线 */
    normal_ops.status_handle(device, event);
}

static void gateway_result_handle(void *device, const aiot_device_msg_result_t *result)
{
    pend_ack_node_t *node = NULL, *next = NULL, *target = NULL;
    gateway_handle_t *gateway_handle = NULL;
    subdev_handle_t *subdev_handle = NULL;
    uint32_t packet_id = 0;
    void *target_device = device;
    if(device == NULL || result == NULL) {
        return;
    }
    gateway_handle = core_gateway_get_handle(device);
    if(gateway_handle == NULL) {
        return;
    }

    packet_id = result->message_id;

    /* 根据packet_id查询设备的PKDN*/
    core_os_mutex_lock(gateway_handle->pend_ack_mutex);
    core_list_for_each_entry_safe(node, next, &gateway_handle->pend_ack_list, linked_node, pend_ack_node_t) {
        if(node->packet_id == packet_id) {
            core_list_del(&node->linked_node);
            target = node;
            break;
        }
    }
    core_os_mutex_unlock(gateway_handle->pend_ack_mutex);

    /* 根据PK、DN查询该消息是否属于子设备 */
    if(target != NULL) {
        subdev_handle = (subdev_handle_t *)core_subdev_search_connnected(gateway_handle, target->product_key, target->device_name);
        if(subdev_handle != NULL) {
            target_device = subdev_handle->device;
        }
    }

    /* 执行消息回调 */
    normal_ops.result_handle(target_device, result);

    /* 资源回收 */
    if(target!= NULL) {
        core_os_free(target->product_key);
        core_os_free(target->device_name);
        core_os_free(target);
    }
}


static device_operation_t gateway_adapter = {
    .message_handle = gateway_message_handle,
    .status_handle = gateway_status_handle,
    .result_handle = gateway_result_handle,
};

const device_operation_t* gateway_adapter_get()
{
    /* 连接等动作保留有直连设备的方法 */
    gateway_adapter.connect = normal_ops.connect;
    gateway_adapter.connect_async = normal_ops.connect_async;
    gateway_adapter.disconnect = normal_ops.disconnect;
    gateway_adapter.sub_topic = normal_ops.sub_topic;
    gateway_adapter.unsub_topic = normal_ops.unsub_topic;
    gateway_adapter.send_message = normal_ops.send_message;

    return &gateway_adapter;
}