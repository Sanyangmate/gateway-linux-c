#ifndef _TOPIC_RULES_H_
#define _TOPIC_RULES_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>

#define  TOPIC_SEGMENT_MAX_LEN     20

/* topic解析出设备信息的结构体 */
typedef struct {
    /* topic前缀字段 */
    char *prefix;
    /* product_key所在字段，从0开始计数 */
    int32_t pk_index;
    /* device_name所在字段，从0开始计数*/
    int32_t dn_index;
} topic_rules_info_t;


int32_t topic_rules_get_device(const char *topic, char product_key[64], char device_name[64]);


#if defined(__cplusplus)
}
#endif

#endif