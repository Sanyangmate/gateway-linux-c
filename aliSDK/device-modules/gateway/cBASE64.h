

#ifndef _BASE64_H_
#define _BASE64_H_

#include <stdint.h>

char* base64encode(const uint8_t *data, uint32_t inputLength, void* (*on_malloc)(uint32_t size));

#endif
