#include "gateway_message.h"
#include "aiot_state_api.h"
#include "device_private.h"
#include "aiot_message_api.h"
#include "device_private.h"
#include "core_string.h"
#include "core_os.h"
#include "core_auth.h"
#include "gateway_private.h"
#include "core_log.h"
#include "subdev_manager.h"

#define TAG "GATEWAY"

/* 设置网关消息回调 */
int32_t aiot_gateway_set_msg_callback(void *gateway_device, gateway_msg_callback_t msg_callback, void *userdata)
{
    gateway_handle_t *gateway_handle = NULL;
    if(gateway_device == NULL || (msg_callback == NULL)) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_gateway_set_msg_callback input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }
    gateway_handle = core_gateway_get_handle(gateway_device);
    if(gateway_handle == NULL) {
        ALOG_ERROR(TAG, STATE_DEVICE_UNKOWN_ERROR, "gateway handle is null\r\n");
        return STATE_DEVICE_UNKOWN_ERROR;
    }

    gateway_handle->config.callback = msg_callback;
    gateway_handle->config.userdata = userdata;
    return STATE_SUCCESS;
}

/* 设置登录时自动绑定topo关系, 1: 自动添加topo  0.不自动添加 */
int32_t aiot_gateway_set_auto_topo(void *gateway_device, int32_t auto_topo)
{
    gateway_handle_t *gateway_handle = NULL;
    if(gateway_device == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_gateway_set_auto_topo input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    if(auto_topo != 0 && auto_topo != 1) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_OUT_RANGE, "auto_topo must be 0 or 1\r\n");
        return STATE_USER_INPUT_OUT_RANGE;
    }

    gateway_handle = core_gateway_get_handle(gateway_device);
    if(gateway_handle == NULL) {
        ALOG_ERROR(TAG, STATE_DEVICE_UNKOWN_ERROR, "gateway handle is null\r\n");
        return STATE_DEVICE_UNKOWN_ERROR;
    }

    ALOG_INFO(TAG, "set auto topo %d \r\n", auto_topo);
    gateway_handle->config.auto_topo = auto_topo;
    return STATE_SUCCESS;
}

/* 设置网关是否代理子设备自动重连 */
int32_t aiot_gateway_set_auto_reconnect(void *gateway_device, int32_t auto_reconnect)
{
    gateway_handle_t *gateway_handle = NULL;
    if(gateway_device == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_gateway_set_auto_reconnect input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    if(auto_reconnect != 0 && auto_reconnect != 1) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_OUT_RANGE, "auto_reconnect must be 0 or 1\r\n");
        return STATE_USER_INPUT_OUT_RANGE;
    }

    gateway_handle = core_gateway_get_handle(gateway_device);
    if(gateway_handle == NULL) {
        ALOG_ERROR(TAG, STATE_DEVICE_UNKOWN_ERROR, "gateway handle is null\r\n");
        return STATE_DEVICE_UNKOWN_ERROR;
    }

    ALOG_INFO(TAG, "set gateway auto_reconnect %d \r\n", auto_reconnect);
    gateway_handle->config.auto_reconnect = auto_reconnect;
    return STATE_SUCCESS;
}

/* 设置批量操作消息的超时时间，单位ms */
int32_t aiot_gateway_set_sync_msg_timeout(void *gateway_device, uint32_t timeout_ms)
{
    gateway_handle_t *gateway_handle = NULL;
    if(gateway_device == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_gateway_set_sync_msg_timeout input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }
    gateway_handle = core_gateway_get_handle(gateway_device);
    if(gateway_handle == NULL) {
        ALOG_ERROR(TAG, STATE_DEVICE_UNKOWN_ERROR, "gateway handle is null\r\n");
        return STATE_DEVICE_UNKOWN_ERROR;
    }

    ALOG_INFO(TAG, "set sync msg timeout internal %d \r\n", timeout_ms);
    gateway_handle->config.msg_timeout = timeout_ms;
    return STATE_SUCCESS;
}

/* 网关批量代理子设备建连 */
int32_t aiot_gateway_batch_connect_subdev(void *gateway_device, void* subdev_table[], int32_t subdev_num)
{
    int32_t res = STATE_SUCCESS;
    int32_t i = 0;
    gateway_handle_t *gateway_handle = NULL;
    subdev_handle_t *subdev_handle = NULL;
    subdev_handle_t **sub_handle_table = NULL;
    aiot_gateway_msg_t *recv = NULL;
    char *product_key = NULL, *device_name = NULL;
    if(gateway_device == NULL || subdev_table == NULL || subdev_num <= 0) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_gateway_batch_connect_subdev input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    gateway_handle = core_gateway_get_handle(gateway_device);
    if(gateway_handle == NULL) {
        ALOG_ERROR(TAG, STATE_DEVICE_UNKOWN_ERROR, "gateway handle is null\r\n");
        return STATE_DEVICE_UNKOWN_ERROR;
    }

    ALOG_INFO(TAG, "aiot_gateway_batch_connect_subdev ... list:\r\n");
    /* 设备对象非空判断 */
    for( i = 0; i < subdev_num; i++ ) {
        if(subdev_table[i] == NULL
                || aiot_device_product_key(subdev_table[i]) == NULL
                || aiot_device_name(subdev_table[i]) == NULL) {
            ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_gateway_batch_connect_subdev input null\r\n");
            return STATE_USER_INPUT_NULL_POINTER;
        }

        product_key = aiot_device_product_key(subdev_table[i]);
        device_name = aiot_device_name(subdev_table[i]);

        ALOG_INFO(TAG, "subdev [%s][%s]\r\n", product_key, device_name);
        subdev_handle = core_subdev_search_all(gateway_handle, product_key, device_name);
        if(subdev_handle != NULL) {
            ALOG_ERROR(TAG, STATE_GATEWAY_SUBDEV_REPEATED_CONNECT, "sub device can't repeated connect\r\n");
            return STATE_GATEWAY_SUBDEV_REPEATED_CONNECT;
        }
    }


    /* 创建子设备管理模块 */
    sub_handle_table = (subdev_handle_t **)core_os_malloc(sizeof(subdev_handle_t *) * subdev_num);
    for( i = 0; i < subdev_num; i++ ) {
        sub_handle_table[i] = core_subdev_init(gateway_handle, subdev_table[i]);
        if(sub_handle_table[i] == NULL) {
            continue;
        }
        /* 更新建连时间，用于计算超时 */
        sub_handle_table[i]->connect_stamp = core_os_time();

        /* 将子设备handle添加进建连中的表中 */
        res = core_subdev_move_to_connnecting(gateway_handle, sub_handle_table[i]);
        if(res < 0) {
            core_subdev_delete_all(gateway_handle, sub_handle_table[i]);
            core_subdev_deinit(gateway_handle, sub_handle_table[i]);
            continue;
        }
    }

    /* 发送批量建连消息 */
    res = core_gateway_subdev_connect(gateway_device, sub_handle_table, subdev_num);
    if(res < STATE_SUCCESS) {
        core_os_free(sub_handle_table);
        return res;
    }
    core_os_free(sub_handle_table);

    /* 接收建连结果 */
    recv = core_pend_msg_from_list(gateway_handle, res);
    if(recv == NULL) {
        ALOG_ERROR(TAG, STATE_GATEWAY_SYNC_RECV_TIMEOUT, "sub device batch login timeout\r\n");
        return STATE_GATEWAY_SYNC_RECV_TIMEOUT;
    } else if(recv->data.reply.code == 200) {
        res = STATE_SUCCESS;
    } else {
        ALOG_ERROR(TAG, STATE_GATEWAY_SYNC_RECV_ERROR, "sub device batch login recv error\r\n");
        res = STATE_GATEWAY_SYNC_RECV_ERROR;
    }

    gateway_msg_delete(recv);
    return res;
}

/* 网关批量增加子设备topo */
int32_t aiot_gateway_batch_add_topo(void *gateway_device, aiot_subdev_meta_info_t subdev_table[], int32_t subdev_num)
{
    gateway_handle_t *gateway_handle = NULL;
    int32_t res = STATE_SUCCESS;
    aiot_gateway_msg_t *recv = NULL;
    int32_t i = 0;
    if(gateway_device == NULL || subdev_table == NULL || subdev_num <= 0) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_gateway_batch_add_topo input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    gateway_handle = core_gateway_get_handle(gateway_device);
    if(gateway_handle == NULL) {
        ALOG_ERROR(TAG, STATE_DEVICE_UNKOWN_ERROR, "gateway handle is null\r\n");
        return STATE_DEVICE_UNKOWN_ERROR;
    }

    ALOG_INFO(TAG, "aiot_gateway_batch_add_topo ... list:\r\n");
    for( i = 0; i < subdev_num; i++ ) {
        ALOG_INFO(TAG, " subdev >:[%s][%s]\r\n", subdev_table[i].product_key, subdev_table[i].device_name);
    }

    /* 发送添加topo消息 */
    res = core_gateway_add_topo(gateway_device, subdev_table, subdev_num);
    if(res < STATE_SUCCESS) {
        return res;
    }

    /* 等待添加topo结果回复 */
    recv = core_pend_msg_from_list(gateway_handle, res);
    if(recv == NULL) {
        ALOG_ERROR(TAG, STATE_GATEWAY_SYNC_RECV_TIMEOUT, "gateway add topo timeout\r\n");
        return STATE_GATEWAY_SYNC_RECV_TIMEOUT;
    } else if(recv->data.reply.code == 200) {
        res = STATE_SUCCESS;
    } else {
        ALOG_ERROR(TAG, STATE_GATEWAY_SYNC_RECV_ERROR, "gateway add topo  recv error\r\n");
        res = STATE_GATEWAY_SYNC_RECV_ERROR;
    }

    gateway_msg_delete(recv);
    return res;
}

/* 网关批量删除子设备topo */
int32_t aiot_gateway_batch_delete_topo(void *gateway_device, aiot_subdev_meta_info_t subdev_table[], int32_t subdev_num)
{
    gateway_handle_t *gateway_handle = NULL;
    int32_t res = STATE_SUCCESS;
    aiot_gateway_msg_t *recv = NULL;
    int32_t i = 0;
    if(gateway_device == NULL || subdev_table == NULL || subdev_num <= 0) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_gateway_batch_delete_topo input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    gateway_handle = core_gateway_get_handle(gateway_device);
    if(gateway_handle == NULL) {
        ALOG_ERROR(TAG, STATE_DEVICE_UNKOWN_ERROR, "gateway handle is null\r\n");
        return STATE_DEVICE_UNKOWN_ERROR;
    }

    ALOG_INFO(TAG, "aiot_gateway_batch_delete_topo ... list:\r\n");
    for( i = 0; i < subdev_num; i++ ) {
        ALOG_INFO(TAG, " subdev >:[%s][%s]\r\n", subdev_table[i].product_key, subdev_table[i].device_name);
    }

    /* 发送删除topo消息 */
    res = core_gateway_delete_topo(gateway_device, subdev_table, subdev_num);
    if(res < STATE_SUCCESS) {
        return res;
    }

    /* 等待删除topo结果回复 */
    recv = core_pend_msg_from_list(gateway_handle, res);
    if(recv == NULL) {
        ALOG_ERROR(TAG, STATE_GATEWAY_SYNC_RECV_TIMEOUT, "gateway del topo timeout\r\n");
        return STATE_GATEWAY_SYNC_RECV_TIMEOUT;
    } else if(recv->data.reply.code == 200) {
        res = STATE_SUCCESS;
    } else {
        ALOG_ERROR(TAG, STATE_GATEWAY_SYNC_RECV_ERROR, "gateway del topo  recv error\r\n");
        res = STATE_GATEWAY_SYNC_RECV_ERROR;
    }

    gateway_msg_delete(recv);
    return res;
}

/* 子设备动态获取密钥 */
int32_t aiot_gateway_dynamic_secret(void *gateway_device, aiot_subdev_meta_info_t subdev_table[], int32_t subdev_num)
{
    gateway_handle_t *gateway_handle = NULL;
    int32_t res = STATE_SUCCESS;
    aiot_gateway_msg_t *recv = NULL;
    int dev_idx = 0, msg_idx = 0;
    int8_t find = 0;
    int32_t i = 0;
    if(gateway_device == NULL || subdev_table == NULL || subdev_num <= 0) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_gateway_dynamic_secret input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    gateway_handle = core_gateway_get_handle(gateway_device);
    if(gateway_handle == NULL) {
        ALOG_ERROR(TAG, STATE_DEVICE_UNKOWN_ERROR, "gateway handle is null\r\n");
        return STATE_DEVICE_UNKOWN_ERROR;
    }

    ALOG_INFO(TAG, "aiot_gateway_dynamic_secret ... list:\r\n");
    for( i = 0; i < subdev_num; i++ ) {
        ALOG_INFO(TAG, " subdev >:[%s][%s]\r\n", subdev_table[i].product_key, subdev_table[i].device_name);
    }

    /* 发送密钥请求 */
    res = core_gateway_query_secret(gateway_device, subdev_table, subdev_num);
    if(res < STATE_SUCCESS) {
        return res;
    }

    /* 等待结果返回 */
    recv = core_pend_msg_from_list(gateway_handle, res);
    if(recv == NULL) {
        ALOG_ERROR(TAG, STATE_GATEWAY_SYNC_RECV_TIMEOUT, "sub device dynamic get secret timeout\r\n");
        return STATE_GATEWAY_SYNC_RECV_TIMEOUT;
    } else if(recv->data.reply.code == 200 && subdev_num == recv->subdev_num) {
        res = STATE_SUCCESS;
        /* 给每个设备进行设备密钥赋值 */
        for(dev_idx = 0; dev_idx < subdev_num; dev_idx++) {
            find = 0;
            for(msg_idx = 0; msg_idx < recv->subdev_num; msg_idx++) {
                if(0 == strcmp(recv->subdev_table[msg_idx].product_key, subdev_table[dev_idx].product_key)
                        && 0 == strcmp(recv->subdev_table[msg_idx].device_name, subdev_table[dev_idx].device_name)) {
                    strncpy(subdev_table[dev_idx].device_secret, recv->subdev_table[msg_idx].device_secret, sizeof(subdev_table[dev_idx].device_secret) - 1);
                    find = 1;
                }
            }
            /* 只要有一个设备没有正确获取到密钥，认为失败 */
            if(find == 0) {
                ALOG_ERROR(TAG, STATE_GATEWAY_SYNC_RECV_ERROR, "sub device dynamic get secret recv error\r\n");
                res = STATE_GATEWAY_SYNC_RECV_ERROR;
                break;
            }
        }
    } else {
        ALOG_ERROR(TAG, STATE_GATEWAY_SYNC_RECV_ERROR, "sub device dynamic get secret recv error\r\n");
        res = STATE_GATEWAY_SYNC_RECV_ERROR;
    }

    gateway_msg_delete(recv);
    return res;
}

/* 动态获取子设备密钥，具有抢注的能力 */
int32_t aiot_gateway_dynamic_register(void *gateway_device, aiot_subdev_meta_info_t subdev_table[], int32_t subdev_num)
{
    gateway_handle_t *gateway_handle = NULL;
    int32_t res = STATE_SUCCESS;
    aiot_gateway_msg_t *recv = NULL;
    int dev_idx = 0, msg_idx = 0;
    int8_t find = 0;
    int32_t i = 0;
    if(gateway_device == NULL || subdev_table == NULL || subdev_num <= 0) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_gateway_dynamic_register input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    gateway_handle = core_gateway_get_handle(gateway_device);
    if(gateway_handle == NULL) {
        ALOG_ERROR(TAG, STATE_DEVICE_UNKOWN_ERROR, "gateway handle is null\r\n");
        return STATE_DEVICE_UNKOWN_ERROR;
    }

    ALOG_INFO(TAG, "aiot_gateway_dynamic_register ... list:\r\n");
    for( i = 0; i < subdev_num; i++ ) {
        ALOG_INFO(TAG, " subdev >:[%s][%s]\r\n", subdev_table[i].product_key, subdev_table[i].device_name);
    }

    /* 发送请求消息 */
    res = core_gateway_product_register(gateway_device, subdev_table, subdev_num);
    if(res < STATE_SUCCESS) {
        return res;
    }

    /* 等待回复结果 */
    recv = core_pend_msg_from_list(gateway_handle, res);
    if(recv == NULL) {
        ALOG_ERROR(TAG, STATE_GATEWAY_SYNC_RECV_TIMEOUT, "sub device dynamic register timeout\r\n");
        return STATE_GATEWAY_SYNC_RECV_TIMEOUT;
    } else if(recv->data.reply.code == 200 && subdev_num == recv->subdev_num) {
        res = STATE_SUCCESS;
        /* 给每个设备进行设备密钥赋值 */
        for(dev_idx = 0; dev_idx < subdev_num; dev_idx++) {
            find = 0;
            for(msg_idx = 0; msg_idx < recv->subdev_num; msg_idx++) {
                if(0 == strcmp(recv->subdev_table[msg_idx].product_key, subdev_table[dev_idx].product_key)
                        && 0 == strcmp(recv->subdev_table[msg_idx].device_name, subdev_table[dev_idx].device_name)) {
                    strncpy(subdev_table[dev_idx].device_secret, recv->subdev_table[msg_idx].device_secret, sizeof(subdev_table[dev_idx].device_secret) - 1);
                    find = 1;
                }
            }
            /* 只要有一个设备没有正确获取到密钥，认为失败 */
            if(find == 0) {
                ALOG_ERROR(TAG, STATE_GATEWAY_SYNC_RECV_ERROR, "sub device dynamic register recv error\r\n");
                res = STATE_GATEWAY_SYNC_RECV_ERROR;
                break;
            }
        }
    } else {
        ALOG_ERROR(TAG, STATE_GATEWAY_SYNC_RECV_ERROR, "sub device dynamic register recv error\r\n");
        res = STATE_GATEWAY_SYNC_RECV_ERROR;
    }

    gateway_msg_delete(recv);
    return res;
}

/* 网关批量代理子设备建连，异步接口，不会阻塞 */
int32_t aiot_gateway_batch_connect_subdev_async(void *gateway_device, void* subdev_table[], int32_t subdev_num)
{
    int32_t i = 0, res = STATE_SUCCESS;
    gateway_handle_t *gateway_handle = NULL;
    subdev_handle_t *subdev_handle = NULL;
    subdev_handle_t **sub_handle_table = NULL;
    char *product_key = NULL, *device_name = NULL;
    if(gateway_device == NULL || subdev_table == NULL || subdev_num <= 0) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_gateway_batch_connect_subdev_async input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    gateway_handle = core_gateway_get_handle(gateway_device);
    if(gateway_handle == NULL) {
        ALOG_ERROR(TAG, STATE_DEVICE_UNKOWN_ERROR, "gateway handle is null\r\n");
        return STATE_DEVICE_UNKOWN_ERROR;
    }

    ALOG_INFO(TAG, "aiot_gateway_batch_connect_subdev_async ... list:\r\n");
    /* 设备对象非空判断 */
    for( i = 0; i < subdev_num; i++ ) {
        if(subdev_table[i] == NULL
                || aiot_device_product_key(subdev_table[i]) == NULL
                || aiot_device_name(subdev_table[i]) == NULL) {
            ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_gateway_batch_connect_subdev input null\r\n");
            return STATE_USER_INPUT_NULL_POINTER;
        }

        product_key = aiot_device_product_key(subdev_table[i]);
        device_name = aiot_device_name(subdev_table[i]);

        ALOG_INFO(TAG, "subdev [%s][%s]\r\n", product_key, device_name);
        subdev_handle = core_subdev_search_all(gateway_handle, product_key, device_name);
        if(subdev_handle != NULL) {
            /* 网关已经代理该设备建连，重复建连报错 */
            ALOG_ERROR(TAG, STATE_GATEWAY_SUBDEV_REPEATED_CONNECT, "sub device can't repeated connect\r\n");
            return STATE_GATEWAY_SUBDEV_REPEATED_CONNECT;
        }
    }

    /* 创建子设备管理模块 */
    sub_handle_table = (subdev_handle_t **)core_os_malloc(sizeof(subdev_handle_t *) * subdev_num);
    for( i = 0; i < subdev_num; i++ ) {
        sub_handle_table[i] = core_subdev_init(gateway_handle, subdev_table[i]);
        if(sub_handle_table[i] == NULL) {
            continue;
        }
        /* 更新建连时间，用于计算超时 */
        sub_handle_table[i]->connect_stamp = core_os_time();

        /* 将子设备handle添加进建连中的表中 */
        res = core_subdev_move_to_connnecting(gateway_handle, sub_handle_table[i]);
        if(res < 0) {
            core_subdev_delete_all(gateway_handle, sub_handle_table[i]);
            core_subdev_deinit(gateway_handle, sub_handle_table[i]);
            continue;
        }
    }

    /* 发送批量建连消息 */
    res = core_gateway_subdev_connect(gateway_device, sub_handle_table, subdev_num);
    if(res < STATE_SUCCESS) {
        core_os_free(sub_handle_table);
        return res;
    }
    core_os_free(sub_handle_table);

    return res;
}

/* 网关批量代理子设备断连，异步接口，不会阻塞  */
int32_t aiot_gateway_batch_disconnect_subdev(void *gateway_device, void* subdev_table[], int32_t subdev_num)
{
    int32_t res = STATE_SUCCESS;
    int32_t i = 0;
    char *product_key = NULL, *device_name = NULL;
    gateway_handle_t *gateway_handle = NULL;
    subdev_handle_t *subdev_handle = NULL;
    aiot_device_status_t status;
    subdev_status_t pre_status;
    if(gateway_device == NULL || subdev_table == NULL || subdev_num <= 0) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_gateway_batch_disconnect_subdev input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    gateway_handle = core_gateway_get_handle(gateway_device);
    if(gateway_handle == NULL) {
        ALOG_ERROR(TAG, STATE_DEVICE_UNKOWN_ERROR, "gateway handle is null\r\n");
        return STATE_DEVICE_UNKOWN_ERROR;
    }

    ALOG_INFO(TAG, "aiot_gateway_batch_disconnect_subdev ... list:\r\n");
    /* 设备对象非空判断 */
    for( i = 0; i < subdev_num; i++ ) {
        if(subdev_table[i] == NULL) {
            ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_gateway_batch_disconnect_subdev input null\r\n");
            return STATE_USER_INPUT_NULL_POINTER;
        } else {
            ALOG_INFO(TAG, "disconnect [%s][%s]\r\n", aiot_device_product_key(subdev_table[i]), aiot_device_name(subdev_table[i]));
        }
    }
    res = core_gateway_subdev_disconnect(gateway_device, subdev_table, subdev_num);
    if(res > 0) {
        res = STATE_SUCCESS;
    }

    status.type = AIOT_DEVICE_STATUS_DISCONNECT;
    status.error_code = 0;
    /* 创建子设备管理模块 */
    for( i = 0; i < subdev_num; i++ ) {
        product_key = aiot_device_product_key(subdev_table[i]);
        device_name = aiot_device_name(subdev_table[i]);

        /* 把设备从子设备管理列表中删除 */
        subdev_handle = core_subdev_search_all(gateway_handle, product_key, device_name);
        if(subdev_handle != NULL) {
            pre_status = subdev_handle->status;
            core_subdev_delete_all(gateway_handle, subdev_handle);
            core_subdev_deinit(gateway_handle, subdev_handle);
            if(pre_status == SUBDEV_STATUS_CONNECTED || pre_status == SUBDEV_STATUS_RECONNECTED ) {
                gateway_handle->subdev_adapter->status_handle(subdev_table[i], &status);
            }
        }
    }

    return res;
}

/* 网关批量增加子设备topo，异步接口，不会阻塞  */
int32_t aiot_gateway_batch_add_topo_async(void *gateway_device, aiot_subdev_meta_info_t subdev_table[], int32_t subdev_num)
{
    gateway_handle_t *gateway_handle = NULL;
    int32_t i = 0;
    if(gateway_device == NULL || subdev_table == NULL || subdev_num <= 0) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_gateway_batch_add_topo_async input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    gateway_handle = core_gateway_get_handle(gateway_device);
    if(gateway_handle == NULL) {
        ALOG_ERROR(TAG, STATE_DEVICE_UNKOWN_ERROR, "gateway handle is null\r\n");
        return STATE_DEVICE_UNKOWN_ERROR;
    }

    ALOG_INFO(TAG, "aiot_gateway_batch_add_topo_async ... list:\r\n");
    for( i = 0; i < subdev_num; i++ ) {
        ALOG_INFO(TAG, " subdev >:[%s][%s]\r\n", subdev_table[i].product_key, subdev_table[i].device_name);
    }

    return core_gateway_add_topo(gateway_device, subdev_table, subdev_num);
}

/* 网关批量删除子设备topo，异步接口，不会阻塞  */
int32_t aiot_gateway_batch_delete_topo_async(void *gateway_device, aiot_subdev_meta_info_t subdev_table[], int32_t subdev_num)
{
    gateway_handle_t *gateway_handle = NULL;
    int32_t i = 0;
    if(gateway_device == NULL || subdev_table == NULL || subdev_num <= 0) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_gateway_batch_delete_topo_async input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    gateway_handle = core_gateway_get_handle(gateway_device);
    if(gateway_handle == NULL) {
        ALOG_ERROR(TAG, STATE_DEVICE_UNKOWN_ERROR, "gateway handle is null\r\n");
        return STATE_DEVICE_UNKOWN_ERROR;
    }

    ALOG_INFO(TAG, "aiot_gateway_batch_delete_topo_async ... list:\r\n");
    for( i = 0; i < subdev_num; i++ ) {
        ALOG_INFO(TAG, " subdev >:[%s][%s]\r\n", subdev_table[i].product_key, subdev_table[i].device_name);
    }

    return core_gateway_delete_topo(gateway_device, subdev_table, subdev_num);
}