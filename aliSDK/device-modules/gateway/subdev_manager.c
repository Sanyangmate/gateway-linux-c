#include <stdio.h>
#include "subdev_manager.h"
#include "subdev_container.h"
#include "core_os.h"
#include "device_private.h"

subdev_handle_t *core_subdev_handle_init(void *origin_device)
{
    subdev_handle_t *subdev_handle = NULL;
    if(origin_device == NULL) {
        return NULL;
    }

    subdev_handle = (subdev_handle_t *) core_os_malloc(sizeof(subdev_handle_t));
    if(subdev_handle == NULL) {
        return NULL;
    }

    memset(subdev_handle, 0, sizeof(subdev_handle_t));
    subdev_handle->device = origin_device;
    subdev_handle->status = SUBDEV_STATUS_INIT;

    return subdev_handle;
}

int32_t core_subdev_handle_deinit(subdev_handle_t *subdev_handle)
{
    if(subdev_handle == NULL) {
        return -1;
    }

    core_os_free(subdev_handle);

    return 0;
}

subdev_handle_t *core_subdev_init(gateway_handle_t *gateway_handle, void *sub_device)
{
    device_handle_t *subdev = (device_handle_t *)sub_device;
    subdev_handle_t *subdev_handle = NULL;
    subdev_handle = core_subdev_handle_init(sub_device);
    if(subdev_handle == NULL) {
        return NULL;
    }

    subdev->has_connected = 1;
    subdev->ops = gateway_handle->subdev_adapter;
    subdev->gateway_device = gateway_handle->device;

    return subdev_handle;
}

void core_subdev_deinit(gateway_handle_t *gateway_handle, subdev_handle_t *subdev_handle)
{
    device_handle_t *subdev = (device_handle_t *)subdev_handle->device;
    subdev->has_connected = 0;
    subdev->ops = &normal_ops;
    subdev->gateway_device = NULL;
    core_subdev_handle_deinit(subdev_handle);
}


int32_t core_subdev_manager_init(gateway_handle_t *gateway_handle)
{
    gateway_handle->online_subdev_mgr = subdev_container_init();
    gateway_handle->conn_subdev_mgr = subdev_container_init();
    gateway_handle->offline_subdev_mgr = subdev_container_init();

    return 0;
}
int32_t core_subdev_manager_deinit(gateway_handle_t *gateway_handle)
{
    subdev_handle_t *subdev_handle = NULL;
    char *product_key = NULL, *device_name = NULL;
    if(gateway_handle->online_subdev_mgr != NULL) {
        while((subdev_handle = subdev_container_first(gateway_handle->online_subdev_mgr)) != NULL) {
            product_key = aiot_device_product_key(subdev_handle->device);
            device_name = aiot_device_name(subdev_handle->device);
            subdev_container_delete(gateway_handle->online_subdev_mgr, product_key, device_name);
            core_subdev_deinit(gateway_handle, subdev_handle);
        }
        subdev_container_deinit(&gateway_handle->online_subdev_mgr);
    }

    if(gateway_handle->conn_subdev_mgr != NULL) {
        while((subdev_handle = subdev_container_first(gateway_handle->conn_subdev_mgr)) != NULL) {
            product_key = aiot_device_product_key(subdev_handle->device);
            device_name = aiot_device_name(subdev_handle->device);
            subdev_container_delete(gateway_handle->conn_subdev_mgr, product_key, device_name);
            core_subdev_deinit(gateway_handle, subdev_handle);
        }
        subdev_container_deinit(&gateway_handle->conn_subdev_mgr);
    }

    if(gateway_handle->offline_subdev_mgr != NULL) {
        while((subdev_handle = subdev_container_first(gateway_handle->offline_subdev_mgr)) != NULL) {
            product_key = aiot_device_product_key(subdev_handle->device);
            device_name = aiot_device_name(subdev_handle->device);
            subdev_container_delete(gateway_handle->offline_subdev_mgr, product_key, device_name);
            core_subdev_deinit(gateway_handle, subdev_handle);
        }
        subdev_container_deinit(&gateway_handle->offline_subdev_mgr);
    }

    return 0;
}

/* 从状态容器中搜索子设备 */
subdev_handle_t *core_subdev_search_connnecting(gateway_handle_t *gateway_handle, char *product_key, char *device_name)
{
    if(gateway_handle == NULL || product_key == NULL || device_name == NULL) {
        return NULL;
    }

    return (subdev_handle_t *)subdev_container_search(gateway_handle->conn_subdev_mgr, product_key, device_name);
}
subdev_handle_t *core_subdev_search_connnected(gateway_handle_t *gateway_handle, char *product_key, char *device_name)
{
    if(gateway_handle == NULL || product_key == NULL || device_name == NULL) {
        return NULL;
    }

    return (subdev_handle_t *)subdev_container_search(gateway_handle->online_subdev_mgr, product_key, device_name);
}
subdev_handle_t *core_subdev_search_disconnected(gateway_handle_t *gateway_handle, char *product_key, char *device_name)
{
    if(gateway_handle == NULL || product_key == NULL || device_name == NULL) {
        return NULL;
    }

    return (subdev_handle_t *)subdev_container_search(gateway_handle->offline_subdev_mgr, product_key, device_name);
}
subdev_handle_t *core_subdev_search_all(gateway_handle_t *gateway_handle, char *product_key, char *device_name)
{
    subdev_handle_t *subdev = NULL;
    if(gateway_handle == NULL || product_key == NULL || device_name == NULL) {
        return NULL;
    }

    subdev = (subdev_handle_t *)subdev_container_search(gateway_handle->conn_subdev_mgr, product_key, device_name);
    if(subdev != NULL) {
        return subdev;
    }

    subdev = (subdev_handle_t *)subdev_container_search(gateway_handle->online_subdev_mgr, product_key, device_name);
    if(subdev != NULL) {
        return subdev;
    }

    subdev = (subdev_handle_t *)subdev_container_search(gateway_handle->offline_subdev_mgr, product_key, device_name);

    return subdev;
}

/* 添加子设备进相应的状态容器，会根据状态从其它容器移除 */
int32_t core_subdev_move_to_connnecting(gateway_handle_t *gateway_handle, subdev_handle_t *subdev_handle)
{
    char *product_key = NULL;
    char *device_name = NULL;
    if(subdev_handle == NULL || subdev_handle == NULL) {
        return -1;
    }

    product_key = aiot_device_product_key(subdev_handle->device);
    device_name = aiot_device_name(subdev_handle->device);
    if(product_key == NULL || device_name == NULL) {
        return -1;
    }

    switch(subdev_handle->status) {
    case SUBDEV_STATUS_INIT:
        subdev_handle->status = SUBDEV_STATUS_CONNECTING;
        break;
    case SUBDEV_STATUS_DISCONNECTED:
        subdev_container_delete(gateway_handle->offline_subdev_mgr, product_key, device_name);
        subdev_handle->status = SUBDEV_STATUS_RECONNECTING;
        break;
    case SUBDEV_STATUS_CONNECTING:
    case SUBDEV_STATUS_RECONNECTING:
        subdev_container_delete(gateway_handle->conn_subdev_mgr, product_key, device_name);
        break;
    /* 非法状态切换 */
    default:
        return -1;
    }

    return subdev_container_insert(gateway_handle->conn_subdev_mgr, product_key, device_name, subdev_handle);
}
int32_t core_subdev_move_to_connnected(gateway_handle_t *gateway_handle, subdev_handle_t *subdev_handle)
{
    char *product_key = NULL;
    char *device_name = NULL;
    if(subdev_handle == NULL || subdev_handle == NULL) {
        return -1;
    }

    product_key = aiot_device_product_key(subdev_handle->device);
    device_name = aiot_device_name(subdev_handle->device);
    if(product_key == NULL || device_name == NULL) {
        return -1;
    }

    switch(subdev_handle->status) {
    case SUBDEV_STATUS_CONNECTED:
    case SUBDEV_STATUS_RECONNECTED:
        subdev_container_delete(gateway_handle->online_subdev_mgr, product_key, device_name);
        break;
    case SUBDEV_STATUS_DISCONNECTED:
        subdev_container_delete(gateway_handle->offline_subdev_mgr, product_key, device_name);
        subdev_handle->status = SUBDEV_STATUS_CONNECTED;
        break;
    case SUBDEV_STATUS_CONNECTING:
        subdev_container_delete(gateway_handle->conn_subdev_mgr, product_key, device_name);
        subdev_handle->status = SUBDEV_STATUS_CONNECTED;
        break;
    case SUBDEV_STATUS_RECONNECTING:
        subdev_container_delete(gateway_handle->conn_subdev_mgr, product_key, device_name);
        subdev_handle->status = SUBDEV_STATUS_RECONNECTED;
        break;
    /* 非法状态切换 */
    default:
        return -1;
    }

    return subdev_container_insert(gateway_handle->online_subdev_mgr, product_key, device_name, subdev_handle);
}
int32_t core_subdev_move_to_disconnected(gateway_handle_t *gateway_handle, subdev_handle_t *subdev_handle)
{
    char *product_key = NULL;
    char *device_name = NULL;
    if(subdev_handle == NULL || subdev_handle == NULL) {
        return -1;
    }

    product_key = aiot_device_product_key(subdev_handle->device);
    device_name = aiot_device_name(subdev_handle->device);
    if(product_key == NULL || device_name == NULL) {
        return -1;
    }

    switch(subdev_handle->status) {
    case SUBDEV_STATUS_CONNECTED:
        subdev_container_delete(gateway_handle->online_subdev_mgr, product_key, device_name);
        subdev_handle->status = SUBDEV_STATUS_DISCONNECTED;
        break;
    case SUBDEV_STATUS_RECONNECTING:
        subdev_container_delete(gateway_handle->conn_subdev_mgr, product_key, device_name);
        subdev_handle->status = SUBDEV_STATUS_DISCONNECTED;
        break;
    case SUBDEV_STATUS_RECONNECTED:
        subdev_container_delete(gateway_handle->online_subdev_mgr, product_key, device_name);
        subdev_handle->status = SUBDEV_STATUS_DISCONNECTED;
        break;
    /* 非法状态切换 */
    default:
        return -1;
    }

    return subdev_container_insert(gateway_handle->offline_subdev_mgr, product_key, device_name, subdev_handle);
}

/* 从状态容器中删除子设备 */
int32_t core_subdev_delete_connnecting(gateway_handle_t *gateway_handle, subdev_handle_t *subdev_handle)
{
    if(gateway_handle == NULL || subdev_handle == NULL) {
        return -1;
    }
    return subdev_container_delete_with_value(gateway_handle->conn_subdev_mgr, subdev_handle);
}
int32_t core_subdev_delete_connnected(gateway_handle_t *gateway_handle, subdev_handle_t *subdev_handle)
{
    if(gateway_handle == NULL || subdev_handle == NULL) {
        return -1;
    }
    return subdev_container_delete_with_value(gateway_handle->online_subdev_mgr, subdev_handle);
}
int32_t core_subdev_delete_disconnected(gateway_handle_t *gateway_handle, subdev_handle_t *subdev_handle)
{
    if(gateway_handle == NULL || subdev_handle == NULL) {
        return -1;
    }
    return subdev_container_delete_with_value(gateway_handle->offline_subdev_mgr, subdev_handle);
}
int32_t core_subdev_delete_all(gateway_handle_t *gateway_handle, subdev_handle_t *subdev_handle)
{
    if(gateway_handle == NULL || subdev_handle == NULL) {
        return -1;
    }

    subdev_container_delete_with_value(gateway_handle->conn_subdev_mgr, subdev_handle);
    subdev_container_delete_with_value(gateway_handle->offline_subdev_mgr, subdev_handle);

    return subdev_container_delete_with_value(gateway_handle->online_subdev_mgr, subdev_handle);
}

/* 从状态容器中获取表头设备 */
subdev_handle_t *core_subdev_front_connnecting(gateway_handle_t *gateway_handle)
{
    if(gateway_handle == NULL) {
        return NULL;
    }

    return subdev_container_first(gateway_handle->conn_subdev_mgr);
}
subdev_handle_t *core_subdev_front_connnected(gateway_handle_t *gateway_handle)
{
    if(gateway_handle == NULL) {
        return NULL;
    }

    return subdev_container_first(gateway_handle->online_subdev_mgr);
}
subdev_handle_t *core_subdev_front_disconnected(gateway_handle_t *gateway_handle)
{
    if(gateway_handle == NULL) {
        return NULL;
    }

    return subdev_container_first(gateway_handle->offline_subdev_mgr);
}

/* 查询状态容器子设备个数 */
int32_t core_subdev_size_connnecting(gateway_handle_t *gateway_handle)
{
    if(gateway_handle == NULL) {
        return -1;
    }

    return subdev_container_size(gateway_handle->conn_subdev_mgr);
}
int32_t core_subdev_size_connnected(gateway_handle_t *gateway_handle)
{
    if(gateway_handle == NULL) {
        return -1;
    }

    return subdev_container_size(gateway_handle->online_subdev_mgr);
}
int32_t core_subdev_size_disconnected(gateway_handle_t *gateway_handle)
{
    if(gateway_handle == NULL) {
        return -1;
    }

    return subdev_container_size(gateway_handle->offline_subdev_mgr);
}

int32_t core_subdev_size_all(gateway_handle_t *gateway_handle)
{
    int32_t size = 0;
    if(gateway_handle == NULL) {
        return -1;
    }

    size += subdev_container_size(gateway_handle->conn_subdev_mgr);
    size += subdev_container_size(gateway_handle->offline_subdev_mgr);
    size += subdev_container_size(gateway_handle->online_subdev_mgr);

    return size;
}