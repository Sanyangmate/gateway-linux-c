#ifndef _SUBDEV_CONTAINER_H_
#define _SUBDEV_CONTAINER_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>

/* 子设备管理模块初始化 */
void *subdev_container_init();
/* 插入子设备 */
int32_t subdev_container_insert(void *mgr, char *pk, char *dn, void *value);
/* 删除子设备 */
int32_t subdev_container_delete(void *mgr, char *pk, char *dn);
/* 删除子设备节点根据value */
int32_t subdev_container_delete_with_value(void *mgr, void *value);
/* 查找子设备 */
void *subdev_container_search(void *mgr, char *pk, char *dn);
/* 查找当前的元素个数 */
int32_t subdev_container_size(void *mgr);
/* 获取链表第一个元素 */
void *subdev_container_first(void *mgr);
/* 遍历子设备 */
/* 先注释掉未使用的轮询功能
int32_t subdev_container_poll_start(void *mgr);
void *subdev_container_poll_get(void *mgr);
int32_t subdev_container_poll_finish(void *mgr);
*/
/* 子设备管理模块删除 */
int32_t subdev_container_deinit(void **mgr);

#if defined(__cplusplus)
}
#endif

#endif