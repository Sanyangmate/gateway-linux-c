#include "subdev_container.h"
#include "core_os.h"
#include "core_string.h"
#include <stdio.h>
#include "core_list.h"

typedef struct {
    char *pk;
    char *dn;
    void *value;
    struct core_list_head linked_node;
} node_t;

typedef struct {
    struct core_list_head node_list;
    int32_t node_num;
    /* 用于查找下一个 */
    node_t *last_node;
    void *muxtex;
} manager_handle_t;

void *subdev_container_init()
{
    manager_handle_t *manager = core_os_malloc(sizeof(manager_handle_t));
    if(manager == NULL) {
        return manager;
    }

    CORE_INIT_LIST_HEAD(&manager->node_list);
    manager->muxtex = core_os_mutex_init();
    manager->node_num = 0;
    manager->last_node = NULL;

    return manager;
}
int32_t subdev_container_insert(void *mgr, char *pk, char *dn, void *value)
{
    manager_handle_t *manager = (manager_handle_t *)mgr;
    node_t *node = NULL, *next = NULL;
    if(manager == NULL || pk == NULL || dn == NULL) {
        return -1;
    }

    core_os_mutex_lock(manager->muxtex);
    core_list_for_each_entry_safe(node, next, &manager->node_list, linked_node, node_t) {
        if(0 == strcmp(node->pk, pk) && 0 == strcmp(node->dn, dn)) {
            node->value = value;
            core_os_mutex_unlock(manager->muxtex);
            return 0;
        }
    }

    node = core_os_malloc(sizeof(node_t));
    if(node == NULL) {
        core_os_mutex_unlock(manager->muxtex);
        return -1;
    }

    memset(node, 0, sizeof(node_t));
    core_strdup(NULL, &node->pk, pk, NULL);
    core_strdup(NULL, &node->dn, dn, NULL);
    node->value = value;
    core_list_add_tail(&node->linked_node, &manager->node_list);
    manager->node_num++;
    core_os_mutex_unlock(manager->muxtex);
    return 0;
}
int32_t subdev_container_delete(void *mgr, char *pk, char *dn)
{
    manager_handle_t *manager = (manager_handle_t *)mgr;
    node_t *node = NULL, *next = NULL;
    if(manager == NULL || pk == NULL || dn == NULL) {
        return -1;
    }

    core_os_mutex_lock(manager->muxtex);
    core_list_for_each_entry_safe(node, next, &manager->node_list, linked_node, node_t) {
        if(0 == strcmp(node->pk, pk) && 0 == strcmp(node->dn, dn)) {
            core_list_del(&node->linked_node);
            core_os_free(node->pk);
            core_os_free(node->dn);
            core_os_free(node);
            manager->node_num--;
            break;
        }
    }

    core_os_mutex_unlock(manager->muxtex);
    return 0;
}
int32_t subdev_container_delete_with_value(void *mgr, void *value)
{
    manager_handle_t *manager = (manager_handle_t *)mgr;
    node_t *node = NULL, *next = NULL;
    if(manager == NULL || value == NULL) {
        return -1;
    }

    core_os_mutex_lock(manager->muxtex);
    core_list_for_each_entry_safe(node, next, &manager->node_list, linked_node, node_t) {
        if(node->value == value) {
            core_list_del(&node->linked_node);
            core_os_free(node->pk);
            core_os_free(node->dn);
            core_os_free(node);
            manager->node_num--;
            break;
        }
    }

    core_os_mutex_unlock(manager->muxtex);
    return 0;
}

int32_t subdev_container_size(void *mgr)
{
    manager_handle_t *manager = (manager_handle_t *)mgr;
    if(manager == NULL) {
        return -1;
    }
    return manager->node_num;
}

void *subdev_container_search(void *mgr, char *pk, char *dn)
{
    manager_handle_t *manager = (manager_handle_t *)mgr;
    node_t *node = NULL, *next = NULL;
    if(manager == NULL || pk == NULL || dn == NULL) {
        return NULL;
    }

    core_os_mutex_lock(manager->muxtex);
    core_list_for_each_entry_safe(node, next, &manager->node_list, linked_node, node_t) {
        if(0 == strcmp(node->pk, pk) && 0 == strcmp(node->dn, dn)) {
            core_os_mutex_unlock(manager->muxtex);
            return node->value;
        }
    }
    core_os_mutex_unlock(manager->muxtex);
    return NULL;
}

void *subdev_container_first(void *mgr)
{
    manager_handle_t *manager = (manager_handle_t *)mgr;
    node_t *node = NULL;
    if(mgr == NULL) {
        return NULL;
    }

    core_os_mutex_lock(manager->muxtex);
    if(manager == NULL || core_list_empty(&manager->node_list)) {
        core_os_mutex_unlock(manager->muxtex);
        return NULL;
    }
    node = core_list_first_entry(&manager->node_list, node_t, linked_node);

    core_os_mutex_unlock(manager->muxtex);

    return node->value;
}

int32_t subdev_container_poll_start(void *mgr)
{
    manager_handle_t *manager = (manager_handle_t *)mgr;
    if(manager == NULL) {
        return -1;
    }

    manager->last_node = core_list_first_entry(&manager->node_list, node_t, linked_node);
    return 0;
}

int32_t subdev_container_poll_finish(void *mgr)
{
    manager_handle_t *manager = (manager_handle_t *)mgr;
    if(manager == NULL) {
        return -1;
    }

    manager->last_node = NULL;
    return 0;
}

void *subdev_container_poll_get(void *mgr)
{
    manager_handle_t *manager = (manager_handle_t *)mgr;
    node_t *node = NULL;

    if(manager == NULL || core_list_empty(&manager->node_list)) {
        return NULL;
    }

    core_os_mutex_lock(manager->muxtex);
    if(manager->last_node == NULL) {
        node = core_list_first_entry(&manager->node_list, node_t, linked_node);
    } else {
        node = core_list_next_entry(manager->last_node, linked_node, node_t);
    }

    if(&node->linked_node == &manager->node_list) {
        core_os_mutex_unlock(manager->muxtex);
        return NULL;
    }

    manager->last_node = node;
    core_os_mutex_unlock(manager->muxtex);

    return node->value;
}


int32_t subdev_container_deinit(void **mgr)
{
    manager_handle_t *manager = NULL;
    node_t *node = NULL, *next = NULL;
    if(mgr == NULL || *mgr == NULL) {
        return -1;
    }

    manager = (manager_handle_t *)*mgr;

    core_os_mutex_lock(manager->muxtex);
    core_list_for_each_entry_safe(node, next, &manager->node_list, linked_node, node_t) {
        core_list_del(&node->linked_node);
        core_os_free(node->pk);
        core_os_free(node->dn);
        core_os_free(node);
    }
    core_os_mutex_unlock(manager->muxtex);
    core_os_mutex_deinit(&manager->muxtex);

    core_os_free(manager);
    *mgr = NULL;

    return 0;
}