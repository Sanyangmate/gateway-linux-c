#include "gateway_message.h"
#include <stdio.h>
#include "core_string.h"
#include "core_os.h"
#include "core_auth.h"
#include "core_log.h"
#include "aiot_state_api.h"
#include "device_private.h"
#include "aiot_message_api.h"
#include "device_private.h"
#include "gateway_private.h"
#include "subdev_manager.h"
#include "gateway_adapter.h"
#include "subdev_adapter.h"

#define TAG "GATEWAY"

/* 接收到的消息添加进链表，等待同步接口读取 */
static int32_t core_add_msg_to_list(gateway_handle_t *gateway_handle, aiot_gateway_msg_t *msg)
{
    resp_msg_node_t *node = NULL;
    node = (resp_msg_node_t *)core_os_malloc(sizeof(resp_msg_node_t));
    if(node == NULL) {
        ALOG_ERROR(TAG, STATE_SYS_DEPEND_MALLOC_FAILED, "malloc failed \r\n");
        return STATE_SYS_DEPEND_MALLOC_FAILED;
    }

    memset(node, 0, sizeof(*node));
    node->timestamp = core_os_time();
    node->msg = msg;

    core_os_mutex_lock(gateway_handle->recv_msg_mutex);
    core_list_add_tail(&node->linked_node, &gateway_handle->recv_msg_list);
    core_os_mutex_unlock(gateway_handle->recv_msg_mutex);

    return 0;
}

/* 读取返回消息 */
aiot_gateway_msg_t *core_pend_msg_from_list(gateway_handle_t *gateway_handle, int32_t id)
{
    resp_msg_node_t *node = NULL, *next = NULL;
    aiot_gateway_msg_t *result = NULL;
    uint32_t time_start = 0, time_end = 0;

    time_start = core_os_time();
    time_end = core_os_time();
    do {
        core_os_mutex_lock(gateway_handle->recv_msg_mutex);
        core_list_for_each_entry_safe(node, next, &gateway_handle->recv_msg_list, linked_node, resp_msg_node_t) {
            if(node->msg != NULL && node->msg->data.reply.id == id) {
                core_list_del(&node->linked_node);
                result = node->msg;
                core_os_free(node);
                break;
            }
        }
        core_os_mutex_unlock(gateway_handle->recv_msg_mutex);
        if(result != NULL) {
            break;
        } else {
            core_os_sleep(200);
            time_end = core_os_time();
        }
    } while(time_end - time_start < gateway_handle->config.msg_timeout);

    return result;
}

/* 网关设备/子设备发送消息，加入到批量发布队列中 */
int32_t core_gateway_send_message(void *gateway_device, const aiot_msg_t *msg)
{
    gateway_handle_t *gateway_handle = core_gateway_get_handle(gateway_device);
    if(gateway_handle == NULL) {
        ALOG_ERROR(TAG, STATE_DEVICE_UNKOWN_ERROR, "gateway handle is null\r\n");
        return STATE_DEVICE_UNKOWN_ERROR;
    }

    return normal_ops.send_message(gateway_device, msg);
}

/* 批量登录的回复消息处理 */
static void _gateway_subdev_batch_login_reply(void *device, const aiot_msg_t *message, void *userdata)
{
    aiot_gateway_msg_t *msg = NULL;
    gateway_handle_t *gateway_handle = NULL;
    subdev_handle_t *subdev_handle = NULL;
    int32_t i = 0;
    aiot_device_status_t status;
    char *product_key = NULL, *device_name = NULL;
    if(device == NULL || message == NULL) {
        return;
    }

    /* 解析回复消息 */
    msg = gateway_msg_parse_batch_reply(message);
    if(msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_PARSE_ERROR, "msg(%s) json parse error\r\n", message->topic);
        return;
    }

    msg->type = AIOT_GWRECV_CONNECT_REPLY;

    gateway_handle = core_gateway_get_handle(device);
    if(gateway_handle == NULL) {
        gateway_msg_delete(msg);
        return;
    }

    for(i = 0; i < msg->subdev_num; i++) {
        product_key = (msg->subdev_table + i)->product_key;
        device_name = (msg->subdev_table + i)->device_name;
        /* 查询该设备是否处于连接中 */
        subdev_handle = core_subdev_search_connnecting(gateway_handle, product_key, device_name);
        if(subdev_handle == NULL) {
            continue;
        }

        if(msg->data.reply.code == 200) {
            /* 通知子设备已连接 */
            status.type = AIOT_DEVICE_STATUS_CONNECT;
            if(subdev_handle->status == SUBDEV_STATUS_RECONNECTING) {
                status.type = AIOT_DEVICE_STATUS_RECONNECT;
            }

            /* 将子设备设置为在线状态 */
            core_subdev_move_to_connnected(gateway_handle, subdev_handle);
            status.error_code = 0;
            gateway_handle->subdev_adapter->status_handle(subdev_handle->device, &status);
        } else {
            if(subdev_handle->status == SUBDEV_STATUS_CONNECTING) {
                /* 第一次建连失败，直接删除，返回用户建连失败 */
                core_subdev_delete_all(gateway_handle, subdev_handle);
                core_subdev_deinit(gateway_handle, subdev_handle);
            } else if(subdev_handle->status == SUBDEV_STATUS_RECONNECTING) {
                /* 第一次建连失败，直接删除，返回用户建连失败 */
                core_subdev_move_to_disconnected(gateway_handle, subdev_handle);
            }

            /* TODO是否需要通知 */
        }
    }

    /* 回复消息加入队列，等待同步接口再读取处理 */
    core_add_msg_to_list(gateway_handle, msg);

    /* 异步接口调用的话，在这里回调异步接口 */
    if(gateway_handle->config.callback != NULL) {
        gateway_handle->config.callback(device, msg, gateway_handle->config.userdata);
    }
}

/* 批量离线操作的回复消息处理 */
static void _gateway_subdev_batch_logout_reply(void *device, const aiot_msg_t *message, void *userdata)
{
    aiot_gateway_msg_t *msg = NULL;
    gateway_handle_t *gateway_handle = NULL;
    subdev_handle_t *subdev_handle = NULL;
    int32_t i = 0;
    aiot_device_status_t status;
    subdev_status_t pre_status;
    void *subdev = NULL;

    char *product_key = NULL, *device_name = NULL;
    if(device == NULL || message == NULL) {
        return;
    }

    msg = gateway_msg_parse_batch_reply(message);
    if(msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_PARSE_ERROR, "msg(%s) json parse error\r\n", message->topic);
        return;
    }

    msg->type = AIOT_GWRECV_DISCONNECT_REPLY;

    gateway_handle = core_gateway_get_handle(device);
    if(gateway_handle == NULL) {
        gateway_msg_delete(msg);
        return;
    }

    for(i = 0; i < msg->subdev_num; i++) {
        product_key = (msg->subdev_table + i)->product_key;
        device_name = (msg->subdev_table + i)->device_name;
        /* 查询所有网关管理设备 */
        subdev_handle = core_subdev_search_all(gateway_handle, product_key, device_name);
        if(subdev_handle == NULL) {
            continue;
        }

        /* 先备份子设备信息，用于执行回调, 再删除子设备 */
        pre_status = subdev_handle->status;
        subdev = subdev_handle->device;
        /* 先从网关的管理容器中将subdev_handle删除*/
        core_subdev_delete_all(gateway_handle, subdev_handle);
        core_subdev_deinit(gateway_handle, subdev_handle);

        /* 将子设备相关的信息删除，并且通知子设备它已经离线 */
        if(msg->data.reply.code == 200
                && (pre_status == SUBDEV_STATUS_CONNECTED
                    || pre_status == SUBDEV_STATUS_RECONNECTED)) {
            /* 通知子设备已连接 */
            status.error_code = 0;
            status.type = AIOT_DEVICE_STATUS_DISCONNECT;
            gateway_handle->subdev_adapter->status_handle(subdev, &status);
        }
    }

    /* 异步接口调用的话，在这里回调异步接口 */
    if(gateway_handle->config.callback != NULL) {
        gateway_handle->config.callback(device, msg, gateway_handle->config.userdata);
    }

    gateway_msg_delete(msg);
}

/* 批量添加topo的回复消息处理 */
static void _gateway_add_topo_reply(void *device, const aiot_msg_t *message, void *userdata)
{
    aiot_gateway_msg_t *msg = NULL;
    gateway_handle_t *gateway_handle = NULL;
    if(device == NULL || message == NULL) {
        return;
    }

    msg = gateway_msg_parse_batch_reply(message);
    if(msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_PARSE_ERROR, "msg(%s) json parse error\r\n", message->topic);
        return;
    }

    msg->type = AIOT_GWRECV_ADD_TOPO_REPLY;

    gateway_handle = core_gateway_get_handle(device);
    if(gateway_handle == NULL) {
        gateway_msg_delete(msg);
        return;
    }

    /* 回复消息加入队列，等待同步接口再读取处理 */
    core_add_msg_to_list(gateway_handle, msg);

    /* 异步接口调用的话，在这里回调异步接口 */
    if(gateway_handle->config.callback != NULL) {
        gateway_handle->config.callback(device, msg, gateway_handle->config.userdata);
    }
}

/* 批量删除topo的回复消息处理 */
static void _gateway_delete_topo_reply(void *device, const aiot_msg_t *message, void *userdata)
{
    aiot_gateway_msg_t *msg = NULL;
    gateway_handle_t *gateway_handle = NULL;
    if(device == NULL || message == NULL) {
        return;
    }

    msg = gateway_msg_parse_batch_reply(message);
    if(msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_PARSE_ERROR, "msg(%s) json parse error\r\n", message->topic);
        return;
    }

    msg->type = AIOT_GWRECV_DELETE_TOPO_REPLY;

    gateway_handle = core_gateway_get_handle(device);
    if(gateway_handle == NULL) {
        gateway_msg_delete(msg);
        return;
    }

    /* 回复消息加入队列，等待同步接口再读取处理 */
    core_add_msg_to_list(gateway_handle, msg);

    /* 异步接口调用的话，在这里回调异步接口 */
    if(gateway_handle->config.callback != NULL) {
        gateway_handle->config.callback(device, msg, gateway_handle->config.userdata);
    }
}

/* 批量获取密钥的回复消息处理 */
static void _gateway_query_secret_reply(void *device, const aiot_msg_t *message, void *userdata)
{
    aiot_gateway_msg_t *msg = NULL;
    gateway_handle_t *gateway_handle = NULL;
    if(device == NULL || message == NULL) {
        return;
    }

    msg = gateway_msg_parse_batch_reply(message);
    if(msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_PARSE_ERROR, "msg(%s) json parse error\r\n", message->topic);
        return;
    }

    gateway_handle = core_gateway_get_handle(device);
    if(gateway_handle == NULL) {
        gateway_msg_delete(msg);
        return;
    }


    core_add_msg_to_list(gateway_handle, msg);
}

/* 批量抢注的回复消息处理 */
static void _gateway_product_register_reply(void *device, const aiot_msg_t *message, void *userdata)
{
    aiot_gateway_msg_t *msg = NULL;
    gateway_handle_t *gateway_handle = NULL;
    if(device == NULL || message == NULL) {
        return;
    }

    msg = gateway_msg_parse_product_register_reply(message);
    if(msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_PARSE_ERROR, "msg(%s) json parse error\r\n", message->topic);
        return;
    }

    gateway_handle = core_gateway_get_handle(device);
    if(gateway_handle == NULL) {
        gateway_msg_delete(msg);
        return;
    }

    core_add_msg_to_list(gateway_handle, msg);
}

/* topo关系变更的处理 */
static void _gateway_topo_changed_notice(void *device, const aiot_msg_t *message, void *userdata)
{
    aiot_gateway_msg_t *msg = NULL;
    gateway_handle_t *gateway_handle = NULL;
    subdev_handle_t *subdev_handle = NULL;
    int32_t i = 0;
    aiot_device_status_t status;
    subdev_status_t pre_status;
    void *subdev = NULL;
    char *product_key = NULL, *device_name = NULL;
    if(device == NULL || message == NULL) {
        return;
    }

    /* 解析topo变更消息 */
    msg = gateway_msg_parse_topo_change(message);
    if(msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_PARSE_ERROR, "msg(%s) json parse error\r\n", message->topic);
        return;
    }

    msg->type = AIOT_GWRECV_TOPO_CHANGE_NOTICE;

    gateway_handle = core_gateway_get_handle(device);
    if(gateway_handle == NULL) {
        gateway_msg_delete(msg);
        return;
    }

    /* 如果为topo关系删除，则将删除topo关系的子设备进行下线处理 */
    if(msg->data.topo_change.status == 1) {
        for(i = 0; i < msg->subdev_num; i++) {
            status.type = AIOT_DEVICE_STATUS_DISCONNECT;
            status.error_code = 0;
            product_key = (msg->subdev_table + i)->product_key;
            device_name = (msg->subdev_table + i)->device_name;
            subdev_handle = core_subdev_search_all(gateway_handle, product_key, device_name);
            if(subdev_handle == NULL) {
                continue;
            }

            /* 先备份子设备信息，用于执行回调, 再删除子设备 */
            pre_status = subdev_handle->status;
            subdev = subdev_handle->device;
            core_subdev_delete_all(gateway_handle, subdev_handle);
            core_subdev_deinit(gateway_handle, subdev_handle);

            if(pre_status == SUBDEV_STATUS_RECONNECTED || pre_status == SUBDEV_STATUS_CONNECTED) {
                /* 通知子设备已断开 */
                gateway_handle->subdev_adapter->status_handle(subdev, &status);
            }

        }
    }

    /* 执行回调处理 */
    if(gateway_handle->config.callback != NULL) {
        gateway_handle->config.callback(device, msg, gateway_handle->config.userdata);
    }
    gateway_msg_delete(msg);
}

/* pk、dn、ds 生成认证信息 */
static subdev_info_t* _subdev_create_auth_info(char *product_key, char *device_name, char *device_secret)
{
    int32_t     res = STATE_SUCCESS;
    subdev_info_t *info;
    char sign_str[65] = {0};
    uint8_t sign[32] = {0};
    char timestamp[21] = {0};
    char *client_id = NULL;
    char *client_id_src[] = { product_key, device_name };
    char *client_id_fmt = "%s.%s|_ss=1|";
    char *plain_text = NULL;
    char *plain_text_src[] = { client_id, device_name, product_key, timestamp};
    char *plain_text_fmt = "clientId%sdeviceName%sproductKey%stimestamp%s";

    if(product_key == NULL || device_name == NULL || device_secret == NULL) {
        return NULL;
    }
    /* 生成client_id */
    client_id_src[0] = product_key;
    client_id_src[1] = device_name;
    res = core_sprintf(NULL, &client_id, client_id_fmt, client_id_src, sizeof(client_id_src)/sizeof(char *), NULL);
    if (res < STATE_SUCCESS) {
        return NULL;
    }

    /* 生成签名报文 */
    core_uint642str(core_os_time(), timestamp, NULL);
    plain_text_src[0] = client_id;
    plain_text_src[1] = device_name;
    plain_text_src[2] = product_key;
    plain_text_src[3] = timestamp;
    res = core_sprintf(NULL, &plain_text, plain_text_fmt, plain_text_src, sizeof(plain_text_src)/sizeof(char *), NULL);
    if (res < STATE_SUCCESS) {
        core_os_free(client_id);
        return NULL;
    }
    core_hmac_sha256((uint8_t *)plain_text, (uint32_t)strlen(plain_text), (uint8_t *)device_secret, (uint32_t)strlen(device_secret), sign);
    core_hex2str(sign, 32, sign_str, 0);
    core_os_free(plain_text);

    info = core_os_malloc(sizeof(subdev_info_t));
    memset(info, 0, sizeof(subdev_info_t));
    core_strdup(NULL, &info->product_key, product_key, NULL);
    core_strdup(NULL, &info->device_name, device_name, NULL);
    core_strdup(NULL, &info->sign_method, "hmacSha256", NULL);
    core_strdup(NULL, &info->sign, sign_str, NULL);
    core_strdup(NULL, &info->timestamp, timestamp, NULL);
    info->client_id = client_id;

    return info;
}

/* pk、dn、ps 生成认证信息 */
static subdev_info_t* _subdev_create_auth_info_product(char *product_key, char *device_name, char *product_secret)
{
    int32_t     res = STATE_SUCCESS;
    subdev_info_t *info;
    char sign_str[65] = {0};
    uint8_t sign[32] = {0};
    char timestamp[21] = {0};
    char *plain_text = NULL;
    char *plain_text_src[] = { device_name, product_key, timestamp};
    char *plain_text_fmt = "deviceName%sproductKey%srandom%s";

    /* 生成签名报文 */
    core_uint642str(core_os_time(), timestamp, NULL);
    res = core_sprintf(NULL, &plain_text, plain_text_fmt, plain_text_src, sizeof(plain_text_src)/sizeof(char *), NULL);
    if (res < STATE_SUCCESS) {
        return NULL;
    }
    core_hmac_sha256((uint8_t *)plain_text, (uint32_t)strlen(plain_text), (uint8_t *)product_secret, (uint32_t)strlen(product_secret), sign);
    core_hex2str(sign, 32, sign_str, 0);
    core_os_free(plain_text);

    info = core_os_malloc(sizeof(subdev_info_t));
    memset(info, 0, sizeof(subdev_info_t));
    core_strdup(NULL, &info->product_key, product_key, NULL);
    core_strdup(NULL, &info->device_name, device_name, NULL);
    core_strdup(NULL, &info->sign_method, "hmacSha256", NULL);
    core_strdup(NULL, &info->sign, sign_str, NULL);
    core_strdup(NULL, &info->timestamp, timestamp, NULL);

    return info;
}

/* 设备认证信息资源回收 */
static int32_t _subdev_delete_auth_info(subdev_info_t *info)
{
    if(info == NULL) {
        return STATE_DEVICE_UNKOWN_ERROR;
    }

    if(info->product_key != NULL) {
        core_os_free(info->product_key);
    }

    if(info->device_name != NULL) {
        core_os_free(info->device_name);
    }
    if(info->sign_method != NULL) {
        core_os_free(info->sign_method);
    }
    if(info->sign != NULL) {
        core_os_free(info->sign);
    }
    if(info->timestamp != NULL) {
        core_os_free(info->timestamp);
    }
    if(info->client_id != NULL) {
        core_os_free(info->client_id);
    }

    core_os_free(info);
    return STATE_SUCCESS;
}

/* 生成并发送代理子设备批量登录消息 */
int32_t core_gateway_subdev_connect(void *gateway_device, subdev_handle_t *subdev_table[], int32_t subdev_num)
{
    aiot_msg_t *msg = NULL;
    subdev_info_t **info_table = NULL;
    int32_t res = STATE_SUCCESS;
    int i = 0;
    char *product_key = NULL, *device_name = NULL, *device_secret = NULL;
    gateway_handle_t *gateway_handle = core_device_module_get(gateway_device, MODULE_GATEWAY);

    info_table = core_os_malloc(subdev_num * sizeof(subdev_info_t *));
    memset(info_table, 0, subdev_num * sizeof(subdev_info_t *));
    for(i = 0; i < subdev_num; i++) {
        product_key = aiot_device_product_key(subdev_table[i]->device);
        device_name = aiot_device_name(subdev_table[i]->device);
        device_secret = core_device_secret(subdev_table[i]->device);
        info_table[i] = _subdev_create_auth_info(product_key, device_name, device_secret);
        if(info_table[i] == NULL) {
            ALOG_ERROR(TAG, STATE_SYS_DEPEND_MALLOC_FAILED, "malloc failed \r\n");
            res = STATE_SYS_DEPEND_MALLOC_FAILED;
            break;
        }
    }

    if(res == STATE_SUCCESS) {
        msg = gateway_msg_create_batch_login(gateway_device, info_table, subdev_num, gateway_handle->config.auto_topo);
        if(msg != NULL) {
            msg->cid = core_device_get_valid_cid(gateway_device);
            res = normal_ops.send_message(gateway_device, msg);
            if(res == STATE_SUCCESS ) {
                res = msg->id;
                for(i = 0; i < subdev_num; i++) {
                    subdev_table[i]->device->owner_cid = msg->cid;
                }
            }
            aiot_msg_delete(msg);
        }
    }

    for(i = 0; i < subdev_num; i++) {
        _subdev_delete_auth_info(info_table[i]);
    }
    if(info_table != NULL) {
        core_os_free(info_table);
    }

    return res;
}

/* 生成并发送代理子设备批量登出消息 */
int32_t core_gateway_subdev_disconnect_cid(void *gateway_device, void* subdev_table[], int32_t subdev_num, int32_t cid)
{
    aiot_msg_t *msg = NULL;
    subdev_info_t **info_table = NULL;
    int32_t res = STATE_SUCCESS;
    int i = 0;
    char *product_key = NULL, *device_name = NULL;

    info_table = core_os_malloc(subdev_num * sizeof(subdev_info_t *));
    memset(info_table, 0, subdev_num * sizeof(subdev_info_t *));
    for(i = 0; i < subdev_num; i++) {
        info_table[i] = (subdev_info_t *)core_os_malloc(sizeof(subdev_info_t));
        if(info_table[i] == NULL) {
            ALOG_ERROR(TAG, STATE_SYS_DEPEND_MALLOC_FAILED, "malloc failed \r\n");
            res = STATE_SYS_DEPEND_MALLOC_FAILED;
            break;
        }
        memset(info_table[i], 0, sizeof(subdev_info_t));
        product_key = aiot_device_product_key(subdev_table[i]);
        device_name = aiot_device_name(subdev_table[i]);
        core_strdup(NULL, &info_table[i]->product_key, product_key, NULL);
        core_strdup(NULL, &info_table[i]->device_name, device_name, NULL);
    }

    if(res == STATE_SUCCESS) {
        msg = gateway_msg_create_batch_logout(gateway_device, info_table, subdev_num);
        if(msg != NULL) {
            msg->cid = cid;
            if(msg != NULL) {
                res = normal_ops.send_message(gateway_device, msg);
                if(res == STATE_SUCCESS ) {
                    res = msg->id;
                }
                aiot_msg_delete(msg);
            }
        } else {
            return STATE_SYS_DEPEND_MALLOC_FAILED;
        }
    }

    for(i = 0; i < subdev_num; i++) {
        _subdev_delete_auth_info(info_table[i]);
    }
    if(info_table != NULL) {
        core_os_free(info_table);
    }

    return res;
}

int32_t core_gateway_subdev_disconnect(void *gateway_device, void* subdev_table[], int32_t subdev_num)
{
    int32_t res = STATE_SUCCESS;
    int32_t last_cid = 0, last_index = 0, i = 0;
    device_handle_t *subdev = NULL;

    /* 子设备logout按照相同cid的子设备发送离线消息 */
    subdev = (device_handle_t *)subdev_table[0];
    last_cid = subdev->owner_cid;
    for(i = 1; i < subdev_num; i++) {
        subdev = (device_handle_t *)subdev_table[i];
        if(last_cid != subdev->owner_cid) {
            res = core_gateway_subdev_disconnect_cid(gateway_device, &subdev_table[last_index], i - last_index, last_cid);
            if(res < STATE_SUCCESS) {
                return res;
            }
            last_index = i;
            last_cid = subdev->owner_cid;
        }
    }

    res = core_gateway_subdev_disconnect_cid(gateway_device, &subdev_table[last_index], subdev_num - last_index, last_cid);
    return res;
}

/* 生成并发送代理子设备添加topo关系消息 */
int32_t core_gateway_add_topo(void *gateway_device, aiot_subdev_meta_info_t subdev_table[], int32_t subdev_num)
{
    aiot_msg_t *msg = NULL;
    subdev_info_t **info_table = NULL;
    int32_t res = STATE_SUCCESS;
    int i = 0;
    aiot_subdev_meta_info_t *device = NULL;

    info_table = core_os_malloc(subdev_num * sizeof(subdev_info_t *));
    memset(info_table, 0, subdev_num * sizeof(subdev_info_t *));
    for(i = 0; i < subdev_num; i++) {
        device = &subdev_table[i];
        info_table[i] = _subdev_create_auth_info(device->product_key, device->device_name, device->device_secret);
        if(info_table[i] == NULL) {
            ALOG_ERROR(TAG, STATE_SYS_DEPEND_MALLOC_FAILED, "malloc failed \r\n");
            res = STATE_SYS_DEPEND_MALLOC_FAILED;
            break;
        }
    }

    if(res == STATE_SUCCESS) {
        msg = gateway_msg_create_topo_add(gateway_device, info_table, subdev_num);
        if(msg != NULL) {
            res = normal_ops.send_message(gateway_device, msg);
            if(res == STATE_SUCCESS ) {
                res = msg->id;
            }
            aiot_msg_delete(msg);
        }
    }

    for(i = 0; i < subdev_num; i++) {
        _subdev_delete_auth_info(info_table[i]);
    }
    if(info_table != NULL) {
        core_os_free(info_table);
    }

    return res;
}

/* 生成并发送代理子设备删除topo关系消息 */
int32_t core_gateway_delete_topo(void *gateway_device, aiot_subdev_meta_info_t subdev_table[], int32_t subdev_num)
{
    aiot_msg_t *msg = NULL;
    subdev_info_t **info_table = NULL;
    int32_t res = STATE_SUCCESS;
    int i = 0;
    aiot_subdev_meta_info_t *device = NULL;

    info_table = core_os_malloc(subdev_num * sizeof(subdev_info_t *));
    memset(info_table, 0, subdev_num * sizeof(subdev_info_t *));
    for(i = 0; i < subdev_num; i++) {
        device = &subdev_table[i];
        info_table[i] = (subdev_info_t *)core_os_malloc(sizeof(subdev_info_t));
        if(info_table[i] == NULL) {
            ALOG_ERROR(TAG, STATE_SYS_DEPEND_MALLOC_FAILED, "malloc failed \r\n");
            res = STATE_SYS_DEPEND_MALLOC_FAILED;
            break;
        }
        memset(info_table[i], 0, sizeof(subdev_info_t));
        core_strdup(NULL, &info_table[i]->product_key, device->product_key, NULL);
        core_strdup(NULL, &info_table[i]->device_name, device->device_name, NULL);
    }

    if(res == STATE_SUCCESS) {
        msg = gateway_msg_create_topo_delete(gateway_device, info_table, subdev_num);
        if(msg != NULL) {
            res = normal_ops.send_message(gateway_device, msg);
            if(res == STATE_SUCCESS ) {
                res = msg->id;
            }
            aiot_msg_delete(msg);
        }
    }


    for(i = 0; i < subdev_num; i++) {
        _subdev_delete_auth_info(info_table[i]);
    }
    if(info_table != NULL) {
        core_os_free(info_table);
    }

    return res;
}

/* 生成并发送代理子设备动态注册消息 */
int32_t core_gateway_query_secret(void *gateway_device, aiot_subdev_meta_info_t subdev_table[], int32_t subdev_num)
{
    aiot_msg_t *msg = NULL;
    subdev_info_t **info_table = NULL;
    int32_t res = STATE_SUCCESS;
    int i = 0;
    aiot_subdev_meta_info_t *device = NULL;

    info_table = core_os_malloc(subdev_num * sizeof(subdev_info_t *));
    memset(info_table, 0, subdev_num * sizeof(subdev_info_t *));
    for(i = 0; i < subdev_num; i++) {
        device = &subdev_table[i];
        info_table[i] = (subdev_info_t *)core_os_malloc(sizeof(subdev_info_t));
        if(info_table[i] == NULL) {
            ALOG_ERROR(TAG, STATE_SYS_DEPEND_MALLOC_FAILED, "malloc failed \r\n");
            res = STATE_SYS_DEPEND_MALLOC_FAILED;
            break;
        }
        memset(info_table[i], 0, sizeof(subdev_info_t));
        core_strdup(NULL, &info_table[i]->product_key, device->product_key, NULL);
        core_strdup(NULL, &info_table[i]->device_name, device->device_name, NULL);
    }

    if(res == STATE_SUCCESS) {
        msg = gateway_msg_create_secret_query(gateway_device, info_table, subdev_num);
        if(msg != NULL) {
            res = normal_ops.send_message(gateway_device, msg);
            if(res == STATE_SUCCESS ) {
                res = msg->id;
            }
            aiot_msg_delete(msg);
        }
    }


    for(i = 0; i < subdev_num; i++) {
        _subdev_delete_auth_info(info_table[i]);
    }
    if(info_table != NULL) {
        core_os_free(info_table);
    }

    return res;
}

/* 生成并发送代理子设备动态注册(抢注)消息 */
int32_t core_gateway_product_register(void *gateway_device, aiot_subdev_meta_info_t subdev_table[], int32_t subdev_num)
{
    aiot_msg_t *msg = NULL;
    subdev_info_t **info_table = NULL;
    int32_t res = STATE_SUCCESS;
    int i = 0;
    aiot_subdev_meta_info_t *device = NULL;

    info_table = core_os_malloc(subdev_num * sizeof(subdev_info_t *));
    memset(info_table, 0, subdev_num * sizeof(subdev_info_t *));
    for(i = 0; i < subdev_num; i++) {
        device = &subdev_table[i];
        info_table[i] = _subdev_create_auth_info_product(device->product_key, device->device_name, device->product_secret);
        if(info_table[i] == NULL) {
            ALOG_ERROR(TAG, STATE_SYS_DEPEND_MALLOC_FAILED, "malloc failed \r\n");
            res = STATE_SYS_DEPEND_MALLOC_FAILED;
            break;
        }
    }

    if(res == STATE_SUCCESS) {
        msg = gateway_msg_create_product_register(gateway_device, info_table, subdev_num);
        if(msg != NULL) {
            res = normal_ops.send_message(gateway_device, msg);
            if(res == STATE_SUCCESS ) {
                res = msg->id;
            }
            aiot_msg_delete(msg);
        }
    }


    for(i = 0; i < subdev_num; i++) {
        _subdev_delete_auth_info(info_table[i]);
    }
    if(info_table != NULL) {
        core_os_free(info_table);
    }

    return res;
}

/* 网关模块初始化回调 */
static void *_gateway_init()
{
    gateway_handle_t *gateway_handle = (gateway_handle_t *)core_os_malloc(sizeof(gateway_handle_t));
    if(gateway_handle == NULL) {
        ALOG_ERROR(TAG, STATE_SYS_DEPEND_MALLOC_FAILED, "malloc failed \r\n");
        return NULL;
    }

    memset(gateway_handle, 0, sizeof(gateway_handle_t));
    gateway_handle->config.msg_timeout = 5000;
    gateway_handle->config.auto_reconnect = 1;

    gateway_handle->subdev_adapter = subdev_adapter_get();
    gateway_handle->gateway_adapter = gateway_adapter_get();

    /* 子设备管理模块初始化 */
    core_subdev_manager_init(gateway_handle);

    CORE_INIT_LIST_HEAD(&gateway_handle->recv_msg_list);
    gateway_handle->recv_msg_mutex = core_os_mutex_init();
    gateway_handle->msg_num = 0;

    CORE_INIT_LIST_HEAD(&gateway_handle->pend_ack_list);
    gateway_handle->pend_ack_mutex = core_os_mutex_init();

    return gateway_handle;
}

/* 网关模块反初始化回调 */
void _gateway_deinit(void *handle)
{
    resp_msg_node_t *recv_node = NULL, *recv_next = NULL;
    pend_ack_node_t *ack_node = NULL, *ack_next = NULL;
    gateway_handle_t *gateway_handle = (gateway_handle_t *)handle;

    /* 子设备管理模块资源回收 */
    core_subdev_manager_deinit(gateway_handle);
    /* 回收回复消息缓存 */
    core_os_mutex_lock(gateway_handle->recv_msg_mutex);
    core_list_for_each_entry_safe(recv_node, recv_next, &gateway_handle->recv_msg_list, linked_node, resp_msg_node_t) {
        gateway_msg_delete(recv_node->msg);
        core_list_del(&recv_node->linked_node);
        core_os_free(recv_node);
    }
    core_os_mutex_unlock(gateway_handle->recv_msg_mutex);
    core_os_mutex_deinit(&gateway_handle->recv_msg_mutex);

    /* 消息ID与设备的关系映射 */
    core_os_mutex_lock(gateway_handle->pend_ack_mutex);
    core_list_for_each_entry_safe(ack_node, ack_next, &gateway_handle->pend_ack_list, linked_node, pend_ack_node_t) {
        core_os_free(ack_node->product_key);
        core_os_free(ack_node->device_name);
        core_list_del(&ack_node->linked_node);
        core_os_free(ack_node);
    }
    core_os_mutex_unlock(gateway_handle->pend_ack_mutex);
    core_os_mutex_deinit(&gateway_handle->pend_ack_mutex);

    core_os_free(gateway_handle);
}

/* 网关模块定时处理回调 */
void _gateway_process(void *handle)
{
    subdev_handle_t **subdev_table = NULL;
    int32_t subdev_num = 0, select = 0;
    subdev_handle_t *subdev_handle = NULL;
    gateway_handle_t *gateway_handle = (gateway_handle_t *)handle;

    if(0 == aiot_device_online(gateway_handle->device)) {
        return;
    }
    /* 处理离线子设备重连 */
    if(gateway_handle->config.auto_reconnect
            && (subdev_num = core_subdev_size_disconnected(gateway_handle)) > 0 ) {
        /* 设置重连设备个数，每次重连限额 */
        if(subdev_num > 5) {
            subdev_num = 5;
        }

        /* 生成重连子设备列表 */
        subdev_table = core_os_malloc(subdev_num * sizeof(subdev_handle_t *));
        while((subdev_handle = core_subdev_front_disconnected(gateway_handle)) != NULL) {
            subdev_handle->connect_stamp = core_os_time();
            subdev_table[select++] = subdev_handle;
            core_subdev_move_to_connnecting(gateway_handle, subdev_handle);
            if(select == subdev_num) {
                break;
            }
        }

        /* 触发子设备重连 */
        if(select > 0) {
            core_gateway_subdev_connect(gateway_handle->device, subdev_table, select);
        }
        core_os_free(subdev_table);
    }

    /* 处理离线子设备重连超时 */
    while((subdev_handle = core_subdev_front_connnecting(gateway_handle)) != NULL) {
        /* 连接超时 */
        if(core_os_time() - subdev_handle->connect_stamp > gateway_handle->config.msg_timeout) {
            if(subdev_handle->status == SUBDEV_STATUS_RECONNECTING) {
                /* 重连设备失败再回队列等下次重连 */
                core_subdev_move_to_disconnected(gateway_handle, subdev_handle);
            } else {
                /* 第一次连接失败，连接超时，直接删除 */
                core_subdev_delete_all(gateway_handle, subdev_handle);
                core_subdev_deinit(gateway_handle, subdev_handle);
            }
        } else {
            /* 从表头出来都是顺序的，所以当出现一个时间条件不满足，后面就都不满足 */
            break;
        }
    }
}

static void _gateway_channel_status(void *handle, const core_channel_status_t *event)
{
    gateway_handle_t *gateway_handle = (gateway_handle_t *)handle;
    subdev_handle_t *subdev_handle = NULL, *cycle_start = NULL;
    aiot_device_status_t status;
    if(event->type != AIOT_DEVICE_STATUS_DISCONNECT) {
        return;
    }

    status.error_code = event->error_code;
    status.type = (aiot_device_status_type_t)event->type;
    /* 子设备由网关托管重连，先将子设备状态置为离线*/
    while((subdev_handle = core_subdev_front_connnected(gateway_handle)) != NULL && subdev_handle != cycle_start) {
        if(subdev_handle->device->owner_cid == event->cid) {
            if(gateway_handle->config.auto_reconnect) {
                /* 在线子设备直接告知已下线，将子设备添加进离线设备表，等待重连 */
                core_subdev_move_to_disconnected(gateway_handle, subdev_handle);
            } else {
                core_subdev_delete_connnected(gateway_handle, subdev_handle);
                core_subdev_deinit(gateway_handle, subdev_handle);
            }
            gateway_handle->subdev_adapter->status_handle(subdev_handle->device, &status);
        } else {
            if(cycle_start == NULL) {
                cycle_start = subdev_handle;
            }
            /* 放到列表尾部*/
            core_subdev_move_to_connnected(gateway_handle, subdev_handle);
        }
    }

    cycle_start = NULL;

    while((subdev_handle = core_subdev_front_connnecting(gateway_handle)) != NULL && subdev_handle != cycle_start) {
        if(subdev_handle->device->owner_cid == event->cid) {
            if(gateway_handle->config.auto_reconnect == 0 || subdev_handle->status == SUBDEV_STATUS_CONNECTING) {
                core_subdev_delete_connnecting(gateway_handle, subdev_handle);
                core_subdev_deinit(gateway_handle, subdev_handle);
            } else if(subdev_handle->status == SUBDEV_STATUS_RECONNECTING) {
                core_subdev_move_to_disconnected(gateway_handle, subdev_handle);
            }
        } else {
            if(cycle_start == NULL) {
                cycle_start = subdev_handle;
            }
            core_subdev_move_to_connnecting(gateway_handle, subdev_handle);
        }
    }
}

/* 网关模块即插即用操作方法 */
static module_ops_t gateway_ops = {
    .internal_ms = 200,
    .on_init = _gateway_init,
    .on_process = _gateway_process,
    .on_channel_status = _gateway_channel_status,
    .on_deinit = _gateway_deinit,
};

gateway_handle_t *core_gateway_get_handle(void *device)
{
    gateway_handle_t *gateway_handle = core_device_module_get(device, MODULE_GATEWAY);
    int32_t res = STATE_SUCCESS;
    device_handle_t *gatedev = (device_handle_t *)device;

    if(gateway_handle == NULL) {
        res = core_device_module_register(device, MODULE_GATEWAY, &gateway_ops);
        if(res != STATE_SUCCESS) {
            return NULL;
        } else {
            gateway_handle = core_device_module_get(device, MODULE_GATEWAY);
            gateway_handle->device = device;
            gatedev->ops = gateway_handle->gateway_adapter;
            aiot_device_register_topic_filter(device, GATEWAY_SUBDEVICE_ADD_TOPO_REPLY_TOPIC_FMT, _gateway_add_topo_reply, 0, NULL);
            aiot_device_register_topic_filter(device, GATEWAY_SUBDEVICE_DELETE_TOPO_REPLY_TOPIC_FMT, _gateway_delete_topo_reply, 0, NULL);
            aiot_device_register_topic_filter(device, GATEWAY_SUBDEVICE_TOPO_CHANGED_TOPIC_FMT, _gateway_topo_changed_notice, 0, NULL);
            aiot_device_register_topic_filter(device, GATEWAY_SUBDEVICE_BATCH_LOGIN_REPLY_TOPIC_FMT, _gateway_subdev_batch_login_reply, 0, NULL);
            aiot_device_register_topic_filter(device, GATEWAY_SUBDEVICE_BATCH_LOGOUT_REPLY_TOPIC_FMT, _gateway_subdev_batch_logout_reply, 0, NULL);
            aiot_device_register_topic_filter(device, GATEWAY_SUBDEVICE_REGISTER_REPLY_TOPIC_FMT, _gateway_query_secret_reply, 0, NULL);
            aiot_device_register_topic_filter(device, GATEWAY_PRODUCT_REGISTER_REPLY_TOPIC_FMT, _gateway_product_register_reply, 0, NULL);
        }
    }

    return gateway_handle;
}
