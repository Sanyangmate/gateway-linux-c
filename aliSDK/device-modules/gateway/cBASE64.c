
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "cBASE64.h"

static int8_t g_encodingTable[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                   'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                   'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                   'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                   'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                   'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                   'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                   '4', '5', '6', '7', '8', '9', '+', '/'
                                  };

static int32_t g_modTable[] = { 0, 2, 1 };


char* base64encode(const uint8_t *data, uint32_t inputLength, void* (*on_malloc)(uint32_t size))
{
    uint32_t i = 0;
    uint32_t j = 0;
    uint32_t len = 0;
    char *result = 0;

    if (NULL == data) {
        return NULL;
    }

    len = 4 * ((inputLength + 2) / 3);
    result = on_malloc(len + 1);
    if(result == NULL) {
        return NULL;
    }

    for (i = 0, j = 0; i < inputLength;) {
        uint32_t octet_a = i < inputLength ? (uint8_t) data[i++] : 0;
        uint32_t octet_b = i < inputLength ? (uint8_t) data[i++] : 0;
        uint32_t octet_c = i < inputLength ? (uint8_t) data[i++] : 0;

        uint32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;

        result[j++] = g_encodingTable[(triple >> 3 * 6) & 0x3F];
        result[j++] = g_encodingTable[(triple >> 2 * 6) & 0x3F];
        result[j++] = g_encodingTable[(triple >> 1 * 6) & 0x3F];
        result[j++] = g_encodingTable[(triple >> 0 * 6) & 0x3F];
    }

    for (i = 0; i < g_modTable[inputLength % 3]; i++) {
        result[len - 1 - i] = '=';
    }
    result[len] = 0;

    return result;
}

