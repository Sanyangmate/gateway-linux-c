#ifndef _SUBDEV_ADAPTER_H_
#define _SUBDEV_ADAPTER_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>
#include "device_private.h"

const device_operation_t* subdev_adapter_get();

#if defined(__cplusplus)
}
#endif

#endif