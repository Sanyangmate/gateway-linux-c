

#ifndef _DEVINFO_MESSAGE_H_
#define _DEVINFO_MESSAGE_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>
#include "aiot_message_api.h"
#include "aiot_devinfo_api.h"

/* 更新设备标签的topic */
#define DEVINFO_UPDATE_TOPIC_FMT               "/sys/%s/%s/thing/deviceinfo/update"
/* 更新设备标签回复的topic */
#define DEVINFO_UPDATE_REPLY_TOPIC             "/sys/+/+/thing/deviceinfo/update_reply"
/* 删除设备标签的topic */
#define DEVINFO_DELETE_TOPIC_FMT               "/sys/%s/%s/thing/deviceinfo/delete"
/* 删除设备标签回复的topic */
#define DEVINFO_DELETE_REPLY_TOPIC             "/sys/+/+/thing/deviceinfo/delete_reply"


/* 创建添加标签的消息 */
aiot_msg_t *devinfo_msg_create_add(void *device, char *key, char *value);
/* 创建删除标签的消息 */
aiot_msg_t *devinfo_msg_create_delete(void *device, char *key);

/* 云端给的回复消息解析 */
aiot_devinfo_msg_reply_t *devinfo_msg_parse_generic_reply(const aiot_msg_t *msg);

/* 删除 aiot_devinfo_msg_reply_t */
void devinfo_msg_delete(aiot_devinfo_msg_reply_t *msg);

#if defined(__cplusplus)
}
#endif

#endif