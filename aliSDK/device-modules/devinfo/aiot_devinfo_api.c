#include "core_log.h"
#include "core_time.h"
#include "core_string.h"
#include "core_os.h"
#include "aiot_state_api.h"
#include "device_private.h"
#include "aiot_message_api.h"
#include "devinfo_message.h"
#include "aiot_devinfo_api.h"

#define TAG "DEVINFO"

typedef struct {
    devinfo_callback_t msg_callback;
    void *userdata;
} devinfo_config_t;

typedef struct {
    void *device;
    devinfo_config_t config;
} devinfo_handle_t;

static devinfo_handle_t *_devinfo_get_handle(void *device);

void _devinfo_msg_reply_callback(void *device, const aiot_msg_t *message, void *userdata)
{
    aiot_devinfo_msg_reply_t *devinfo_msg = NULL;
    char *ops = (char *)userdata;
    devinfo_handle_t *devinfo_handle = _devinfo_get_handle(device);
    if(device == NULL || devinfo_handle == NULL || devinfo_handle->config.msg_callback == NULL) {
        return;
    }

    devinfo_msg = devinfo_msg_parse_generic_reply(message);
    if(devinfo_msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_PARSE_ERROR, "msg(%s) parse error\r\n", message->topic);
        return;
    }

    if(ops != NULL) {
        ALOG_INFO(TAG, "%s devinfo success, msg_id %u\r\n", ops, devinfo_msg->msg_id);
    }

    if(devinfo_handle->config.msg_callback != NULL) {
        devinfo_handle->config.msg_callback(device, devinfo_msg, devinfo_handle->config.userdata);
    }

    /* 回收消息资源 */
    devinfo_msg_delete(devinfo_msg);
}

int32_t aiot_device_devinfo_set_callback(void *device, devinfo_callback_t callback, void *userdata)
{
    devinfo_handle_t *devinfo_handle = _devinfo_get_handle(device);
    if(device == NULL || devinfo_handle == NULL || callback == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_devinfo_set_callback input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    devinfo_handle->config.msg_callback = callback;
    devinfo_handle->config.userdata = userdata;

    aiot_device_register_topic_filter(device, DEVINFO_UPDATE_REPLY_TOPIC, _devinfo_msg_reply_callback, 0, "add");
    aiot_device_register_topic_filter(device, DEVINFO_DELETE_REPLY_TOPIC, _devinfo_msg_reply_callback, 0, "delete");

    return STATE_SUCCESS;
}

int32_t aiot_device_devinfo_add(void *device, char *key, char *value)
{
    aiot_msg_t *msg = NULL;
    int32_t res = STATE_SUCCESS;
    devinfo_handle_t *devinfo_handle = _devinfo_get_handle(device);
    if(device == NULL || devinfo_handle == NULL || key == NULL || value == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_devinfo_add input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    msg = devinfo_msg_create_add(device, key, value);
    if(msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_CREATE_ERROR, "devinfo message create error \r\n");
        return STATE_MESSAGE_CREATE_ERROR;
    }

    ALOG_INFO(TAG, "add devinfo key %s, value %s, msg_id %u\r\n", key, value, msg->id);

    res = aiot_device_send_message(device, msg);
    if(res >= STATE_SUCCESS) {
        res = msg->id;
    }
    aiot_msg_delete(msg);

    return res;
}

int32_t aiot_device_devinfo_delete(void *device, char *key)
{
    aiot_msg_t *msg = NULL;
    int32_t res = STATE_SUCCESS;
    devinfo_handle_t *devinfo_handle = _devinfo_get_handle(device);
    if(device == NULL || devinfo_handle == NULL || key == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_devinfo_delete input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    msg = devinfo_msg_create_delete(device, key);
    if(msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_CREATE_ERROR, "devinfo message create error \r\n");
        return STATE_MESSAGE_CREATE_ERROR;
    }

    ALOG_INFO(TAG, "delete devinfo key %s, msg_id %u\r\n", key, msg->id);
    res = aiot_device_send_message(device, msg);
    if(res >= STATE_SUCCESS) {
        res = msg->id;
    }

    aiot_msg_delete(msg);

    return res;
}

static void *_devinfo_init()
{
    devinfo_handle_t *devinfo_handle = (devinfo_handle_t *)core_os_malloc(sizeof(devinfo_handle_t));
    if(devinfo_handle == NULL) {
        return NULL;
    }

    memset(devinfo_handle, 0, sizeof(devinfo_handle_t));
    return devinfo_handle;
}
static void _devinfo_deinit(void *handle)
{
    core_os_free(handle);
}

/* 日志上报即插即用操作方法 */
static module_ops_t devinfo_ops = {
    .internal_ms = -1,
    .on_init = _devinfo_init,
    .on_process = NULL,
    .on_status = NULL,
    .on_deinit = _devinfo_deinit,
};

static devinfo_handle_t *_devinfo_get_handle(void *device)
{
    devinfo_handle_t *devinfo_handle = core_device_module_get(device, MODULE_DEVINFO);
    int32_t res = STATE_SUCCESS;

    if(devinfo_handle == NULL) {
        res = core_device_module_register(device, MODULE_DEVINFO, &devinfo_ops);
        if(res != STATE_SUCCESS) {
            return NULL;
        } else {
            devinfo_handle = core_device_module_get(device, MODULE_DEVINFO);
            devinfo_handle->device = device;
        }
    }
    return devinfo_handle;
}