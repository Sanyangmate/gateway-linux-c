#include "core_os.h"
#include "core_string.h"
#include "cJSON.h"
#include "device_private.h"
#include "aiot_state_api.h"
#include "aiot_message_api.h"
#include "devinfo_message.h"

static aiot_msg_t *_devinfo_msg_create_template(void *device, const char* fmt, cJSON *root)
{
    char *topic = NULL, *payload = NULL;
    char *src[] = { NULL, NULL };
    uint32_t sdevinfo_counter = 2;
    aiot_msg_t *result = NULL;

    src[0] = aiot_device_product_key(device);
    src[1] = aiot_device_name(device);

    if(STATE_SUCCESS != core_sprintf(NULL, &topic, fmt, src, sdevinfo_counter, "")) {
        return NULL;
    }

    payload = cJSON_PrintUnformatted(root);
    if (payload == NULL) {
        core_os_free(topic);
        return NULL;
    }

    result = aiot_msg_create_raw(topic, (uint8_t *)payload, strlen(payload));
    core_os_free(topic);
    core_os_free(payload);
    return result;
}

/* 创建添加标签的消息 */
aiot_msg_t *devinfo_msg_create_add(void *device, char *key, char *value)
{
    cJSON *root = NULL, *params = NULL, *devinfo = NULL;
    aiot_msg_t *result = NULL;
    int32_t id = 0;

    root = cJSON_CreateObject();
    if (root == NULL) {
        return NULL;
    }

    params = cJSON_CreateArray();
    if (params == NULL) {
        cJSON_Delete(root);
        return NULL;
    }

    id = core_device_get_next_alink_id(device);
    _cJSON_ADDInt2StrToObject(root, "id", id);
    cJSON_AddItemToObject(root, "params", params);
    devinfo = cJSON_CreateObject();
    cJSON_AddStringToObject(devinfo, "attrKey", key);
    cJSON_AddStringToObject(devinfo, "attrValue", value);
    cJSON_AddItemToArray(params, devinfo);

    result = _devinfo_msg_create_template(device, DEVINFO_UPDATE_TOPIC_FMT, root);
    result->id = id;
    cJSON_Delete(root);

    return result;
}

/* 创建删除标签的消息 */
aiot_msg_t *devinfo_msg_create_delete(void *device, char *key)
{
    cJSON *root = NULL, *params = NULL, *devinfo = NULL;
    aiot_msg_t *result = NULL;
    int32_t id = 0;

    root = cJSON_CreateObject();
    if (root == NULL) {
        return NULL;
    }

    params = cJSON_CreateArray();
    if (params == NULL) {
        cJSON_Delete(root);
        return NULL;
    }

    id = core_device_get_next_alink_id(device);
    _cJSON_ADDInt2StrToObject(root, "id", id);
    cJSON_AddItemToObject(root, "params", params);
    devinfo = cJSON_CreateObject();
    cJSON_AddStringToObject(devinfo, "attrKey", key);
    cJSON_AddItemToArray(params, devinfo);

    result = _devinfo_msg_create_template(device, DEVINFO_DELETE_TOPIC_FMT, root);
    result->id = id;
    cJSON_Delete(root);

    return result;
}

/* 删除 aiot_devinfo_msg_reply_t */
void devinfo_msg_delete(aiot_devinfo_msg_reply_t *msg)
{
    if(msg == NULL) {
        return;
    }

    if(msg->data != NULL) {
        core_os_free(msg->data);
    }

    if(msg->message != NULL) {
        core_os_free(msg->message);
    }

    core_os_free(msg);
}

/* 云端给的回复消息解析 */
aiot_devinfo_msg_reply_t *devinfo_msg_parse_generic_reply(const aiot_msg_t *msg)
{
    aiot_devinfo_msg_reply_t *recv = NULL;
    cJSON *root = NULL, *item = NULL;
    if(msg == NULL) {
        return NULL;
    }

    recv = (aiot_devinfo_msg_reply_t *)core_os_malloc(sizeof(aiot_devinfo_msg_reply_t));
    if(recv == NULL) {
        return NULL;
    }
    memset(recv, 0, sizeof(aiot_devinfo_msg_reply_t));

    root = cJSON_ParseWithLength((char *)msg->payload, msg->payload_lenth);
    if(root == NULL) {
        devinfo_msg_delete(recv);
        return NULL;
    }

    if(NULL != (item = cJSON_GetObjectItem(root, "code"))) {
        recv->code = item->valueint;
    } else {
        devinfo_msg_delete(recv);
        cJSON_Delete(root);
        return NULL;
    }

    if(NULL != (item = cJSON_GetObjectItem(root, "data"))) {
        recv->data = cJSON_PrintUnformatted(item);
        recv->data_len = strlen(recv->data);
    }

    if(NULL != (item = cJSON_GetObjectItem(root, "message"))) {
        recv->message = cJSON_PrintUnformatted(item);
        recv->message_len = strlen(recv->message);
    }

    if(NULL != (item = cJSON_GetObjectItem(root, "id")) && item->valuestring != NULL) {
        core_str2uint(item->valuestring, strlen(item->valuestring), &recv->msg_id);
    }

    cJSON_Delete(root);
    return recv;
}