#include "dm_message.h"
#include "device_private.h"
#include "core_os.h"
#include "core_string.h"
#include "aiot_state_api.h"
#include "cJSON.h"
#include "aiot_message_api.h"

typedef struct {
    char *msg_id;
    char *rrpc_id;
    int32_t cid;
    char *rrpc_topic;
} dm_msg_context_t;

static dm_msg_context_t *dm_msg_context_init()
{
    dm_msg_context_t *context = (dm_msg_context_t *)core_os_malloc(sizeof(dm_msg_context_t));
    if(context == NULL) {
        return NULL;
    }
    memset(context, 0, sizeof(dm_msg_context_t));
    context->msg_id = NULL;
    context->rrpc_id = NULL;
    context->cid = 0;
    context->rrpc_topic = NULL;

    return context;
}

void* dm_msg_context_clone(void *ctx)
{
    dm_msg_context_t *src_ctx = (dm_msg_context_t *)ctx;
    dm_msg_context_t *dst_ctx = (dm_msg_context_t *)core_os_malloc(sizeof(dm_msg_context_t));
    if(src_ctx == NULL || dst_ctx == NULL) {
        return NULL;
    }
    memset(dst_ctx, 0, sizeof(dm_msg_context_t));

    dst_ctx->cid = src_ctx->cid;
    if(src_ctx->msg_id != NULL) {
        core_strdup(NULL, &dst_ctx->msg_id, src_ctx->msg_id, "");
    }

    if(src_ctx->rrpc_id != NULL) {
        core_strdup(NULL, &dst_ctx->rrpc_id, src_ctx->rrpc_id, "");
    }

    if(src_ctx->rrpc_topic != NULL) {
        core_strdup(NULL, &dst_ctx->rrpc_topic, src_ctx->rrpc_topic, "");
    }

    return dst_ctx;
}
void dm_msg_context_deinit(void *ctx)
{
    dm_msg_context_t *context = (dm_msg_context_t *)ctx;
    if(context == NULL) {
        return;
    }

    if(context->msg_id != NULL) {
        core_os_free(context->msg_id);
    }

    if(context->rrpc_id != NULL) {
        core_os_free(context->rrpc_id);
    }

    if(context->rrpc_topic != NULL) {
        core_os_free(context->rrpc_topic);
    }

    core_os_free(context);
}

aiot_msg_t *_dm_post_msg_fmt_extend(void *device, const char* fmt, char *event_id, cJSON *root)
{
    char *topic = NULL, *payload = NULL;
    char *src[] = { NULL, NULL, event_id};
    uint32_t src_counter = 3;
    aiot_msg_t *result = NULL;

    if(event_id == NULL) {
        src_counter = 2;
    }

    src[0] = aiot_device_product_key(device);
    src[1] = aiot_device_name(device);

    if(STATE_SUCCESS != core_sprintf(NULL, &topic, fmt, src, src_counter, "")) {
        return NULL;
    }

    payload = cJSON_PrintUnformatted(root);
    if (payload == NULL) {
        core_os_free(topic);
        return NULL;
    }

    result = aiot_msg_create_raw(topic, (uint8_t *)payload, strlen(payload));
    core_os_free(topic);
    core_os_free(payload);
    return result;
}

int32_t _dm_msg_get_pk_dn(const aiot_msg_t *msg, char *pk, char *dn, char *service_id)
{
    char *topic = NULL;
    uint32_t topic_len = 0, i = 0, level = 0, start = 0;

    if(msg == NULL || msg->topic == NULL) {
        return -1;
    }
    topic = msg->topic;
    topic_len = strlen(msg->topic);

    for (i = 0; i < topic_len; i++) {
        if (topic[i] == '/') {
            if(level == 2 && pk != NULL) {
                /* 读取pk */
                memcpy(pk, topic + start + 1, i - start - 1);
            } else if(level == 3 && dn != NULL) {
                /* 读取dn */
                memcpy(dn, topic + start + 1, i - start - 1);
                if(service_id == NULL) {
                    return STATE_SUCCESS;
                }
            }
            else if(level == 6 && service_id != NULL) {
                /* 读取dn */
                memcpy(service_id, topic + start + 1, i - start - 1);
                return STATE_SUCCESS;
            }
            start = i;
            level++;
        }
    }

    if(level == 6 && service_id != NULL) {
        /* 读取dn */
        memcpy(service_id, topic + start + 1, i - start - 1);
        return STATE_SUCCESS;
    }

    return -1;
}

aiot_msg_t *_dm_reply_msg_fmt_extend(void *device, const char* fmt, cJSON *root)
{
    char *topic = NULL, *payload = NULL;
    char *src[] = { NULL, NULL };
    uint32_t src_counter = 2;
    aiot_msg_t *result = NULL;

    src[0] = aiot_device_product_key(device);
    src[1] = aiot_device_name(device);

    if(STATE_SUCCESS != core_sprintf(NULL, &topic, fmt, src, src_counter, "")) {
        return NULL;
    }

    payload = cJSON_PrintUnformatted(root);
    if (payload == NULL) {
        core_os_free(topic);
        return NULL;
    }

    result = aiot_msg_create_raw(topic, (uint8_t *)payload, strlen(payload));
    core_os_free(topic);
    core_os_free(payload);
    return result;
}

/* 主动发送的消息 */
aiot_msg_t *dm_msg_create_property_post(void *device, char *params, int8_t ack)
{
    cJSON *root = NULL, *sys = NULL, *params_item = NULL;
    aiot_msg_t *result = NULL;
    int32_t id = 0;
    root = cJSON_CreateObject();
    if (root == NULL) {
        return NULL;
    }

    sys = cJSON_CreateObject();
    if (sys == NULL) {
        return NULL;
    }

    params_item = cJSON_Parse(params);
    if(params_item == NULL) {
        return NULL;
    }
    cJSON_AddItemToObject(root, "params", params_item);

    id = core_device_get_next_alink_id(device);
    _cJSON_ADDInt2StrToObject(root, "id", id);

    cJSON_AddStringToObject(root, "version", ALINK_VERSION);
    cJSON_AddItemToObject(root, "sys", sys);
    cJSON_AddNumberToObject(sys, "ack", ack);

    result = _dm_post_msg_fmt_extend(device, DM_PROPERTY_POST_TOPIC_FMT, NULL, root);
    if(result != NULL) {
        result->id = id;
    }

    cJSON_Delete(root);

    return result;
}

aiot_msg_t *dm_msg_create_event_post(void *device, char *event_id, char *params, int8_t ack)
{
    cJSON *root = NULL, *sys = NULL, *params_item = NULL;
    aiot_msg_t *result = NULL;
    int32_t id = 0;

    root = cJSON_CreateObject();
    if (root == NULL) {
        return NULL;
    }

    sys = cJSON_CreateObject();
    if (sys == NULL) {
        return NULL;
    }

    params_item = cJSON_Parse(params);
    if(params_item == NULL) {
        return NULL;
    }
    cJSON_AddItemToObject(root, "params", params_item);

    id = core_device_get_next_alink_id(device);
    _cJSON_ADDInt2StrToObject(root, "id", id);

    cJSON_AddStringToObject(root, "version", ALINK_VERSION);
    cJSON_AddItemToObject(root, "sys", sys);
    cJSON_AddNumberToObject(sys, "ack", ack);

    result = _dm_post_msg_fmt_extend(device, DM_EVENT_POST_TOPIC_FMT, event_id, root);
    if(result != NULL) {
        result->id = id;
    }

    cJSON_Delete(root);

    return result;
}

aiot_msg_t *dm_msg_create_batch_post(void *device, char *params, int8_t ack)
{
    cJSON *root = NULL, *sys = NULL, *params_item = NULL;
    aiot_msg_t *result = NULL;
    int32_t id = 0;

    root = cJSON_CreateObject();
    if (root == NULL) {
        return NULL;
    }

    sys = cJSON_CreateObject();
    if (sys == NULL) {
        return NULL;
    }
    params_item = cJSON_Parse(params);
    if(params_item == NULL) {
        return NULL;
    }
    cJSON_AddItemToObject(root, "params", params_item);

    id = core_device_get_next_alink_id(device);
    _cJSON_ADDInt2StrToObject(root, "id", id);

    cJSON_AddStringToObject(root, "version", ALINK_VERSION);
    cJSON_AddItemToObject(root, "sys", sys);
    cJSON_AddNumberToObject(sys, "ack", ack);

    result = _dm_post_msg_fmt_extend(device, DM_BATCH_POST_TOPIC_FMT, NULL, root);
    if(result != NULL) {
        result->id = id;
    }

    cJSON_Delete(root);

    return result;
}
aiot_msg_t *dm_msg_create_propertyset_reply(void *device, void *context, uint32_t code, char *data)
{
    cJSON *root = NULL;
    aiot_msg_t *result = NULL;
    dm_msg_context_t *ctx = (dm_msg_context_t *)context;

    root = cJSON_CreateObject();
    if (root == NULL) {
        return NULL;
    }

    cJSON_AddStringToObject(root, "id", ctx->msg_id);
    cJSON_AddNumberToObject(root, "code", code);
    cJSON_AddRawToObject(root, "data", data);

    result = _dm_reply_msg_fmt_extend(device, DM_PROPERTY_SET_REPLY_TOPIC_FMT, root);

    cJSON_Delete(root);

    return result;
}
aiot_msg_t *dm_msg_create_service_reply(void *device, void *context, char *service_id, uint32_t code, char *data)
{
    cJSON *root = NULL;
    aiot_msg_t *result = NULL;
    char *payload = NULL;
    char *topic = NULL;
    char *src[] = { NULL, NULL, service_id};
    uint32_t src_counter = 3;
    dm_msg_context_t *ctx = (dm_msg_context_t *)context;

    if(ctx->rrpc_topic == NULL) {
        src[0] = aiot_device_product_key(device);
        src[1] = aiot_device_name(device);

        if(STATE_SUCCESS != core_sprintf(NULL, &topic, DM_SERVICE_INVOKE_REPLY_TOPIC_FMT, src, src_counter, "")) {
            return NULL;
        }
    } else {
        src[0] = ctx->rrpc_id;
        src[1] = ctx->rrpc_topic;
        src_counter = 2;
        if(STATE_SUCCESS != core_sprintf(NULL, &topic, "/ext/rrpc/%s%s", src, src_counter, "")) {
            return NULL;
        }
    }

    root = cJSON_CreateObject();
    if (root == NULL) {
        return NULL;
    }
    cJSON_AddStringToObject(root, "id", ctx->msg_id);
    cJSON_AddNumberToObject(root, "code", code);
    cJSON_AddRawToObject(root, "data", data);
    payload = cJSON_PrintUnformatted(root);
    if (payload == NULL) {
        cJSON_Delete(root);
        return NULL;
    }
    cJSON_Delete(root);

    result = aiot_msg_create_raw(topic, (uint8_t *)payload, strlen(payload));
    if(topic != NULL) {
        core_os_free(topic);
    }
    core_os_free(payload);

    if(ctx->rrpc_topic != NULL) {
        result->cid = ctx->cid;
    }

    return result;
}

aiot_msg_t *dm_msg_create_get_desired(void *device, char *params, int8_t ack)
{
    cJSON *root = NULL, *sys = NULL, *params_item = NULL;
    aiot_msg_t *result = NULL;
    int32_t id = 0;

    root = cJSON_CreateObject();
    if (root == NULL) {
        return NULL;
    }

    sys = cJSON_CreateObject();
    if (sys == NULL) {
        return NULL;
    }

    params_item = cJSON_Parse(params);
    if(params_item == NULL) {
        return NULL;
    }
    cJSON_AddItemToObject(root, "params", params_item);

    id = core_device_get_next_alink_id(device);
    _cJSON_ADDInt2StrToObject(root, "id", id);
    cJSON_AddStringToObject(root, "version", ALINK_VERSION);
    cJSON_AddItemToObject(root, "sys", sys);
    cJSON_AddNumberToObject(sys, "ack", ack);

    result = _dm_post_msg_fmt_extend(device, DM_GET_DESIRED_TOPIC_FMT, NULL, root);
    if(result != NULL) {
        result->id = id;
    }

    cJSON_Delete(root);

    return result;
}
aiot_msg_t *dm_msg_create_delete_desired(void *device, char *params, int8_t ack)
{
    cJSON *root = NULL, *sys = NULL, *params_item = NULL;
    aiot_msg_t *result = NULL;
    int32_t id = 0;

    root = cJSON_CreateObject();
    if (root == NULL) {
        return NULL;
    }

    sys = cJSON_CreateObject();
    if (sys == NULL) {
        return NULL;
    }

    params_item = cJSON_Parse(params);
    if(params_item == NULL) {
        return NULL;
    }
    cJSON_AddItemToObject(root, "params", params_item);

    id = core_device_get_next_alink_id(device);
    _cJSON_ADDInt2StrToObject(root, "id", id);
    cJSON_AddStringToObject(root, "version", ALINK_VERSION);
    cJSON_AddItemToObject(root, "sys", sys);
    cJSON_AddNumberToObject(sys, "ack", ack);

    result = _dm_post_msg_fmt_extend(device, DM_DELETE_DESIRED_TOPIC_FMT, NULL, root);
    if(result != NULL) {
        result->id = id;
    }

    cJSON_Delete(root);

    return result;
}


void dm_msg_delete(aiot_dm_msg_t *msg)
{
    if(msg == NULL) {
        return;
    }

    switch(msg->type)
    {
    case AIOT_DMRECV_POST_REPLY:
    case AIOT_DMRECV_BATCH_POST_REPLY:
    case AIOT_DMRECV_GET_DESIRED_REPLY:
    case AIOT_DMRECV_DELETE_DESIRED_REPLY:
        if(msg->data.generic_reply.data) {
            core_os_free(msg->data.generic_reply.data);
        }

        if(msg->data.generic_reply.message) {
            core_os_free(msg->data.generic_reply.message);
        }

        break;
    case AIOT_DMRECV_PROPERTY_SET:
        if(msg->data.property_set.params) {
            core_os_free(msg->data.property_set.params);
        }

        break;
    case AIOT_DMRECV_SERVICE_INVOKE:
        if(msg->data.service_invoke.service_id) {
            core_os_free(msg->data.service_invoke.service_id);
        }

        if(msg->data.service_invoke.params) {
            core_os_free(msg->data.service_invoke.params);
        }
        break;
    case AIOT_DMRECV_RAW_REPLY:
        if(msg->data.raw_data.data) {
            core_os_free(msg->data.raw_data.data);
        }
        break;
    case AIOT_DMRECV_RAW_DOWN:
        if(msg->data.raw_data.data) {
            core_os_free(msg->data.raw_data.data);
        }
        break;
    default:
        break;
    }
    dm_msg_context_deinit(msg->context);
    core_os_free(msg);
}
/* 接收消息后解析消息 */
aiot_dm_msg_t *dm_msg_parse_generic_reply(const aiot_msg_t *msg)
{
    aiot_dm_msg_t *recv = NULL;
    cJSON *root = NULL, *item = NULL;
    int32_t len = 0;
    dm_msg_context_t *context = dm_msg_context_init();
    if(msg == NULL || context == NULL) {
        return NULL;
    }

    recv = (aiot_dm_msg_t *)core_os_malloc(sizeof(aiot_dm_msg_t));
    if(recv == NULL) {
        return NULL;
    }
    memset(recv, 0, sizeof(aiot_dm_msg_t));
    recv->type = AIOT_DMRECV_POST_REPLY;

    root = cJSON_ParseWithLength((char *)msg->payload, msg->payload_lenth);
    if(root == NULL) {
        dm_msg_delete(recv);
        return NULL;
    }

    if(NULL != (item = cJSON_GetObjectItem(root, ALINK_JSON_KEY_CODE))) {
        recv->data.generic_reply.code = item->valueint;
    } else {
        dm_msg_context_deinit(context);
        dm_msg_delete(recv);
        cJSON_Delete(root);
        return NULL;
    }

    if(NULL != (item = cJSON_GetObjectItem(root, ALINK_JSON_KEY_DATA))) {
        recv->data.generic_reply.data = cJSON_PrintUnformatted(item);
        recv->data.generic_reply.data_len = strlen(recv->data.generic_reply.data);
    }

    if(NULL != (item = cJSON_GetObjectItem(root, ALINK_JSON_KEY_MESSAGE))) {
        recv->data.generic_reply.message = cJSON_PrintUnformatted(item);
        recv->data.generic_reply.message_len = strlen(recv->data.generic_reply.message);
    }

    if(NULL != (item = cJSON_GetObjectItem(root, ALINK_JSON_KEY_ID)) && item->valuestring != NULL) {
        core_strdup(NULL, &context->msg_id, item->valuestring, "");
        len = strlen(item->valuestring);
        core_str2uint(context->msg_id, len, &recv->data.generic_reply.msg_id);
        recv->context = context;
    }

    cJSON_Delete(root);
    return recv;
}
aiot_dm_msg_t *dm_msg_parse_property_set(const aiot_msg_t *msg)
{
    aiot_dm_msg_t *recv = NULL;
    cJSON *root = NULL, *item = NULL;
    dm_msg_context_t *context = dm_msg_context_init();
    if(msg == NULL || context == NULL) {
        return NULL;
    }

    recv = (aiot_dm_msg_t *)core_os_malloc(sizeof(aiot_dm_msg_t));
    if(recv == NULL) {
        return NULL;
    }
    memset(recv, 0, sizeof(aiot_dm_msg_t));
    recv->type = AIOT_DMRECV_PROPERTY_SET;

    root = cJSON_ParseWithLength((char *)msg->payload, msg->payload_lenth);
    if(root == NULL) {
        dm_msg_context_deinit(context);
        dm_msg_delete(recv);
        return NULL;
    }

    if(NULL != (item = cJSON_GetObjectItem(root, ALINK_JSON_KEY_PARAMS))) {
        recv->data.property_set.params = cJSON_PrintUnformatted(item);
        recv->data.property_set.params_len = strlen(recv->data.property_set.params);
    }

    if(NULL != (item = cJSON_GetObjectItem(root, ALINK_JSON_KEY_ID)) && item->valuestring != NULL) {
        core_strdup(NULL, &context->msg_id, item->valuestring, "");
        recv->context = context;
    }

    cJSON_Delete(root);
    return recv;
}
aiot_dm_msg_t *dm_msg_parse_service_invoke(const aiot_msg_t *msg)
{
    aiot_dm_msg_t *recv = NULL;
    cJSON *root = NULL, *item = NULL;
    char service_id[128];
    dm_msg_context_t *context = dm_msg_context_init();
    if(msg == NULL || context == NULL) {
        return NULL;
    }

    memset(service_id, 0, sizeof(service_id));
    if(STATE_SUCCESS != _dm_msg_get_pk_dn(msg, NULL, NULL, service_id)) {
        return NULL;
    }

    recv = (aiot_dm_msg_t *)core_os_malloc(sizeof(aiot_dm_msg_t));
    if(recv == NULL) {
        return NULL;
    }
    memset(recv, 0, sizeof(aiot_dm_msg_t));
    recv->type = AIOT_DMRECV_SERVICE_INVOKE;
    core_strdup(NULL, &recv->data.service_invoke.service_id, service_id, NULL);

    root = cJSON_ParseWithLength((char *)msg->payload, msg->payload_lenth);
    if(root == NULL) {
        dm_msg_context_deinit(context);
        dm_msg_delete(recv);
        return NULL;
    }

    if(NULL != (item = cJSON_GetObjectItem(root, ALINK_JSON_KEY_PARAMS))) {
        recv->data.service_invoke.params = cJSON_PrintUnformatted(item);
        recv->data.service_invoke.params_len = strlen(recv->data.service_invoke.params);
    } else {
        dm_msg_context_deinit(context);
        dm_msg_delete(recv);
        cJSON_Delete(root);
        return NULL;
    }

    recv->context = context;
    if(NULL != (item = cJSON_GetObjectItem(root, ALINK_JSON_KEY_ID)) && item->valuestring != NULL) {
        core_strdup(NULL, &context->msg_id, item->valuestring, "");
    }

    if(msg->rrpc_id != NULL) {
        core_strdup(NULL, &context->rrpc_id, msg->rrpc_id, "");
        context->cid = msg->cid;
        core_strdup(NULL, &context->rrpc_topic, msg->topic, "");
    }

    cJSON_Delete(root);
    return recv;
}

aiot_msg_t *dm_msg_create_up_raw(void *device, uint8_t *data, uint32_t data_len)
{
    aiot_msg_t *result = NULL;
    char *topic = NULL;
    char *src[] = { NULL, NULL };
    uint32_t src_counter = 2;

    src[0] = aiot_device_product_key(device);
    src[1] = aiot_device_name(device);

    if(STATE_SUCCESS != core_sprintf(NULL, &topic, DM_UP_RAW_TOPIC_FMT, src, src_counter, "")) {
        return NULL;
    }

    result = aiot_msg_create_raw(topic, (uint8_t *)data, data_len);
    core_os_free(topic);

    return result;
}

aiot_msg_t *dm_msg_create_down_raw_reply(void *device, uint8_t *data, uint32_t data_len)
{
    aiot_msg_t *result = NULL;
    char *topic = NULL;
    char *src[] = { NULL, NULL };
    uint32_t src_counter = 2;

    src[0] = aiot_device_product_key(device);
    src[1] = aiot_device_name(device);

    if(STATE_SUCCESS != core_sprintf(NULL, &topic, DM_DOWN_RAW_REPLY_TOPIC_FMT, src, src_counter, "")) {
        return NULL;
    }

    result = aiot_msg_create_raw(topic, (uint8_t *)data, data_len);
    core_os_free(topic);

    return result;
}

aiot_dm_msg_t *dm_msg_parse_up_raw_reply(const aiot_msg_t *msg)
{
    aiot_dm_msg_t *recv = NULL;
    if(msg == NULL) {
        return NULL;
    }

    recv = (aiot_dm_msg_t *)core_os_malloc(sizeof(aiot_dm_msg_t));
    if(recv == NULL) {
        return NULL;
    }
    memset(recv, 0, sizeof(aiot_dm_msg_t));
    recv->type = AIOT_DMRECV_RAW_REPLY;

    recv->data.raw_data.data = core_os_malloc(msg->payload_lenth);
    if( recv->data.raw_data.data == NULL ) {
        core_os_free(recv);
        return NULL;
    }
    memcpy(recv->data.raw_data.data, msg->payload, msg->payload_lenth);
    recv->data.raw_data.data_len = msg->payload_lenth;

    return recv;
}

aiot_dm_msg_t *dm_msg_parse_down_raw(const aiot_msg_t *msg)
{
    aiot_dm_msg_t *recv = NULL;
    if(msg == NULL) {
        return NULL;
    }

    recv = (aiot_dm_msg_t *)core_os_malloc(sizeof(aiot_dm_msg_t));
    if(recv == NULL) {
        return NULL;
    }
    memset(recv, 0, sizeof(aiot_dm_msg_t));
    recv->type = AIOT_DMRECV_RAW_DOWN;

    recv->data.raw_data.data = core_os_malloc(msg->payload_lenth);
    if( recv->data.raw_data.data == NULL ) {
        core_os_free(recv);
        return NULL;
    }
    memcpy(recv->data.raw_data.data, msg->payload, msg->payload_lenth);
    recv->data.raw_data.data_len = msg->payload_lenth;

    return recv;
}