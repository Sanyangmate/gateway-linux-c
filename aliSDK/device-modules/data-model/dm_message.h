

#ifndef _DATA_MODEL_MESSAGE_H_
#define _DATA_MODEL_MESSAGE_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>
#include "aiot_message_api.h"
#include "aiot_dm_api.h"

/* 属性及事件上报topic */
#define DM_PROPERTY_POST_TOPIC_FMT          "/sys/%s/%s/thing/event/property/post"
#define DM_EVENT_POST_TOPIC_FMT             "/sys/%s/%s/thing/event/%s/post"
/* 属性或事件上报回复topic */
#define DM_POST_REPLY_TOPIC_FMT             "/sys/+/+/thing/event/+/post_reply"

/* 批量属性及事件上报topic */
#define DM_BATCH_POST_TOPIC_FMT             "/sys/%s/%s/thing/event/property/batch/post"
/* 批量属性及事件上报回复topic */
#define DM_BATCH_POST_REPLY_TOPIC_FMT       "/sys/+/+/thing/event/property/batch/post_reply"


/* 属性设置topic */
#define DM_PROPERTY_SET_TOPIC_FMT           "/sys/+/+/thing/service/property/set"
/* 属性设置回复topic */
#define DM_PROPERTY_SET_REPLY_TOPIC_FMT     "/sys/%s/%s/thing/service/property/set_reply"

/* 服务调用topic */
#define DM_SERVICE_INVOKE_TOPIC_FMT         "/sys/+/+/thing/service/+"
/* 服务调用回复topic */
#define DM_SERVICE_INVOKE_REPLY_TOPIC_FMT   "/sys/%s/%s/thing/service/%s_reply"

/* 获取期望属性topic */
#define DM_GET_DESIRED_TOPIC_FMT            "/sys/%s/%s/thing/property/desired/get"
/* 获取期望属性回复topic */
#define DM_GET_DESIRED_REPLY_TOPIC_FMT      "/sys/+/+/thing/property/desired/get_reply"
/* 删除期望属性topic */
#define DM_DELETE_DESIRED_TOPIC_FMT         "/sys/%s/%s/thing/property/desired/delete"
/* 删除期望属性回复topic */
#define DM_DELETE_DESIRED_REPLY_TOPIC_FMT   "/sys/+/+/thing/property/desired/delete_reply"


/* 透传属性及事件上报topic */
#define DM_UP_RAW_TOPIC_FMT                 "/sys/%s/%s/thing/model/up_raw"
/* 透传属性及事件上报回复topic */
#define DM_UP_RAW_REPLY_TOPIC_FMT           "/sys/+/+/thing/model/up_raw_reply"
/* 透传下行调用(属性设置/服务调用)topic */
#define DM_DOWN_RAW_TOPIC_FMT               "/sys/+/+/thing/model/down_raw"
/* 透传下行调用(属性设置/服务调用)回复topic */
#define DM_DOWN_RAW_REPLY_TOPIC_FMT         "/sys/%s/%s/thing/model/down_raw_reply"

/* ALINK请求的JSON格式 */
#define ALINK_REQUEST_FMT               "{\"id\":\"%s\",\"version\":\"1.0\",\"params\":%s,\"sys\":{\"ack\":%s}}"
/* ALINK应答的JSON格式 */
#define ALINK_RESPONSE_FMT              "{\"id\":\"%s\",\"code\":%s,\"data\":%s}"
#define ALINK_JSON_KEY_ID               "id"
#define ALINK_JSON_KEY_CODE             "code"
#define ALINK_JSON_KEY_PARAMS           "params"
#define ALINK_JSON_KEY_DATA             "data"
#define ALINK_JSON_KEY_MESSAGE          "message"
#define ALINK_VERSION                   "1.0"

/* 主动发送的消息 */
aiot_msg_t *dm_msg_create_property_post(void *device, char *params, int8_t ack);
aiot_msg_t *dm_msg_create_event_post(void *device, char *event_id, char *params, int8_t ack);
aiot_msg_t *dm_msg_create_batch_post(void *device, char *params, int8_t ack);
aiot_msg_t *dm_msg_create_get_desired(void *device, char *params, int8_t ack);
aiot_msg_t *dm_msg_create_delete_desired(void *device, char *params, int8_t ack);

/* 向云端回复的消息 */
aiot_msg_t *dm_msg_create_propertyset_reply(void *device, void *context, uint32_t code, char *data);
aiot_msg_t *dm_msg_create_service_reply(void *device, void *context, char *service_id, uint32_t code, char *data);

/* 接收消息后解析消息 */
void *dm_msg_context_clone(void *ctx);
void dm_msg_context_deinit(void *ctx);
aiot_dm_msg_t *dm_msg_parse_generic_reply(const aiot_msg_t *msg);
aiot_dm_msg_t *dm_msg_parse_property_set(const aiot_msg_t *msg);
aiot_dm_msg_t *dm_msg_parse_service_invoke(const aiot_msg_t *msg);

/* 透传消息处理定义 */
aiot_msg_t *dm_msg_create_up_raw(void *device, uint8_t *data, uint32_t data_len);
aiot_msg_t *dm_msg_create_down_raw_reply(void *device, uint8_t *data, uint32_t data_len);
aiot_dm_msg_t *dm_msg_parse_up_raw_reply(const aiot_msg_t *msg);
aiot_dm_msg_t *dm_msg_parse_down_raw(const aiot_msg_t *msg);

void dm_msg_delete(aiot_dm_msg_t *msg);
#if defined(__cplusplus)
}
#endif

#endif