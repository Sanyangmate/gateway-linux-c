#include "aiot_dm_api.h"
#include "device_private.h"
#include "aiot_state_api.h"
#include "dm_message.h"
#include "core_string.h"
#include "core_log.h"
#include "core_os.h"

#define TAG "DM"

typedef struct {
    dm_msg_callback_t msg_callback;
    void *userdata;
} dm_config_t;

typedef struct {
    int32_t counter;
    int32_t has_subscriber;
} dm_runtime_t;

typedef struct {
    void *device;
    dm_config_t config;
    dm_runtime_t runtime;
} dm_handle_t;

static module_ops_t dm_ops;
static int32_t _sub_batch_post_topic(dm_handle_t *dm_handle);

static dm_handle_t *_dm_get_handle(void *device)
{
    dm_handle_t *dm_handle = core_device_module_get(device, MODULE_DM);
    int32_t res = STATE_SUCCESS;

    if(dm_handle == NULL) {
        res = core_device_module_register(device, MODULE_DM, &dm_ops);
        if(res != STATE_SUCCESS) {
            return NULL;
        } else {
            dm_handle = core_device_module_get(device, MODULE_DM);
            dm_handle->device = device;
            if(1 == aiot_device_online(device)) {
                _sub_batch_post_topic(dm_handle);
            }
        }
    }
    return dm_handle;
}

static void* _dm_init()
{
    dm_handle_t *dm_handle = (dm_handle_t *)core_os_malloc(sizeof(dm_handle_t));
    if(dm_handle == NULL) {
        return NULL;
    }

    memset(dm_handle, 0, sizeof(dm_handle_t));
    return dm_handle;
}

static void _dm_status_event(void *handle, aiot_device_status_t event)
{
    dm_handle_t *dm_handle = (dm_handle_t *)handle;
    if(event.type == AIOT_DEVICE_STATUS_CONNECT && dm_handle->runtime.has_subscriber == 0) {
        _sub_batch_post_topic(dm_handle);
    }
}

static void _dm_deinit(void *handle)
{
    core_os_free(handle);
}

static module_ops_t dm_ops = {
    .internal_ms = 1000,
    .on_init = _dm_init,
    .on_process = NULL,
    .on_status = _dm_status_event,
    .on_deinit = _dm_deinit,
};

typedef struct {
    char* topic;
    aiot_device_msg_callback_t callback;
} _dm_topic_filter;

static void _dm_msg_generic_reply_callback(void *device, const aiot_msg_t *message, void *userdata)
{
    aiot_dm_msg_t *dm_msg = NULL;
    dm_handle_t *dm_handle = _dm_get_handle(device);
    if(device == NULL || dm_handle == NULL || dm_handle->config.msg_callback == NULL) {
        return;
    }

    dm_msg = dm_msg_parse_generic_reply(message);
    if(dm_msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_PARSE_ERROR, "msg(%s) json parse error\r\n", message->topic);
        return;
    }

    dm_msg->type = AIOT_DMRECV_POST_REPLY;
    dm_handle->config.msg_callback(device, dm_msg, dm_handle->config.userdata);
    dm_msg_delete(dm_msg);
}

static void _dm_msg_batch_post_reply_callback(void *device, const aiot_msg_t *message, void *userdata)
{
    aiot_dm_msg_t *dm_msg = NULL;
    dm_handle_t *dm_handle = _dm_get_handle(device);
    if(device == NULL || dm_handle == NULL || dm_handle->config.msg_callback == NULL) {
        return;
    }

    dm_msg = dm_msg_parse_generic_reply(message);
    if(dm_msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_PARSE_ERROR, "msg(%s) json parse error\r\n", message->topic);
        return;
    }

    dm_msg->type = AIOT_DMRECV_BATCH_POST_REPLY;
    dm_handle->config.msg_callback(device, dm_msg, dm_handle->config.userdata);
    dm_msg_delete(dm_msg);
}

static void _dm_msg_property_set_callback(void *device, const aiot_msg_t *message, void *userdata)
{
    aiot_dm_msg_t *dm_msg = NULL;
    dm_handle_t *dm_handle = _dm_get_handle(device);
    if(device == NULL || dm_handle == NULL || dm_handle->config.msg_callback == NULL) {
        return;
    }

    dm_msg = dm_msg_parse_property_set(message);
    if(dm_msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_PARSE_ERROR, "msg(%s) json parse error\r\n", message->topic);
        return;
    }

    dm_msg->type = AIOT_DMRECV_PROPERTY_SET;
    dm_handle->config.msg_callback(device, dm_msg, dm_handle->config.userdata);
    dm_msg_delete(dm_msg);
}

static void _dm_msg_service_invoke_callback(void *device, const aiot_msg_t *message, void *userdata)
{
    aiot_dm_msg_t *dm_msg = NULL;
    dm_handle_t *dm_handle = _dm_get_handle(device);
    if(device == NULL || dm_handle == NULL || dm_handle->config.msg_callback == NULL) {
        return;
    }

    dm_msg = dm_msg_parse_service_invoke(message);
    if(dm_msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_PARSE_ERROR, "msg(%s) json parse error\r\n", message->topic);
        return;
    }

    dm_msg->type = AIOT_DMRECV_SERVICE_INVOKE;
    dm_handle->config.msg_callback(device, dm_msg, dm_handle->config.userdata);
    dm_msg_delete(dm_msg);
}

static void _dm_msg_desired_get_reply_callback(void *device, const aiot_msg_t *message, void *userdata)
{
    aiot_dm_msg_t *dm_msg = NULL;
    dm_handle_t *dm_handle = _dm_get_handle(device);
    if(device == NULL || dm_handle == NULL || dm_handle->config.msg_callback == NULL) {
        return;
    }

    dm_msg = dm_msg_parse_generic_reply(message);
    if(dm_msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_PARSE_ERROR, "msg(%s) json parse error\r\n", message->topic);
        return;
    }

    dm_msg->type = AIOT_DMRECV_GET_DESIRED_REPLY;
    dm_handle->config.msg_callback(device, dm_msg, dm_handle->config.userdata);
    dm_msg_delete(dm_msg);
}

static void _dm_msg_desired_delete_reply_callback(void *device, const aiot_msg_t *message, void *userdata)
{
    aiot_dm_msg_t *dm_msg = NULL;
    dm_handle_t *dm_handle = _dm_get_handle(device);
    if(device == NULL || dm_handle == NULL || dm_handle->config.msg_callback == NULL) {
        return;
    }

    dm_msg = dm_msg_parse_generic_reply(message);
    if(dm_msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_PARSE_ERROR, "msg(%s) json parse error\r\n", message->topic);
        return;
    }

    dm_msg->type = AIOT_DMRECV_DELETE_DESIRED_REPLY;
    dm_handle->config.msg_callback(device, dm_msg, dm_handle->config.userdata);
    dm_msg_delete(dm_msg);
}

/* 主动上报透传数据后，云端回复 */
static void _dm_msg_up_raw_reply_callback(void *device, const aiot_msg_t *message, void *userdata)
{
    aiot_dm_msg_t *dm_msg = NULL;
    dm_handle_t *dm_handle = _dm_get_handle(device);
    if(device == NULL || dm_handle == NULL || dm_handle->config.msg_callback == NULL) {
        return;
    }

    dm_msg = dm_msg_parse_up_raw_reply(message);
    if(dm_msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_PARSE_ERROR, "msg(%s) parse error\r\n", message->topic);
        return;
    }

    dm_msg->type = AIOT_DMRECV_RAW_REPLY;
    dm_handle->config.msg_callback(device, dm_msg, dm_handle->config.userdata);
    dm_msg_delete(dm_msg);
}

/* 云端主动下发的消息处理 */
static void _dm_msg_down_raw_callback(void *device, const aiot_msg_t *message, void *userdata)
{
    aiot_dm_msg_t *dm_msg = NULL;
    dm_handle_t *dm_handle = _dm_get_handle(device);
    if(device == NULL || dm_handle == NULL || dm_handle->config.msg_callback == NULL) {
        return;
    }

    dm_msg = dm_msg_parse_down_raw(message);
    if(dm_msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_PARSE_ERROR, "msg(%s) json parse error\r\n", message->topic);
        return;
    }

    dm_msg->type = AIOT_DMRECV_RAW_DOWN;
    dm_handle->config.msg_callback(device, dm_msg, dm_handle->config.userdata);
    dm_msg_delete(dm_msg);
}

static int32_t _sub_batch_post_topic(dm_handle_t *dm_handle)
{
    /* 批量属性及事件上报回复topic */
    char *fmt = "/sys/%s/%s/thing/event/property/batch/post_reply";
    char *topic = NULL;
    char *src[] = { NULL, NULL };

    src[0] = aiot_device_product_key(dm_handle->device);
    src[1] = aiot_device_name(dm_handle->device);

    if(STATE_SUCCESS != core_sprintf(NULL, &topic, fmt, src, sizeof(src) / sizeof(char *), "")) {
        return -1;
    }

    aiot_device_register_topic_filter(dm_handle->device, topic, _dm_msg_batch_post_reply_callback, 1, NULL);
    if(topic != NULL) {
        core_os_free(topic);
    }

    topic = NULL;

    dm_handle->runtime.has_subscriber = 1;

    return STATE_SUCCESS;
}

static _dm_topic_filter _dm_recv_table[] = {
    {
        .topic = DM_POST_REPLY_TOPIC_FMT,
        .callback = _dm_msg_generic_reply_callback,
    },
    {
        .topic = DM_BATCH_POST_REPLY_TOPIC_FMT,
        .callback = _dm_msg_batch_post_reply_callback,
    },
    {
        .topic = DM_PROPERTY_SET_TOPIC_FMT,
        .callback = _dm_msg_property_set_callback,
    },
    {
        .topic = DM_SERVICE_INVOKE_TOPIC_FMT,
        .callback = _dm_msg_service_invoke_callback,
    },
    {
        .topic = DM_GET_DESIRED_REPLY_TOPIC_FMT,
        .callback = _dm_msg_desired_get_reply_callback,
    },
    {
        .topic = DM_DELETE_DESIRED_REPLY_TOPIC_FMT,
        .callback = _dm_msg_desired_delete_reply_callback,
    },
    {
        .topic = DM_UP_RAW_REPLY_TOPIC_FMT,
        .callback = _dm_msg_up_raw_reply_callback,
    },
    {
        .topic = DM_DOWN_RAW_TOPIC_FMT,
        .callback = _dm_msg_down_raw_callback,
    },
};

/* 设置接收消息后的回调函数 */
int32_t aiot_device_dm_set_msg_callback(void *device, dm_msg_callback_t msg_callback, void *userdata)
{
    int i = 0;
    _dm_topic_filter *filter = NULL;
    dm_handle_t *dm_handle = _dm_get_handle(device);
    if(device == NULL || dm_handle == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_dm_set_msg_callback input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    dm_handle->config.msg_callback = msg_callback;
    dm_handle->config.userdata = userdata;

    for(i = 0; i < sizeof(_dm_recv_table) / sizeof(_dm_topic_filter); i++) {
        filter = &_dm_recv_table[i];
        aiot_device_register_topic_filter(device, filter->topic, filter->callback, 0, NULL);
    }

    return STATE_SUCCESS;
}

/* 发送接口 */
int32_t aiot_device_dm_property_post(void *device, char *params, int8_t post_reply)
{
    aiot_msg_t *msg = NULL;
    int32_t res = STATE_SUCCESS;
    dm_handle_t *dm_handle = _dm_get_handle(device);
    if(device == NULL || dm_handle == NULL || params == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_dm_property_post input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    msg = dm_msg_create_property_post(device, params, post_reply);
    res = aiot_device_send_message(device, msg);
    if(res == STATE_SUCCESS) {
        res = msg->id;
    }

    aiot_msg_delete(msg);
    return res;
}

int32_t aiot_device_dm_event_post(void *device, char *event_id, char *params, int8_t post_reply)
{
    aiot_msg_t *msg = NULL;
    int32_t res = STATE_SUCCESS;
    dm_handle_t *dm_handle = _dm_get_handle(device);
    if(device == NULL || dm_handle == NULL || event_id == NULL || params == NULL ) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_dm_event_post input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    msg = dm_msg_create_event_post(device, event_id, params, post_reply);
    res = aiot_device_send_message(device, msg);
    if(res == STATE_SUCCESS) {
        res = msg->id;
    }

    aiot_msg_delete(msg);
    return res;
}

int32_t aiot_device_dm_batch_post(void *device, char *params, int8_t post_reply)
{
    aiot_msg_t *msg = NULL;
    int32_t res = STATE_SUCCESS;
    dm_handle_t *dm_handle = _dm_get_handle(device);
    if(device == NULL || dm_handle == NULL || params == NULL ) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_dm_batch_post input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    msg = dm_msg_create_batch_post(device, params, post_reply);
    res = aiot_device_send_message(device, msg);
    if(res == STATE_SUCCESS) {
        res = msg->id;
    }

    aiot_msg_delete(msg);
    return res;
}
int32_t aiot_device_dm_propertyset_reply(void *device, void *context, uint32_t code, char *data)
{
    aiot_msg_t *msg = NULL;
    int32_t res = STATE_SUCCESS;
    dm_handle_t *dm_handle = _dm_get_handle(device);
    if(device == NULL || context == NULL || dm_handle == NULL || data == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_dm_propertyset_reply input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    msg = dm_msg_create_propertyset_reply(device, context, code, data);
    res = aiot_device_send_message(device, msg);
    aiot_msg_delete(msg);
    return res;
}
int32_t aiot_device_dm_service_reply(void *device, void *context, char *service_id, uint32_t code, char *data)
{
    aiot_msg_t *msg = NULL;
    int32_t res = STATE_SUCCESS;
    dm_handle_t *dm_handle = _dm_get_handle(device);
    if(device == NULL || dm_handle == NULL || service_id == NULL || data == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_dm_service_reply input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    msg = dm_msg_create_service_reply(device, context, service_id, code, data);
    res = aiot_device_send_message(device, msg);
    aiot_msg_delete(msg);
    return res;
}
int32_t aiot_device_dm_get_desired(void *device, char *params)
{
    aiot_msg_t *msg = NULL;
    int32_t res = STATE_SUCCESS;
    dm_handle_t *dm_handle = _dm_get_handle(device);
    if(device == NULL || dm_handle == NULL || params == NULL ) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_dm_get_desired input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    msg = dm_msg_create_get_desired(device, params, 1);
    res = aiot_device_send_message(device, msg);
    if(res == STATE_SUCCESS) {
        res = msg->id;
    }

    aiot_msg_delete(msg);
    return res;
}
int32_t aiot_device_dm_delete_desired(void *device, char *params)
{
    aiot_msg_t *msg = NULL;
    int32_t res = STATE_SUCCESS;
    dm_handle_t *dm_handle = _dm_get_handle(device);
    if(device == NULL || params == NULL || dm_handle == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_dm_delete_desired input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    msg = dm_msg_create_delete_desired(device, params, 1);
    res = aiot_device_send_message(device, msg);
    if(res == STATE_SUCCESS) {
        res = msg->id;
    }

    aiot_msg_delete(msg);
    return res;
}
int32_t aiot_device_dm_raw_post(void *device, uint8_t *data, uint32_t data_len)
{
    aiot_msg_t *msg = NULL;
    int32_t res = STATE_SUCCESS;
    dm_handle_t *dm_handle = _dm_get_handle(device);
    if(device == NULL || dm_handle == NULL || data == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_dm_raw_post input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    msg = dm_msg_create_up_raw(device, data, data_len);
    res = aiot_device_send_message(device, msg);
    aiot_msg_delete(msg);
    return res;
}
int32_t aiot_device_dm_raw_reply(void *device, void *context, uint8_t *data, uint32_t data_len)
{
    aiot_msg_t *msg = NULL;
    int32_t res = STATE_SUCCESS;
    dm_handle_t *dm_handle = _dm_get_handle(device);
    if(device == NULL || dm_handle == NULL || data == NULL ) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_dm_raw_reply input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    msg = dm_msg_create_down_raw_reply(device, data, data_len);
    res = aiot_device_send_message(device, msg);
    aiot_msg_delete(msg);
    return res;
}

void *aiot_dm_msg_context_clone(void *context)
{
    return dm_msg_context_clone(context);
}

int32_t aiot_dm_msg_context_free(void *context)
{
    dm_msg_context_deinit(context);
    return STATE_SUCCESS;
}