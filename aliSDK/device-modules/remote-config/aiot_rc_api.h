/**
 * @file aiot_rc_api.h
 * @brief 设备远程配置模块头文件, 提供设备通过物联网平台远程配置的能力
 * @date 2022-01-20
 *
 * @copyright Copyright (C) 2020-2025 Alibaba Group Holding Limited
 *
 */

#ifndef _AIOT_RC_API_H
#define _AIOT_RC_API_H

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>

/**
 * @brief 远程配置消息的结构体
 *
 */
typedef struct {
    /**
     * @brief Alink协议的消息id
     */
    uint32_t msg_id;
    /**
     * @brief 配置id，用于表示哪一次配置
     */
    char     *config_id;
    /**
     * @brief 配置文件的长度
     */
    uint32_t config_size;
    /**
     * @brief 配置文件的url
     */
    char     *url;
    /**
     * @brief 配置文件的签名
     */
    char     *sign;
    /**
     * @brief 配置文件的签名算法类型
     */
    char     *sign_method;
} aiot_rc_msg_t;

/**
 * @brief 设备远程配置消息回调函数原型，用户定义后, 可通过 @ref aiot_device_rc_set_callback 配置
 *
 * @param[in] device 设备句柄
 * @param[in] recv    接收到的消息 @ref aiot_rc_msg_t
 * @param[in] userdata 用户设置的上下文，可通过 @ref aiot_device_rc_set_callback 配置
 */
typedef void (*rc_callback_t)(void *device, const aiot_rc_msg_t *recv, void *userdata);

/**
 * @brief 设置远程配置
 *
 * @param[in] device 设备句柄
 * @param[in] callback 消息回调函数
 * @param[in] userdata 执行回调消息的上下文
 * @return int32_t
 * @retval STATE_SUCCESS 参数配置成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_device_rc_set_callback(void *device, rc_callback_t callback, void *userdata);

/**
 * @brief 请求远程配置文件信息
 *
 * @param[in] device 设备句柄
 * @return int32_t
 * @retval >=STATE_SUCCESS  发送请求成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_device_rc_request(void *device);

/**
 * @brief 云端下推的远程配置后，回复云端
 *
 * @param[in] device 设备句柄
 * @param[in] code   远程配置结果
 * @return int32_t
 * @retval >=STATE_SUCCESS  回复消息成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_device_rc_push_reply(void *device, int32_t code);

#if defined(__cplusplus)
}
#endif

#endif