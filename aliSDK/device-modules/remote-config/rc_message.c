#include "core_os.h"
#include "core_string.h"
#include "cJSON.h"
#include "device_private.h"
#include "aiot_state_api.h"
#include "aiot_message_api.h"
#include "rc_message.h"

static aiot_msg_t *_rc_msg_create_template(void *device, const char* fmt, cJSON *root)
{
    char *topic = NULL, *payload = NULL;
    char *src[] = { NULL, NULL };
    uint32_t src_counter = 2;
    aiot_msg_t *result = NULL;

    src[0] = aiot_device_product_key(device);
    src[1] = aiot_device_name(device);

    if(STATE_SUCCESS != core_sprintf(NULL, &topic, fmt, src, src_counter, "")) {
        return NULL;
    }

    payload = cJSON_PrintUnformatted(root);
    if (payload == NULL) {
        core_os_free(topic);
        return NULL;
    }

    result = aiot_msg_create_raw(topic, (uint8_t *)payload, strlen(payload));
    core_os_free(topic);
    core_os_free(payload);
    return result;
}

/* 创建主动获取远程配置文件的消息 */
aiot_msg_t *rc_msg_create_get(void *device)
{
    cJSON *root = NULL, *params = NULL;
    aiot_msg_t *result = NULL;
    int32_t id = 0;

    root = cJSON_CreateObject();
    if (root == NULL) {
        return NULL;
    }

    params = cJSON_CreateObject();
    if (params == NULL) {
        cJSON_Delete(root);
        return NULL;
    }

    id = core_device_get_next_alink_id(device);
    _cJSON_ADDInt2StrToObject(root, "id", id);
    cJSON_AddItemToObject(root, "params", params);
    cJSON_AddStringToObject(params, "configScope", "product");
    cJSON_AddStringToObject(params, "getType", "file");

    result = _rc_msg_create_template(device, RC_GET_TOPIC_FMT, root);
    result->id = id;
    cJSON_Delete(root);

    return result;
}

/* 创建push_reply消息 */
aiot_msg_t *rc_msg_create_push_reply(void *device, int32_t code)
{
    cJSON *root = NULL;
    aiot_msg_t *result = NULL;
    int32_t id = 0;

    root = cJSON_CreateObject();
    if (root == NULL) {
        return NULL;
    }

    id = core_device_get_next_alink_id(device);
    _cJSON_ADDInt2StrToObject(root, "id", id);
    cJSON_AddNumberToObject(root, "code", code);
    cJSON_AddRawToObject(root, "data", "{}");

    result = _rc_msg_create_template(device, RC_PUSH_REPLYTOPIC_FMT, root);
    result->id = id;
    cJSON_Delete(root);

    return result;
}

/* 删除 aiot_rc_msg_t */
void rc_msg_delete(aiot_rc_msg_t *msg)
{
    if(msg == NULL) {
        return;
    }

    if(msg->config_id != NULL) {
        core_os_free(msg->config_id);
    }

    if(msg->url != NULL) {
        core_os_free(msg->url);
    }

    if(msg->sign != NULL) {
        core_os_free(msg->sign);
    }

    if(msg->sign_method != NULL) {
        core_os_free(msg->sign_method);
    }

    core_os_free(msg);
}

/* 云端给的回复消息解析 */
aiot_rc_msg_t *rc_msg_parse_get_reply(const aiot_msg_t *msg)
{
    aiot_rc_msg_t *recv = NULL;
    cJSON *root = NULL, *data = NULL, *item = NULL;
    if(msg == NULL) {
        return NULL;
    }

    root = cJSON_ParseWithLength((char *)msg->payload, msg->payload_lenth);
    if(root == NULL) {
        return NULL;
    }

    data = cJSON_GetObjectItem(root, "data");
    if(NULL == data) {
        cJSON_Delete(root);
        return NULL;
    }

    recv = (aiot_rc_msg_t *)core_os_malloc(sizeof(aiot_rc_msg_t));
    if(recv == NULL) {
        cJSON_Delete(root);
        return NULL;
    }
    memset(recv, 0, sizeof(aiot_rc_msg_t));

    item = cJSON_GetObjectItem(root, "id");
    if(NULL == item) {
        goto error;
    }
    core_str2uint(item->valuestring, strlen(item->valuestring), &recv->msg_id);

    item = cJSON_GetObjectItem(data, "configSize");
    if(NULL == item) {
        goto error;
    }
    recv->config_size = item->valueint;

    item = cJSON_GetObjectItem(data, "configId");
    if(NULL == item) {
        goto error;
    }
    core_strdup(NULL, &recv->config_id, item->valuestring, "");

    item = cJSON_GetObjectItem(data, "sign");
    if(NULL == item) {
        goto error;
    }
    core_strdup(NULL, &recv->sign, item->valuestring, "");

    item = cJSON_GetObjectItem(data, "signMethod");
    if(NULL == item) {
        goto error;
    }
    core_strdup(NULL, &recv->sign_method, item->valuestring, "");

    item = cJSON_GetObjectItem(data, "url");
    if(NULL == item) {
        goto error;
    }
    core_strdup(NULL, &recv->url, item->valuestring, "");

    cJSON_Delete(root);
    return recv;
error:
    cJSON_Delete(root);
    rc_msg_delete(recv);
    return NULL;
}

/* 云端主动下推的消息解析 */
aiot_rc_msg_t *rc_msg_parse_push(const aiot_msg_t *msg)
{
    aiot_rc_msg_t *recv = NULL;
    cJSON *root = NULL, *params = NULL, *item = NULL;
    if(msg == NULL) {
        return NULL;
    }

    root = cJSON_ParseWithLength((char *)msg->payload, msg->payload_lenth);
    if(root == NULL) {
        return NULL;
    }

    params = cJSON_GetObjectItem(root, "params");
    if(NULL == params) {
        cJSON_Delete(root);
        return NULL;
    }

    recv = (aiot_rc_msg_t *)core_os_malloc(sizeof(aiot_rc_msg_t));
    if(recv == NULL) {
        cJSON_Delete(root);
        return NULL;
    }
    memset(recv, 0, sizeof(aiot_rc_msg_t));

    item = cJSON_GetObjectItem(root, "id");
    if(NULL == item) {
        goto error;
    }
    core_str2uint(item->valuestring, strlen(item->valuestring), &recv->msg_id);

    item = cJSON_GetObjectItem(params, "configSize");
    if(NULL == item) {
        goto error;
    }
    recv->config_size = item->valueint;

    item = cJSON_GetObjectItem(params, "configId");
    if(NULL == item) {
        goto error;
    }
    core_strdup(NULL, &recv->config_id, item->valuestring, "");

    item = cJSON_GetObjectItem(params, "sign");
    if(NULL == item) {
        goto error;
    }
    core_strdup(NULL, &recv->sign, item->valuestring, "");

    item = cJSON_GetObjectItem(params, "signMethod");
    if(NULL == item) {
        goto error;
    }
    core_strdup(NULL, &recv->sign_method, item->valuestring, "");

    item = cJSON_GetObjectItem(params, "url");
    if(NULL == item) {
        goto error;
    }
    core_strdup(NULL, &recv->url, item->valuestring, "");

    cJSON_Delete(root);
    return recv;
error:
    cJSON_Delete(root);
    rc_msg_delete(recv);
    return NULL;
}

