#include "core_log.h"
#include "core_time.h"
#include "core_string.h"
#include "aiot_state_api.h"
#include "device_private.h"
#include "aiot_message_api.h"
#include "rc_message.h"
#include "aiot_rc_api.h"
#include "core_os.h"

#define TAG "REMOTE-CONFIG"
typedef struct {
    rc_callback_t msg_callback;
    void *userdata;
} rc_config_t;

typedef struct {
    void *device;
    rc_config_t config;
} rc_handle_t;

static rc_handle_t *_rc_get_handle(void *device);

void _rc_msg_get_reply_callback(void *device, const aiot_msg_t *message, void *userdata)
{
    aiot_rc_msg_t *rc_msg = NULL;
    rc_handle_t *rc_handle = _rc_get_handle(device);
    if(device == NULL || rc_handle == NULL || rc_handle->config.msg_callback == NULL) {
        return;
    }

    rc_msg = rc_msg_parse_get_reply(message);
    if(rc_msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_PARSE_ERROR, "msg(%s) parse error\r\n", message->topic);
        return;
    }

    ALOG_INFO("remote-config msg recv: id %u, url:%s", rc_msg->config_id, rc_msg->url);
    if(rc_handle->config.msg_callback != NULL) {
        rc_handle->config.msg_callback(device, rc_msg, rc_handle->config.userdata);
    }

    /* 回收消息资源 */
    rc_msg_delete(rc_msg);
}

void _rc_msg_push_callback(void *device, const aiot_msg_t *message, void *userdata)
{
    aiot_rc_msg_t *rc_msg = NULL;
    rc_handle_t *rc_handle = _rc_get_handle(device);
    if(device == NULL || rc_handle == NULL || rc_handle->config.msg_callback == NULL) {
        return;
    }

    rc_msg = rc_msg_parse_push(message);
    if(rc_msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_PARSE_ERROR, "msg(%s)  parse error\r\n", message->topic);
        return;
    }

    ALOG_INFO("remote-config msg recv: id %u, url:%s", rc_msg->config_id, rc_msg->url);
    if(rc_handle->config.msg_callback != NULL) {
        rc_handle->config.msg_callback(device, rc_msg, rc_handle->config.userdata);
    }

    /* 回收消息资源 */
    rc_msg_delete(rc_msg);
}

int32_t aiot_device_rc_set_callback(void *device, rc_callback_t callback, void *userdata)
{
    rc_handle_t *rc_handle = _rc_get_handle(device);
    if(device == NULL || rc_handle == NULL || callback == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_rc_set_callback input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    rc_handle->config.msg_callback = callback;
    rc_handle->config.userdata = userdata;

    aiot_device_register_topic_filter(device, RC_GET_REPLY_TOPIC_FMT, _rc_msg_get_reply_callback, 0, NULL);
    aiot_device_register_topic_filter(device, RC_PUSH_TOPIC_FMT, _rc_msg_push_callback, 0, NULL);

    return STATE_SUCCESS;
}

int32_t aiot_device_rc_request(void *device)
{
    aiot_msg_t *msg = NULL;
    int32_t res = STATE_SUCCESS;
    rc_handle_t *rc_handle = _rc_get_handle(device);
    if(device == NULL || rc_handle == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_rc_send input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    msg = rc_msg_create_get(device);
    if(msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_CREATE_ERROR, "remote-config message create error \r\n");
        return STATE_MESSAGE_CREATE_ERROR;
    }

    res = aiot_device_send_message(device, msg);

    aiot_msg_delete(msg);

    return res;
}

int32_t aiot_device_rc_push_reply(void *device, int32_t code)
{
    aiot_msg_t *msg = NULL;
    int32_t res = STATE_SUCCESS;
    rc_handle_t *rc_handle = _rc_get_handle(device);
    if(device == NULL || rc_handle == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_rc_send input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    msg = rc_msg_create_push_reply(device, code);
    if(msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_CREATE_ERROR, "remote-config message create error \r\n");
        return STATE_MESSAGE_CREATE_ERROR;
    }

    res = aiot_device_send_message(device, msg);

    aiot_msg_delete(msg);

    return res;
}

static void *_rc_init()
{
    rc_handle_t *rc_handle = (rc_handle_t *)core_os_malloc(sizeof(rc_handle_t));
    if(rc_handle == NULL) {
        return NULL;
    }

    memset(rc_handle, 0, sizeof(rc_handle_t));
    return rc_handle;
}
static void _rc_deinit(void *handle)
{
    core_os_free(handle);
}

/* 日志上报即插即用操作方法 */
static module_ops_t rc_ops = {
    .internal_ms = -1,
    .on_init = _rc_init,
    .on_process = NULL,
    .on_status = NULL,
    .on_deinit = _rc_deinit,
};

static rc_handle_t *_rc_get_handle(void *device)
{
    rc_handle_t *rc_handle = core_device_module_get(device, MODULE_REMOTE_CONFIG);
    int32_t res = STATE_SUCCESS;

    if(rc_handle == NULL) {
        res = core_device_module_register(device, MODULE_REMOTE_CONFIG, &rc_ops);
        if(res != STATE_SUCCESS) {
            return NULL;
        } else {
            rc_handle = core_device_module_get(device, MODULE_REMOTE_CONFIG);
            rc_handle->device = device;
        }
    }
    return rc_handle;
}