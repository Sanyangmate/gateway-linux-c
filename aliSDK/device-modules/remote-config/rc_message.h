

#ifndef _RC_MESSAGE_H_
#define _RC_MESSAGE_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>
#include "aiot_message_api.h"
#include "aiot_rc_api.h"

/* 远程配置请求topic */
#define RC_GET_TOPIC_FMT                     "/sys/%s/%s/thing/config/get"
/* 远程配置请求回复 */
#define RC_GET_REPLY_TOPIC_FMT               "/sys/+/+/thing/config/get_reply"
/* 远程配置云端主动下推*/
#define RC_PUSH_TOPIC_FMT                    "/sys/+/+/thing/config/push"
/* 云端主动下推，设备端回复*/
#define RC_PUSH_REPLYTOPIC_FMT               "/sys/%s/%s/thing/config/push_reply"

/* 创建主动获取远程配置文件的消息 */
aiot_msg_t *rc_msg_create_get(void *device);

/* 创建push_reply消息 */
aiot_msg_t *rc_msg_create_push_reply(void *device, int32_t code);

/* 云端给的回复消息解析 */
aiot_rc_msg_t *rc_msg_parse_get_reply(const aiot_msg_t *msg);

/* 云端主动下推的消息解析 */
aiot_rc_msg_t *rc_msg_parse_push(const aiot_msg_t *msg);

/* 删除 aiot_rc_msg_t */
void rc_msg_delete(aiot_rc_msg_t *msg);

#if defined(__cplusplus)
}
#endif

#endif