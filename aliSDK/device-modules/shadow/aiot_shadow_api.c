#include "core_log.h"
#include "core_string.h"
#include "aiot_state_api.h"
#include "device_private.h"
#include "aiot_message_api.h"
#include "shadow_message.h"
#include "aiot_shadow_api.h"
#include "core_os.h"

#define TAG "SHADOW"
typedef struct {
    aiot_shadow_recv_callback_t msg_callback;
    void *userdata;
} shadow_config_t;

typedef struct {
    void *device;
    shadow_config_t config;
} shadow_handle_t;

static shadow_handle_t *_shadow_get_handle(void *device);

void _shadow_msg_notify_callback(void *device, const aiot_msg_t *message, void *userdata)
{
    shadow_msg_recv_t *shadow_msg = NULL;
    aiot_shadow_recv_t recv;
    shadow_handle_t *shadow_handle = _shadow_get_handle(device);
    if(device == NULL || shadow_handle == NULL || shadow_handle->config.msg_callback == NULL) {
        return;
    }
    memset(&recv, 0, sizeof(recv));

    shadow_msg = shadow_msg_parse_get(message);
    if(shadow_msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_PARSE_ERROR, "msg(%s) json parse error\r\n", message->topic);
        return;
    }

    if(0 == strcmp(shadow_msg->method, "control")) {
        recv.type = AIOT_SHADOWRECV_CONTROL;
        recv.data.control.payload = shadow_msg->payload;
        recv.data.control.payload_len = strlen(shadow_msg->payload);
        recv.data.control.version = shadow_msg->version;
    } else if(0 == strcmp(shadow_msg->method, "reply")) {
        if(shadow_msg->status != NULL) {
            recv.type = AIOT_SHADOWRECV_GENERIC_REPLY;
            recv.data.generic_reply.payload = shadow_msg->payload;
            recv.data.generic_reply.payload_len = strlen(shadow_msg->payload);
            recv.data.generic_reply.status = shadow_msg->status;
            recv.data.generic_reply.timestamp = shadow_msg->timestamp;
        } else {
            recv.type = AIOT_SHADOWRECV_GET_REPLY;
            recv.data.get_reply.payload = shadow_msg->payload;
            recv.data.get_reply.payload_len = strlen(shadow_msg->payload);
            recv.data.get_reply.version = shadow_msg->version;
        }
    }

    shadow_handle->config.msg_callback(device, &recv, shadow_handle->config.userdata);

    /* 回收消息资源 */
    shadow_msg_delete(shadow_msg);
}

int32_t aiot_device_shadow_set_callback(void *device, aiot_shadow_recv_callback_t callback, void *userdata)
{
    shadow_handle_t *shadow_handle = _shadow_get_handle(device);
    if(device == NULL || shadow_handle == NULL || callback == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_shadow_set_callback input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    shadow_handle->config.msg_callback = callback;
    shadow_handle->config.userdata = userdata;

    aiot_device_register_topic_filter(device, SHADOW_GET_TOPIC, _shadow_msg_notify_callback, 0, NULL);

    return STATE_SUCCESS;
}

int32_t aiot_device_shadow_update(void *device, char *reported, int64_t version)
{
    shadow_msg_t shadow_msg;
    aiot_msg_t *msg = NULL;
    int32_t res = STATE_SUCCESS;

    if(device == NULL || reported == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_shadow_update input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }
    memset(&shadow_msg, 0, sizeof(shadow_msg));
    shadow_msg.method = "update";
    shadow_msg.reported = reported;
    shadow_msg.version = version;

    msg = shadow_msg_create_update(device, &shadow_msg);
    if(msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_CREATE_ERROR, "shadow message create error \r\n");
        return STATE_MESSAGE_CREATE_ERROR;
    }

    res = aiot_device_send_message(device, msg);
    aiot_msg_delete(msg);

    return res;
}

int32_t aiot_device_shadow_clean_desired(void *device, int64_t version)
{
    shadow_msg_t shadow_msg;
    aiot_msg_t *msg = NULL;
    int32_t res = STATE_SUCCESS;

    if(device == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_shadow_clean_desired input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }
    memset(&shadow_msg, 0, sizeof(shadow_msg));
    shadow_msg.method = "update";
    shadow_msg.desired = "\"null\"";
    shadow_msg.version = version;

    msg = shadow_msg_create_update(device, &shadow_msg);
    if(msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_CREATE_ERROR, "shadow message create error \r\n");
        return STATE_MESSAGE_CREATE_ERROR;
    }

    res = aiot_device_send_message(device, msg);
    aiot_msg_delete(msg);

    return res;
}

int32_t aiot_device_shadow_get(void *device)
{
    shadow_msg_t shadow_msg;
    aiot_msg_t *msg = NULL;
    int32_t res = STATE_SUCCESS;

    if(device == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_shadow_get input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }
    memset(&shadow_msg, 0, sizeof(shadow_msg));
    shadow_msg.method = "get";

    msg = shadow_msg_create_update(device, &shadow_msg);
    if(msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_CREATE_ERROR, "shadow message create error \r\n");
        return STATE_MESSAGE_CREATE_ERROR;
    }

    res = aiot_device_send_message(device, msg);
    aiot_msg_delete(msg);

    return res;
}

int32_t aiot_device_shadow_delete_reported(void *device, char *reported, int64_t version)
{
    shadow_msg_t shadow_msg;
    aiot_msg_t *msg = NULL;
    int32_t res = STATE_SUCCESS;

    if(device == NULL || reported == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_shadow_delete_reported input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }
    memset(&shadow_msg, 0, sizeof(shadow_msg));
    shadow_msg.method = "delete";
    shadow_msg.reported = reported;
    shadow_msg.version = version;

    msg = shadow_msg_create_update(device, &shadow_msg);
    if(msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_CREATE_ERROR, "shadow message create error \r\n");
        return STATE_MESSAGE_CREATE_ERROR;
    }

    res = aiot_device_send_message(device, msg);
    aiot_msg_delete(msg);

    return res;
}

void *_shadow_init()
{
    shadow_handle_t *shadow_handle = (shadow_handle_t *)core_os_malloc(sizeof(shadow_handle_t));
    if(shadow_handle == NULL) {
        return NULL;
    }

    memset(shadow_handle, 0, sizeof(shadow_handle_t));
    return shadow_handle;
}
void _shadow_deinit(void *handle)
{
    core_os_free(handle);
}

/* 日志上报即插即用操作方法 */
static module_ops_t shadow_ops = {
    .internal_ms = -1,
    .on_init = _shadow_init,
    .on_process = NULL,
    .on_status = NULL,
    .on_deinit = _shadow_deinit,
};

static shadow_handle_t *_shadow_get_handle(void *device)
{
    shadow_handle_t *shadow_handle = core_device_module_get(device, MODULE_SHADOW);
    int32_t res = STATE_SUCCESS;

    if(shadow_handle == NULL) {
        res = core_device_module_register(device, MODULE_SHADOW, &shadow_ops);
        if(res != STATE_SUCCESS) {
            return NULL;
        } else {
            shadow_handle = core_device_module_get(device, MODULE_SHADOW);
            shadow_handle->device = device;
        }
    }
    return shadow_handle;
}