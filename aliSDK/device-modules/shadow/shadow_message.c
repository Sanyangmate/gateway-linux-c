#include "core_os.h"
#include "core_log.h"
#include "cJSON.h"
#include "device_private.h"
#include "aiot_state_api.h"
#include "aiot_message_api.h"
#include "shadow_message.h"


static aiot_msg_t *_shadow_msg_create_template(void *device, const char* fmt, cJSON *root)
{
    char *topic = NULL, *payload = NULL;
    char *src[] = { NULL, NULL };
    uint32_t src_counter = 2;
    aiot_msg_t *result = NULL;

    src[0] = aiot_device_product_key(device);
    src[1] = aiot_device_name(device);

    if(STATE_SUCCESS != core_sprintf(NULL, &topic, fmt, src, src_counter, "")) {
        return NULL;
    }

    payload = cJSON_PrintUnformatted(root);
    if (payload == NULL) {
        core_os_free(topic);
        return NULL;
    }

    result = aiot_msg_create_raw(topic, (uint8_t *)payload, strlen(payload));
    core_os_free(topic);
    core_os_free(payload);
    return result;
}

/* 创建主动上报的消息 */
aiot_msg_t *shadow_msg_create_update(void *device, shadow_msg_t *shadow_msg)
{
    cJSON *root = NULL, *state = NULL;
    aiot_msg_t *result = NULL;

    if(device == NULL || shadow_msg->method == NULL) {
        return NULL;
    }

    root = cJSON_CreateObject();
    if (root == NULL) {
        return NULL;
    }

    if(shadow_msg->desired != NULL || shadow_msg->reported != NULL) {
        state = cJSON_CreateObject();
        if (state == NULL) {
            cJSON_Delete(root);
            return NULL;
        }

        if(shadow_msg->reported != NULL) {
            cJSON_AddRawToObject(state, "reported", shadow_msg->reported);
        }

        if(shadow_msg->desired != NULL) {
            cJSON_AddRawToObject(state, "desired", shadow_msg->desired);
        }

        cJSON_AddItemToObject(root, "state", state);
    }

    cJSON_AddNumberToObject(root, "version", shadow_msg->version);
    cJSON_AddStringToObject(root, "method", shadow_msg->method);

    result = _shadow_msg_create_template(device, SHADOW_UPDATE_TOPIC_FMT, root);
    cJSON_Delete(root);

    return result;
}

/* 删除aiot_shadow_recv_t消息 */
void shadow_msg_delete(shadow_msg_recv_t *msg)
{
    if(msg == NULL) {
        return;
    }

    if(msg->method != NULL) {
        core_os_free(msg->method);
    }

    if(msg->payload != NULL) {
        core_os_free(msg->payload);
    }

    if(msg->status != NULL) {
        core_os_free(msg->status);
    }

    core_os_free(msg);
}

/* 解析云端下发消息 */
shadow_msg_recv_t *shadow_msg_parse_get(const aiot_msg_t *msg)
{
    shadow_msg_recv_t *recv = NULL;
    cJSON *root = NULL, *item = NULL;
    int32_t len = 0;

    root = cJSON_ParseWithLength((char *)msg->payload, msg->payload_lenth);
    if(root == NULL) {
        return NULL;
    }

    recv = (shadow_msg_recv_t *)core_os_malloc(sizeof(shadow_msg_recv_t));
    if(recv == NULL) {
        cJSON_Delete(root);
        return NULL;
    }
    memset(recv, 0, sizeof(shadow_msg_recv_t));
    recv->status = NULL;

    item = cJSON_GetObjectItem(root, "method");
    if(NULL == item) {
        cJSON_Delete(root);
        shadow_msg_delete(recv);
        return NULL;
    }
    core_strdup(NULL, &recv->method, item->valuestring, NULL);
    len = strlen(item->valuestring);


    item = cJSON_GetObjectItem(root, "payload");
    if(NULL == item) {
        cJSON_Delete(root);
        shadow_msg_delete(recv);
        return NULL;
    }
    recv->payload = cJSON_PrintUnformatted(item);

    if(0 == memcmp(recv->method, "reply", len) && NULL == cJSON_GetObjectItem(item, "state")) {
        item = cJSON_GetObjectItem(item, "status");
        if(item != NULL) {
            core_strdup(NULL, &recv->status, item->valuestring, NULL);
        }
    }

    item = cJSON_GetObjectItem(root, "version");
    if(NULL != item) {
        recv->version = item->valueint;
    }

    item = cJSON_GetObjectItem(root, "timestamp");
    if(NULL != item) {
        recv->timestamp = item->valuedouble;
    }
    cJSON_Delete(root);

    return recv;
}

