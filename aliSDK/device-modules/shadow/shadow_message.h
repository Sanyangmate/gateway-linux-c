

#ifndef _SHADOW_MESSAGE_H_
#define _SHADOW_MESSAGE_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>
#include "aiot_message_api.h"

/* 设备-->云: 更新设备影子属性值 */
#define SHADOW_UPDATE_TOPIC_FMT             "/shadow/update/%s/%s"
/* 云-->设备: 更新设备影子期望值 */
#define SHADOW_GET_TOPIC                    "/shadow/get/+/+"

typedef struct {
    char *method;
    char *reported;
    char *desired;
    int64_t version;
} shadow_msg_t;

typedef struct {
    char *method;
    char *payload;
    int64_t version;
    uint64_t timestamp;
    char *status;
} shadow_msg_recv_t;

/* 创建主动上报的消息 */
aiot_msg_t *shadow_msg_create_update(void *device, shadow_msg_t *shadow_msg);

/* 解析云端下发消息 */
shadow_msg_recv_t *shadow_msg_parse_get(const aiot_msg_t *msg);

/* 删除aiot_shadow_recv_t消息 */
void shadow_msg_delete(shadow_msg_recv_t *msg);

#if defined(__cplusplus)
}
#endif

#endif