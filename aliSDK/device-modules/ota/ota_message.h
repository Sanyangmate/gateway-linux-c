

#ifndef _OTA_MESSAGE_H_
#define _OTA_MESSAGE_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>
#include "aiot_message_api.h"
#include "aiot_ota_api.h"

#define OTA_REPORT_VERSION_TOPIC_FMT             "/ota/device/inform/%s/%s"
#define OTA_REPORT_PROGRESS_TOPIC_FMT            "/ota/device/progress/%s/%s"
#define OTA_REQUEST_FIRMWARE_TOPIC_FMT           "/sys/%s/%s/thing/ota/firmware/get"
#define OTA_NOTIFY_TOPIC_FMT                     "/ota/device/upgrade/+/+"
#define OTA_REQUEST_FIRMWARE_REPLY_TOPIC_FMT     "/sys/+/+/thing/ota/firmware/get_reply"

aiot_msg_t *ota_msg_create_report_version(void *device, char *module, char *version);
aiot_msg_t *ota_msg_create_request(void *device, char *module);
aiot_msg_t *ota_msg_create_report_state(void *device, char *module, int32_t state);

aiot_ota_msg_t *ota_msg_parse_notify(const aiot_msg_t *msg);

aiot_ota_msg_t *ota_msg_clone(const aiot_ota_msg_t *msg);
void ota_msg_delete(aiot_ota_msg_t *msg);

#if defined(__cplusplus)
}
#endif

#endif