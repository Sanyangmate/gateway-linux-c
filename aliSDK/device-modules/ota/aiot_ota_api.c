#include "ota_message.h"
#include "aiot_ota_api.h"
#include "aiot_state_api.h"
#include "device_private.h"
#include "aiot_message_api.h"
#include "core_log.h"
#include "core_os.h"

#define TAG "OTA"
typedef struct {
    ota_msg_callback_t msg_callback;
    void *userdata;
} ota_config_t;

typedef struct {
    void *device;
    ota_config_t config;
} ota_handle_t;

void *_ota_init()
{
    ota_handle_t *ota_handle = (ota_handle_t *)core_os_malloc(sizeof(ota_handle_t));
    if(ota_handle == NULL) {
        return NULL;
    }

    memset(ota_handle, 0, sizeof(ota_handle_t));
    return ota_handle;
}
void _ota_deinit(void *handle)
{
    core_os_free(handle);
}

static module_ops_t ota_ops = {
    .internal_ms = -1,
    .on_init = _ota_init,
    .on_process = NULL,
    .on_status = NULL,
    .on_deinit = _ota_deinit,
};

static ota_handle_t *_ota_get_handle(void *device)
{
    ota_handle_t *ota_handle = core_device_module_get(device, MODULE_OTA);
    int32_t res = STATE_SUCCESS;

    if(ota_handle == NULL) {
        res = core_device_module_register(device, MODULE_OTA, &ota_ops);
        if(res != STATE_SUCCESS) {
            return NULL;
        } else {
            ota_handle = core_device_module_get(device, MODULE_OTA);
            ota_handle->device = device;
        }
    }
    return ota_handle;
}

void _ota_msg_notify_callback(void *device, const aiot_msg_t *message, void *userdata)
{
    aiot_ota_msg_t *ota_msg = NULL;
    ota_handle_t *ota_handle = _ota_get_handle(device);
    if(device == NULL || ota_handle == NULL || ota_handle->config.msg_callback == NULL) {
        return;
    }

    ota_msg = ota_msg_parse_notify(message);
    if(ota_msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_PARSE_ERROR, "msg(%s) json parse error\r\n", message->topic);
        return;
    }

    ALOG_INFO(TAG, "ota notify: version %s\r\n", ota_msg->version);

    ota_handle->config.msg_callback(device, ota_msg, ota_handle->config.userdata);
    ota_msg_delete(ota_msg);
}

int32_t aiot_device_ota_set_callback(void *device, ota_msg_callback_t callback, void *userdata)
{
    ota_handle_t *ota_handle = _ota_get_handle(device);
    if(device == NULL || ota_handle == NULL || callback == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_ota_set_callback input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    ota_handle->config.msg_callback = callback;
    ota_handle->config.userdata = userdata;

    aiot_device_register_topic_filter(device, OTA_NOTIFY_TOPIC_FMT, _ota_msg_notify_callback, 0, NULL);
    aiot_device_register_topic_filter(device, OTA_REQUEST_FIRMWARE_REPLY_TOPIC_FMT, _ota_msg_notify_callback, 0, NULL);

    return STATE_SUCCESS;
}
int32_t aiot_device_ota_report_version(void *device, char *module, char *version)
{
    aiot_msg_t *msg = NULL;
    int32_t res = STATE_SUCCESS;
    if(device == NULL || version == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_ota_report_version input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    ALOG_INFO(TAG, "aiot_device_ota_report_version: version %s\r\n", version);
    msg = ota_msg_create_report_version(device, module, version);
    /* send 会判断msg是否为空 */
    res = aiot_device_send_message(device, msg);
    aiot_msg_delete(msg);

    return res;
}
int32_t aiot_device_ota_report_state(void *device, char *module, int32_t state)
{
    aiot_msg_t *msg = NULL;
    int32_t res = STATE_SUCCESS;
    if(device == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_ota_report_state input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    ALOG_INFO(TAG, "aiot_device_ota_report_state: %d\r\n", state);
    msg = ota_msg_create_report_state(device, module, state);
    /* send 会判断msg是否为空 */
    res = aiot_device_send_message(device, msg);
    aiot_msg_delete(msg);

    return res;
}
int32_t aiot_device_ota_request_firmware(void *device, char *module)
{
    aiot_msg_t *msg = NULL;
    int32_t res = STATE_SUCCESS;
    if(device == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_ota_request_firmware input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    ALOG_INFO(TAG, "aiot_device_ota_request_firmware\r\n");
    msg = ota_msg_create_request(device, module);
    /* send 会判断msg是否为空 */
    res = aiot_device_send_message(device, msg);
    aiot_msg_delete(msg);

    return res;
}

aiot_ota_msg_t* aiot_ota_msg_clone(const aiot_ota_msg_t *msg)
{
    if(msg == NULL) {
        return NULL;
    }

    return ota_msg_clone(msg);
}

void aiot_ota_msg_free(aiot_ota_msg_t *msg)
{
    if(msg != NULL) {
        ota_msg_delete(msg);
    }
}