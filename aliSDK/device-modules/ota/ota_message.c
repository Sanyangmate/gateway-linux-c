#include "ota_message.h"
#include "device_private.h"
#include "core_string.h"
#include "core_os.h"
#include "aiot_state_api.h"
#include "cJSON.h"
#include "aiot_message_api.h"

static aiot_msg_t *_ota_msg_create_template(void *device, const char* fmt, cJSON *root)
{
    char *topic = NULL, *payload = NULL;
    char *src[] = { NULL, NULL };
    uint32_t src_counter = 2;
    aiot_msg_t *result = NULL;

    src[0] = aiot_device_product_key(device);
    src[1] = aiot_device_name(device);

    if(STATE_SUCCESS != core_sprintf(NULL, &topic, fmt, src, src_counter, "")) {
        return NULL;
    }

    payload = cJSON_PrintUnformatted(root);
    if (payload == NULL) {
        core_os_free(topic);
        return NULL;
    }

    result = aiot_msg_create_raw(topic, (uint8_t *)payload, strlen(payload));
    core_os_free(topic);
    core_os_free(payload);
    return result;
}

aiot_msg_t *ota_msg_create_report_version(void *device, char *module, char *version)
{
    cJSON *root = NULL, *params_item = NULL;
    aiot_msg_t *result = NULL;
    int32_t id = 0;

    root = cJSON_CreateObject();
    if (root == NULL) {
        return NULL;
    }

    params_item = cJSON_CreateObject();
    if (params_item == NULL) {
        cJSON_Delete(root);
        return NULL;
    }

    id = core_device_get_next_alink_id(device);
    cJSON_AddItemToObject(root, "params", params_item);
    _cJSON_ADDInt2StrToObject(root, "id", id);

    cJSON_AddStringToObject(params_item, "version", version);
    if(module != NULL) {
        cJSON_AddStringToObject(params_item, "module", module);
    }

    result = _ota_msg_create_template(device, OTA_REPORT_VERSION_TOPIC_FMT, root);

    cJSON_Delete(root);

    return result;
}
aiot_msg_t *ota_msg_create_request(void *device, char *module)
{
    cJSON *root = NULL, *params_item = NULL;
    aiot_msg_t *result = NULL;
    int32_t id = 0;

    root = cJSON_CreateObject();
    if (root == NULL) {
        return NULL;
    }

    params_item = cJSON_CreateObject();
    if (params_item == NULL) {
        cJSON_Delete(root);
        return NULL;
    }

    id = core_device_get_next_alink_id(device);
    cJSON_AddItemToObject(root, "params", params_item);
    _cJSON_ADDInt2StrToObject(root, "id", id);
    cJSON_AddStringToObject(root, "version", "1.0");
    cJSON_AddStringToObject(root, "method", "thing.ota.firmware.get");

    if(module != NULL) {
        cJSON_AddStringToObject(params_item, "module", module);
    }

    result = _ota_msg_create_template(device, OTA_REQUEST_FIRMWARE_TOPIC_FMT, root);

    cJSON_Delete(root);

    return result;
}
aiot_msg_t *ota_msg_create_report_state(void *device, char *module, int32_t state)
{
    cJSON *root = NULL, *params_item = NULL;
    aiot_msg_t *result = NULL;
    int32_t id = 0;

    root = cJSON_CreateObject();
    if (root == NULL) {
        return NULL;
    }

    params_item = cJSON_CreateObject();
    if (params_item == NULL) {
        cJSON_Delete(root);
        return NULL;
    }

    id = core_device_get_next_alink_id(device);
    cJSON_AddItemToObject(root, "params", params_item);
    _cJSON_ADDInt2StrToObject(root, "id", id);
    cJSON_AddStringToObject(root, "version", "1.0");

    cJSON_AddNumberToObject(root, "step", state);
    if(module != NULL) {
        cJSON_AddStringToObject(params_item, "module", module);
    }

    result = _ota_msg_create_template(device, OTA_REPORT_PROGRESS_TOPIC_FMT, root);

    cJSON_Delete(root);

    return result;
}

aiot_ota_msg_t *ota_msg_clone(const aiot_ota_msg_t *msg)
{
    aiot_ota_msg_t *result = (aiot_ota_msg_t *)core_os_malloc(sizeof(aiot_ota_msg_t));
    if(result == NULL) {
        return result;
    }
    memset(result, 0, sizeof(aiot_ota_msg_t));
    result->is_diff = msg->is_diff;
    result->size_total = msg->size_total;

    if(msg->version != NULL) {
        core_strdup(NULL, &result->version, msg->version, NULL);
    }

    if(msg->http_url != NULL) {
        core_strdup(NULL, &result->http_url, msg->http_url, NULL);
    }

    /*
    if(msg->mqtt_url != NULL) {
        result->mqtt_url = core_os_malloc(sizeof(mqtt_url_t));
        memcpy(result->mqtt_url, msg->mqtt_url, sizeof(mqtt_url_t));
    }
    */

    if(msg->expect_digest != NULL) {
        core_strdup(NULL, &result->expect_digest, msg->expect_digest, NULL);
    }

    if(msg->module != NULL) {
        core_strdup(NULL, &result->module, msg->module, NULL);
    }

    if(msg->extra_data != NULL) {
        core_strdup(NULL, &result->extra_data, msg->extra_data, NULL);
    }

    return result;
}

void ota_msg_delete(aiot_ota_msg_t *msg)
{
    if(msg->version != NULL) {
        core_os_free(msg->version);
    }

    if(msg->http_url != NULL) {
        core_os_free(msg->http_url);
    }

    /*
    if(msg->mqtt_url != NULL) {
        core_os_free(msg->mqtt_url);
    }
    */

    if(msg->expect_digest != NULL) {
        core_os_free(msg->expect_digest);
    }

    if(msg->module != NULL) {
        core_os_free(msg->module);
    }

    if(msg->extra_data != NULL) {
        core_os_free(msg->extra_data);
    }
    core_os_free(msg);
}

aiot_ota_msg_t *ota_msg_parse_notify(const aiot_msg_t *msg)
{
    aiot_ota_msg_t *recv = NULL;
    cJSON *root = NULL, *data = NULL, *item = NULL;
    if(msg == NULL) {
        return NULL;
    }

    root = cJSON_ParseWithLength((char *)msg->payload, msg->payload_lenth);
    if(root == NULL) {
        return NULL;
    }

    data = cJSON_GetObjectItem(root, "data");
    if(NULL == data) {
        cJSON_Delete(root);
        return NULL;
    }

    recv = (aiot_ota_msg_t *)core_os_malloc(sizeof(aiot_ota_msg_t));
    if(recv == NULL) {
        cJSON_Delete(root);
        return NULL;
    }
    memset(recv, 0, sizeof(aiot_ota_msg_t));

    if(NULL != (item = cJSON_GetObjectItem(data, "version")) && item->valuestring != NULL) {
        core_strdup(NULL, &recv->version, item->valuestring, "");
    } else {
        cJSON_Delete(root);
        ota_msg_delete(recv);
        return NULL;
    }
    if(NULL != (item = cJSON_GetObjectItem(data, "url")) && item->valuestring != NULL) {
        core_strdup(NULL, &recv->http_url, item->valuestring, "");
    }

    if(NULL != (item = cJSON_GetObjectItem(data, "size"))) {
        recv->size_total = item->valuedouble;
    }

    if(NULL != (item = cJSON_GetObjectItem(data, "isDiff"))) {
        recv->is_diff = item->valueint;
    }

    if(NULL != (item = cJSON_GetObjectItem(data, "signMethod")) && item->valuestring != NULL) {
        if (strcmp(item->valuestring, "SHA256") == 0 || strcmp(item->valuestring, "Sha256") == 0) {
            recv->digest_method = AIOT_OTA_DIGEST_SHA256;
        } else if (strcmp(item->valuestring, "Md5") == 0 || strcmp(item->valuestring, "MD5") == 0) {
            recv->digest_method = AIOT_OTA_DIGEST_MD5;
        } else {
            recv->digest_method = AIOT_OTA_DIGEST_MAX;
        }
    }
    if(NULL != (item = cJSON_GetObjectItem(data, "sign")) && item->valuestring != NULL) {
        core_strdup(NULL, &recv->expect_digest, item->valuestring, "");
    }

    if(NULL != (item = cJSON_GetObjectItem(data, "module")) && item->valuestring != NULL) {
        core_strdup(NULL, &recv->module, item->valuestring, "");
    }

    if(NULL != (item = cJSON_GetObjectItem(data, "extData")) && item->valuestring != NULL) {
        recv->extra_data = cJSON_PrintUnformatted(item);
    }

    /*
    if(NULL != cJSON_GetObjectItem(data, "streamId") && NULL != cJSON_GetObjectItem(data, "streamFileId")){
        recv->mqtt_url = core_os_malloc(sizeof(mqtt_url_t));
        if(recv->mqtt_url != NULL) {
            memset(recv->mqtt_url, 0, sizeof(mqtt_url_t));
            item = cJSON_GetObjectItem(data, "streamId");
            recv->mqtt_url->stream_id = item->valueint;
            item = cJSON_GetObjectItem(data, "streamFileId");
            recv->mqtt_url->stream_file_id = item->valueint;
        }
    }
    */

    cJSON_Delete(root);
    return recv;
}
