/**
 * @file tunnel_private.h
 * @brief 安全隧道内部结构体定义
 *
 * @copyright Copyright (C) 2015-2020 Alibaba Group Holding Limited
 *
 * @details
 *
 */
#ifndef _TUNNEL_PRIVATE_H_
#define _TUNNEL_PRIVATE_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>
#include "tunnel_buffer_mgr.h"
#include "tunnel_websocket_channel.h"
#include "tunnel_session_protocol.h"
#include "tunnel_session_mgr.h"
#include "aiot_tunnel_api.h"

/* 定义重连时的运行时参数 */
typedef struct {
    int                    retry_times;
    uint64_t               connect_time;
} retry_info_t;

#define DEFAULT_LEN_SERVICE_TYPE         128            /* service type最长的字符串长度 */
#define DEFAULT_LEN_IP                   128            /* ip/host最长字符串长度 */
#define DEFAULT_LEN_TUNNEL_ID            128            /* 隧道id的最长长度 */
#define DEFAULT_MAX_SESSION_COUNT        10             /* 单个隧道支持最大的session个数 */
#define DEFAULT_MAX_BACKOFF_COUNT        7              /* 指数退避最大次数，2^7, 最大退避时间128s*/
#define DEFAULT_MAX_MSG_LEN              (4 * 1024)     /* 单条消息最大的长度 */
#define DEFAULT_MSG_BUFFER_LEN           (DEFAULT_MAX_MSG_LEN + 256) /* 消息缓存大小 */

/* websocket 建连报鉴权出错 */
#define WEBSOCKET_CLOSED_CODE_AUTH_FAIL                401
/* websocket 被断开：token过期 */
#define WEBSOCKET_CLOSED_CODE_TOKEN_EXPIRED            4001


/* 定义新建session时的参数 */
typedef struct {
    int32_t    tcp_flag;
    char       type[DEFAULT_LEN_SERVICE_TYPE];
    aiot_session_ops_t *ops;
    void       *userdata;
} session_params_t;

/* 本地服务节点定义 */
typedef struct {
    session_params_t params;
    struct core_list_head       node;
} service_node_t;

/* 本地服务链表，其node为service_node_t */
typedef struct {
    unsigned int                service_count;
    struct core_list_head       service_list;
} service_list_t;

/* 定义隧道状态 */
typedef enum {
    /* 处于断线状态，还会重连 */
    CLOUD_CHANNEL_CLOSED = 0,
    /* 处于连接状态 */
    CLOUD_CHANNEL_CONNECTED,
    /* token过期状态，不会重连 */
    CLOUD_CHANNEL_EXPIRED,
} tunnel_state_t;

typedef struct {
    /* 配置参数 */
    char                         tunnel_id[DEFAULT_LEN_TUNNEL_ID];  /* 隧道ID */
    aiot_tunnel_connect_param_t  params;                            /* 连接云端通道的参数 */
    service_list_t               local_services;                    /* 本地服务列表 */
    aiot_tunnel_event_callback_t event_callback;                    /* 事件回调 */
    void*                        userdata;                          /* 用户上下文数据 */

    /* 运行时参数 */
    void*                        websocket_channel;                 /* websocket的连接管理 */
    session_list_t               session_list;                      /* 建立的session的管理hash表 */
    RA_BUFFER_INFO_S             cloud_recv_buffer;                 /* 接收云端数据使用的buffer，用于向本地服务转发 */
    RA_BUFFER_INFO_S             cloud_resp_buffer;                 /* 向云端发送回复报文的缓存buffer */
    retry_info_t                 retry_info;                        /* 用于重连退避算法 */
    void*                        lock;                              /* 隧道的状态锁，保证隧道的状态唯一 */
    tunnel_state_t               cloud_channel_state;               /* 云通道的连接状态 */
    void*                        process_thread;                    /* 保存线程的句柄 */
    int32_t                      thread_running;                    /* 线程退出的标识 */
    uint32_t                     msg_id;                            /* 与云端通信的消息id */
    int32_t                      has_connected;                     /* 表示隧道已经在建连 */
    void*                        proxy_service;                     /* tcp代理服务句柄 */
} tunnel_handle_t;

/* session的对象定义 */
typedef struct {
    /* 对应的隧道对象 */
    void *tunnel_handle;
    /* 会话的类型 */
    char *type;
    /* 会话的id */
    char *id;
    /* 用户回调处理方法 */
    aiot_session_ops_t *ops;
    /* 用户自定义内容 */
    char *userdata;
    /* 支持并发数据发送 */
    void *send_lock;
} session_handle_t;



#endif /* _TUNNEL_PRIVATE_H_ */
