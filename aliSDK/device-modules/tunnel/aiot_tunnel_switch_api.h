/**
 * @file aiot_tunnel_switch_api.h
 * @brief 设备的隧道开关模块头文件，提供主动请求隧道、监听物联网平台操作隧道的指令的能力。
 * @date 2022-01-20
 *
 * @copyright Copyright (C) 2020-2025 Alibaba Group Holding Limited
 *
 */

#ifndef _AIOT_TUNNEL_SWITCH_API_H
#define _AIOT_TUNNEL_SWITCH_API_H

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>

/**
 * @brief 隧道开关模块，物联网平台下发隧道操作类型
 *
 */
typedef enum {
    /**
     * @brief 关闭隧道连接
     */
    AIOT_TUNNEL_OPERATOPN_CLOSE,
    /**
     * @brief 打开隧道连接
     */
    AIOT_TUNNEL_OPERATOPN_OPEN,
    /**
     * @brief 更新隧道连接信息, 发生在设备主动请求默认的远程登录隧道信息。
     */
    AIOT_TUNNEL_OPERATOPN_UPDATE,
} aiot_ts_ops_t;

/**
 * @brief 隧道开关消息的结构体
 *
 */
typedef struct {
    /**
     * @brief 消息id，用户可不关注
     */
    uint32_t msg_id;
    /**
     * @brief 字符串，隧道ID，用于标示指定隧道
     */
    char *tunnel_id;
    /**
     * @brief 隧道操作类型，【1，打开隧道】 【0，关闭隧道】
     */
    aiot_ts_ops_t operation;
    /**
     * @brief 隧道连接的地址
     */
    char *host;
    /**
     * @brief 隧道连接的端口号
     */
    uint32_t port;
    /**
     * @brief 隧道连接的具体路径
     */
    char *path;
    /**
     * @brief 隧道建连的token
     */
    char *token;
    /**
     * @brief 隧道token过期的剩余时间,单位s
     */
    uint32_t expired_time;
    /**
     * @brief 隧道token创建的时间,设备系统时间，单位ms
     */
    uint64_t created_time;
    /**
     * @brief 如果是关闭操作，反馈是隧道过期，还是用户关闭
     */
    char *close_reason;
    /**
     * @brief 用户自定义数据，需要用户在隧道创建时传入
     */
    char *udi;
} aiot_ts_msg_t;

/**
 * @brief 设备隧道开关模块消息回调函数原型，用户定义后, 可通过 @ref aiot_device_ts_set_callback 配置
 *
 * @param[in] device 设备句柄
 * @param[in] recv    接收到的消息 @ref aiot_ts_msg_t
 * @param[in] userdata 用户设置的上下文，可通过 @ref aiot_device_ts_set_callback 配置
 */
typedef void (*ts_callback_t)(void *device, const aiot_ts_msg_t *recv, void *userdata);

/**
 * @brief 设置远程配置
 *
 * @param[in] device 设备句柄
 * @param[in] callback 消息回调函数
 * @param[in] userdata 执行回调消息的上下文
 * @return int32_t
 * @retval STATE_SUCCESS 参数配置成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_device_ts_set_callback(void *device, ts_callback_t callback, void *userdata);

/**
 * @brief 请求默认的远程登录隧道建连信息
 *
 * @param[in] device 设备句柄
 * @return int32_t
 * @retval >=STATE_SUCCESS  发送请求成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval others 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_device_ts_request(void *device);

/**
 * @brief 拷贝隧道的消息
 *
 * @param[in] msg 拷贝的消息
 * @return aiot_ts_msg_t *
 * @retval 非空 拷贝成功，返回拷贝的消息
 */
aiot_ts_msg_t *aiot_ts_msg_clone(const aiot_ts_msg_t *msg);

/**
 * @brief 释放隧道的消息资源
 *
 * @param[in] msg 待释放的消息
 */
void aiot_ts_msg_free(const aiot_ts_msg_t *msg);

#if defined(__cplusplus)
}
#endif

#endif