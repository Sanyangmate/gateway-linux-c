#include <stdio.h>
#include "aiot_state_api.h"
#include "tunnel_proxy_service.h"
#include "core_list.h"
#include "core_os.h"
#include "core_log.h"
#include "core_adapter.h"

#define TAG "PROXY_SERVICE"

typedef struct {
    void *network_handle;
    void *session;
    struct core_list_head  node;
} proxy_node_t;

/* tcp代理模式下会话处理代理句柄 */
typedef struct {
    int32_t session_counter;
    struct core_list_head  session_list;
} proxy_service_handle_t;

/* 代理模式下连接到本地服务 */
static void *connect2Local_service(char *host_addr, int port)
{
    void* network_handle = NULL;
    int socket_type = CORE_SYSDEP_SOCKET_TCP_CLIENT;
    uint16_t tmp_port = port;
    uint32_t timeout_ms = 5 * 1000;
    int32_t res = STATE_SUCCESS;

    ALOG_INFO(TAG, "connect2Local_service %s, %d\r\n", host_addr, port);
    network_handle = core_network_init();
    if (network_handle == NULL) {
        return NULL;
    }

    /* 网络句柄参数配置 */
    if ((res = core_network_setopt(network_handle, CORE_SYSDEP_NETWORK_SOCKET_TYPE,
                                   &socket_type)) < STATE_SUCCESS ||
            (res = core_network_setopt(network_handle, CORE_SYSDEP_NETWORK_HOST,
                                       host_addr)) < STATE_SUCCESS ||
            (res = core_network_setopt(network_handle, CORE_SYSDEP_NETWORK_PORT,
                                       &tmp_port)) < STATE_SUCCESS ||
            (res = core_network_setopt(network_handle,
                                       CORE_SYSDEP_NETWORK_CONNECT_TIMEOUT_MS,
                                       &timeout_ms)) < STATE_SUCCESS) {
        core_network_deinit(&network_handle);
        ALOG_ERROR(TAG, STATE_TUNNEL_INNER_ERROR, "core_sysdep_network_setopt error\r\n");
        return NULL;
    }

    /* 执行网络建连 */
    if((res = core_network_establish(network_handle)) < STATE_SUCCESS)
    {
        core_network_deinit(&network_handle);
        return NULL;
    }
    return network_handle;
}

static int32_t add_session_to_list(proxy_service_handle_t *service, void *session, void *network)
{
    proxy_node_t *proxy_node = NULL;

    proxy_node = core_os_malloc(sizeof(proxy_node_t));
    if(proxy_node == NULL) {
        return -1;
    }

    memset(proxy_node, 0, sizeof(*proxy_node));
    proxy_node->network_handle = network;
    proxy_node->session = session;
    core_list_add(&proxy_node->node, &service->session_list);
    service->session_counter++;

    return STATE_SUCCESS;
}

static int32_t delete_session_from_list(proxy_service_handle_t *service, void *session)
{
    proxy_node_t *item = NULL, *next = NULL;
    core_list_for_each_entry_safe(item, next, &service->session_list, node, proxy_node_t) {
        if(item->session == session) {
            core_list_del(&item->node);
            core_network_deinit(&item->network_handle);
            core_os_free(item);
            service->session_counter--;
            return STATE_SUCCESS;
        }
    }

    return -1;
}

static proxy_node_t* get_session_from_list(proxy_service_handle_t *service, void *session)
{
    proxy_node_t *item = NULL, *next = NULL;
    core_list_for_each_entry_safe(item, next, &service->session_list, node, proxy_node_t) {
        if(item->session == session) {
            return item;
        }
    }

    return NULL;
}

static int32_t session_new(void *session, void *userdata)
{
    proxy_params_t *params = (proxy_params_t *)userdata;
    proxy_service_handle_t *service_handle = NULL;
    void* network_handle = NULL;
    if(params == NULL || params->proxy_handle == NULL) {
        return -1;
    }
    service_handle = (proxy_service_handle_t *)params->proxy_handle;

    network_handle = connect2Local_service(params->ip, params->port);
    if(network_handle == NULL) {
        return -1;
    }

    return add_session_to_list(service_handle, session, network_handle);
}
static int32_t session_release(void *session, void *userdata)
{
    proxy_params_t *params = (proxy_params_t *)userdata;
    proxy_service_handle_t *service_handle = NULL;
    if(params == NULL || params->proxy_handle == NULL || session == NULL) {
        return -1;
    }
    service_handle = (proxy_service_handle_t *)params->proxy_handle;

    return delete_session_from_list(service_handle, session);
}
static int32_t session_data(void *session, uint8_t *data, uint32_t len, void *userdata)
{
    proxy_params_t *params = (proxy_params_t *)userdata;
    proxy_service_handle_t *service_handle = NULL;
    proxy_node_t* proxy_node = NULL;
    int res = STATE_SUCCESS;

    if(params == NULL || params->proxy_handle == NULL || session == NULL) {
        return -1;
    }
    service_handle = (proxy_service_handle_t *)params->proxy_handle;
    ALOG_HEX(TAG, LOG_LEVEL_DEBUG, '>', (uint8_t *)data, len);
    proxy_node = get_session_from_list(service_handle, session);
    if(proxy_node != NULL) {
        res = core_network_send(proxy_node->network_handle, data, len, 500, NULL);
        if(res == len) {
            return STATE_SUCCESS;
        }
    }
    return -1;
}

/* 初始化一个代理对象 */
void *tcp_proxy_init()
{
    proxy_service_handle_t *handle = NULL;

    handle = (proxy_service_handle_t *)core_os_malloc(sizeof(proxy_service_handle_t));
    memset(handle, 0, sizeof(proxy_service_handle_t));

    CORE_INIT_LIST_HEAD(&handle->session_list);
    return handle;
}

/* 处理本地服务并完成消息转发 */
static int local_service_data_proc(proxy_node_t *session_handle)
{
    int recv_len = 0;
    char buffer[1024] = {0};

    /* 接收本地服务的数据 */
    recv_len = core_network_recv(session_handle->network_handle, (uint8_t *)buffer, sizeof(buffer), 10, NULL);
    if (recv_len < 0) {
        ALOG_ERROR(TAG, STATE_TUNNEL_INNER_ERROR, "local service exit, release session: %s", aiot_session_id(session_handle));
        return -1;
    }
    else if (recv_len == 0) {
        return STATE_SUCCESS;
    }
    ALOG_HEX(TAG, LOG_LEVEL_DEBUG, '<', (uint8_t *)buffer, recv_len);
    /* 生成透传消息 */
    aiot_session_send_data(session_handle->session, (uint8_t *)buffer, recv_len);
    return STATE_SUCCESS;
}
typedef struct
{
    int                      fd_num;                            /* 可读的fds的数目 */
    proxy_node_t            *session_array[10];                /* 可读取的session的缓存 */
} active_fd_callback_params_t;

/* 处理session接收 */
int32_t tcp_proxy_run(void *handle)
{
    proxy_service_handle_t *service_handle = (proxy_service_handle_t *)handle;
    active_fd_callback_params_t params = { .fd_num = 0 };
    proxy_node_t *item = NULL, *next = NULL;
    int32_t i = 0;
    int32_t res = STATE_SUCCESS;
    if(service_handle == NULL || service_handle->session_counter == 0) {
        return -1;
    }

    core_list_for_each_entry_safe(item, next, &service_handle->session_list, node, proxy_node_t) {
        params.session_array[params.fd_num++] = item;
    }
    /* 接收本地消息发送至云端 */
    if (params.fd_num > 0) {
        /* 依次读取可读的本地服务socket */
        for (i = 0; i < params.fd_num; i++) {
            res = local_service_data_proc(params.session_array[i]);
            if(res != STATE_SUCCESS) {
                aiot_session_release(params.session_array[i]->session);
                delete_session_from_list(service_handle, params.session_array[i]->session);
            }
        }
    }

    return STATE_SUCCESS;
}

/* 删除代理节点 */
int32_t tcp_proxy_deinit(void **handle)
{
    proxy_node_t *item = NULL, *next = NULL;
    proxy_service_handle_t *service_handle = *(proxy_service_handle_t **)handle;

    core_list_for_each_entry_safe(item, next, &service_handle->session_list, node, proxy_node_t) {
        core_list_del(&item->node);
        core_network_deinit(&item->network_handle);
    }

    core_os_free(service_handle);
    *handle = NULL;

    return STATE_SUCCESS;
}

static aiot_session_ops_t tcp_ops = {
    .session_new = session_new,
    .session_release = session_release,
    .session_recv_data = session_data,
};

aiot_session_ops_t *tcp_proxy_ops()
{
    return &tcp_ops;
}
