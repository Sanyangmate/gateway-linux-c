
#include "tunnel_session_mgr.h"
#include "core_stdinc.h"
#include "core_log.h"
#include "core_os.h"
#include "core_adapter.h"

static session_node_t *creat_one_session_node(char *session_id, void *handle)
{
    session_node_t *session_node = NULL;
    /* 创建session空间 */
    session_node = (session_node_t *)core_os_malloc(sizeof(session_node_t));
    if(NULL == session_node) {
        return NULL;
    }
    memset(session_node, 0, sizeof(session_node_t));

    session_node->session_handle = handle;
    core_strdup(NULL, &session_node->session_id, session_id, NULL);
    return session_node;
}

static int release_one_session_node(session_node_t *session_node)
{
    core_os_free(session_node->session_id);
    core_os_free(session_node);
    return 0;
}

/*************************************************************************
 * 接口名称：init_session_list
 * 描       述：初始化session哈希表
 * 输入参数：
 * 输出参数：
 * 返  回 值：成功：0
 *          失败：非0
 * 说       明：
 *************************************************************************/
int init_session_list(session_list_t *session_list)
{
    if (NULL == session_list) {
        return -1;
    }

    session_list->list_lock = core_os_mutex_init();
    CORE_INIT_LIST_HEAD(&session_list->session_list);

    return STATE_SUCCESS;
}

/*************************************************************************
 * 接口名称：release_all_session_from_list
 * 描       述：释放所有的session
 * 输入参数：
 * 输出参数：
 * 返  回 值：成功：0
 *          失败：非0
 * 说       明：
 *************************************************************************/
int release_all_session_from_list(session_list_t *session_list)
{
    session_node_t *item = NULL, *next = NULL;
    core_os_mutex_lock(session_list->list_lock);

    core_list_for_each_entry_safe(item, next, &session_list->session_list, node, session_node_t) {
        core_list_del(&item->node);
        release_one_session_node(item);
    }

    core_os_mutex_unlock(session_list->list_lock);
    return 0;
}

/*************************************************************************
 * 接口名称：add_one_session_to_list
 * 描       述：向哈希表中添加一个session资源节点
 * 输入参数：
 * 输出参数：
 * 返  回 值：成功：0
 *          失败：非0
 * 说       明：
 *************************************************************************/
int add_one_session_to_list(session_list_t *session_list, char* session_id, void *session_handle)
{
    if (NULL == session_list) {
        return -1;
    }
    core_os_mutex_lock(session_list->list_lock);

    session_node_t *session_info = creat_one_session_node(session_id, session_handle);
    if(NULL == session_info) {
        core_os_mutex_unlock(session_list->list_lock);
        return -1;
    }

    core_list_add(&session_info->node, &session_list->session_list);
    core_os_mutex_unlock(session_list->list_lock);

    return STATE_SUCCESS;
}

/*************************************************************************
 * 接口名称：release_one_session_from_list
 * 描       述：从哈希表中释放一个session资源节点
 * 输入参数：
 * 输出参数：
 * 返  回 值：成功：0
 *          失败：非0
 * 说       明：
 *************************************************************************/
int release_one_session_from_list(session_list_t *session_list,char* session_id)
{
    /* 找到此session */
    session_node_t *session_node = get_one_session_from_list(session_list, session_id);
    if (NULL == session_list) {
        return -1;
    }

    if(NULL == session_node) {
        return STATE_SUCCESS;
    }

    core_os_mutex_lock(session_list->list_lock);
    /* 从List中remove出session */
    core_list_del(&session_node->node);
    /* 释放session */
    release_one_session_node(session_node);
    core_os_mutex_unlock(session_list->list_lock);

    return STATE_SUCCESS;
}

/*************************************************************************
 * 接口名称：get_one_session_from_list
 * 描       述：从哈希表中获取一个session
 * 输入参数：
 * 输出参数：
 * 返  回 值：成功：0
 *          失败：非0
 * 说       明：
 *************************************************************************/
session_node_t* get_one_session_from_list(session_list_t *session_list,char* session_id)
{
    session_node_t *iterm = NULL, *next = NULL;
    if (NULL == session_list) {
        return NULL;
    }
    core_os_mutex_lock(session_list->list_lock);

    core_list_for_each_entry_safe(iterm, next, &session_list->session_list, node, session_node_t) {
        if(0 == strncmp(iterm->session_id, session_id, strlen(session_id))) {
            core_os_mutex_unlock(session_list->list_lock);
            return iterm;
        }
    }
    core_os_mutex_unlock(session_list->list_lock);

    return NULL;

}

/*************************************************************************
 * 接口名称：has_one_session
 * 描       述：判断session是否存在
 * 输入参数：
 * 输出参数：
 * 返  回 值：成功：0
 *          失败：非0
 * 说       明：
 *************************************************************************/
int has_one_session(session_list_t *session_list,char* session_id)
{
    if (NULL == session_list) {
        return -1;
    }

    session_node_t *node = get_one_session_from_list(session_list, session_id);
    if(node == NULL) {
        return -1;
    }

    return STATE_SUCCESS;
}

/*********************************************************
 * 接口名称：get_session_num_from_list
 * 描       述：获取本地服务session的数量
 * 输入参数：session_list_t *session_list
 * 输出参数：
 * 返  回 值：
 * 说       明：
 *********************************************************/
int get_session_num_from_list(session_list_t *session_list)
{
    int num = 0;
    session_node_t *iterm = NULL, *next = NULL;
    if (NULL == session_list) {
        return -1;
    }
    core_os_mutex_lock(session_list->list_lock);

    core_list_for_each_entry_safe(iterm, next, &session_list->session_list, node, session_node_t) {
        num++;
    }
    core_os_mutex_unlock(session_list->list_lock);

    return num;
}

/*************************************************************************
 * 接口名称：iterate_each_session
 * 描       述：遍历session_list中的每一个session资源
 * 输入参数：
 * 输出参数：
 * 返  回 值：成功：0
 *          失败：非0
 * 说       明：
 *************************************************************************/
void iterate_each_session(session_list_t *session_list, iter_callback_t callback, void *data)
{
    if (NULL == session_list) {
        return;
    }
    core_os_mutex_lock(session_list->list_lock);
    session_node_t *iterm = NULL, *next = NULL;
    core_list_for_each_entry_safe(iterm, next, &session_list->session_list, node, session_node_t) {
        if(callback != NULL) {
            callback(iterm,data);
        }
    }
    core_os_mutex_unlock(session_list->list_lock);
}



