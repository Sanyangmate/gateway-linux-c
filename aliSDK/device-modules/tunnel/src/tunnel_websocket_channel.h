/**
 * @file tunnel_proxy_channel.h
 *
 * @copyright Copyright (C) 2015-2020 Alibaba Group Holding Limited
 *
 * @details
 *
 */

#ifndef _TUNNEL_WEBSOCKET_CHANNEL_H_
#define _TUNNEL_WEBSOCKET_CHANNEL_H_

#include "tunnel_buffer_mgr.h"

void* websocket_channel_init(char *tunnel_id, char *host, char *port, char *path, char *token, void *cred);
void websocket_channel_deinit(void **handle);
int32_t websocket_channel_connect(void *handle);
int32_t websocket_channel_disconnect(void *handle);
int32_t websocket_channel_send(void *handle, char *data, uint32_t len, uint32_t timeout);
int32_t websocket_channel_recv(void *channel_handle, RA_BUFFER_INFO_S *channel_buffer, uint32_t timeout);
int websocket_channel_keepalive(void *handle);


#endif
