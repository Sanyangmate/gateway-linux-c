#include <core_stdinc.h>
#include "nopoll.h"
#include "core_log.h"
#include "core_os.h"
#include "core_adapter.h"
#include "tunnel_websocket_channel.h"

#define TAG "WEBSOCKET"

#define SEND_WAITING_PERIOD_MS 50
typedef struct {
    void* network_handle;
    noPollConn *conn;
    char *tunnel_id;
    char *host;
    char *port;
    char *path;
    char *token;
    void *cred;
    void *read_lock;
    void *write_lock;
    int32_t close_code;
    int32_t status;
    uint64_t                 ping_time;                         //发送心跳的时间
    uint64_t                 pong_time;                         //发送心跳的时间
} websocket_channel_t;


static void nopoll_log_callback(noPollCtx * ctx, noPollDebugLevel level, char * log_msg, noPollPtr user_data)
{
    ALOG_INFO(TAG, "%s\r\n",log_msg);
}

static nopoll_bool recv_nopoll_pong_cb(noPollCtx * ctx, noPollConn * conn, noPollPtr user_data)
{
    websocket_channel_t *channel = (websocket_channel_t *)user_data;

    if(channel != NULL) {
        ALOG_INFO(TAG, "websocket %s pong\r\n", channel->tunnel_id);
        channel->pong_time = core_os_time();
        return nopoll_true;
    } else {
        return nopoll_false;
    }
}

int websocket_channel_keepalive(void *handle)
{
    uint64_t tp_now;
    websocket_channel_t *channel = (websocket_channel_t *)handle;
    tp_now = core_os_time();

    /* 已经通过回调通知离线 */
    if(channel->status == 0) {
        return channel->close_code;
    }

    /* 心跳超时 */
    if(tp_now - channel->pong_time >= 70 * 1000) {
        return -1;
    }

    /* 发送心跳 */
    if(tp_now - channel->ping_time >= 20 * 1000) {
        /* 发送通道保活信号 */
        ALOG_INFO(TAG, "websocket %s ping\r\n", channel->tunnel_id);
        core_os_mutex_lock(channel->write_lock);
        nopoll_conn_send_ping(channel->conn);
        core_os_mutex_unlock(channel->write_lock);
        channel->ping_time = core_os_time();
    }
    return STATE_SUCCESS;
}

static void close_nopoll_conn_cb(noPollCtx  * ctx,noPollConn * conn, noPollPtr user_data)
{
    void* network_handle = nopoll_conn_get_userdata(conn);
    websocket_channel_t *channel = (websocket_channel_t *)user_data;

    channel->status = 0;
    channel->close_code = nopoll_conn_get_close_status(conn);
    if(NULL != network_handle)
    {
        ALOG_INFO(TAG, "websocket %s closed\r\n", channel->tunnel_id);
        nopoll_ctx_set_userdata(ctx, NULL);
        core_network_deinit(&network_handle);
    }
}

static int read_callback(noPollConn * conn, char *buffer, int buffer_size)
{
    int recv_len = -1;
    void* network_handle = nopoll_conn_get_userdata(conn);

    recv_len = core_network_recv(network_handle, (uint8_t *)buffer, buffer_size, 100, NULL);
    if(recv_len < STATE_SUCCESS) {
        return -1;
    }
    return recv_len;
}

static int write_callback(noPollConn * conn, char *buffer, int buffer_size)
{
    int send_len = -1;
    void* network_handle = nopoll_conn_get_userdata(conn);

    send_len = core_network_send(network_handle, (uint8_t *)buffer, buffer_size, 100, NULL);
    if(send_len < STATE_SUCCESS) {
        return -1;
    }

    return send_len;
}

void* websocket_channel_init(char *tunnel_id, char *host, char *port, char *path, char *token, void *cred)
{
    websocket_channel_t *channel;

    if(host == NULL || path == NULL || token == NULL) {
        return NULL;
    }

    channel = core_os_malloc(sizeof(websocket_channel_t));
    if(channel == NULL) {
        return NULL;
    }
    memset(channel, 0, sizeof(websocket_channel_t));

    core_strdup(NULL, &channel->tunnel_id, tunnel_id, NULL);
    core_strdup(NULL, &channel->host, host, NULL);
    core_strdup(NULL, &channel->port, port, NULL);
    core_strdup(NULL, &channel->path, path, NULL);
    core_strdup(NULL, &channel->token, token, NULL);
    channel->cred = cred;

    channel->ping_time = core_os_time();
    channel->pong_time = core_os_time();

    channel->close_code = 0;
    channel->status = 0;
    channel->read_lock = core_os_mutex_init();
    channel->write_lock = core_os_mutex_init();

    return channel;
}

void websocket_channel_deinit(void **handle)
{
    websocket_channel_t *channel = NULL;

    if(handle == NULL || *handle == NULL) {
        return;
    }

    channel = (websocket_channel_t *)(*handle);

    if(channel->tunnel_id != NULL) {
        core_os_free(channel->tunnel_id);
    }
    if(channel->host != NULL) {
        core_os_free(channel->host);
    }
    if(channel->port != NULL) {
        core_os_free(channel->port);
    }
    if(channel->path != NULL) {
        core_os_free(channel->path);
    }
    if(channel->token != NULL) {
        core_os_free(channel->token);
    }
    if(channel->conn != NULL) {
        nopoll_conn_close(channel->conn);
        nopoll_cleanup_library();
    }

    core_os_mutex_deinit(&channel->read_lock);
    core_os_mutex_deinit(&channel->write_lock);

    core_os_free(channel);
    *handle = NULL;
}

int32_t _tcp_connect(websocket_channel_t *channel)
{
    void *network_handle = NULL;
    int32_t res = STATE_SUCCESS;
    int socket_type = CORE_SYSDEP_SOCKET_TCP_CLIENT;
    uint32_t tmp_port = 0;
    uint16_t u16_port = 0;
    uint32_t timeout_ms = 5 * 1000;

    core_str2uint(channel->port, strlen(channel->port), &tmp_port);
    u16_port = tmp_port;

    /* 网络句柄初始化 */
    network_handle = core_network_init();
    if (network_handle == NULL) {
        return -1;
    }

    if ((res = core_network_setopt(network_handle, CORE_SYSDEP_NETWORK_SOCKET_TYPE,
                                   &socket_type)) < STATE_SUCCESS ||
            (res = core_network_setopt(network_handle, CORE_SYSDEP_NETWORK_HOST,
                                       channel->host)) < STATE_SUCCESS ||
            (res = core_network_setopt(network_handle, CORE_SYSDEP_NETWORK_PORT,
                                       &u16_port)) < STATE_SUCCESS ||
            (res = core_network_setopt(network_handle, CORE_SYSDEP_NETWORK_CONNECT_TIMEOUT_MS,
                                       &timeout_ms)) < STATE_SUCCESS ||
            (res = core_network_setopt(network_handle, CORE_SYSDEP_NETWORK_CRED, channel->cred)) < STATE_SUCCESS) {
        core_network_deinit(&network_handle);
        return -1;
    }

    /* 进行网络连接 */
    res = core_network_establish(network_handle);
    if(res < STATE_SUCCESS) {
        core_network_deinit(&network_handle);
        return -1;
    }
    channel->network_handle = network_handle;

    return STATE_SUCCESS;
}

static int32_t websocket_connect(websocket_channel_t *channel)
{
    noPollCtx *ctx = NULL;
    noPollConn *conn = NULL;
    noPollConnOpts *opts = NULL;
    int socket_fd = -1;
    int32_t ret_code = STATE_SUCCESS;
    char ext_header[128];
    int32_t time_counter = 0;

    if(channel == NULL) {
        return -1;
    }

    memcpy(&socket_fd, channel->network_handle, sizeof(socket_fd));

    /* create context */
    ctx = nopoll_ctx_new();
    if (ctx == NULL) {
        ALOG_ERROR(TAG, STATE_PORT_MALLOC_FAILED, "nopoll new failed\r\n");
        ret_code = STATE_PORT_MALLOC_FAILED;
        goto error_label;
    }
    nopoll_ctx_set_read_write_handle(ctx, read_callback, write_callback);
    nopoll_ctx_set_userdata(ctx, channel->network_handle);
    nopoll_log_set_handler(ctx,(noPollLogHandler)nopoll_log_callback,NULL);

    opts = nopoll_conn_opts_new();
    if (opts == NULL) {
        ALOG_ERROR(TAG, STATE_PORT_MALLOC_FAILED, "nopoll new failed\r\n");
        ret_code = STATE_PORT_MALLOC_FAILED;
        goto error_label;
    }

    memset(ext_header, 0, sizeof(ext_header));
    snprintf(ext_header, sizeof(ext_header), "\r\ntunnel-access-token: %s\r\nSec-WebSocket-Protocol: aliyun.iot.securetunnel-v1.1", channel->token);
    nopoll_conn_opts_set_exttunnel_headers(opts, ext_header);

    conn = nopoll_conn_new_with_socket(ctx, opts, socket_fd, channel->host, channel->port, NULL, channel->path, NULL, NULL);
    if (!nopoll_conn_is_ok(conn)) {
        ret_code = -1;
        goto error_label;
    }

    /* wait for ready */
    while ((time_counter++) < 3) {
        if (nopoll_conn_wait_until_connection_ready (conn, 5)) {
            /* 连接成功 */
            ALOG_INFO(TAG, "open_cloud_proxy_channel success \r\n");
            channel->conn = conn;
            nopoll_conn_set_on_pong(conn, recv_nopoll_pong_cb, channel);
            nopoll_conn_set_on_close(conn, close_nopoll_conn_cb, channel);
            channel->status = 1;
            return STATE_SUCCESS;
        }
    }


    ret_code = nopoll_conn_get_close_status(conn);
    ALOG_INFO(TAG, "open_cloud_proxy_channel failed, %d \r\n", ret_code);
error_label:
    if (NULL != conn) {
        nopoll_conn_close(conn);
    } else if (NULL != ctx) {
        nopoll_ctx_unref(ctx);
    }

    return ret_code;
}

int32_t websocket_channel_connect(void *handle)
{
    websocket_channel_t *channel = (websocket_channel_t *)handle;
    int32_t res = STATE_SUCCESS;
    if(channel == NULL) {
        return -1;
    }

    ALOG_INFO(TAG, "connect websocket service host %s, port %s\r\n", channel->host, channel->port);

    core_os_mutex_lock(channel->read_lock);
    core_os_mutex_lock(channel->write_lock);
    res = _tcp_connect(channel);
    if(res < STATE_SUCCESS) {
        ALOG_ERROR(TAG, res, "tls connect failed\r\n");
        core_os_mutex_unlock(channel->write_lock);
        core_os_mutex_unlock(channel->read_lock);
        return res;
    }
    ALOG_INFO(TAG, "tls connect success\r\n");

    res = websocket_connect(channel);
    if(res != STATE_SUCCESS) {
        /* websocket连接失败，将tcp资源也释放 */
        ALOG_ERROR(TAG, res, "websocket connect failed\r\n");
        core_network_deinit(&channel->network_handle);
    }
    core_os_mutex_unlock(channel->write_lock);
    core_os_mutex_unlock(channel->read_lock);
    return res;
}

int32_t websocket_channel_disconnect(void *handle)
{
    websocket_channel_t *channel = (websocket_channel_t *)handle;
    if(channel == NULL) {
        return -1;
    }
    core_os_mutex_lock(channel->read_lock);
    core_os_mutex_lock(channel->write_lock);
    /* finish connection */
    if (NULL != channel->conn) {
        nopoll_conn_close(channel->conn);
        nopoll_cleanup_library();
        channel->conn = NULL;
    }
    core_os_mutex_unlock(channel->write_lock);
    core_os_mutex_unlock(channel->read_lock);

    return STATE_SUCCESS;
}

int32_t websocket_channel_send(void *handle, char *data, uint32_t len, uint32_t timeout)
{
    websocket_channel_t *channel = (websocket_channel_t *)handle;
    noPollConn *conn = NULL;
    int retry_times = 3;
    int retry_interval = 50;
    int ret = 0;
    int remain_tmp = 0;

    if(channel == NULL || data == NULL) {
        return -1;
    }

    conn = channel->conn;
    /* 超时时间计算边界判断 */
    if(timeout > 10000) {
        timeout = 10000;
    }
    else if(timeout < 50) {
        timeout = 50;
    }

    core_os_mutex_lock(channel->write_lock);
    /* 首先尽力发送待发送数据 */
    if(nopoll_conn_pending_write_bytes(conn) > 0) {
        do {
            if (nopoll_conn_pending_write_bytes(conn) > 0) {
                if(0 == nopoll_conn_complete_pending_write(conn)) {
                    /* 待发送数据已经发送完成 */
                    break;
                }
            }

            core_os_sleep(retry_interval);

        } while (--retry_times);

        /* 用户指定数据没有发送 */
        if (nopoll_conn_pending_write_bytes(conn) > 0) {
            core_os_mutex_unlock(channel->write_lock);
            return 0;
        }
    }

    /* ALOG_HEX(TAG, LOG_LEVEL_DEBUG, 'S', (uint8_t *)data, len); */
    /* 发送数据 */
    ret = nopoll_conn_send_binary(conn, data, len);
    if(ret == len) {
        /* 成功发送 */
        core_os_mutex_unlock(channel->write_lock);
        return len;
    }
    else if(ret == -1) {
        /* 异常返回 */
        core_os_mutex_unlock(channel->write_lock);
        return ret;
    } else {
        /* 其它情况需要继续等待超时，计算当前发送数据为0 */
        ret = 0;
    }


    /* 部分成功或网络异常需要重试 */
    retry_times = timeout / retry_interval;
    do {
        if (nopoll_conn_pending_write_bytes(conn) > 0) {
            remain_tmp = nopoll_conn_complete_pending_write(conn);
            if(remain_tmp == 0) {
                //发送完成则退出
                ret = len;
                break;
            }
            else if(remain_tmp > 0) {
                ret = len - remain_tmp;
            }
        }
        else {
            ret = len;
            break;
        }

        core_os_sleep(retry_interval);
    } while (--retry_times);
    core_os_mutex_unlock(channel->write_lock);

    return ret;
}

int websocket_channel_recv(void *channel_handle, RA_BUFFER_INFO_S *channel_buffer, uint32_t timeout)
{
    websocket_channel_t *channel = (websocket_channel_t *)channel_handle;
    noPollConn *conn = NULL;
    noPollMsg *msg = NULL;
    int amount = 0, total = 0;
    char *payload = NULL;
    int ret = 0;

    if(channel == NULL || channel_buffer == NULL) {
        return -1;
    }
    core_os_mutex_lock(channel->read_lock);
    conn = channel->conn;
    do {
        /* 接收msg数据,此接口为非阻塞接口 */
        msg = nopoll_conn_get_msg(conn);
        if (msg == NULL) {
            /* 没有数据则直接退出,有可读事件但是无完整数据此时适当等待 */
            core_os_mutex_unlock(channel->read_lock);
            return -1;
        }

        /* 读取当前报文大小 */
        amount = nopoll_msg_get_payload_size(msg);
        if (amount <= 0) {
            /* 没有数据则直接退出 */
            nopoll_msg_unref(msg);
            ALOG_ERROR(TAG, 0, "can not get cloud message size!\r\n");
            core_os_mutex_unlock(channel->read_lock);
            return 0;
        }

        payload = (char*)nopoll_msg_get_payload(msg);
        if(NULL == payload) {
            nopoll_msg_unref(msg);
            ALOG_ERROR(TAG, 0, "can not get cloud message payload!\r\n");
            core_os_mutex_unlock(channel->read_lock);
            return 0;
        }

        /* ALOG_HEX(TAG, LOG_LEVEL_DEBUG, 'R', (uint8_t *)payload, amount); */
        /* 将msg数据放入导入回调结构中,需要判断超出范围 */
        ret = write_tunnel_buffer(channel_buffer, payload, amount);
        total += amount;
        if(ret < 0) {
            nopoll_msg_unref(msg);
            ALOG_ERROR(TAG, 0, "write the cloud channel buffer error!\r\n");
            core_os_mutex_unlock(channel->read_lock);
            return -1;
        }

        /* 判断接收的msg的完整性 */
        if(nopoll_msg_is_final(msg)) {
            /* 已经接收到完整数据包，则进行数据处理 */
            nopoll_msg_unref(msg);
            break;
        } else {
            ALOG_ERROR(TAG, 0, "this websocket message not have Fin flag!\r\n");
        }
        nopoll_msg_unref(msg);

    } while (1);
    core_os_mutex_unlock(channel->read_lock);

    return total;
}
