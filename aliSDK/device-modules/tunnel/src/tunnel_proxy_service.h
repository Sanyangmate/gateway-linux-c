/**
 * @file tunnel_proxy_service.h
 * @brief 安全隧道会话处理：tcp代理模式
 *
 * @copyright Copyright (C) 2020-2025 Alibaba Group Holding Limited
 *
 * @details
 *
 */
#ifndef _TUNNEL_PROXY_SERVICE_H_
#define _TUNNEL_PROXY_SERVICE_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>
#include "aiot_tunnel_api.h"

/* TCP代理模式session创建时的参数 */
typedef struct {
    void       *proxy_handle;
    char       ip[128];
    unsigned   port;
} proxy_params_t;

/* 初始化一个代理对象 */
void *tcp_proxy_init();

/* 处理session接收 */
int32_t tcp_proxy_run(void *handle);

/* 删除代理节点 */
int32_t tcp_proxy_deinit(void **handle);

aiot_session_ops_t *tcp_proxy_ops();




#endif /* _TUNNEL_PROXY_SERVICE_H_ */
