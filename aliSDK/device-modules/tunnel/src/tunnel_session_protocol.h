/**
 * @file tunnel_proxy_protocol.h
 *
 * @copyright Copyright (C) 2015-2020 Alibaba Group Holding Limited
 *
 * @details
 *
 */

#ifndef _TUNNEL_SESSION_PROTOCOL_H_
#define _TUNNEL_SESSION_PROTOCOL_H_

/* 定义与云端通信的命令类型 */
typedef enum {
    /* 回复消息命令字 */
    CLOUD_CMD_RESP_OK = 1,
    /* 创建session命令字 */
    CLOUD_CMD_NEW_SESSION = 2,
    /* 释放session命令字 */
    CLOUD_CMD_RELEASE_SESSION = 3,
    /* 透传数据命令字 */
    CLOUD_CMD_RAW_DATA = 4,
} tunnel_cloud_cmd_t;

/* 定义与云端通信的结构化数据结构 */
typedef struct {
    tunnel_cloud_cmd_t       msg_type;               /* 指令类型 */
    char                     session_id[64];         /* session的唯一id */
    char                     srv_type[64];           /* 服务类型，非必须 */
    uint32_t                 msg_id;                 /* 消息id */
    uint64_t                 timestamp;              /* 时间戳 */
    uint8_t                  *payload;               /* 数据载体 */
    uint32_t                 payload_len;            /* 数据载体长度 */
} tunnel_cloud_msg_t;

/* 解析云端来的数据，将序列化的数据解析成结构化的msg */
int cloud_protocol_parse_msg(RA_BUFFER_INFO_S *channel_buffer, tunnel_cloud_msg_t *msg);
/* 创建释放session请求的消息 */
int cloud_protocol_msg_release_session_request(RA_BUFFER_INFO_S *channel_buffer, uint32_t msg_id, char *session_id);
/* 创建response的消息 */
int cloud_protocol_msg_response_with_code(RA_BUFFER_INFO_S *channel_buffer,int code, char *msg, uint32_t msg_id, char *session_id);
/* 创建透传数据的消息 */
int cloud_protocol_msg_raw_data_post(RA_BUFFER_INFO_S *channel_buffer, uint32_t msg_id, const char *data, int32_t len, const char *session_id);

#endif
