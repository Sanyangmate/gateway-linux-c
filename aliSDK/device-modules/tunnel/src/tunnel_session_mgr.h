/**
 * @file tunnel_session_mgr.h
 * @brief 管理隧道的绘画
 *
 * @copyright Copyright (C) 2015-2020 Alibaba Group Holding Limited
 *
 * @details
 *
 */

#ifndef _TUNNEL_SESSION_MGR_H_
#define _TUNNEL_SESSION_MGR_H_

#include "core_list.h"

/* session节点 */
typedef struct {
    char  *session_id;             /* session的唯一id */
    void  *session_handle;         /* session的句柄 */
    struct core_list_head  node;
} session_node_t;

/* session链表 */
typedef struct {
    struct core_list_head  session_list;           /* session服务链表 */
    void *list_lock;                               /* 链表锁 */
} session_list_t;

typedef int (* iter_callback_t)(session_node_t *session_info, void *data);

int init_session_list(session_list_t *session_list);
int add_one_session_to_list(session_list_t *session_list, char* session_id, void *session_handle);
int release_one_session_from_list(session_list_t *session_list, char* session_id);
int release_all_session_from_list(session_list_t *session_list);
void iterate_each_session(session_list_t *session_list,iter_callback_t callback, void* data);
session_node_t* get_one_session_from_list(session_list_t *session_list, char* session_id);
int get_session_num_from_list(session_list_t *session_list);
int has_one_session(session_list_t *session_list, char* session_id);

#endif /* ADVANCED_SERVICES_REMOTE_ACCESS_DAEMON_REMOTE_ACCESS_SESSION_MGR_H_ */
