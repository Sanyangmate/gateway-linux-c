
#include <stdio.h>
#include "core_stdinc.h"
#include "core_string.h"
#include "core_log.h"
#include "core_os.h"
#include "cJSON.h"
#include "tunnel_buffer_mgr.h"
#include "tunnel_session_protocol.h"

#define TAG "TUNNEL"

/*消息报文头格式-->通用*/
#define MSG_HEAD_FMT "{\"frame_type\":%d,\"frame_id\":%u,\"session_id\":\"%s\"}"
/*CLOUD_CMD_RESP_OK响应报文格式*/
#define MSG_RESPONSE_FMT "{\"code\":%d,\"msg\":\"%s\"}"
#define DEFAULT_MSG_HDR_LEN      1024

/* 生成报文头 */
static int32_t splice_proxy_protocol_header(char* buffer, int32_t size, int32_t msg_type, int32_t payload_len, uint32_t msg_id, char *token)
{
    memset(buffer, 0, size);
    int32_t ret = snprintf(&buffer[2], size - 3, MSG_HEAD_FMT, msg_type, msg_id, token == NULL ? "" : token);

    buffer[0] = (ret & 0xff00) >> 8;
    buffer[1] = ret & 0xff;
    ret += 2;
    return ret;
}

/* 生成报文payload */
static int32_t splice_proxy_protocol_response_payload(char* buffer, int32_t size, int32_t code, char *data, char *msg)
{
    memset(buffer, 0, size);
    int32_t ret = snprintf(buffer, size - 1, MSG_RESPONSE_FMT, code, !msg ? "null" : msg);

    return ret;
}

/* 解析报文头 */
static int32_t cloud_protocol_parse_header(char *buf, int32_t buf_len, tunnel_cloud_msg_t *hdr)
{
    cJSON *root = NULL, *item = NULL;

    memset(hdr, 0, sizeof(tunnel_cloud_msg_t));

    root = cJSON_ParseWithLength(buf, buf_len);
    if(root == NULL) {
        return -1;
    }

    /* 解析消息类型 */
    item = cJSON_GetObjectItem(root, "frame_type");
    if(item == NULL) {
        cJSON_Delete(root);
        return -1;
    }
    hdr->msg_type = item->valueint;

    /* 解析消息id */
    item = cJSON_GetObjectItem(root, "frame_id");
    if(item == NULL) {
        cJSON_Delete(root);
        return -1;
    }
    hdr->msg_id = item->valuedouble;

    /* 解析服务类型，只有new_session时需要 */
    item = cJSON_GetObjectItem(root, "service_type");
    if(item != NULL) {
        strncpy(hdr->srv_type, item->valuestring, strlen(item->valuestring) < 63 ? strlen(item->valuestring) : 63);
    }

    /* 解析session_id */
    item = cJSON_GetObjectItem(root, "session_id");
    if(item != NULL) {
        strncpy(hdr->session_id, item->valuestring, strlen(item->valuestring) < 63 ? strlen(item->valuestring) : 63);
    }

    cJSON_Delete(root);
    return STATE_SUCCESS;
}

/* 解析一整个payload */
int32_t cloud_protocol_parse_msg(RA_BUFFER_INFO_S *channel_buffer, tunnel_cloud_msg_t *hdr)
{
    char *buffer = get_tunnel_buffer_read_pointer(channel_buffer);
    int32_t buffer_len = get_tunnel_buffer_read_len(channel_buffer);
    char *frame_buf = NULL;
    int32_t frame_len = 0;
    if(buffer == NULL || hdr == NULL) {
        return -1;
    }

    frame_len = buffer[0] << 8 | buffer[1];
    frame_buf = &buffer[2];

    /* 长度校验 */
    if(frame_len > buffer_len - 2) {
        return -1;
    }

    /* 解析头部 */
    if (STATE_SUCCESS != cloud_protocol_parse_header(frame_buf, frame_len, hdr)) {
        /* 此数据包头异常，可以重新开始接收 */
        ALOG_ERROR(TAG, 0, "cloud_protocol_parse_header failed: %s", buffer);
        return -1;
    }

    /* 拷贝payload */
    hdr->payload = (uint8_t *)(buffer + 2 + frame_len);
    hdr->payload_len = buffer_len - 2 - frame_len;
    return STATE_SUCCESS;
}

/* 生成释放session的报文 */
int32_t cloud_protocol_msg_release_session_request(RA_BUFFER_INFO_S *channel_buffer, uint32_t msg_id, char *session_id)
{
    int32_t header_len = 0;
    reset_tunnel_buffer(channel_buffer);

    header_len = splice_proxy_protocol_header(channel_buffer->buffer,DEFAULT_MSG_HDR_LEN,CLOUD_CMD_RELEASE_SESSION,0, msg_id, session_id);
    if(header_len <= 0) {
        ALOG_ERROR(TAG, 0, "splice Header error!\r\n");
        return -1;
    }

    if(0 != move_tunnel_buffer_write_pointer(channel_buffer,header_len)) {
        ALOG_ERROR(TAG, 0, "move buffer write pointer failed!\r\n");
        return -1;
    }

    ALOG_INFO(TAG, "release session request header:%.*s\r\n", header_len - 2,channel_buffer->buffer + 2);

    return STATE_SUCCESS;
}

/* 生成回复消息，带错误码 */
int32_t cloud_protocol_msg_response_with_code(RA_BUFFER_INFO_S *channel_buffer,int32_t code, char *msg, uint32_t msg_id, char *session_id)
{
    int32_t payload_len = 0;
    int32_t header_len = 0;
    char common_buffer[DEFAULT_MSG_HDR_LEN] = {0};
    reset_tunnel_buffer(channel_buffer);

    payload_len = splice_proxy_protocol_response_payload(channel_buffer->buffer,channel_buffer->size,code, NULL, msg);
    if (payload_len <= 0) {
        return -1;
    }
    if(0 != move_tunnel_buffer_write_pointer(channel_buffer,payload_len)) {
        return -1;
    }
    header_len = splice_proxy_protocol_header(common_buffer,DEFAULT_MSG_HDR_LEN,CLOUD_CMD_RESP_OK,payload_len, msg_id, session_id);
    if(header_len <= 0) {
        return -1;
    }

    ALOG_INFO(TAG, "response  payload:%.*s\r\n", payload_len, channel_buffer->buffer);
    ALOG_INFO(TAG, "response  header:%.*s\r\n", header_len, common_buffer + 2);

    //拼接报文
    if( 0 != join_content_before_tunnel_buffer(common_buffer,header_len,channel_buffer)) {
        ALOG_ERROR(TAG, 0, "join content before buffer error\r\n");
        return -1;
    }

    return STATE_SUCCESS;
}

/* 生成透传消息报文 */
int32_t cloud_protocol_msg_raw_data_post(RA_BUFFER_INFO_S *channel_buffer, uint32_t msg_id, const char *data, int32_t len, const char *session_id)
{
    reset_tunnel_buffer(channel_buffer);

    //获取报文头
    int32_t header_len = splice_proxy_protocol_header(get_tunnel_buffer_write_pointer(channel_buffer), DEFAULT_MSG_HDR_LEN, CLOUD_CMD_RAW_DATA, len, msg_id, (char*)session_id);
    if(header_len <= 0) {
        ALOG_ERROR(TAG, 0, "splice local services packets header error!\r\n");
        return -1;
    }

    if(0 != move_tunnel_buffer_write_pointer(channel_buffer, header_len)) {
        return -1;
    }

    memcpy(get_tunnel_buffer_write_pointer(channel_buffer), data, len);
    if(0 != move_tunnel_buffer_write_pointer(channel_buffer, len)) {
        return -1;
    }

    return STATE_SUCCESS;
}

