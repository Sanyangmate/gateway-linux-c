#include "core_log.h"
#include "core_time.h"
#include "core_string.h"
#include "core_os.h"
#include "aiot_state_api.h"
#include "device_private.h"
#include "aiot_message_api.h"
#include "tunnel_switch_message.h"
#include "aiot_tunnel_switch_api.h"

#define TAG "TUNNEL-SWITCH"
typedef struct {
    ts_callback_t msg_callback;
    void *userdata;
} ts_config_t;

typedef struct {
    void *device;
    ts_config_t config;
    int32_t has_sub;
} ts_handle_t;

static ts_handle_t *_ts_get_handle(void *device);

void _ts_msg_request_reply_callback(void *device, const aiot_msg_t *message, void *userdata)
{
    aiot_ts_msg_t *ts_msg = NULL;
    ts_handle_t *ts_handle = _ts_get_handle(device);
    if(device == NULL || ts_handle == NULL || ts_handle->config.msg_callback == NULL) {
        return;
    }

    ts_msg = ts_msg_parse_request_reply(message);
    if(ts_msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_PARSE_ERROR, "msg(%s) parse error\r\n", message->topic);
        return;
    }

    ALOG_INFO(TAG, "tunnel msg recv: tunnel_id %s, will expired after %ds\r\n", ts_msg->tunnel_id, ts_msg->expired_time);
    if(ts_handle->config.msg_callback != NULL) {
        ts_handle->config.msg_callback(device, ts_msg, ts_handle->config.userdata);
    }

    /* 回收消息资源 */
    ts_msg_delete(ts_msg);
}

void _ts_msg_push_callback(void *device, const aiot_msg_t *message, void *userdata)
{
    aiot_ts_msg_t *ts_msg = NULL;
    ts_handle_t *ts_handle = _ts_get_handle(device);
    if(device == NULL || ts_handle == NULL || ts_handle->config.msg_callback == NULL) {
        return;
    }

    ts_msg = ts_msg_parse_notify(message);
    if(ts_msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_PARSE_ERROR, "msg(%s)  parse error\r\n", message->topic);
        return;
    }

    ALOG_INFO(TAG, "tunnel msg recv: tunnel_id %s, will expired after %ds\r\n", ts_msg->tunnel_id, ts_msg->expired_time);
    if(ts_handle->config.msg_callback != NULL) {
        ts_handle->config.msg_callback(device, ts_msg, ts_handle->config.userdata);
    }

    /* 回收消息资源 */
    ts_msg_delete(ts_msg);
}

static int32_t _ts_sub_topic(void *device)
{
    int32_t res = -1;
    char *topic = NULL;
    char *src[2] = { NULL, NULL};
    ts_handle_t *ts_handle = _ts_get_handle(device);
    if(ts_handle == NULL || device == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    /* 在线才去订阅 */
    if(aiot_device_online(device) && ts_handle->has_sub == 0) {
        src[0] = aiot_device_product_key(device);
        src[1] = aiot_device_name(device);
        core_sprintf(NULL, &topic, TUNNEL_TOPIC_REQUEST_REPLY_FMT, src, sizeof(src)/sizeof(char *), NULL);
        if(topic != NULL) {
            res = aiot_device_register_topic_filter(device, topic, _ts_msg_request_reply_callback, 1, NULL);
            core_os_free(topic);
        }

        topic = NULL;
        core_sprintf(NULL, &topic, TUNNEL_TOPIC_NOTIFY_FMT, src, sizeof(src)/sizeof(char *), NULL);
        if(topic != NULL) {
            res = aiot_device_register_topic_filter(device, topic, _ts_msg_push_callback, 1, NULL);
            core_os_free(topic);
        }

        ts_handle->has_sub = 1;
    }

    return res;
}

int32_t aiot_device_ts_set_callback(void *device, ts_callback_t callback, void *userdata)
{
    ts_handle_t *ts_handle = _ts_get_handle(device);
    if(device == NULL || ts_handle == NULL || callback == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_ts_set_callback input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    ts_handle->config.msg_callback = callback;
    ts_handle->config.userdata = userdata;
    if(aiot_device_online(device)) {
        _ts_sub_topic(device);
    }

    return STATE_SUCCESS;
}

int32_t aiot_device_ts_request(void *device)
{
    aiot_msg_t *msg = NULL;
    int32_t res = STATE_SUCCESS;
    ts_handle_t *ts_handle = _ts_get_handle(device);
    if(device == NULL || ts_handle == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_ts_send input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    msg = ts_msg_create_request(device);
    if(msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_CREATE_ERROR, "remote-config message create error \r\n");
        return STATE_MESSAGE_CREATE_ERROR;
    }

    res = aiot_device_send_message(device, msg);

    aiot_msg_delete(msg);

    return res;
}

static void *_ts_init()
{
    ts_handle_t *ts_handle = (ts_handle_t *)core_os_malloc(sizeof(ts_handle_t));
    if(ts_handle == NULL) {
        return NULL;
    }

    memset(ts_handle, 0, sizeof(ts_handle_t));
    ts_handle->has_sub = 0;
    return ts_handle;
}
static void _ts_deinit(void *handle)
{
    core_os_free(handle);
}

void _ts_on_status(void *handle, aiot_device_status_t event)
{
    ts_handle_t *ts_handle = (ts_handle_t *)handle;
    if(ts_handle != NULL && event.type == AIOT_DEVICE_STATUS_CONNECT) {
        _ts_sub_topic(ts_handle->device);
    }
}

/* 日志上报即插即用操作方法 */
static module_ops_t ts_ops = {
    .internal_ms = -1,
    .on_init = _ts_init,
    .on_process = NULL,
    .on_status = _ts_on_status,
    .on_deinit = _ts_deinit,
};

static ts_handle_t *_ts_get_handle(void *device)
{
    ts_handle_t *ts_handle = core_device_module_get(device, MODULE_REMOTE_CONFIG);
    int32_t res = STATE_SUCCESS;

    if(ts_handle == NULL) {
        res = core_device_module_register(device, MODULE_REMOTE_CONFIG, &ts_ops);
        if(res != STATE_SUCCESS) {
            return NULL;
        } else {
            ts_handle = core_device_module_get(device, MODULE_REMOTE_CONFIG);
            ts_handle->device = device;
        }
    }
    return ts_handle;
}

aiot_ts_msg_t *aiot_ts_msg_clone(const aiot_ts_msg_t *msg)
{
    return ts_msg_clone(msg);
}

void aiot_ts_msg_free(const aiot_ts_msg_t *msg)
{
    ts_msg_delete(msg);
}