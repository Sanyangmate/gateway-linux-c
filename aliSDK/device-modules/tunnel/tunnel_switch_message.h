

#ifndef _TUNNEL_SWITCH_MESSAGE_H_
#define _TUNNEL_SWITCH_MESSAGE_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>
#include "aiot_message_api.h"
#include "aiot_tunnel_switch_api.h"

/* 安全隧道通知，云端-->设备 */
#define TUNNEL_TOPIC_NOTIFY_FMT            "/sys/%s/%s/secure_tunnel/notify"
/* 设备主动请求默认隧道信息 */
#define TUNNEL_TOPIC_REQUEST               "/sys/%s/%s/secure_tunnel/proxy/request"
/* 设备主动请求隧道信息回复*/
#define TUNNEL_TOPIC_REQUEST_REPLY_FMT     "/sys/%s/%s/secure_tunnel/proxy/request_reply"

/* 创建主动请求默认隧道信息 */
aiot_msg_t *ts_msg_create_request(void *device);

/* 云端给的回复消息解析 */
aiot_ts_msg_t *ts_msg_parse_request_reply(const aiot_msg_t *msg);

/* 云端主动下推的消息解析 */
aiot_ts_msg_t *ts_msg_parse_notify(const aiot_msg_t *msg);

/* 删除 aiot_ts_msg_t */
void ts_msg_delete(const aiot_ts_msg_t *msg);

/* 拷贝 aiot_ts_msg_t */
aiot_ts_msg_t *ts_msg_clone(const aiot_ts_msg_t *msg);

#if defined(__cplusplus)
}
#endif

#endif