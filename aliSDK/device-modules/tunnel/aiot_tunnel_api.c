#include "core_stdinc.h"
#include "core_log.h"
#include "core_os.h"
#include "core_adapter.h"
#include "tunnel_private.h"
#include "tunnel_proxy_service.h"

#define TAG "TUNNEL"

static uint32_t tunnel_msg_id(tunnel_handle_t *tunnel)
{
    return ++tunnel->msg_id;
}

void *aiot_tunnel_init(char *tunnel_id)
{
    tunnel_handle_t *tunnel = NULL;
    if(tunnel_id == NULL || strlen(tunnel_id) > 128) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_tunnel_init input null\r\n");
        return NULL;
    }

    tunnel = core_os_malloc(sizeof(tunnel_handle_t));
    if(tunnel == NULL) {
        ALOG_ERROR(TAG, STATE_SYS_DEPEND_MALLOC_FAILED, "aiot_tunnel_init input null\r\n");
        return NULL;
    }

    memset(tunnel, 0, sizeof(tunnel_handle_t));
    /* 配置类参数初始化 */
    memcpy(tunnel->tunnel_id, tunnel_id, strlen(tunnel_id));
    CORE_INIT_LIST_HEAD(&tunnel->local_services.service_list);

    /* 运行时参数初始化 */
    init_session_list(&tunnel->session_list);
    create_tunnel_buffer(&tunnel->cloud_recv_buffer, DEFAULT_MSG_BUFFER_LEN);
    create_tunnel_buffer(&tunnel->cloud_resp_buffer, DEFAULT_MSG_BUFFER_LEN);
    tunnel->retry_info.connect_time = 0;
    tunnel->retry_info.retry_times = 0;
    tunnel->lock = core_os_mutex_init();
    tunnel->cloud_channel_state = CLOUD_CHANNEL_CLOSED;
    tunnel->thread_running = 0;
    tunnel->process_thread = NULL;

    /* tcp代理服务初始化 */
    tunnel->proxy_service = tcp_proxy_init();
    return tunnel;
}

/* 设置隧道建连参数 */
int32_t aiot_tunnel_set_connect_params(void *tunnel, const aiot_tunnel_connect_param_t *params)
{
    tunnel_handle_t *tunnel_handle = (tunnel_handle_t *)tunnel;
    if(tunnel == NULL || params == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_tunnel_set_connect_params input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    /* 检查是否已经有内存占用，有的话先释放 */
    if(tunnel_handle->params.host != NULL) {
        core_os_free(tunnel_handle->params.host);
        tunnel_handle->params.host = NULL;
    }
    if(tunnel_handle->params.path != NULL) {
        core_os_free(tunnel_handle->params.path);
        tunnel_handle->params.path = NULL;
    }
    if(tunnel_handle->params.token != NULL) {
        core_os_free(tunnel_handle->params.token);
        tunnel_handle->params.token = NULL;
    }

    /* 拷贝建连参数 */
    core_strdup(NULL, &tunnel_handle->params.host, params->host, NULL);
    tunnel_handle->params.port = params->port;
    core_strdup(NULL, &tunnel_handle->params.path, params->path, NULL);
    core_strdup(NULL, &tunnel_handle->params.token, params->token, NULL);

    return STATE_SUCCESS;
}

int32_t aiot_tunnel_add_proxy_service(void *tunnel, char *proxy_type, const aiot_tunnel_proxy_params_t *params)
{
    tunnel_handle_t *tunnel_handle = (tunnel_handle_t *)tunnel;
    service_node_t *service_node = NULL;
    proxy_params_t *proxy_ctx = NULL;
    if(tunnel == NULL || proxy_type == NULL || params == NULL || params->ip == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_tunnel_add_proxy_service input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    if(strlen(params->ip) > DEFAULT_LEN_IP || strlen(proxy_type) > DEFAULT_LEN_SERVICE_TYPE) {
        return STATE_USER_INPUT_OUT_RANGE;
    }

    /* node初始化 */
    service_node = core_os_malloc(sizeof(service_node_t));
    memset(service_node, 0, sizeof(service_node_t));

    /* TCP代理模式回调上下文赋值 */
    proxy_ctx = (proxy_params_t *)core_os_malloc(sizeof(proxy_params_t));
    memset(proxy_ctx, 0, sizeof(proxy_params_t));
    proxy_ctx->proxy_handle = tunnel_handle->proxy_service;
    memcpy(proxy_ctx->ip, params->ip, strlen(params->ip));
    proxy_ctx->port = params->port;

    /* 设置TCP代理服务的操作方法 */
    memcpy(service_node->params.type, proxy_type, strlen(proxy_type));
    service_node->params.ops = tcp_proxy_ops();
    service_node->params.userdata = proxy_ctx;
    service_node->params.tcp_flag = 1;

    /* 把服务添加进列表 */
    core_list_add(&service_node->node, &tunnel_handle->local_services.service_list);

    return STATE_SUCCESS;
}

int32_t aiot_tunnel_add_userdefine_service(void *tunnel, char *proxy_type, aiot_session_ops_t *ops, void *userdata)
{
    tunnel_handle_t *tunnel_handle = (tunnel_handle_t *)tunnel;
    service_node_t *service_node = NULL;
    if(tunnel == NULL || proxy_type == NULL || ops == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_tunnel_add_userdefine_service input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    if(ops->session_new == NULL || ops->session_recv_data == NULL || ops->session_release == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_tunnel_add_userdefine_service ops null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    if(strlen(proxy_type) > DEFAULT_LEN_SERVICE_TYPE) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_tunnel_add_userdefine_service proxy_type too long\r\n");
        return STATE_USER_INPUT_OUT_RANGE;
    }

    /* node初始化 */
    service_node = core_os_malloc(sizeof(service_node_t));
    memset(service_node, 0, sizeof(service_node_t));

    memcpy(service_node->params.type, proxy_type, strlen(proxy_type));
    service_node->params.ops = ops;
    service_node->params.userdata = userdata;
    service_node->params.tcp_flag = 0;

    /* 把服务添加进列表 */
    core_list_add(&service_node->node, &tunnel_handle->local_services.service_list);

    return STATE_SUCCESS;
}

/* 根据session类型查询ip,port */
static service_node_t *get_service_node(service_list_t *services, char *type)
{
    service_node_t *item = NULL, *next = NULL;

    core_list_for_each_entry_safe(item, next, &services->service_list, node, service_node_t) {
        if (strcmp(type, item->params.type) == 0) {
            return item;
        }
    }

    return NULL;
}


int32_t aiot_tunnel_set_event_callback(void *tunnel, aiot_tunnel_event_callback_t callback, void *userdata)
{
    tunnel_handle_t *tunnel_handle = (tunnel_handle_t *)tunnel;
    if(tunnel == NULL || callback == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_tunnel_set_event_callback input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    /* 设置事件回调 */
    tunnel_handle->event_callback = callback;
    tunnel_handle->userdata = userdata;

    return STATE_SUCCESS;
}


/* 根据重连的次数，获取退避时间，单位s */
static int get_wait_time_period(int retry_times)
{
    if(retry_times > DEFAULT_MAX_BACKOFF_COUNT) {
        retry_times = DEFAULT_MAX_BACKOFF_COUNT;
    }

    return 1 << retry_times;
}

/* 判断重连退避时间是否到达 */
static int whether_connect_time_up(retry_info_t *retry_info)
{
    uint64_t timeout = get_wait_time_period(retry_info->retry_times);
    uint64_t tp_now = core_os_time();

    if(tp_now - retry_info->connect_time >= timeout * 1000) {
        return STATE_SUCCESS;
    }
    else {
        return STATE_TUNNEL_INNER_ERROR;
    }
}

/* 隧道建连动作执行 */
static int32_t _tunnel_connect(tunnel_handle_t *tunnel_handle)
{
    char port[16];
    int32_t res = STATE_SUCCESS;
    aiot_sysdep_network_cred_t cred; /* 安全凭据结构体, 如果要用TLS, 这个结构体中配置CA证书等参数 */

    extern const char *ali_ca_cert;
    /* 创建SDK的安全凭据, 用于建立TLS连接 */
    memset(&cred, 0, sizeof(aiot_sysdep_network_cred_t));
    cred.option = AIOT_SYSDEP_NETWORK_CRED_SVRCERT_CA;  /* 使用RSA证书校验MQTT服务端 */
    cred.max_tls_fragment = 16384; /* 最大的分片长度为16K, 其它可选值还有4K, 2K, 1K, 0.5K */
    cred.sni_enabled = 1;                               /* TLS建连时, 支持Server Name Indicator */
    cred.x509_server_cert = ali_ca_cert;                 /* 用来验证MQTT服务端的RSA根证书 */
    cred.x509_server_cert_len = strlen(ali_ca_cert);     /* 用来验证MQTT服务端的RSA根证书长度 */

    memset(port, 0, sizeof(port));
    core_uint2str(tunnel_handle->params.port, port, NULL);

    /* 创建云端通道连接资源 */
    tunnel_handle->websocket_channel = websocket_channel_init(tunnel_handle->tunnel_id, tunnel_handle->params.host, port,
                                       tunnel_handle->params.path, tunnel_handle->params.token, &cred);
    if (NULL == tunnel_handle->websocket_channel) {
        ALOG_ERROR(TAG, STATE_TUNNEL_INNER_ERROR, "cloud channel init failed\r\n");
        return STATE_TUNNEL_INNER_ERROR;
    }

    /* 执行建连 */
    res = websocket_channel_connect(tunnel_handle->websocket_channel);
    if(res != STATE_SUCCESS) {
        ALOG_ERROR(TAG, STATE_TUNNEL_INNER_ERROR, "cloud channel open failed\r\n");
        websocket_channel_deinit(&tunnel_handle->websocket_channel);
        return res;
    }

    return STATE_SUCCESS;
}

/* 隧道关闭状态下的状态机运行 */
static void tunnel_closed_state_process(tunnel_handle_t *tunnel_handle)
{
    int32_t res = STATE_SUCCESS;
    /* 判断是否启动进行重连, 指数退避策略 */
    if(STATE_SUCCESS != whether_connect_time_up(&tunnel_handle->retry_info)) {
        return;
    }

    ALOG_INFO(TAG, "tunnel %s reconnecting \r\n", tunnel_handle->tunnel_id);
    res = _tunnel_connect(tunnel_handle);
    if(STATE_SUCCESS == res) {
        /* 重连成功 */
        tunnel_handle->cloud_channel_state = CLOUD_CHANNEL_CONNECTED;
        if(tunnel_handle->event_callback != NULL) {
            tunnel_handle->event_callback(tunnel_handle, AIOT_TUNNEL_EVT_CONNECT, tunnel_handle->userdata);
        }
    } else if(res == WEBSOCKET_CLOSED_CODE_AUTH_FAIL || res == WEBSOCKET_CLOSED_CODE_TOKEN_EXPIRED) {
        /* token过去或者建连失败，将不再重连 */
        ALOG_ERROR(TAG, STATE_TUNNEL_TOKEN_EXPIRED, "tunnel %s has expired\r\n", tunnel_handle->tunnel_id);
        tunnel_handle->cloud_channel_state = CLOUD_CHANNEL_EXPIRED;
        if(tunnel_handle->event_callback != NULL) {
            tunnel_handle->event_callback(tunnel_handle, AIOT_TUNNEL_EVT_EXPIRED, tunnel_handle->userdata);
        }
    } else {
        /* 更新退避重连计数 */
        tunnel_handle->retry_info.retry_times++;
        tunnel_handle->retry_info.connect_time = core_os_time();
    }
}

static session_handle_t* create_session(tunnel_handle_t *tunnel_handle, char *type, char *session_id, session_params_t *params)
{
    session_handle_t* session = core_os_malloc(sizeof(session_handle_t));
    memset(session, 0, sizeof(session_handle_t));

    core_strdup(NULL, &session->id, session_id, NULL);
    core_strdup(NULL, &session->type, type, NULL);
    session->tunnel_handle = tunnel_handle;
    session->userdata = params->userdata;
    session->ops = params->ops;
    session->send_lock = core_os_mutex_init();

    return session;
}

static int32_t delete_session(session_handle_t* session)
{
    if(session == NULL) {
        return -1;
    }

    if(session->type != NULL) {
        core_os_free(session->type);
    }

    if(session->id != NULL) {
        core_os_free(session->id);
    }

    if(session->send_lock != NULL) {
        core_os_mutex_deinit(&session->send_lock);
    }

    core_os_free(session);

    return STATE_SUCCESS;
}

/* 执行创建新的session的动作 */
static void new_session_process(tunnel_handle_t *tunnel_handle, tunnel_cloud_msg_t *recv_msg)
{
    /* 云端指令是创建一个新的session */
    char *session_id = recv_msg->session_id;
    service_node_t *service = NULL;
    service_list_t *local_services = &tunnel_handle->local_services;
    session_params_t session_params;
    session_handle_t *session_handle = NULL;
    int32_t res = STATE_SUCCESS;

    ALOG_INFO(TAG, "new_session_process session_id:%s, type %s, msg_id %u\r\n",recv_msg->session_id, recv_msg->srv_type, recv_msg->msg_id);
    /* 查询对应service_type的ip和port */
    service = get_service_node(local_services, recv_msg->srv_type);
    if(service == NULL) {
        /* 不支持的service_type */
        ALOG_ERROR(TAG, STATE_TUNNEL_INNER_ERROR, "unkown service type\r\n");
        cloud_protocol_msg_response_with_code(&tunnel_handle->cloud_resp_buffer, 1, "unkown service type", recv_msg->msg_id, NULL);
        return;
    }
    memset(&session_params, 0, sizeof(session_params_t));
    strncpy(session_params.type, recv_msg->srv_type, strlen(recv_msg->srv_type));
    session_params.ops = service->params.ops;
    session_params.userdata = service->params.userdata;

    /* 判断session List 是否已经上限 */
    if (get_session_num_from_list(&tunnel_handle->session_list) >= DEFAULT_MAX_SESSION_COUNT) {
        ALOG_ERROR(TAG, STATE_TUNNEL_INNER_ERROR, "session List is limited\r\n");
        cloud_protocol_msg_response_with_code(&tunnel_handle->cloud_resp_buffer, 1, "socketfd insert error", recv_msg->msg_id, NULL);
        return;
    }

    session_handle = create_session(tunnel_handle, recv_msg->srv_type, recv_msg->session_id, &session_params);
    res = session_handle->ops->session_new(session_handle, session_handle->userdata);
    if (res != STATE_SUCCESS) {
        ALOG_ERROR(TAG, STATE_TUNNEL_INNER_ERROR, "failed to connect to local service\r\n");
        cloud_protocol_msg_response_with_code(&tunnel_handle->cloud_resp_buffer, 2, "local service is not available", recv_msg->msg_id, NULL);
        delete_session(session_handle);
        return;
    }

    res = add_one_session_to_list(&tunnel_handle->session_list, session_id, session_handle);
    if (res != STATE_SUCCESS) {
        ALOG_ERROR(TAG, STATE_TUNNEL_INNER_ERROR, "failed to alloc new session\r\n");
        cloud_protocol_msg_response_with_code(&tunnel_handle->cloud_resp_buffer, 2, "memory error", recv_msg->msg_id, NULL);
        session_handle->ops->session_release(session_handle, session_handle->userdata);
        delete_session(session_handle);
        return;
    }

    /* 回复云端添加session成功 */
    cloud_protocol_msg_response_with_code(&tunnel_handle->cloud_resp_buffer, 0, "new session success", recv_msg->msg_id, session_id);
}

/* 释放session  */
static void release_session_process(tunnel_handle_t *tunnel_handle, tunnel_cloud_msg_t *recv_msg)
{
    session_node_t* session_node = NULL;
    session_handle_t *session_handle = NULL;
    if(CLOUD_CHANNEL_CONNECTED != tunnel_handle->cloud_channel_state) {
        ALOG_ERROR(TAG, STATE_TUNNEL_INNER_ERROR, "cloud channel does not finish handshake!\r\n");
        return;
    }

    ALOG_INFO(TAG, "release_session_process session_id: %s\r\n", recv_msg->session_id);
    session_node = get_one_session_from_list(&tunnel_handle->session_list, recv_msg->session_id);
    if(session_node == NULL) {
        ALOG_ERROR(TAG, STATE_TUNNEL_INNER_ERROR, "release_session not found, session_id: %s\r\n", recv_msg->session_id);
        return;
    }
    session_handle = (session_handle_t *)session_node->session_handle;

    release_one_session_from_list(&tunnel_handle->session_list, recv_msg->session_id);
    session_handle->ops->session_release(session_handle, session_handle->userdata);
    delete_session(session_handle);
    cloud_protocol_msg_response_with_code(&tunnel_handle->cloud_resp_buffer, 0, "release session success", recv_msg->msg_id, recv_msg->session_id);

    return;
}

static void raw_data_process(tunnel_handle_t *tunnel_handle, tunnel_cloud_msg_t *recv_msg)
{
    session_handle_t *session_handle = NULL;
    int32_t res = STATE_SUCCESS;
    if(CLOUD_CHANNEL_CONNECTED != tunnel_handle->cloud_channel_state) {
        ALOG_ERROR(TAG, STATE_TUNNEL_INNER_ERROR, "cloud channel does not finish handshake!\r\n");
        return;
    }
    ALOG_INFO(TAG, "cloud->device raw_data session_id: %s, len %d\r\n", recv_msg->session_id, recv_msg->payload_len);
    /* 数据：则转发到本地相应的服务中，保证发送数据的完整性 */
    session_node_t *session_node = get_one_session_from_list(&tunnel_handle->session_list, recv_msg->session_id);
    if (NULL == session_node) {
        /* session异常则退出 */
        ALOG_ERROR(TAG, STATE_TUNNEL_INNER_ERROR, "session id invalid: %s", recv_msg->session_id);
        return;
    }

    session_handle = session_node->session_handle;
    res = session_handle->ops->session_recv_data(session_handle, recv_msg->payload, recv_msg->payload_len, session_handle->userdata);
    /* 发送透传数据给本地服务 */
    if(res < STATE_SUCCESS) {
        /* session异常则退出 */
        ALOG_ERROR(TAG, STATE_TUNNEL_INNER_ERROR, "send raw_data error, release the session: %s", recv_msg->session_id);
        cloud_protocol_msg_release_session_request(&tunnel_handle->cloud_resp_buffer, tunnel_msg_id(tunnel_handle), recv_msg->session_id);
        release_one_session_from_list(&tunnel_handle->session_list, recv_msg->session_id);
        return;
    }
}

/* 处理云端来的消息 */
static void cloud_data_proc(tunnel_handle_t *tunnel_handle)
{
    void *cloud_connection = tunnel_handle->websocket_channel;
    RA_BUFFER_INFO_S *cloud_recv_buffer = &tunnel_handle->cloud_recv_buffer;
    int ret_code = STATE_SUCCESS;
    char *send_data = NULL;
    int32_t send_len = 0;
    tunnel_cloud_msg_t recv_msg;

    for(;;)
    {
        ret_code = websocket_channel_recv(cloud_connection, cloud_recv_buffer, 10 * 1000);
        if(ret_code <= 0) {
            /* 确认已经无包可取时退出 */
            break;
        }

        /* 读取自定义的proxy协议的数据头，判断数据类型，分别如下：*/
        memset(&recv_msg, 0, sizeof(tunnel_cloud_msg_t));
        ret_code = cloud_protocol_parse_msg(cloud_recv_buffer, &recv_msg);
        if (STATE_SUCCESS != ret_code) {
            /* 数据包异常，则丢弃，处理下一个包 */
            reset_tunnel_buffer(cloud_recv_buffer);
            continue;
        }

        /* 云端下行数据和命令的处理 */
        if (recv_msg.msg_type == CLOUD_CMD_NEW_SESSION) {
            /* 命令：本地服务session的开启 */
            new_session_process(tunnel_handle, &recv_msg);
        }
        else if (recv_msg.msg_type == CLOUD_CMD_RELEASE_SESSION) {
            /* 命令：则做本地服务session的关闭 */
            release_session_process(tunnel_handle, &recv_msg);
        }
        else if (recv_msg.msg_type == CLOUD_CMD_RAW_DATA) {
            /* 数据：云端传输给本地服务的数据 */
            raw_data_process(tunnel_handle, &recv_msg);
        }
        else {
            ALOG_ERROR(TAG, STATE_TUNNEL_INNER_ERROR, "recv error websocket package!");
        }

        /* 处理成功则清除缓存 */
        reset_tunnel_buffer(cloud_recv_buffer);

        /* 发送数据并清空发送缓存 */
        if(get_tunnel_buffer_read_len(&tunnel_handle->cloud_resp_buffer) > 0) {
            send_data = tunnel_handle->cloud_resp_buffer.buffer;
            send_len = get_tunnel_buffer_read_len(&tunnel_handle->cloud_resp_buffer);
            send_len = websocket_channel_send(tunnel_handle->websocket_channel, send_data, send_len, 10 * 1000);
            if(send_len == get_tunnel_buffer_read_len(&tunnel_handle->cloud_resp_buffer)) {
                reset_tunnel_buffer(&tunnel_handle->cloud_resp_buffer);
            } else {
                ALOG_ERROR(TAG, STATE_TUNNEL_INNER_ERROR, "websocket send error\r\n");
            }
        }
        reset_tunnel_buffer(&tunnel_handle->cloud_resp_buffer);
    }
}

/* 隧道连接状态下的状态机处理 */
static void tunnel_connected_state_process(tunnel_handle_t *tunnel_handle)
{
    int32_t res = STATE_SUCCESS;
    /* 接收本地消息发送至云端 */
    tcp_proxy_run(tunnel_handle->proxy_service);

    /* 接收云端消息转发至本地session */
    cloud_data_proc(tunnel_handle);

    /* 检查是否掉线 */
    res = websocket_channel_keepalive(tunnel_handle->websocket_channel);
    if(STATE_SUCCESS != res) {
        if(res == WEBSOCKET_CLOSED_CODE_AUTH_FAIL || res == WEBSOCKET_CLOSED_CODE_TOKEN_EXPIRED) {
            /* token过去或者建连失败，将不再重连 */
            tunnel_handle->cloud_channel_state = CLOUD_CHANNEL_EXPIRED;
            ALOG_ERROR(TAG, STATE_TUNNEL_TOKEN_EXPIRED, "tunnel %s has expired\r\n", tunnel_handle->tunnel_id);
            if(tunnel_handle->event_callback != NULL) {
                tunnel_handle->event_callback(tunnel_handle, AIOT_TUNNEL_EVT_EXPIRED, tunnel_handle->userdata);
            }
        } else {
            tunnel_handle->cloud_channel_state = CLOUD_CHANNEL_CLOSED;
            /* 如果状态发生变化，执行事件回调 */
            if(tunnel_handle->event_callback != NULL) {
                tunnel_handle->event_callback(tunnel_handle, AIOT_TUNNEL_EVT_DISCONNECT, tunnel_handle->userdata);
            }
        }

        websocket_channel_disconnect(tunnel_handle->websocket_channel);
        websocket_channel_deinit(&tunnel_handle->websocket_channel);
    }
}

/* daemon处理线程 */
static void *process_thread(void *args)
{
    tunnel_handle_t *tunnel_handle = (tunnel_handle_t *)args;

    while (tunnel_handle->thread_running) {
        if(CLOUD_CHANNEL_CLOSED == tunnel_handle->cloud_channel_state) {
            tunnel_closed_state_process(tunnel_handle);
        } else if(CLOUD_CHANNEL_CONNECTED == tunnel_handle->cloud_channel_state) {
            tunnel_connected_state_process(tunnel_handle);
        } else {
            /* 隧道过期后等待关闭 */
            core_os_sleep(1000);
        }
    }

    return NULL;
}

int32_t aiot_tunnel_connect(void *tunnel)
{
    tunnel_handle_t *tunnel_handle = (tunnel_handle_t *)tunnel;
    int32_t res = STATE_SUCCESS;

    if(tunnel == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_tunnel_connect input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    core_os_mutex_lock(tunnel_handle->lock);
    if(tunnel_handle->has_connected == 1) {
        core_os_mutex_unlock(tunnel_handle->lock);
        ALOG_ERROR(TAG, STATE_TUNNEL_CONNECT_REPEATED, "aiot_tunnel_connect reconnected\r\n");
        return STATE_TUNNEL_CONNECT_REPEATED;
    }
    tunnel_handle->has_connected = 1;

    ALOG_INFO(TAG, "aiot_tunnel_connect tunnel_id %s\r\n", tunnel_handle->tunnel_id);
    res = _tunnel_connect(tunnel_handle);
    if(res != STATE_SUCCESS) {
        core_os_mutex_unlock(tunnel_handle->lock);
        ALOG_ERROR(TAG, STATE_TUNNEL_CONNECT_FAILED, "aiot_tunnel_connect failed\r\n");
        tunnel_handle->has_connected = 0;
        return STATE_TUNNEL_CONNECT_FAILED;
    }

    if(tunnel_handle->event_callback != NULL) {
        tunnel_handle->event_callback(tunnel_handle, AIOT_TUNNEL_EVT_CONNECT, tunnel_handle->userdata);
    }

    ALOG_INFO(TAG, "websocket %s opened\r\n", tunnel_handle->tunnel_id);
    tunnel_handle->cloud_channel_state = CLOUD_CHANNEL_CONNECTED;
    tunnel_handle->thread_running = 1;
    tunnel_handle->process_thread = core_os_thread_init("TUNNEL_PROCESS", process_thread, tunnel_handle);
    if (tunnel_handle->process_thread ==  NULL) {
        core_os_mutex_unlock(tunnel_handle->lock);
        ALOG_ERROR(TAG, STATE_PORT_PTHREAD_CREATE_FAILED, "pthread_create recv_thread failed\r\n");
        return STATE_PORT_PTHREAD_CREATE_FAILED;
    }

    core_os_mutex_unlock(tunnel_handle->lock);
    return STATE_SUCCESS;
}

int release_one_session(session_node_t *session_info, void *data)
{
    session_handle_t *session = NULL;
    if(session_info != NULL) {
        session = (session_handle_t *)session_info->session_handle;
        session->ops->session_release(session, session->userdata);
        delete_session(session);
    }
    return STATE_SUCCESS;
}

int32_t release_all_session(tunnel_handle_t *tunnel_handle)
{
    iterate_each_session(&tunnel_handle->session_list, release_one_session, NULL);
    release_all_session_from_list(&tunnel_handle->session_list);
    return STATE_SUCCESS;
}

int32_t aiot_tunnel_disconnect(void *tunnel)
{
    tunnel_handle_t *tunnel_handle = (tunnel_handle_t *)tunnel;
    if(tunnel == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_tunnel_disconnect input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    core_os_mutex_lock(tunnel_handle->lock);
    if(tunnel_handle->has_connected == 0) {
        core_os_mutex_unlock(tunnel_handle->lock);
        ALOG_ERROR(TAG, STATE_TUNNEL_DISCONNECT_REPEATED, "aiot_tunnel_disconnect repeated\r\n");
        return STATE_TUNNEL_DISCONNECT_REPEATED;
    }
    tunnel_handle->has_connected = 0;

    tunnel_handle->thread_running = 0;
    core_os_thread_join(&tunnel_handle->process_thread);
    /* 先断开websocket连接 */
    if(tunnel_handle->websocket_channel != NULL) {
        websocket_channel_disconnect(tunnel_handle->websocket_channel);
        websocket_channel_deinit(&tunnel_handle->websocket_channel);
        tunnel_handle->websocket_channel = NULL;
    }

    if(tunnel_handle->event_callback != NULL) {
        tunnel_handle->event_callback(tunnel_handle, AIOT_TUNNEL_EVT_DISCONNECT, tunnel_handle->userdata);
    }

    /* 清除连接运行时数据 */
    release_all_session(tunnel_handle);
    reset_tunnel_buffer(&tunnel_handle->cloud_resp_buffer);
    reset_tunnel_buffer(&tunnel_handle->cloud_recv_buffer);
    tunnel_handle->cloud_channel_state = CLOUD_CHANNEL_CLOSED;
    core_os_mutex_unlock(tunnel_handle->lock);

    return STATE_SUCCESS;
}

void aiot_tunnel_deinit(void **tunnel)
{
    tunnel_handle_t *tunnel_handle = NULL;
    service_node_t *item = NULL, *next = NULL;
    if(tunnel == NULL || *tunnel == NULL) {
        return;
    }

    tunnel_handle = (tunnel_handle_t *)(*tunnel);
    core_os_mutex_lock(tunnel_handle->lock);

    if(tunnel_handle->process_thread != NULL) {
        tunnel_handle->thread_running = 0;
        core_os_thread_join(&tunnel_handle->process_thread);
    }

    if(tunnel_handle->params.host != NULL) {
        core_os_free(tunnel_handle->params.host);
    }
    if(tunnel_handle->params.path != NULL) {
        core_os_free(tunnel_handle->params.path);
    }
    if(tunnel_handle->params.token != NULL) {
        core_os_free(tunnel_handle->params.token);
    }

    core_list_for_each_entry_safe(item, next, &tunnel_handle->local_services.service_list, node, service_node_t)
    {
        core_list_del(&item->node);
        if(item->params.tcp_flag == 1 && item->params.userdata != NULL) {
            core_os_free(item->params.userdata);
        }
        core_os_free(item);
    }

    if(tunnel_handle->websocket_channel != NULL) {
        websocket_channel_disconnect(tunnel_handle->websocket_channel);
        websocket_channel_deinit(&tunnel_handle->websocket_channel);
        tunnel_handle->websocket_channel = NULL;
    }

    release_tunnel_buffer(&tunnel_handle->cloud_recv_buffer);
    release_tunnel_buffer(&tunnel_handle->cloud_resp_buffer);
    release_all_session(tunnel_handle);

    core_os_mutex_deinit(&tunnel_handle->session_list.list_lock);

    core_os_mutex_unlock(tunnel_handle->lock);
    core_os_mutex_deinit(&tunnel_handle->lock);

    tcp_proxy_deinit(&tunnel_handle->proxy_service);
    core_os_free(tunnel_handle);

    *tunnel = NULL;
}

/* 设备主动断开session */
int32_t aiot_session_release(void *session)
{
    int32_t writen_len = 0;
    RA_BUFFER_INFO_S channel_buffer;
    tunnel_handle_t *tunnel_handle = NULL;
    session_handle_t *session_handle = (session_handle_t *)session;
    if(session == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_session_release input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    tunnel_handle = (tunnel_handle_t *)session_handle->tunnel_handle;
    /* 查询是否还在隧道的session表中 */
    if(STATE_SUCCESS != has_one_session(&tunnel_handle->session_list, session_handle->id)) {
        return -1;
    }
    /* 先从链表中删除session */
    release_one_session_from_list(&tunnel_handle->session_list, session_handle->id);
    ALOG_INFO(TAG, "device release session_id: %s\r\n", session_handle->id);
    create_tunnel_buffer(&channel_buffer, DEFAULT_MSG_BUFFER_LEN);
    cloud_protocol_msg_release_session_request(&channel_buffer, tunnel_msg_id(session_handle->tunnel_handle), session_handle->id);
    /*发送至云端，如果消息阻塞，则整包丢弃*/
    writen_len = websocket_channel_send(tunnel_handle->websocket_channel, channel_buffer.buffer, get_tunnel_buffer_read_len(&channel_buffer), 10 * 1000);
    if (writen_len != get_tunnel_buffer_read_len(&channel_buffer)) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_session_release send request failed\r\n");
    }
    release_tunnel_buffer(&channel_buffer);

    delete_session(session_handle);
    return STATE_SUCCESS;
}

/* 向session发送数据 */
int32_t  aiot_session_send_data(void *session, uint8_t *data, uint32_t len)
{
    int32_t writen_len = 0, slice_len = 0, res = 0;
    RA_BUFFER_INFO_S channel_buffer;
    tunnel_handle_t *tunnel_handle = NULL;
    session_handle_t *session_handle = (session_handle_t *)session;
    if(session == NULL || data == NULL || len <= 0) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_session_send_data input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    tunnel_handle = (tunnel_handle_t *)session_handle->tunnel_handle;
    /* 查询是否还在隧道的session表中 */
    if(STATE_SUCCESS != has_one_session(&tunnel_handle->session_list, session_handle->id)) {
        return -1;
    }
    ALOG_INFO(TAG, "device->cloud raw_data session_id: %s, len %d\r\n", session_handle->id, len);
    create_tunnel_buffer(&channel_buffer, DEFAULT_MSG_BUFFER_LEN);
    core_os_mutex_lock(session_handle->send_lock);
    /* 增加发送锁，需要保证数据一致性 */
    while(len > 0) {
        /* 发送数据长度大于单帧最大长度,分片发送 */
        if(len > DEFAULT_MAX_MSG_LEN) {
            slice_len = DEFAULT_MAX_MSG_LEN;
        } else {
            slice_len = len;
        }
        len -= slice_len;

        cloud_protocol_msg_raw_data_post(&channel_buffer, tunnel_msg_id(tunnel_handle), (const char *)(data + res), slice_len, session_handle->id);
        /* 发送至云端，如果消息阻塞，则整包丢弃 */
        writen_len = websocket_channel_send(tunnel_handle->websocket_channel, channel_buffer.buffer, get_tunnel_buffer_read_len(&channel_buffer), 10 * 1000);
        if (writen_len != get_tunnel_buffer_read_len(&channel_buffer)) {
            res += slice_len;
            break;
        } else {
            res += slice_len;
        }
    }
    core_os_mutex_unlock(session_handle->send_lock);
    release_tunnel_buffer(&channel_buffer);

    return res;
}

/* 获取session的类型 */
char* aiot_session_type(void *session)
{
    session_handle_t *session_handle = (session_handle_t *)session;
    if(session == NULL) {
        return NULL;
    }

    return session_handle->type;
}
/* 获取sesssion_id */
char* aiot_session_id(void *session)
{
    session_handle_t *session_handle = (session_handle_t *)session;
    if(session == NULL) {
        return NULL;
    }

    return session_handle->id;
}
