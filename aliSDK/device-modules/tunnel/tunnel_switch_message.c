#include "core_os.h"
#include "core_string.h"
#include "cJSON.h"
#include "device_private.h"
#include "aiot_state_api.h"
#include "aiot_message_api.h"
#include "tunnel_switch_message.h"

static aiot_msg_t *_ts_msg_create_template(void *device, const char* fmt, cJSON *root)
{
    char *topic = NULL, *payload = NULL;
    char *src[] = { NULL, NULL };
    uint32_t src_counter = 2;
    aiot_msg_t *result = NULL;

    src[0] = aiot_device_product_key(device);
    src[1] = aiot_device_name(device);

    if(STATE_SUCCESS != core_sprintf(NULL, &topic, fmt, src, src_counter, "")) {
        return NULL;
    }

    payload = cJSON_PrintUnformatted(root);
    if (payload == NULL) {
        core_os_free(topic);
        return NULL;
    }

    result = aiot_msg_create_raw(topic, (uint8_t *)payload, strlen(payload));
    core_os_free(topic);
    core_os_free(payload);
    return result;
}

/* 创建主动获取远程配置文件的消息 */
aiot_msg_t *ts_msg_create_request(void *device)
{
    cJSON *root = NULL;
    aiot_msg_t *result = NULL;
    int32_t id = 0;

    root = cJSON_CreateObject();
    if (root == NULL) {
        return NULL;
    }

    id = core_device_get_next_alink_id(device);
    _cJSON_ADDInt2StrToObject(root, "id", id);
    cJSON_AddRawToObject(root, "params", "{}");

    result = _ts_msg_create_template(device, TUNNEL_TOPIC_REQUEST, root);
    result->id = id;
    cJSON_Delete(root);

    return result;
}

/* 云端给的回复消息解析 */
aiot_ts_msg_t *ts_msg_parse_request_reply(const aiot_msg_t *msg)
{
    aiot_ts_msg_t *recv = NULL;
    cJSON *root = NULL, *data = NULL, *item = NULL;
    if(msg == NULL) {
        return NULL;
    }

    root = cJSON_ParseWithLength((char *)msg->payload, msg->payload_lenth);
    if(root == NULL) {
        goto error;
    }

    recv = (aiot_ts_msg_t *)core_os_malloc(sizeof(aiot_ts_msg_t));
    if(recv == NULL) {
        goto error;
    }
    memset(recv, 0, sizeof(aiot_ts_msg_t));
    recv->operation = AIOT_TUNNEL_OPERATOPN_UPDATE;

    item = cJSON_GetObjectItem(root, "id");
    if(NULL == item) {
        goto error;
    }
    core_str2uint(item->valuestring, strlen(item->valuestring), &recv->msg_id);

    data = cJSON_GetObjectItem(root, "data");
    if(NULL == data) {
        goto error;
    }

    item = cJSON_GetObjectItem(data, "tunnel_id");
    if(NULL == item) {
        goto error;
    }
    core_strdup(NULL, &recv->tunnel_id, item->valuestring, "");

    item = cJSON_GetObjectItem(data, "host");
    if(NULL == item) {
        goto error;
    }
    core_strdup(NULL, &recv->host, item->valuestring, "");

    item = cJSON_GetObjectItem(data, "port");
    if(NULL == item) {
        goto error;
    }
    recv->port = item->valueint;

    item = cJSON_GetObjectItem(data, "path");
    if(NULL == item) {
        goto error;
    }
    core_strdup(NULL, &recv->path, item->valuestring, "");

    item = cJSON_GetObjectItem(data, "token");
    if(NULL == item) {
        goto error;
    }
    core_strdup(NULL, &recv->token, item->valuestring, "");

    item = cJSON_GetObjectItem(data, "token_expire");
    if(NULL != item) {
        recv->expired_time = item->valueint;
        recv->created_time = core_os_time();
    }

    item = cJSON_GetObjectItem(data, "udi");
    if(NULL != item) {
        core_strdup(NULL, &recv->udi, item->valuestring, "");
    }


    cJSON_Delete(root);
    return recv;

error:
    if(root != NULL) {
        cJSON_Delete(root);
    }
    if(recv != NULL) {
        ts_msg_delete(recv);
    }
    return NULL;
}

/* 云端主动下推的消息解析 */
aiot_ts_msg_t *ts_msg_parse_notify(const aiot_msg_t *msg)
{
    aiot_ts_msg_t *recv = NULL;
    cJSON *root = NULL, *item = NULL;
    if(msg == NULL) {
        return NULL;
    }

    root = cJSON_ParseWithLength((char *)msg->payload, msg->payload_lenth);
    if(root == NULL) {
        goto error;
    }

    recv = (aiot_ts_msg_t *)core_os_malloc(sizeof(aiot_ts_msg_t));
    if(recv == NULL) {
        goto error;
    }
    memset(recv, 0, sizeof(aiot_ts_msg_t));

    item = cJSON_GetObjectItem(root, "operation");
    if (item == NULL) {
        /* 不打开也不关闭，只是更新隧道信息 */
        recv->operation = AIOT_TUNNEL_OPERATOPN_UPDATE;
    } else  if (0 == strncmp(item->valuestring, "connect", strlen("connect"))) {
        recv->operation = AIOT_TUNNEL_OPERATOPN_OPEN;
    } else if (0 == strncmp(item->valuestring, "close", strlen("close"))) {
        recv->operation = AIOT_TUNNEL_OPERATOPN_CLOSE;
    }

    item = cJSON_GetObjectItem(root, "tunnel_id");
    if(NULL == item) {
        goto error;
    }
    core_strdup(NULL, &recv->tunnel_id, item->valuestring, "");

    if(AIOT_TUNNEL_OPERATOPN_CLOSE != recv->operation) {
        item = cJSON_GetObjectItem(root, "host");
        if(NULL == item) {
            goto error;
        }
        core_strdup(NULL, &recv->host, item->valuestring, "");

        item = cJSON_GetObjectItem(root, "port");
        if(NULL == item) {
            goto error;
        }
        recv->port = item->valueint;

        item = cJSON_GetObjectItem(root, "path");
        if(NULL == item) {
            goto error;
        }
        core_strdup(NULL, &recv->path, item->valuestring, "");

        item = cJSON_GetObjectItem(root, "token");
        if(NULL == item) {
            goto error;
        }
        core_strdup(NULL, &recv->token, item->valuestring, "");

        item = cJSON_GetObjectItem(root, "token_expire");
        if(NULL != item) {
            recv->expired_time = item->valueint;
            recv->created_time = core_os_time();
        }

        item = cJSON_GetObjectItem(root, "udi");
        if(NULL != item) {
            core_strdup(NULL, &recv->udi, item->valuestring, "");
        }
    } else {
        item = cJSON_GetObjectItem(root, "close_reason");
        if(NULL != item) {
            core_strdup(NULL, &recv->close_reason, item->valuestring, "");
        }
    }

    cJSON_Delete(root);
    return recv;

error:
    if(root != NULL) {
        cJSON_Delete(root);
    }
    if(recv != NULL) {
        ts_msg_delete(recv);
    }
    return NULL;
}

aiot_ts_msg_t *ts_msg_clone(const aiot_ts_msg_t *msg)
{
    aiot_ts_msg_t *clone_msg = NULL;
    if(msg == NULL) {
        return NULL;
    }

    clone_msg = (aiot_ts_msg_t *)core_os_malloc(sizeof(aiot_ts_msg_t));
    memset(clone_msg, 0, sizeof(aiot_ts_msg_t));
    clone_msg->msg_id = msg->msg_id;
    clone_msg->operation = msg->operation;
    clone_msg->port = msg->port;
    clone_msg->expired_time = msg->expired_time;
    clone_msg->created_time = msg->created_time;

    if(msg->tunnel_id != NULL) {
        core_strdup(NULL, &clone_msg->tunnel_id, msg->tunnel_id, "");
    }

    if(msg->host != NULL) {
        core_strdup(NULL, &clone_msg->host, msg->host, "");
    }

    if(msg->path != NULL) {
        core_strdup(NULL, &clone_msg->path, msg->path, "");
    }

    if(msg->token != NULL) {
        core_strdup(NULL, &clone_msg->token, msg->token, "");
    }

    if(msg->close_reason != NULL) {
        core_strdup(NULL, &clone_msg->close_reason, msg->close_reason, "");
    }

    if(msg->udi != NULL) {
        core_strdup(NULL, &clone_msg->udi, msg->udi, "");
    }

    return clone_msg;
}
/* 删除 aiot_ts_msg_t */
void ts_msg_delete(const aiot_ts_msg_t *msg)
{
    if(msg == NULL) {
        return;
    }

    if(msg->tunnel_id != NULL) {
        core_os_free(msg->tunnel_id);
    }

    if(msg->host != NULL) {
        core_os_free(msg->host);
    }

    if(msg->path != NULL) {
        core_os_free(msg->path);
    }

    if(msg->token != NULL) {
        core_os_free(msg->token);
    }

    if(msg->close_reason != NULL) {
        core_os_free(msg->close_reason);
    }

    if(msg->udi != NULL) {
        core_os_free(msg->udi);
    }

    core_os_free((void *)msg);
}


