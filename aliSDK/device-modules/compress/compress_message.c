#include "core_os.h"
#include "core_log.h"
#include "core_time.h"
#include "cJSON.h"
#include "device_private.h"
#include "aiot_state_api.h"
#include "aiot_message_api.h"
#include "compress_message.h"

static aiot_msg_t *_compress_msg_create_template(void *device, const char* fmt, cJSON *root)
{
    char *topic = NULL, *payload = NULL;
    char *src[] = { NULL, NULL };
    uint32_t src_counter = 2;
    aiot_msg_t *result = NULL;

    src[0] = aiot_device_product_key(device);
    src[1] = aiot_device_name(device);

    if(STATE_SUCCESS != core_sprintf(NULL, &topic, fmt, src, src_counter, "")) {
        return NULL;
    }

    payload = cJSON_PrintUnformatted(root);
    if (payload == NULL) {
        core_os_free(topic);
        return NULL;
    }

    result = aiot_msg_create_raw(topic, (uint8_t *)payload, strlen(payload));
    core_os_free(topic);
    core_os_free(payload);
    return result;
}

aiot_msg_t *compress_msg_create_topic_update(void *device, char **compr_topics, uint32_t compr_num, char **decompr_topics, uint32_t decompr_num)
{
    cJSON *root = NULL, *params = NULL, *param = NULL;
    aiot_msg_t *result = NULL;
    int32_t i = 0, id = 0;

    root = cJSON_CreateObject();
    if (root == NULL) {
        return NULL;
    }

    params = cJSON_CreateArray();
    if (params == NULL) {
        cJSON_Delete(root);
        return NULL;
    }
    cJSON_AddItemToObject(root, "params", params);

    for(i = 0; i < compr_num; i++) {
        param = cJSON_CreateObject();
        if (param == NULL) {
            cJSON_Delete(root);
            break;
        }
        cJSON_AddStringToObject(param, "topic", compr_topics[i]);
        cJSON_AddStringToObject(param, "operation", "decompress");
        cJSON_AddStringToObject(param, "format", "gzip");
        /* 将设备添加进device_list */
        cJSON_AddItemToArray(params, param);
    }

    for(i = 0; i < decompr_num; i++) {
        param = cJSON_CreateObject();
        if (param == NULL) {
            cJSON_Delete(root);
            break;
        }
        cJSON_AddStringToObject(param, "topic", decompr_topics[i]);
        cJSON_AddStringToObject(param, "operation", "compress");
        cJSON_AddStringToObject(param, "format", "gzip");
        /* 将设备添加进device_list */
        cJSON_AddItemToArray(params, param);
    }

    id = core_device_get_next_alink_id(device);
    _cJSON_ADDInt2StrToObject(root, "id", id);

    result = _compress_msg_create_template(device, COMPRESS_UPDATE_TOPIC_FMT, root);
    result->id = id;
    cJSON_Delete(root);

    return result;
}

/* 解析云端下发消息 */
compress_recv_t *compress_msg_parse_update_reply(const aiot_msg_t *msg)
{
    compress_recv_t *recv = NULL;
    cJSON *root = NULL, *code = NULL, *id = NULL, *message = NULL;
    if(msg == NULL) {
        return NULL;
    }

    root = cJSON_ParseWithLength((char *)msg->payload, msg->payload_lenth);
    if(root == NULL) {
        return NULL;
    }

    id = cJSON_GetObjectItem(root, "id");
    if(NULL == id) {
        cJSON_Delete(root);
        return NULL;
    }

    code = cJSON_GetObjectItem(root, "code");
    if(NULL == code) {
        cJSON_Delete(root);
        return NULL;
    }

    recv = (compress_recv_t *)core_os_malloc(sizeof(compress_recv_t));
    if(recv == NULL) {
        cJSON_Delete(root);
        return NULL;
    }

    message = cJSON_GetObjectItem(root, "code");
    if(message != NULL) {
        recv->message = cJSON_PrintUnformatted(message);
    }
    recv->code = code->valueint;
    core_str2uint(id->valuestring, strlen(id->valuestring), &recv->id);

    cJSON_Delete(root);
    return recv;
}
void compress_msg_delete(compress_recv_t *msg)
{
    if(msg == NULL) {
        return;
    }

    if(msg->message != NULL) {
        core_os_free(msg->message);
    }
    core_os_free(msg);
}