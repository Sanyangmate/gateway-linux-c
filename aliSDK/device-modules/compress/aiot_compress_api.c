#include "core_log.h"
#include "core_time.h"
#include "core_string.h"
#include "core_os.h"
#include "aiot_state_api.h"
#include "device_private.h"
#include "aiot_message_api.h"
#include "compress_message.h"
#include "aiot_compress_api.h"
#include "libdeflate.h"

#define TAG "COMPRESS"
typedef struct {
    compress_callback_t callback;
    void *userdata;
    uint8_t level;
    char **compr_list;
    uint32_t compr_num;
    char **decompr_list;
    uint32_t decompr_num;
} compress_config_t;

typedef struct {
    void *device;
    compress_config_t config;
    void *data_mutex;
} compress_handle_t;

static compress_handle_t *_compress_get_handle(void *device);

/* 查询topic是为压缩topic，是的话返回STATE_SUCCESS */
static int32_t _compress_topic_filter(compress_handle_t *compress_handle, char *topic)
{
    int32_t i = 0;
    core_os_mutex_lock(compress_handle->data_mutex);
    for(i = 0; i < compress_handle->config.compr_num; i++) {
        if (strcmp(compress_handle->config.compr_list[i], topic) == STATE_SUCCESS) {
            core_os_mutex_unlock(compress_handle->data_mutex);
            return STATE_SUCCESS;
        }
    }
    core_os_mutex_unlock(compress_handle->data_mutex);
    return STATE_MQTT_TOPIC_COMPARE_FAILED;
}

/* 查询topic是为解压缩topic，是的话返回STATE_SUCCESS */
static int32_t _decompress_topic_filter(compress_handle_t *compress_handle, char *topic)
{
    int32_t i = 0;
    core_os_mutex_lock(compress_handle->data_mutex);
    for(i = 0; i < compress_handle->config.decompr_num; i++) {
        if (strcmp(compress_handle->config.decompr_list[i], topic) == STATE_SUCCESS) {
            core_os_mutex_unlock(compress_handle->data_mutex);
            return STATE_SUCCESS;
        }
    }
    core_os_mutex_unlock(compress_handle->data_mutex);
    return STATE_MQTT_TOPIC_COMPARE_FAILED;
}


/* 执行压缩操作 */
static int32_t _compress_process(void *context, const aiot_msg_t *src, aiot_msg_t **dest)
{
    compress_handle_t *compress_handle = (compress_handle_t *)context;
    uint8_t *buffer = NULL;
    uint32_t compr_len = 0;
    struct libdeflate_compressor *compressor = NULL;
    if(context == NULL || src == NULL || dest == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    if(src->payload_lenth <= 0) {
        return STATE_USER_INPUT_OUT_RANGE;
    }

    /* 未匹配成功为正常情况，返回SUCCESS */
    if(STATE_SUCCESS != _compress_topic_filter(compress_handle, src->topic)) {
        return STATE_SUCCESS;
    }
    *dest = aiot_msg_clone(src);

    compressor = libdeflate_alloc_compressor(compress_handle->config.level);
    if (compressor == NULL) {
        ALOG_ERROR(TAG, STATE_COMPRESS_COMPR_FAILED, "Compressor sysdep malloc failed !\r\n");
        return STATE_SYS_DEPEND_MALLOC_FAILED;
    }

    /* 处理压缩 */
    compr_len = libdeflate_gzip_compress_bound(compressor, src->payload_lenth);
    buffer = core_os_malloc(compr_len);
    if(buffer == NULL) {
        libdeflate_free_compressor(compressor);
        ALOG_ERROR(TAG,STATE_COMPRESS_COMPR_FAILED, "Compressor sysdep malloc failed !\r\n");
        return STATE_SYS_DEPEND_MALLOC_FAILED;
    }

    compr_len = libdeflate_gzip_compress(compressor, src->payload, src->payload_lenth, buffer, compr_len);
    if(compr_len <= 0) {
        libdeflate_free_compressor(compressor);
        core_os_free(buffer);
        ALOG_ERROR(TAG, STATE_COMPRESS_COMPR_FAILED, "Compressor gzip process error !\r\n");
        return STATE_COMPRESS_COMPR_FAILED;
    }
    libdeflate_free_compressor(compressor);

    /* payload */
    if((*dest)->payload != NULL) {
        core_os_free((*dest)->payload);
    }
    (*dest)->payload = buffer;
    (*dest)->payload_lenth = compr_len;
    ALOG_INFO(TAG, "compressed [%d] --> [%d]\r\n", src->payload_lenth, (*dest)->payload_lenth);

    return STATE_COMPRESS_SUCCESS;
}

/* 执行解压缩动作 */
static int32_t _decompress_process(void *context, const aiot_msg_t *src, aiot_msg_t **dest)
{
    compress_handle_t *compress_handle = (compress_handle_t *)context;
    struct libdeflate_decompressor *decompressor = NULL;
    uint32_t expect_len = 0, res = 0;
    size_t decompr_len = 0;
    uint8_t *buffer = NULL;
    if(compress_handle == NULL || src == NULL || dest == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    /* 未匹配成功为正常情况，返回SUCCESS */
    if(STATE_SUCCESS != _decompress_topic_filter(compress_handle, src->topic)) {
        return STATE_SUCCESS;
    }
    *dest = aiot_msg_clone(src);

    /* 未达到压缩的长度阈值，直接返回 */
    expect_len = src->payload[src->payload_lenth - 1] << 24 | src->payload[src->payload_lenth - 2] << 16 | src->payload[src->payload_lenth - 3] << 8 | src->payload[src->payload_lenth - 4];
    if(expect_len >= 256 * 1024 || expect_len <= 0) {
        ALOG_ERROR(TAG, STATE_COMPRESS_DECOMPR_FAILED, "Decompressor dismatch payload lenth!\r\n");
        return STATE_COMPRESS_DECOMPR_FAILED;
    }

    /* 申请解压后的内存 */
    buffer = core_os_malloc(expect_len);
    if(buffer == NULL) {
        ALOG_ERROR(TAG, STATE_COMPRESS_DECOMPR_FAILED, "Decompressor sysdep malloc failed !\r\n");
        return STATE_SYS_DEPEND_MALLOC_FAILED;
    }

    /* 处理解压缩 */
    decompressor = libdeflate_alloc_decompressor();
    if (decompressor == NULL) {
        core_os_free(buffer);
        ALOG_ERROR(TAG, STATE_COMPRESS_DECOMPR_FAILED, "Decompressor sysdep malloc failed !\r\n");
        return STATE_COMPRESS_COMPR_FAILED;
    }

    res = libdeflate_gzip_decompress(decompressor, src->payload, src->payload_lenth, buffer, expect_len, &decompr_len);
    if(res != 0 ) {
        libdeflate_free_decompressor(decompressor);
        core_os_free(buffer);
        ALOG_ERROR(TAG, STATE_COMPRESS_DECOMPR_FAILED, "Decompressor gzip process error !\r\n");
        return STATE_COMPRESS_COMPR_FAILED;
    }
    libdeflate_free_decompressor(decompressor);
    if((*dest)->payload != NULL) {
        core_os_free((*dest)->payload);
    }
    (*dest)->payload = buffer;
    (*dest)->payload_lenth = decompr_len;
    ALOG_INFO(TAG, "decompressed [%d] --> [%d]\r\n", src->payload_lenth, (*dest)->payload_lenth);
    return STATE_COMPRESS_SUCCESS;
}

int32_t aiot_device_compress_set_level(void *device, uint8_t level)
{
    compress_handle_t *compress_handle = _compress_get_handle(device);;

    if(device == NULL || compress_handle == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_compress_set_level input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    if(level < 1|| level > 9) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_OUT_RANGE, "aiot_device_compress_set_level error, please check zone %d\r\n", level);
        return STATE_USER_INPUT_OUT_RANGE;
    }

    ALOG_INFO(TAG, "set time zone %d\r\n", level);
    compress_handle->config.level = level;

    return STATE_SUCCESS;
}

int32_t aiot_device_compress_set_compr_list(void *device, char **topic_list, int32_t topic_num)
{
    compress_handle_t *compress_handle = _compress_get_handle(device);
    int32_t i = 0;

    if(device == NULL || compress_handle == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_compress_set_level input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }
    if(compress_handle->config.compr_list != NULL) {
        for(i = 0; i < compress_handle->config.compr_num; i++) {
            core_os_free(compress_handle->config.compr_list[i]);
        }
        core_os_free(compress_handle->config.compr_list);
    }

    compress_handle->config.compr_list = (char **)core_os_malloc(sizeof(char *) * topic_num);
    for(i = 0; i < topic_num; i++) {
        compress_handle->config.compr_list[i] = NULL;
        core_strdup(NULL, &compress_handle->config.compr_list[i], topic_list[i], NULL);
    }
    compress_handle->config.compr_num = topic_num;

    return STATE_SUCCESS;
}

int32_t aiot_device_compress_set_decompr_list(void *device, char **topic_list, int32_t topic_num)
{
    compress_handle_t *compress_handle = _compress_get_handle(device);
    int32_t i = 0;
    if(device == NULL || compress_handle == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_compress_set_level input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }
    if(compress_handle->config.decompr_list != NULL) {
        for(i = 0; i < compress_handle->config.decompr_num; i++) {
            core_os_free(compress_handle->config.decompr_list[i]);
        }
        core_os_free(compress_handle->config.decompr_list);
    }

    compress_handle->config.decompr_list = (char **)core_os_malloc(sizeof(char *) * topic_num);
    for(i = 0; i < topic_num; i++) {
        compress_handle->config.decompr_list[i] = NULL;
        core_strdup(NULL, &compress_handle->config.decompr_list[i], topic_list[i], NULL);
    }
    compress_handle->config.decompr_num = topic_num;

    return STATE_SUCCESS;
}


static void _compress_response_callback(void *device, const aiot_msg_t *message, void *userdata)
{
    compress_recv_t *compress_resp = NULL;
    aiot_compress_recv_t recv;
    compress_handle_t *compress_handle = _compress_get_handle(device);
    if(device == NULL || message == NULL || compress_handle == NULL) {
        return;
    }

    compress_resp = compress_msg_parse_update_reply(message);
    if(compress_resp == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_PARSE_ERROR, "msg (%s) parse error\r\n", message->topic);
        return;
    }

    if(compress_handle->config.callback != NULL) {
        memset(&recv, 0, sizeof(aiot_compress_recv_t));
        recv.type = AIOT_COMPRESS_UPDATE_REPLY;
        recv.data.update_reply.code = compress_resp->code;
        recv.data.update_reply.id = compress_resp->id;
        recv.data.update_reply.message = compress_resp->message;

        compress_handle->config.callback(device, &recv, compress_handle->config.userdata);
    }

    compress_msg_delete(compress_resp);
}

int32_t aiot_device_compress_set_callback(void *device, compress_callback_t callback, void *userdata)
{
    compress_handle_t *compress_handle = _compress_get_handle(device);;

    if(device == NULL || callback == NULL || compress_handle == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_compress_set_callback input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    compress_handle->config.callback = callback;
    compress_handle->config.userdata = userdata;

    return STATE_SUCCESS;
}

static int32_t compress_register_topic(void *device, char *format)
{
    char *topic = NULL;
    char *src[] = { NULL, NULL };
    uint32_t src_counter = 2;
    int32_t res = STATE_SUCCESS;

    src[0] = aiot_device_product_key(device);
    src[1] = aiot_device_name(device);

    if(STATE_SUCCESS != core_sprintf(NULL, &topic, format, src, src_counter, "")) {
        return STATE_PORT_MALLOC_FAILED;
    }
    res = aiot_device_register_topic_filter(device, topic, _compress_response_callback, 1, NULL);
    core_os_free(topic);

    return res;
}

int32_t aiot_device_compress_update_topic(void *device)
{
    aiot_msg_t *msg = NULL;
    int32_t res = STATE_SUCCESS;
    compress_handle_t *compress_handle = _compress_get_handle(device);
    if(device == NULL || compress_handle == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_compress_request input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }
    compress_register_topic(device, COMPRESS_UPDATE_REPLY_TOPIC_FMT);
    core_os_sleep(100);
    /* 创建日志上传消息 */
    msg = compress_msg_create_topic_update(device, compress_handle->config.compr_list, compress_handle->config.compr_num, compress_handle->config.decompr_list, compress_handle->config.decompr_num);
    if(msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_CREATE_ERROR, "compress message create error \r\n");
        return STATE_MESSAGE_CREATE_ERROR;
    }

    res = aiot_device_send_message(device, msg);

    aiot_msg_delete(msg);

    return res;
}
static void *_compress_malloc(size_t size)
{
    return core_os_malloc(size);
}


void *_compress_init()
{
    compress_handle_t *compress_handle = (compress_handle_t *)core_os_malloc(sizeof(compress_handle_t));
    if(compress_handle == NULL) {
        return NULL;
    }

    memset(compress_handle, 0, sizeof(compress_handle_t));
    compress_handle->config.level = 1;
    libdeflate_set_memory_allocator(_compress_malloc, core_os_free);
    return compress_handle;
}

void _compress_deinit(void *handle)
{
    compress_handle_t *compress_handle = (compress_handle_t *)handle;
    int32_t i = 0;
    for(i = 0; i < compress_handle->config.compr_num; i++) {
        core_os_free(compress_handle->config.compr_list[i]);
    }
    core_os_free(compress_handle->config.compr_list);
    for(i = 0; i < compress_handle->config.decompr_num; i++) {
        core_os_free(compress_handle->config.decompr_list[i]);
    }
    core_os_free(compress_handle->config.decompr_list);
    core_os_free(handle);
}

/* 日志上报即插即用操作方法 */
static module_ops_t compress_ops = {
    .internal_ms = -1,
    .on_init = _compress_init,
    .on_process = NULL,
    .on_status = NULL,
    .on_deinit = _compress_deinit,
};

static compress_handle_t *_compress_get_handle(void *device)
{
    compress_handle_t *compress_handle = core_device_module_get(device, MODULE_COMPRESS);
    int32_t res = STATE_SUCCESS;

    if(compress_handle == NULL) {
        res = core_device_module_register(device, MODULE_COMPRESS, &compress_ops);
        if(res != STATE_SUCCESS) {
            return NULL;
        } else {
            compress_handle = core_device_module_get(device, MODULE_COMPRESS);
            compress_handle->device = device;
            core_device_compress_data_t compr, decompr;
            compr.handler = _compress_process;
            compr.context = compress_handle;
            core_device_set_compr_data(device, compr);

            decompr.handler = _decompress_process;
            decompr.context = compress_handle;
            core_device_set_decompr_data(device, decompr);
        }
    }
    return compress_handle;
}