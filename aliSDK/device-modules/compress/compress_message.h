#ifndef _COMPRESS_MESSAGE_H_
#define _COMPRESS_MESSAGE_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>
#include "aiot_message_api.h"

/* 更新压缩topic列表 */
#define COMPRESS_UPDATE_TOPIC_FMT                "/sys/%s/%s/codec/topic/update"
/* 更新压缩topic列表回复 */
#define COMPRESS_UPDATE_REPLY_TOPIC_FMT          "/sys/%s/%s/codec/topic/update_reply"

typedef struct {
    uint32_t id;
    uint32_t code;
    char *message;
} compress_recv_t;

/* 创建上报压缩topic列表消息 */
aiot_msg_t *compress_msg_create_topic_update(void *device, char **compr_topics, uint32_t compr_num, char **decompr_topics, uint32_t decompr_num);
/* 解析上报的reply消息 */
compress_recv_t *compress_msg_parse_update_reply(const aiot_msg_t *msg);
/* 删除compress_recv_t消息 */
void compress_msg_delete(compress_recv_t *msg);

#if defined(__cplusplus)
}
#endif

#endif