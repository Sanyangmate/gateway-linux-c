/**
 * @file aiot_logpost_api.h
 * @brief 设备日志模块头文件, 提供设备端日志上云的能力
 * @date 2022-01-20
 *
 * @copyright Copyright (C) 2020-2025 Alibaba Group Holding Limited
 *
 */
#ifndef _AIOT_LOGPOST_API_H
#define _AIOT_LOGPOST_API_H

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>

/**
 * @brief 错误码：未使能日志上传功能, 可通过物联网平台的控制台，设备管理页面，打开设备日志上报开关
 *
 */
#define  STATE_LOGPOST_DISABLE                                 (-0x1401)

/**
 * @brief 日志级别枚举类型定义
 */
typedef enum {
    AIOT_LOGPOST_LEVEL_FATAL,
    AIOT_LOGPOST_LEVEL_ERR,
    AIOT_LOGPOST_LEVEL_WARN,
    AIOT_LOGPOST_LEVEL_INFO,
    AIOT_LOGPOST_LEVEL_DEBUG,
} aiot_logpost_level_t;

/**
 * @brief 日志数据结构体定义
 *
 */
typedef struct {
    /**
     * @brief utc时间戳, 单位为ms, 此数值会直接展示在云端控制台设备日志页面
     */
    uint64_t timestamp;

    /**
     * @brief 日志级别, 请查看@ref aiot_logpost_level_t 定义
     */
    aiot_logpost_level_t loglevel;

    /**
     * @brief 模块名称, <b>必须为以结束符'\0'结尾的字符串</b>
     */
    char *module_name;

    /**
     * @brief 状态码, 可用于标识日志对应的状态
     */
    int32_t code;

    /**
     * @brief 消息标示符, 用于标识云端下行消息, 可从data-module模块的消息接收回调函数中获得对应的标识符, 如果用户设置为0, 此字段将不上传。
     */
    uint64_t msg_id;

    /**
     * @brief 日志内容, <b>必须为以结束符'\0'结尾的字符串</b>
     */
    char *content;
} aiot_log_t;

/**
 * @brief logpost模块内部发生值得用户关注的状态变化时, 通知用户的事件类型
 */
typedef enum {
    /**
     * @brief 接受到云端下发的日志配置数据
     */
    AIOT_LOGPOSTEVT_CONFIG_DATA,
    /**
     * @brief 上报日志后云端给的回复
     */
    AIOT_LOGPOSTEVT_POST_REPLY,
} aiot_logpost_event_type_t;

/**
 * @brief logpost模块内部发生值得用户关注的状态变化时, 通知用户的事件内容
 */
typedef struct {
    /**
     * @brief 事件内容所对应的事件类型, 更多信息请参考@ref aiot_logpost_event_type_t
     */
    aiot_logpost_event_type_t  type;

    union {
        /**
         * @brief 日志配置数据结构体
         */
        struct {
            /**
             * @brief 日志开关状态, 0: 关闭日志上传; 1: 打开日志上传
             */
            uint8_t on_off;
        } config_data;
        /**
         * @brief 日志上报后云端回复的数据结构
         */
        struct {
            /**
             * @brief 云端接收到的消息id
             */
            uint8_t msg_id;
            /**
             * @brief 消息状态，200表示成功
             */
            uint32_t code;
        } post_reply;
    } data;
} aiot_logpost_event_t;

/**
 * @brief 设备日志的事件回调函数原型，用户定义后, 可通过 @ref aiot_device_logpost_set_callback 配置
 *
 * @param[in] device 设备句柄
 * @param[in] event  事件的数据结构 @ref aiot_logpost_event_t
 * @param[in] userdata 用户设置的上下文，可通过 @ref aiot_device_logpost_set_callback 配置
 */
typedef void (*aiot_logpost_event_callback_t)(void *device,
        const aiot_logpost_event_t *event, void *userdata);

/**
 * @brief 设置日志上报后云端是否给回复
 *
 * @param[in] device 设备句柄
 * @param[in] post_reply [0] 不需要回复 [1] 需要回复
 *
 * @return int32_t
 * @retval STATE_SUCCESS 参数配置成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval 其它 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_device_logpost_set_post_reply(void *device, int8_t post_reply);

/**
 * @brief 设置日志上报消息回调，包含云端主动下推日志开关消息/主动获取日志开关回复消息
 *
 * @param[in] device 设备句柄
 * @param[in] callback 消息回调
 * @param[in] userdata 执行回调消息的上下文
 * @return int32_t
 * @retval STATE_SUCCESS 参数配置成功
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval 其它 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_device_logpost_set_callback(void *device, aiot_logpost_event_callback_t callback, void *userdata);

/**
 * @brief 向云端发送设备日志
 *
 * @param[in] device 设备句柄
 * @param[in] log_msg 日志消息结构体对象
 * @return int32_t
 * @retval >=STATE_SUCCESS  消息id
 * @retval STATE_USER_INPUT_NULL_POINTER 入参为空
 * @retval 其它 参考@ref aiot_state_api.h
 *
 */
int32_t aiot_device_logpost_send(void *device, const aiot_log_t *log_msg);
#if defined(__cplusplus)
}
#endif

#endif