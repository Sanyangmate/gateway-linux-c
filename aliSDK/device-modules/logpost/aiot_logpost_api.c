#include "core_log.h"
#include "core_os.h"
#include "core_string.h"
#include "logpost_message.h"
#include "aiot_logpost_api.h"
#include "aiot_state_api.h"
#include "device_private.h"
#include "aiot_message_api.h"

#define TAG "LOGPOST"
static int32_t core_logpost_check_config(void *device);

typedef struct {
    aiot_logpost_event_callback_t msg_callback;
    void *userdata;
    int8_t post_reply;
} logpost_config_t;

typedef struct {
    void *device;
    int32_t enable;
    int32_t has_get_config;
    uint64_t last_get_config_time;
    logpost_config_t config;
} logpost_handle_t;

void *_logpost_init()
{
    logpost_handle_t *logpost_handle = (logpost_handle_t *)core_os_malloc(sizeof(logpost_handle_t));
    if(logpost_handle == NULL) {
        return NULL;
    }

    memset(logpost_handle, 0, sizeof(logpost_handle_t));
    return logpost_handle;
}
void _logpost_deinit(void *handle)
{
    core_os_free(handle);
}

void _logpost_on_status(void *handle, aiot_device_status_t event)
{
    logpost_handle_t *logpost_handle = (logpost_handle_t *)handle;
    if(logpost_handle->has_get_config != 1 && event.type == AIOT_DEVICE_STATUS_CONNECT) {
        core_logpost_check_config(logpost_handle->device);
        logpost_handle->last_get_config_time = core_os_time();
    }
}

/* 日志上报即插即用操作方法 */
static module_ops_t logpost_ops = {
    .internal_ms = -1,
    .on_init = _logpost_init,
    .on_process = NULL,
    .on_status = _logpost_on_status,
    .on_deinit = _logpost_deinit,
};

static logpost_handle_t *_logpost_get_handle(void *device)
{
    logpost_handle_t *logpost_handle = core_device_module_get(device, MODULE_LOGPOST);
    int32_t res = STATE_SUCCESS;

    if(logpost_handle == NULL) {
        res = core_device_module_register(device, MODULE_LOGPOST, &logpost_ops);
        if(res != STATE_SUCCESS) {
            return NULL;
        } else {
            logpost_handle = core_device_module_get(device, MODULE_LOGPOST);
            logpost_handle->device = device;
            if(aiot_device_online(device)) {
                core_logpost_check_config(device);
                logpost_handle->last_get_config_time = core_os_time();
            }
        }
    }
    return logpost_handle;
}

void _logpost_msg_notify_callback(void *device, const aiot_msg_t *message, void *userdata)
{
    aiot_logpost_recv_t *logpost_msg = NULL;
    aiot_logpost_event_t event;
    logpost_handle_t *logpost_handle = _logpost_get_handle(device);
    if(device == NULL || logpost_handle == NULL || logpost_handle->config.msg_callback == NULL) {
        return;
    }

    logpost_msg = logpost_msg_parse_notify(message);
    if(logpost_msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_PARSE_ERROR, "msg(%s) json parse error\r\n", message->topic);
        return;
    }

    /* 设置已经获取到云端日志配置 */
    logpost_handle->has_get_config = 1;
    logpost_handle->enable = logpost_msg->mode;

    memset(&event, 0, sizeof(event));
    event.type = AIOT_LOGPOSTEVT_CONFIG_DATA;
    event.data.config_data.on_off = logpost_msg->mode;
    logpost_handle->config.msg_callback(device, &event, logpost_handle->config.userdata);

    /* 回收消息资源 */
    logpost_msg_delete(logpost_msg);
}

void _logpost_msg_reply_callback(void *device, const aiot_msg_t *message, void *userdata)
{
    aiot_logpost_recv_t *logpost_msg = NULL;
    aiot_logpost_event_t event;
    logpost_handle_t *logpost_handle = _logpost_get_handle(device);
    if(device == NULL || logpost_handle == NULL || logpost_handle->config.msg_callback == NULL) {
        return;
    }

    logpost_msg = logpost_msg_parse_post_reply(message);
    if(logpost_msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_PARSE_ERROR, "msg(%s) json parse error\r\n", message->topic);
        return;
    }

    memset(&event, 0, sizeof(event));
    event.type = AIOT_LOGPOSTEVT_POST_REPLY;
    event.data.post_reply.code = logpost_msg->code;
    event.data.post_reply.msg_id = logpost_msg->id;
    if(logpost_handle->config.msg_callback != NULL && logpost_handle->config.post_reply == 1) {
        logpost_handle->config.msg_callback(device, &event, logpost_handle->config.userdata);
    }

    /* 回收消息资源 */
    logpost_msg_delete(logpost_msg);
}

int32_t aiot_device_logpost_set_callback(void *device, aiot_logpost_event_callback_t callback, void *userdata)
{
    logpost_handle_t *logpost_handle = _logpost_get_handle(device);
    if(device == NULL || logpost_handle == NULL || callback == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_logpost_set_callback input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    logpost_handle->config.msg_callback = callback;
    logpost_handle->config.userdata = userdata;

    aiot_device_register_topic_filter(device, LOGPOST_CONFIG_PUSH_TOPIC, _logpost_msg_notify_callback, 0, NULL);
    aiot_device_register_topic_filter(device, LOGPOST_CONFIG_GET_REPLY_TOPIC, _logpost_msg_notify_callback, 0, NULL);

    return STATE_SUCCESS;
}
int32_t aiot_device_logpost_set_post_reply(void *device, int8_t post_reply)
{
    logpost_handle_t *logpost_handle = _logpost_get_handle(device);
    if(device == NULL || logpost_handle == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_logpost_set_post_reply input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    if(post_reply != 0 && post_reply != 1) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_OUT_RANGE, "post_reply must be 0 or 1\r\n");
        return STATE_USER_INPUT_OUT_RANGE;
    }

    logpost_handle->config.post_reply = post_reply;

    return STATE_SUCCESS;
}

/* 主动获取云端日志开关配置 */
int32_t core_logpost_check_config(void *device)
{
    aiot_msg_t *msg = NULL;
    int32_t res = STATE_SUCCESS;
    char *topic = NULL;
    char *src[2] = { NULL, NULL};
    if(device == NULL) {
        return STATE_USER_INPUT_NULL_POINTER;
    }

    msg = logpost_msg_create_get_config(device);
    if(msg != NULL) {
        res = aiot_device_send_message(device, msg);
        aiot_msg_delete(msg);
    } else {
        ALOG_ERROR(TAG, STATE_MESSAGE_CREATE_ERROR, "logpost get config message create error \r\n");
        res = STATE_MESSAGE_CREATE_ERROR;
    }

    src[0] = aiot_device_product_key(device);
    src[1] = aiot_device_name(device);
    core_sprintf(NULL, &topic, LOGPOST_CONFIG_POST_REPLY_TOPIC, src, sizeof(src)/sizeof(char *), NULL);
    if(topic != NULL) {
        aiot_device_register_topic_filter(device, topic, _logpost_msg_reply_callback, 1, NULL);
        core_os_free(topic);
    }

    return res;
}

/* 日志上传 */
int32_t aiot_device_logpost_send(void *device, const aiot_log_t *log_msg)
{
    aiot_msg_t *msg = NULL;
    int32_t res = STATE_SUCCESS;
    logpost_handle_t *logpost_handle = _logpost_get_handle(device);
    if(device == NULL || logpost_handle == NULL || log_msg == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_NULL_POINTER, "aiot_device_logpost_send input null\r\n");
        return STATE_USER_INPUT_NULL_POINTER;
    }

    if(log_msg->loglevel > AIOT_LOGPOST_LEVEL_DEBUG
            || log_msg->module_name == NULL
            || log_msg->content == NULL) {
        ALOG_ERROR(TAG, STATE_USER_INPUT_OUT_RANGE, "aiot_device_logpost_send error, please check log_msg\r\n");
        return STATE_USER_INPUT_OUT_RANGE;
    }

    if(logpost_handle->enable != 1) {
        ALOG_ERROR(TAG, STATE_LOGPOST_DISABLE, "logpost is disable\r\n");
        return STATE_LOGPOST_DISABLE;
    }

    /* 创建日志上传消息 */
    msg = logpost_msg_create_log_post(device, log_msg, logpost_handle->config.post_reply);
    if(msg == NULL) {
        ALOG_ERROR(TAG, STATE_MESSAGE_CREATE_ERROR, "logpost message create error \r\n");
        return STATE_MESSAGE_CREATE_ERROR;
    }

    res = aiot_device_send_message(device, msg);
    if(res == STATE_SUCCESS) {
        res = msg->id;
    }
    aiot_msg_delete(msg);

    return res;
}