

#ifndef _LOG_MESSAGE_H_
#define _LOG_MESSAGE_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>
#include "aiot_message_api.h"
#include "aiot_logpost_api.h"

/* 日志上报topic */
#define LOGPOST_POST_TOPIC_FMT                  "/sys/%s/%s/thing/log/post"
/* 主动上报日志回复 */
#define LOGPOST_CONFIG_POST_REPLY_TOPIC         "/sys/%s/%s/thing/log/post_reply"
/* 获取日志开关状态topic */
#define LOGPOST_CONFIG_GET_TOPIC_FMT            "/sys/%s/%s/thing/config/log/get"
/* 云端日志开关状态变更topic */
#define LOGPOST_CONFIG_PUSH_TOPIC               "/sys/+/+/thing/config/log/push"
/* 主动获取日志开关状态回复topic */
#define LOGPOST_CONFIG_GET_REPLY_TOPIC          "/sys/+/+/thing/config/log/get_reply"


typedef struct {
    /* 开关状态 0:关闭日志上传  1:开启日志上传*/
    int32_t mode;
    /* 消息回复的消息id */
    uint32_t id;
    uint32_t code;
} aiot_logpost_recv_t;

/* 创建主动获取日志开关状态消息 */
aiot_msg_t *logpost_msg_create_get_config(void *device);
/* 创建日志上报消息 */
aiot_msg_t *logpost_msg_create_log_post(void *device, const aiot_log_t *log, int8_t ack);
/* 解析云端下发消息 */
aiot_logpost_recv_t *logpost_msg_parse_notify(const aiot_msg_t *msg);
/* 解析云端下发消息 */
aiot_logpost_recv_t *logpost_msg_parse_post_reply(const aiot_msg_t *msg);
/* 删除aiot_logpost_recv_t消息 */
void logpost_msg_delete(aiot_logpost_recv_t *msg);

#if defined(__cplusplus)
}
#endif

#endif