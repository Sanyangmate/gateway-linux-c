#include "core_os.h"
#include "core_log.h"
#include "core_time.h"
#include "cJSON.h"
#include "device_private.h"
#include "aiot_state_api.h"
#include "aiot_message_api.h"
#include "logpost_message.h"

static const char *logpost_loglevel[] = {
    "FATAL",
    "ERROR",
    "WARN",
    "INFO",
    "DEBUG",
};

static aiot_msg_t *_logpost_msg_create_template(void *device, const char* fmt, cJSON *root)
{
    char *topic = NULL, *payload = NULL;
    char *src[] = { NULL, NULL };
    uint32_t src_counter = 2;
    aiot_msg_t *result = NULL;

    src[0] = aiot_device_product_key(device);
    src[1] = aiot_device_name(device);

    if(STATE_SUCCESS != core_sprintf(NULL, &topic, fmt, src, src_counter, "")) {
        return NULL;
    }

    payload = cJSON_PrintUnformatted(root);
    if (payload == NULL) {
        core_os_free(topic);
        return NULL;
    }

    result = aiot_msg_create_raw(topic, (uint8_t *)payload, strlen(payload));
    core_os_free(topic);
    core_os_free(payload);
    return result;
}

aiot_msg_t *logpost_msg_create_get_config(void *device)
{
    cJSON *root = NULL, *params = NULL;
    aiot_msg_t *result = NULL;
    int32_t id = 0;

    root = cJSON_CreateObject();
    if (root == NULL) {
        return NULL;
    }

    params = cJSON_CreateObject();
    if (params == NULL) {
        cJSON_Delete(root);
        return NULL;
    }

    id = core_device_get_next_alink_id(device);
    _cJSON_ADDInt2StrToObject(root, "id", id);
    cJSON_AddItemToObject(root, "params", params);
    cJSON_AddStringToObject(params, "configScope", "device");
    cJSON_AddStringToObject(params, "getType", "content");

    result = _logpost_msg_create_template(device, LOGPOST_CONFIG_GET_TOPIC_FMT, root);
    cJSON_Delete(root);

    return result;
}

aiot_msg_t *logpost_msg_create_log_post(void *device, const aiot_log_t *log, int8_t ack)
{
    cJSON *root = NULL, *params = NULL, *param = NULL, *sys = NULL;
    aiot_msg_t *result = NULL;
    int32_t id = 0;
    char utc[32] = { 0 };
    char code[11] = { 0 };
    char msg_id[22] = { 0 };

    root = cJSON_CreateObject();
    if (root == NULL) {
        return NULL;
    }

    sys = cJSON_CreateObject();
    if (sys == NULL) {
        cJSON_Delete(root);
        return NULL;
    }
    cJSON_AddItemToObject(root, "sys", sys);
    cJSON_AddNumberToObject(sys, "ack", ack);

    params = cJSON_CreateArray();
    if (params == NULL) {
        cJSON_Delete(root);
        return NULL;
    }
    cJSON_AddItemToObject(root, "params", params);

    param = cJSON_CreateObject();
    if (param == NULL) {
        cJSON_Delete(root);
        return NULL;
    }

    if(log->timestamp == 0) {
        _core_log_append_date(NULL, core_log_get_timestamp(NULL), utc);
    } else {
        _core_log_append_date(NULL, log->timestamp, utc);
    }
    cJSON_AddStringToObject(param, "utcTime", utc);
    cJSON_AddStringToObject(param, "logLevel", logpost_loglevel[log->loglevel]);
    cJSON_AddStringToObject(param, "module", log->module_name);
    core_int2str(log->code, code, NULL);
    cJSON_AddStringToObject(param, "code", code);
    core_uint642str(log->msg_id, msg_id, NULL);
    cJSON_AddStringToObject(param, "traceContext", msg_id);
    cJSON_AddStringToObject(param, "logContent", log->content);
    cJSON_AddItemToArray(params, param);

    id = core_device_get_next_alink_id(device);
    _cJSON_ADDInt2StrToObject(root, "id", id);

    result = _logpost_msg_create_template(device, LOGPOST_POST_TOPIC_FMT, root);
    result->id = id;
    cJSON_Delete(root);

    return result;
}
aiot_logpost_recv_t *logpost_msg_parse_notify(const aiot_msg_t *msg)
{
    aiot_logpost_recv_t *recv = NULL;
    cJSON *root = NULL, *data = NULL, *content = NULL, *mode = NULL;
    if(msg == NULL) {
        return NULL;
    }

    root = cJSON_ParseWithLength((char *)msg->payload, msg->payload_lenth);
    if(root == NULL) {
        return NULL;
    }

    data = cJSON_GetObjectItem(root, "data");
    if(NULL == data) {
        data = cJSON_GetObjectItem(root, "params");
        if(NULL == data) {
            cJSON_Delete(root);
            return NULL;
        }
    }

    content = cJSON_GetObjectItem(data, "content");
    if(NULL == content) {
        cJSON_Delete(root);
        return NULL;
    }

    mode = cJSON_GetObjectItem(content, "mode");
    if(NULL == mode) {
        cJSON_Delete(root);
        return NULL;
    }

    recv = (aiot_logpost_recv_t *)core_os_malloc(sizeof(aiot_logpost_recv_t));
    if(recv == NULL) {
        cJSON_Delete(root);
        return NULL;
    }

    recv->mode = mode->valueint;
    cJSON_Delete(root);
    return recv;
}
/* 解析云端下发消息 */
aiot_logpost_recv_t *logpost_msg_parse_post_reply(const aiot_msg_t *msg)
{
    aiot_logpost_recv_t *recv = NULL;
    cJSON *root = NULL, *code = NULL, *id = NULL;
    if(msg == NULL) {
        return NULL;
    }

    root = cJSON_ParseWithLength((char *)msg->payload, msg->payload_lenth);
    if(root == NULL) {
        return NULL;
    }


    id = cJSON_GetObjectItem(root, "id");
    if(NULL == id) {
        cJSON_Delete(root);
        return NULL;
    }


    code = cJSON_GetObjectItem(root, "code");
    if(NULL == code) {
        cJSON_Delete(root);
        return NULL;
    }

    recv = (aiot_logpost_recv_t *)core_os_malloc(sizeof(aiot_logpost_recv_t));
    if(recv == NULL) {
        cJSON_Delete(root);
        return NULL;
    }

    recv->code = code->valueint;
    core_str2uint(id->valuestring, strlen(id->valuestring), &recv->id);

    cJSON_Delete(root);
    return recv;
}
void logpost_msg_delete(aiot_logpost_recv_t *msg)
{
    if(msg == NULL) {
        return;
    }

    core_os_free(msg);
}