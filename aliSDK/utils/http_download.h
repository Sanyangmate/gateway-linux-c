#ifndef _HTTP_DOWNLOAD_H_
#define _HTTP_DOWNLOAD_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdint.h>


#define      HTTP_DOWNLOAD_ERR_URL_INVALID        -0x8001
#define      HTTP_DOWNLOAD_ERR_UNKOWN             -0x8002
#define      HTTP_DOWNLOAD_ERR_FETCH_FAILED       -0x8003
#define      HTTP_DOWNLOAD_ERR_CHECKSUM_MISMATCH  -0x8004
#define      HTTP_DOWNLOAD_ERR_SAVE_FAILD         -0x8005

/**
 * @brief 文件下载的额外参数
 */
typedef struct {
    /* 文件区间下载，起始偏移地址, 默认为HTTP_DOWNLOAD_RANGE_START_DEFAULT */
    uint32_t range_start;
    /* 文件区间下载，终止偏移地址; 若下载至文件结尾，置为HTTP_DOWNLOAD_RANGE_END_DEFAULT*/
    uint32_t range_end;
    /* 建连超时时间、单位ms */
    uint32_t connect_timeout;
    /* 单帧接收超时时间、单位ms, 超过该时间就会报接收错误 */
    uint32_t recv_timeout;
    /* 单帧接收数据大小 */
    uint32_t frame_size;
} http_download_params_t;

#define HTTP_DOWNLOAD_BUFFER_LEN          (2 * 1024)
#define HTTP_DOWNLOAD_CONNECT_TIMEOUT_MS  (10 * 1000)
#define HTTP_DOWNLOAD_RECV_TIMEOUT_MS     (10 * 1000)
#define HTTP_DOWNLOAD_RANGE_START_DEFAULT (0)
#define HTTP_DOWNLOAD_RANGE_END_DEFAULT   (-1)

#define HTTP_DOWNLOAD_PARAMS_DEFAULT() { \
    .range_start = HTTP_DOWNLOAD_RANGE_START_DEFAULT,    \
    .range_end = HTTP_DOWNLOAD_RANGE_END_DEFAULT,        \
    .connect_timeout = HTTP_DOWNLOAD_CONNECT_TIMEOUT_MS, \
    .recv_timeout = HTTP_DOWNLOAD_RECV_TIMEOUT_MS,       \
    .frame_size = HTTP_DOWNLOAD_BUFFER_LEN,              \
} \

/**
 * @brief 文件数据保存回调函数类型定义
 * @details
 *      文件正常保存时，返回写入的数据长度
 *      文件保存异常时，返回-1, 会中断下载流程
 */
typedef int32_t (*file_save_func_t)(const char *filename, uint32_t offset, uint8_t *data, uint32_t data_len, void *userdata);


/**
 * @brief http下载文件
 *
 * @param[in] url 文件下载地址
 * @param[in] extra_params 其它扩展参数，没有时可填NULL
 * @param[in] filename  保存在本地的文件名，会在回调函数中回传
 * @param[in] save_func 文件数据保存回调函数
 * @param[in] userdata  执行回调时返回的用户指针
 *
 * @return http_download_result_t
 * @retval <HTTP_DOWNLOAD_SUCCESS  执行失败
 * @retval HTTP_DOWNLOAD_SUCCESS   执行成功
 */
int32_t core_http_download_request(char *url, http_download_params_t extra_params, char *filename, file_save_func_t save_func, void* userdata);

#if defined(__cplusplus)
}
#endif

#endif /* #ifndef _CORE_HTTP_H_ */