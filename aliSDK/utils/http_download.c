#include <stdio.h>
#include "http_download.h"
#include "aiot_state_api.h"
#include "core_http.h"
#include "core_os.h"
#include "utils_crc64.h"

#define HTTP_DOWNLOAD_HOST_MAX_LEN        256
#define HTTP_DOWNLOAD_PATH_MAX_LEN        256
#define HTTP_DOWNLOAD_DEFAULT_TLS_PORT    443

#define HTTP_DOWNLOAD_HTTPCLIENT_MAX_URL_LEN           (256)
#define HTTP_DOWNLOAD_MAX_DIGIT_NUM_OF_UINT32          (20)
#define HTTP_DOWNLOAD_RESPONSE_PARTIAL                 (206)
#define HTTP_DOWNLOAD_RESPONSE_OK                      (200)

#define TAG "HTTP_DOWNLOAD"

typedef struct {
    char host[HTTP_DOWNLOAD_HOST_MAX_LEN];
    char path[HTTP_DOWNLOAD_PATH_MAX_LEN];
    uint16_t port;
    file_save_func_t save_func;
    char            *filename;
    void            *userdata;
    http_download_params_t params;

    /* 运行时参数 */
    void            *http_handle;
    uint32_t        size_fetched;
    uint32_t        content_len;
    int32_t         http_rsp_status_code;
    int32_t         result;
    uint64_t        content_crc64;
    uint64_t        calc_crc64;
    int32_t         running;
} http_download_t;

/* 位于external/ali_ca_cert.c中的服务器证书 */
extern const char *ali_ca_cert;

/* 解析URL, 从中解出来host, path */
static int32_t _download_parse_url(const char *url, char *host, uint32_t max_host_len, char *path,
                                   uint32_t max_path_len)
{
    char *host_ptr = (char *) strstr(url, "://");
    uint32_t host_len = 0;
    uint32_t path_len;
    char *path_ptr;
    char *fragment_ptr;

    if (host_ptr == NULL) {
        return HTTP_DOWNLOAD_ERR_URL_INVALID;
    }
    host_ptr += 3;

    path_ptr = strchr(host_ptr, '/');
    if (NULL == path_ptr) {
        return HTTP_DOWNLOAD_ERR_URL_INVALID;
    }

    if (host_len == 0) {
        host_len = path_ptr - host_ptr;
    }

    memcpy(host, host_ptr, host_len);
    host[host_len] = '\0';
    fragment_ptr = strchr(host_ptr, '#');
    if (fragment_ptr != NULL) {
        path_len = fragment_ptr - path_ptr;
    } else {
        path_len = strlen(path_ptr);
    }

    memcpy(path, path_ptr, path_len);
    path[path_len] = '\0';
    return STATE_SUCCESS;
}

static http_download_t *_http_download_init()
{
    http_download_t *download = core_os_malloc(sizeof(http_download_t));
    if(download == NULL) {
        return download;
    }

    memset(download, 0, sizeof(http_download_t));
    download->port = HTTP_DOWNLOAD_DEFAULT_TLS_PORT;
    download->result = HTTP_DOWNLOAD_ERR_UNKOWN;
    download->filename = NULL;

    return download;
}

static void _http_download_deinit(http_download_t *download)
{
    if(download->http_handle != NULL) {
        core_http_deinit(&download->http_handle);
    }

    if(download->filename != NULL) {
        core_os_free(download->filename);
    }
    core_os_free(download);
}
/* 对于收到的http报文进行处理的回调函数, 内部处理完后再调用用户的回调函数 */
void _http_download_recv_handler(void *handle, const aiot_http_recv_t *packet, void *userdata)
{
    http_download_t *download = (http_download_t *)userdata;
    uint32_t size = 0;
    int32_t offset = 0;
    int32_t ret = 0;
    if (NULL == download || NULL == packet) {
        return;
    }
    switch (packet->type) {
    case AIOT_HTTPRECV_STATUS_CODE : {
        download->http_rsp_status_code = packet->data.status_code.code;
    }
    break;
    case AIOT_HTTPRECV_HEADER: {
        if ((strlen(packet->data.header.key) == strlen("Content-Length")) &&
                (memcmp(packet->data.header.key, "Content-Length", strlen(packet->data.header.key)) == 0)) {
            /* 在用户指定的range并非全部固件的情况下, content_len < size_total, 所以不能简单替换 */
            core_str2uint(packet->data.header.value, (uint8_t)strlen(packet->data.header.value), &size);

            /* 该字段表示用户期望总共下载多少字节. 如果字段为0, 表示未设置过, 则设置为总共的字节数 */
            download->content_len = (download->content_len > 0) ? download->content_len : size;
        }

        /* 只有整个文件下载触发校验 */
        if(download->params.range_start == HTTP_DOWNLOAD_RANGE_START_DEFAULT && download->params.range_end == HTTP_DOWNLOAD_RANGE_END_DEFAULT) {
            if ((strlen(packet->data.header.key) == strlen("x-oss-hash-crc64ecma")) &&
                    (memcmp(packet->data.header.key, "x-oss-hash-crc64ecma", strlen(packet->data.header.key)) == 0)) {
                if( STATE_SUCCESS == core_str2uint64(packet->data.header.value, (uint8_t)strlen(packet->data.header.value), &download->content_crc64)) {
                    download->calc_crc64 = 0;
                }
            }
        }
    }
    break;
    case AIOT_HTTPRECV_BODY: {
        if (HTTP_DOWNLOAD_RESPONSE_OK != download->http_rsp_status_code
                /* HTTP回复报文的code应该是200或者206, 否则这个下载链接不可用 */
                && HTTP_DOWNLOAD_RESPONSE_PARTIAL != download->http_rsp_status_code) {
            download->result = HTTP_DOWNLOAD_ERR_FETCH_FAILED;
            ALOG_ERROR(TAG, HTTP_DOWNLOAD_ERR_FETCH_FAILED, "wrong http respond code\r\n");
        } else if (0 == download->content_len) {
            /* HTTP回复报文的header里面应该有Content-Length, 否则这个下载链接为trunked编码, 不可用 */
            download->result = HTTP_DOWNLOAD_ERR_FETCH_FAILED;
            ALOG_ERROR(TAG, HTTP_DOWNLOAD_ERR_FETCH_FAILED, "wrong http respond header\r\n");
        } else {
            /* 正常的固件的报文 */
            /* 在按照多个range分片下载的情况下, 判断用户下载到的固件的累计大小是否超过了整体的值 */
            if (download->size_fetched > download->content_len) {
                ALOG_ERROR(TAG, HTTP_DOWNLOAD_ERR_FETCH_FAILED, "downloaded exceeds expected\r\n");
                break;
            }

            offset = download->size_fetched + download->params.range_start;
            ret = download->save_func(download->filename, offset, packet->data.body.buffer, packet->data.body.len, download->userdata);
            if(ret != packet->data.body.len) {
                download->running = 0;
                download->result = HTTP_DOWNLOAD_ERR_SAVE_FAILD;
                ALOG_ERROR(TAG, HTTP_DOWNLOAD_ERR_SAVE_FAILD, "local save failed\r\n");
            }

            /* 该字段表示累计下载了多少字节, 不区分range */
            download->size_fetched += packet->data.body.len;

            /* 计算digest, 如果下载完成, 还要看看是否与云端计算出来的一致 */
            download->calc_crc64 = upload_crc64_update(download->calc_crc64, packet->data.body.buffer, packet->data.body.len);
            if (download->size_fetched == download->content_len) {
                /* 不需要校验或者校验成功 */
                if(download->content_crc64 == 0) {
                    /* 不需要校验 */
                    download->result = STATE_SUCCESS;
                }
                else if(download->content_crc64 == download->calc_crc64) {
                    download->result = STATE_SUCCESS;
                    ALOG_INFO(TAG, "crc64 matched\r\n");
                } else {
                    download->result = HTTP_DOWNLOAD_ERR_CHECKSUM_MISMATCH;
                    ALOG_ERROR(TAG, HTTP_DOWNLOAD_ERR_CHECKSUM_MISMATCH, "crc64 mismatch\r\n");
                }
            }
        }
    }
    break;
    default:
        break;
    }
}

static int32_t _http_download_connect(http_download_t *download)
{
    int32_t res = STATE_SUCCESS;
    void *http_handle = core_http_init();
    if(http_handle == NULL) {
        return STATE_PORT_MALLOC_FAILED;
    }

    aiot_sysdep_network_cred_t cred;
    memset(&cred, 0, sizeof(aiot_sysdep_network_cred_t));
    cred.option = AIOT_SYSDEP_NETWORK_CRED_SVRCERT_CA;  /* 使用RSA证书校验MQTT服务端 */
    cred.max_tls_fragment = 16384; /* 最大的分片长度为16K, 其它可选值还有4K, 2K, 1K, 0.5K */
    cred.sni_enabled = 1;                               /* TLS建连时, 支持Server Name Indicator */
    cred.x509_server_cert = ali_ca_cert;                 /* 用来验证MQTT服务端的RSA根证书 */
    cred.x509_server_cert_len = strlen(ali_ca_cert);     /* 用来验证MQTT服务端的RSA根证书长度 */
    if ((STATE_SUCCESS != core_http_setopt(http_handle, CORE_HTTPOPT_RECV_HANDLER, _http_download_recv_handler)) ||
            (STATE_SUCCESS != core_http_setopt(http_handle, CORE_HTTPOPT_USERDATA, (void *)download)) ||
            (STATE_SUCCESS != core_http_setopt(http_handle, CORE_HTTPOPT_BODY_BUFFER_MAX_LEN, (void *)&download->params.frame_size)) ||
            (STATE_SUCCESS != core_http_setopt(http_handle, CORE_HTTPOPT_NETWORK_CRED, (void *)&cred)) ||
            (STATE_SUCCESS != core_http_setopt(http_handle, CORE_HTTPOPT_HOST, (void *)download->host)) ||
            (STATE_SUCCESS != core_http_setopt(http_handle, CORE_HTTPOPT_PORT, (void *)&download->port)) ||
            (STATE_SUCCESS != core_http_setopt(http_handle, CORE_HTTPOPT_CONNECT_TIMEOUT_MS, (void *)&download->params.connect_timeout)) ||
            (STATE_SUCCESS != core_http_setopt(http_handle, CORE_HTTPOPT_RECV_TIMEOUT_MS, (void *)&download->params.recv_timeout))) {
        core_http_deinit(&http_handle);
        return STATE_PORT_MALLOC_FAILED;
    }

    res = core_http_connect(http_handle);
    if (res != STATE_SUCCESS) {
        core_http_deinit(&http_handle);
        return res;
    }

    download->http_handle = http_handle;
    return res;
}

static int32_t _http_download_send_request(http_download_t *download)
{
    char header[1024];
    int32_t pos = 0;
    core_http_request_t request = {
        .method = "GET",
        .path = download->path,
        .header = header,
        .content = NULL,
        .content_len = 0
    };

    memset(header, 0, sizeof(header));
    pos += sprintf(header + pos, "Accept: text/html, application/xhtml+xml, application/xml;q=0.9, */*;q=0.8\r\n");
    if(download->params.range_end != HTTP_DOWNLOAD_RANGE_END_DEFAULT) {
        pos += sprintf(header + pos, "Range: bytes=%d-%d\r\n", download->params.range_start, download->params.range_end);
    } else {
        pos += sprintf(header + pos, "Range: bytes=%d-\r\n", download->params.range_start);
    }
    if(pos < 0) {
        return HTTP_DOWNLOAD_ERR_UNKOWN;
    }

    return core_http_send(download->http_handle, &request);
}

static int32_t _http_download_recv(http_download_t *download)
{
    int32_t res = STATE_SUCCESS;

    download->running = 1;
    while(download->running) {
        res = core_http_recv(download->http_handle);
        if(res <= 0) {
            if(res == STATE_HTTP_READ_BODY_FINISHED) {
                res = STATE_SUCCESS;
            }
            break;
        }
    }

    return res;
}

int32_t core_http_download_request(char *url, http_download_params_t extra_params, char *filename, file_save_func_t save_func, void* userdata)
{
    int32_t res = STATE_SUCCESS;
    http_download_t *download = NULL;

    if(url == NULL || save_func == NULL) {
        return -1;
    }

    download = _http_download_init();
    if(download == NULL) {
        return -1;
    }

    res = _download_parse_url(url, download->host, HTTP_DOWNLOAD_HOST_MAX_LEN, download->path,
                              HTTP_DOWNLOAD_PATH_MAX_LEN);
    if (res != STATE_SUCCESS) {
        _http_download_deinit(download);
        return HTTP_DOWNLOAD_ERR_URL_INVALID;
    }

    download->params = extra_params;
    download->save_func = save_func;
    download->userdata = userdata;
    core_strdup(NULL, &download->filename, filename, NULL);

    /* 建立连接 */
    res = _http_download_connect(download);
    if(res < STATE_SUCCESS) {
        _http_download_deinit(download);
        return HTTP_DOWNLOAD_ERR_URL_INVALID;
    }

    /* 发送请求 */
    res = _http_download_send_request(download);
    if(res != STATE_SUCCESS) {
        _http_download_deinit(download);
        return res;
    }

    /* 接收数据 */
    res = _http_download_recv(download);
    res = download->result;

    _http_download_deinit(download);
    return res;
}
