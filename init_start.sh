#!/bin/bash

# 修改权限
sudo chmod 777 "$0"

#创建ipc通信所需要的路径
mkdir /tmp/ipc/unixsock -p

mkdir /tmp/ipc/msgqueue -p
mkdir /tmp/ipc/msgqueue/peer -p
mkdir /tmp/ipc/shmem -p

mkdir /tmp/ipc/sem -p

chmod 777 /tmp/ipc/unixsock
chmod 777 /tmp/ipc/msgqueue
chmod 777 /tmp/ipc/msgqueue/peer

chmod 777 /tmp/ipc/sem 

echo 初始化配置ok，按顺序运行
